<?php
/**
 * Created by PhpStorm.
 * User: azano
 * Date: 7/10/2016
 * Time: 6:15 PM
 */

namespace app\commands;

use app\lib\My;
use app\models\Categories;
use app\models\Groups;
use app\models\Items;
use app\modules\upload\models\Upload;
use app\tables\CategoriesParser;
use app\tables\CategoriesParserGroupsLink;
use app\tables\CategoryLink;
use app\tables\ItemUpload;
use Imagine\Exception\InvalidArgumentException;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use MongoDB;
use yii\rbac\Item;

//function dd($data)
//{
//    var_dump($data);
//    exit;
//}
//function pre($data)
//{
//    print_r($data);
//    exit();
//}

class NewParserController extends Controller
{

    var $host = 'mongodb://mongo-dev.lxc';

    public function actionParse($list, $v = null)
    {
        $client = new MongoDB\Client($this->host);
        $collection = $client->parsing->$list;
        if ($v) {
            $v = (double)$v;
            $result = $collection->find(['version' => $v]);
        } else {
            $result = $collection->find();
        }
        $p = 0;
        $i = 0;
        $t1 = microtime(true);
        print_r("Начало обработки записей из коллекции $list из парсера для добавления товаров\n");

        foreach ($result as $entry) {
            $i++;
            print_r("Запись в обработке: $i          \r");
            $item = Items::findOne([
                'source' => $entry['source'],
                'article' => $entry['article']
            ]);
            /** @var $item Items */
            if (!$item) {
                $item = new Items();
                $item->source = $entry['source'];
                $item->article = $entry['article'];
                $item->created_at = date('Y-m-d h:i:s');
            } else {
                $item->updated_at = date('Y-m-d h:i:s', time());
            }
            $item->title = html_entity_decode($entry['title']);

            if (isset($entry['group'])) {
                $group = Groups::findOne([
                    'parser_code' => $entry['group'],
                    'source' => $entry['source']
                ]);
                if (!$group) {
                    $group = new Groups();
                    $group->title = html_entity_decode($item->title);
                    $group->parser_code = $entry['group'];
                    $group->source = $entry['source'];
                    if (!$group->save()) {
                        print_r("\nGroup save error:\n");
                        print_r($group->errors);
                        exit();
                    }
                }
                // Создание группы
            } else {
                if (!$item->group_id) {
                    $group = Groups::findOne([
                        'parser_code' => $entry['article'],
                        'source' => $entry['source']
                    ]);
                    if (!$group) {
                        $group = new Groups();
                        $group->title = $item->title;
                        $group->parser_code = $entry['group'];
                        $group->source = $entry['source'];
                        if (!$group->save()) {
                            print_r("\nGroup save error:\n");
                            print_r($group->errors);
                            exit();
                        }
                    }
                } else {
                    $group = Groups::findOne([
                        'id' => $item->group_id
                    ]);
                }
            }
            $item->group_id = $group->id;

            if (isset($entry['attr'])) {
                $params = [
                    'attr' => $entry['attr']
                ];
                $item->params = Json::encode($params);
            }
            if (isset($entry['size']) && $entry['size']) $item->sizes = $entry['size'];
            if (isset($entry['weight']) && $entry['weight']) $item->weight = $entry['weight'];
            if (isset($entry['price']) && $entry['price']) $item->set_price = $entry['price'];
            if (isset($entry['description']) && $entry['description']) $item->text = $entry['description'];

            if ($entry['category']) {
                $parent_category_id = null;
                foreach ($entry['category'] as $alias => $name) {
                    $category = CategoriesParser::findOne([
                        'alias' => My::str2url($alias),
                        'source' => $entry['source']
                    ]);
                    /** @var $category CategoriesParser */
                    if (!$category) {
                        $category = new CategoriesParser();
                        $category->alias = My::str2url($alias);
                        $category->source = $entry['source'];
                        $category->created_at = date('Y-m-d h:i:s');
                        $category->parent_id = $parent_category_id;
                    } else {
                        $category->updated_at = date('Y-m-d h:i:s');
                        $category->parent_id = $parent_category_id;
                    }
                    $category->title = $name;
                    if (!$category->save()) {
                        $line = __LINE__;
                        print_r("\n$line: Category save error:\n");
                        print_r($category->errors);
                        exit();
                    }
                    $parent_category_id = $category->id;
                }
            }
            if (isset($category) && isset($group)) {
                $link = CategoriesParserGroupsLink::findOne([
                    'category_parser_id' => $category->id,
                    'group_id' => $group->id
                ]);
                if (!$link) {
                    $link = new CategoriesParserGroupsLink();
                    $link->category_parser_id = $category->id;
                    $link->group_id = $group->id;
                    if (!$link->save()) {
                        print_r("\nLink save error:\n");
                        print_r($link->attributes);
                        print_r($link->errors);
                        exit();
                    }
                }
            }

            if (!$item->save()) {
                print_r("\nItem save error:\n");
                print_r($item->errors);
                exit();
            }
        }

        $time = microtime(true) - $t1;
        print_r("Обрабортка записей завершена\n");
        print_r("Обработано записей: $i\n");
        print_r("Затрачено времени: $time\n");

    }

    public function actionDownload($list, $v = null)
    {
        $client = new MongoDB\Client($this->host);
        $collection = $client->parsing->$list;
        if ($v) {
            $v = (double)$v;
            $result = $collection->find(['version' => $v]);
        } else {
            $result = $collection->find();
        }
        $p = 0;
        $i = 0;
        $t1 = microtime(true);
        print_r("Начало обработки записей из коллекции $list из парсера для загрузки изображений\n");

        foreach ($result as $entry) {
            $i++;
            $item = Items::findOne([
                'source' => $entry['source'],
                'article' => $entry['article']
            ]);

            if ($item) {
                $group = Groups::findOne($item->group_id);
                $images = $entry['images'];
                $upload_list = [];
                foreach ($images as $image) {
                    if ($image) {
                        $upload_base = ItemUpload::findOne([
                            'src' => $image
                        ]);
                        $upload = ItemUpload::findOne([
                            'item_id' => $item->id,
                            'src' => $image
                        ]);
                        if ($upload_base && !$upload) {
                            $upload = new ItemUpload();
                            $upload->upload_id = $upload_base->upload_id;
                            $upload->item_id = $item->id;
                            $upload->src = $image;
                            $upload->save();
                            if (!$group->cover_image_id) {
                                $group->cover_image_id = $upload->upload_id;
                                $group->save();
                            }
                            if (!$item->cover_image_id) {
                                $item->cover_image_id = $upload->upload_id;
                                $item->save();
                            }
                        } elseif (!$upload_base) {
                            try {
                                $image_id = Upload::uploadByPath($image, ['screen', 'small', '100']);
                                if (!$group->cover_image_id) {
                                    $group->cover_image_id = $image_id;
                                    $group->save();
                                }
                                if (!$item->cover_image_id) {
                                    $item->cover_image_id = $image_id;
                                    $item->save();
                                }
                                $upload = new ItemUpload();
                                $upload->upload_id = $image_id;
                                $upload->item_id = $item->id;
                                $upload->src = $image;
                                $upload->save();
                                sleep(1);
                            } catch (InvalidArgumentException $e) {
                                print_r(__LINE__ . " : $item->id $entry[cursor2] " . $e->getMessage() . " \n");
                            }
                        } else {
                            if (!$group->cover_image_id) {
                                $group->cover_image_id = $upload->upload_id;
                                $group->save();
                            }
                            if (!$item->cover_image_id) {
                                $item->cover_image_id = $upload->upload_id;
                                $item->save();
                            }
                        }
                        $p++;
                        if ($upload) {
                            $upload_list[] = $upload->upload_id;
                        }
                        $t2 = microtime(true) - $t1;
                        print_r("Обработка записи $i. Времени прошло: $t2. Загружено изображений: $p                           \r");
                    }
                }

                ItemUpload::deleteAll([
                    'and',
                    ['item_id' => $item->id],
                    ['not in', 'upload_id', $upload_list]
                ]);
            }
        }

        $time = microtime(true) - $t1;
        print_r("Обработка записей завершена\n");
        print_r("Обработано записей: $i\n");
        print_r("Загружено изображений: $p\n");
        print_r("Затрачено времени: $time\n");
    }

    public function actionAutoStone()
    {
        $groups = Groups::find()
            ->where([
                'source' => 'j-flash.ru'
            ])
            ->all();
        $cat_id = 6465;
        /** @var Groups $group */
        foreach ($groups as $group) {
            $link = CategoryLink::findOne([
                'category_id' => $cat_id,
                'group_id' => $group->id
            ]);
            if (!$link) {
                $link = new CategoryLink();
                $link->category_id = $cat_id;
                $link->group_id = $group->id;
                if (!$link->save()) {
                    print_r($link->errors);
                    exit();
                }
            }
            $group->status = true;
            if (!$group->save()) {
                print_r($group->errors);
                exit();
            }

            $items = Items::findAll(['group_id' => $group->id]);
            /** @var Items $item */
            foreach ($items as $item) {
                $item->status = true;
                if (!$item->save()) {
                    print_r($item->errors);
                    exit();
                }
            }
        }
    }

    public function actionAutoT()
    {
        $start = microtime(true);
        $cat_ids = [
            3 => 6475,
            10 => 6475,
            13 => 6476
        ];
        /** @var Groups $group */
        foreach ($cat_ids as $parser => $local) {
            $this->moveItems($parser, $local);
        }
        $time = microtime(true) - $start;
        print_r("End in $time\n");
    }

    private function moveItems($from, $to)
    {
        $items = CategoriesParserGroupsLink::find()
            ->asArray()
            ->select(['group_id'])
            ->where(['category_parser_id' => $from])
            ->column();
        foreach ($items as $item) {
            $category_link = CategoryLink::findOne([
                'category_id' => $to,
                'group_id' => $item
            ]);
            if (!$category_link) {
                $category_link = new CategoryLink();
                $category_link->category_id = $to;
                $category_link->group_id = $item;
                if (!$category_link->save()) {
                    print_r(__LINE__ . "\n");
                    print_r($category_link->errors);
                    exit();
                }
            }
        }
        $sub_cats = CategoriesParser::find()
            ->asArray()
            ->select(['*'])
            ->where(['parent_id' => $from])
            ->all();
        foreach ($sub_cats as $sub_cat) {
            $category = Categories::findOne([
                'pid' => $to,
                'alias' => substr($sub_cat['alias'], 0, 45)
            ]);
            if (!$category) {
                $category = new Categories();
                $category->pid = $to;
                $category->title = $sub_cat['title'];
                $category->alias = substr($sub_cat['alias'], 0, 45);
                $parent_category = Categories::findOne($to);
                $category->status = 1;
                $order = Categories::find()
                    ->asArray()
                    ->select(['MAX(position) as order'])
                    ->where(['pid' => $to])
                    ->one();
                $order = $order['order'];
                if ($order !== null) {
                    $category->position = $order + 1;
                } else {
                    $category->position = 0;
                }
                if (!$category->save()) {
                    print_r(__LINE__ . "\n");
                    print_r($category->errors);
                    exit();
                }
                $category->path = mb_substr($parent_category->path, 0, -1) . ",$category->id}";
                if (!$category->save()) {
                    print_r(__LINE__ . "\n");
                    print_r($category->errors);
                    exit();
                }
            }
            $this->moveItems($sub_cat['id'], $category['id']);
        }
    }

    public function actionFlashFix()
    {
        $items = Items::findAll(['source' => 'j-flash.ru']);
        foreach ($items as $item) {
            $attr = ArrayHelper::getValue($item['params'], 'attr', []);
            $volume = ArrayHelper::getValue($attr, 'Объем');
            if ($volume) {
                $volume_sub = ", $volume гб";
                if (mb_strpos($item->title, $volume_sub) === false) {
                    $item->title .= $volume_sub;
                    $item->save();
                }
            }
        }
    }

}