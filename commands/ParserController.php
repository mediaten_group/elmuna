<?php
/**
 * Created by PhpStorm.
 * User: dan.judex
 * Date: 29.09.2015
 * Time: 11:24
 */

namespace app\commands;

use app\models\Categories;
use app\models\CategoriesLinks;
use app\models\Colors;
use app\models\Groups;
use app\models\Items;
use app\lib\My;
use app\models\ItemsParser;
use app\modules\upload\models\Upload;
use app\tables\CategoryLink;
use app\tables\ColorsGifts;
use app\tables\ItemUpload;
use yii;
use yii\console\Controller;
use yii\db\IntegrityException;
use yii\helpers\Json;
use MongoDB;
use yii\imagine\Image;

function dd($data)
{
    var_dump($data);
    exit;
}

function pre($array)
{
    print_r($array);
}

function pree($array)
{
    pre($array);
    exit();
}

class ParserController extends Controller
{
    public $defaultAction = 'index';
    public $version;
    private $mongo = "mongodb://mongo-dev.lxc";

    public function actionIndex($method, $version = 1.2, $cursor = 0)
    {
        $this->version = (float)$version;
        if ($method == 'catalog') {
            $this->actionCreateCatalog($cursor);
        }
        if ($method == 'images') {
            $this->actionSetImages($cursor);
        }
        if ($method == 'reposition') {
            $this->reposition();
        }
        if ($method == 'categories') {
            $this->actionCategories();
        }
        if ($method == 'color') {
            $this->actionSetColor();
        }
        if ($method == 'formcolor') {
            $this->actionFormColor();
        }
    }

    public function actionFix()
    {
        Items::updateAll(['source' => 'gifts.ru'], 'catalog is not null');
    }

    public function actionCategories()
    {
        $client = new MongoDB\Client($this->mongo);
        $collection = $client->gifts->list;
        $result = $collection->find(['version' => $this->version]);
        $a = [];
        $source = 'gifts.ru';
        $t1 = microtime(true);
        foreach ($result as $entry) {
            $path_size = count($entry['path']);
            if ($path_size == 3) {
                $path = $entry['path'];
                $l1 = $path[1]['href'];
                if (preg_match('/mod/', $l1)) {
                    $cat = CategoriesLinks::findOne([
                        'href' => $path[0]['href'],
                        'source' => $source
                    ]);
                    if (!$cat) {
                        $cat = new CategoriesLinks();
                        $cat->href = $path[0]['href'];
                        $cat->title = $path[0]['title'];
                        $cat->parser_ver = $this->version;
                        $cat->date = My::dateTime();
                        $cat->source = $source;
                        $cat->save();
                    }
                } else {
                    $cat = CategoriesLinks::findOne([
                        'href' => $path[1]['href'],
                        'source' => $source
                    ]);
                    if (!$cat) {
                        $cat = new CategoriesLinks();
                        $cat->href = $path[1]['href'];
                        $cat->title = $path[1]['title'];
                        $cat->parser_ver = $this->version;
                        $cat->date = My::dateTime();
                        $cat->source = $source;
                        $cat->save();
                    }
                }
            }
            if ($path_size == 4) {
                $path = $entry['path'];
                $l1 = $path[2]['href'];
                if (preg_match('/mod/', $l1)) {
                    $cat = CategoriesLinks::findOne([
                        'href' => $path[1]['href'],
                        'source' => $source
                    ]);
                    if (!$cat) {
                        $cat = new CategoriesLinks();
                        $cat->href = $path[1]['href'];
                        $cat->title = $path[1]['title'];
                        $cat->parser_ver = $this->version;
                        $cat->date = My::dateTime();
                        $cat->source = $source;
                        $cat->save();
                    }
                } else {
                    $cat = CategoriesLinks::findOne([
                        'href' => $path[2]['href'],
                        'source' => $source
                    ]);
                    if (!$cat) {
                        $cat = new CategoriesLinks();
                        $cat->href = $path[2]['href'];
                        $cat->title = $path[2]['title'];
                        $cat->parser_ver = $this->version;
                        $cat->date = My::dateTime();
                        $cat->source = $source;
                        $cat->save();
                    }
                }
            }
        }
        echo "over in " . (microtime(true) - $t1) . " sec\n";
    }

    public function actionCreateCatalog($cursor)
    {
        $client = new MongoDB\Client($this->mongo);
        $collection = $client->parsing->gifts_links_list;
        $source = 'gifts.ru';
        $result = $collection->find(['version' => $this->version]);
        $p = 0;
        $items_add = 0;
        $groups_add = 0;
        $exists = 0;
        $t1 = microtime(true);
        $id_list = [];
        foreach ($result as $entry) {
            if ($p >= $cursor) {
                if (($p++ % 10) == 0) {
                    echo $p . "\n";
                }
                $path_size = count($entry['path']);
                $path = $entry['path'];
                $group = null;
                $item = Items::findOne([
                    'article' => $entry['article2'][0]
                ]);
                //Делаем говно только если уже нет товара с таким артикулом
                if (!$item) {
                    if ($path_size < 3) {
                    } elseif ($path_size == 3) {
                        $l1 = $path[1]['href'];
                        if (preg_match('/mod/', $l1)) {
                            // Третий уровень каталога, имеется заданное имя группы
                            if (!Groups::findOne(['title' => $path[1]['title']]))
                                $groups_add++;
                            $group = Groups::parser($path[1]['title'], $path[0]['title'], $path[0]['href'], $this->version, $source);
                        } else {
                            // Третий уровень каталога, нет заданного имени группы
                            // По какой-то причине у всех этих с сайта gifts в качестве пути первого уровня выступает корневая директория
                            if (!Groups::findOne(['title' => $entry['title2']]))
                                $groups_add++;
                            $group = Groups::parser($entry['title2'], $path[1]['title'], $path[1]['href'], $this->version, $source);
                        }
                    } elseif ($path_size == 4) {
                        $l1 = $path[2]['href'];
                        if (preg_match('/mod/', $l1)) {
                            // Четвертый уровень каталога, имеется заданное имя группы
                            if (!Groups::findOne(['title' => $path[2]['title']]))
                                $groups_add++;
                            $group = Groups::parser($path[2]['title'], $path[1]['title'], $path[1]['href'], $this->version, $source);
                        } else {
                            // Четвертый уровень каталога, нет заданного имени группы
                            // По какой-то причине у всех этих с сайта gifts в качестве пути первого уровня выступает корневая директория
                            if (!Groups::findOne(['title' => $entry['title2']]))
                                $groups_add++;
                            $group = Groups::parser($entry['title2'], $path[2]['title'], $path[2]['href'], $this->version, $source);
                        }
                    }
                    if ($group) {
                        if (!$item) {
                            $item = new Items();
                            $item->group_id = $group->id;
                            $item->title = $entry['title2'];
                            $item->status = 1;
                            $item->created_at = My::dateTime();
                            $item->price = $entry['price'];
                            $item->article = $entry['article2'][0];
                            $item->sizes = $entry['sizes'];
                            $item->material = $entry['material'];
                            $item->weight = $entry['weight'];
                            $item->box_size = $entry['box_size'];
                            $item->box_weight = $entry['box_weight'];
                            $item->box_vol = $entry['box_vol'];
                            $item->box_count = $entry['box_count'];
                            $item->text = $entry['text'];
                            $item->source = 'gifts.ru';
                            $id_list[] = $item->id;
                            try {
                                if ($entry['constructor']) {
                                    $path_parts = pathinfo($entry['constructor']);
                                    if ($item->calc != '/files/constructor/' . $path_parts['basename']) {
                                        usleep(500000);
                                        $file = file_get_contents($entry['constructor']);
                                        file_put_contents('web/files/constructor/' . $path_parts['basename'], $file);
                                        $item->calc = '/files/constructor/' . $path_parts['basename'];
                                    }
                                } else {
                                    $item->calc = null;
                                }
                            } catch (yii\base\ErrorException $e) {
                                print_r($e->getMessage());
                            }

                            $item->save();
                            $items_add++;
                        }
                    } else {
                        echo 'Не удалось создать группу для товара с артикулом ' . $entry['article2'][0] . "\n";
                    }
                } else {

                    try {
                        if ($entry['constructor']) {
                            $path_parts = pathinfo($entry['constructor']);
                            if ($item->calc != '/files/constructor/' . $path_parts['basename']) {
                                usleep(500000);
                                $file = file_get_contents($entry['constructor']);
                                file_put_contents('web/files/constructor/' . $path_parts['basename'], $file);
                                $item->calc = '/files/constructor/' . $path_parts['basename'];
                            }
                        } else {
                            $item->calc = null;
                        }
                    } catch (yii\base\ErrorException $e) {
                        print_r($e->getMessage());
                    }
                    $item->status = 1;
                    $item->price = $entry['price'];
                    $item->source = 'gifts.ru';
                    $id_list[] = $item->id;
                    $item->save();
                }
            }
        }
        Items::updateAll(
            ['status' => 0],
            [
                'and',
                ['source' => 'gifts.ru'],
                ['not in', 'id', $id_list]
            ]
        );
        echo 'Время операции: ' . (microtime(true) - $t1) . " сек\n";
        echo 'Обработано записей: ' . $p . "\n";
        echo 'Добавлено групп: ' . $groups_add . "\n";
        echo 'Добавлено предметов: ' . $items_add . "\n";
        echo 'Добавленных ранее предметов: ' . $exists . "\n";
    }

    public function actionFormColor()
    {
        $client = new MongoDB\Client($this->mongo);
        $collection = $client->gifts->list;
        $result = $collection->find(['version' => $this->version]);
        $t1 = microtime(true);
        $source = 'gifts.ru';
        $p = 0;
        foreach ($result as $entry) {
            if (($p++ % 100) == 0) {
                echo $p . "\n";
            }
            $item = Items::findOne([
                'article' => $entry['article2'][0]
            ]);
            if ($item) {
                $colors = new ColorsGifts();
                $colors->pantone = mb_strtolower($entry['color_pantone']);
                $colors->sol = strtolower($entry['color_sol']);
                $colors->name = strtolower($entry['title']);
                $colors->source = $source;
                $colors->item_id = $item->id;
                if (!$colors->save()) {
                    print_r($colors->errors);
                    exit();
                };
            }
        }
        echo 'Время операции: ' . (microtime(true) - $t1) . " сек\n";
    }

    public function actionSetColor()
    {
        $client = new MongoDB\Client($this->mongo);
        $collection = $client->gifts->list;
        $colors_pantone = $client->gifts->pantone;
        $colors_normal = $client->gifts->color;
        $result = $collection->find(['version' => $this->version]);
        $t1 = microtime(true);
        $p = 0;
        foreach ($result as $entry) {
//            if (($p++ % 100) == 0) {
//                echo $p . "\n";
//            }
            $path_size = count($entry['path']);
            if ($path_size > 2) {
                $item = Items::findOne([
                    'article' => $entry['article2'][0]
                ]);
                if ($entry['color_pantone']) {
                    $color = $colors_pantone->findOne(['gifts' => $entry['color_pantone']]);
                    if ($color) {
                        $c = Colors::findOne([
                            'hex' => $color['hex']
                        ]);
                        if (!$c) {
                            $c = new Colors();
                            $c->title = $c->alias = $c->hex = $color['hex'];
                            $c->save();
                        }
                        $item->color = $color['hex'];
                        $item->save();
                    } else {
                        $color = trim(preg_split('/ Акутально/', $entry['color_pantone'])[0]);
                        if (isset($this->color[$color])) {
                            $c = Colors::findOne([
                                'hex' => $this->color[$color]
                            ]);
                            if (!$c) {
                                $c = new Colors();
                                $c->title = $c->alias = $c->hex = $this->color[$color];
                                $c->save();
                            };
                            if ($c->errors) {
                                print_r($c->errors);
                            }
                            $item->color = $this->color[$color];
                            $item->save();
                        } else {
                            echo $item->article . "\t" . $color . "\n";
                        }
                    }
                } elseif ($entry['color_name']) {
//                    $c = trim(html_entity_decode($entry['color_name']));
//                    $color = $colors_normal->findOne(['color_orig' => $entry['color_name']]);
//                    $c = Colors::findOne([
//                        'hex'=>$color['hex']
//                    ]);
//                    if (!$c) {
//                        $c = new Colors();
//                        $c->title = $color['color_ru'];
//                        $c->alias = $c->hex = $color['hex'];
//                        $c->save();
//                    }
//                    $item->color = $color['hex'];
                    $item->color = null;
                    $item->save();
                } elseif ($entry['color_sol']) {
//                    echo $entry['article2'][0] . "\t" . $entry['color_sol'] . "\n";
                } else {
//                    echo $entry['article2'][0] . "\t no color \n";
                }
            }
        }

        echo 'Время операции: ' . (microtime(true) - $t1) . " сек\n";
    }

    public function actionSetImages($cursor)
    {
        //1415
        $client = new MongoDB\Client($this->mongo);
        $collection = $client->parsing->gifts_links_list;
        $result = $collection->find(['version' => $this->version]);
        $p = 0;
        $i = 0;
        $t1 = microtime(true);
        foreach ($result as $entry) {
            if ($p >= $cursor) {
                $item = Items::findOne([
                    'article' => $entry['article2'][0]
                ]);
                if ($item) {
//                    ItemUpload::deleteAll(['item_id'=>$item->id]);
                    
                    $k = 0;
                    $group = Groups::findOne($item->group_id);
                    $images = $entry['images'];
                    foreach ($images as $image) {

                        usleep(250000);
                        $upload = ItemUpload::findOne([
                            'item_id' => $item->id,
                            'src' => $image
                        ]);
                        if (!$upload) {
                            $image_id = Upload::uploadByPath($image, ['screen', 'small', '100']);
                            if (!$group->cover_image_id) {
                                $group->cover_image_id = $image_id;
                                $group->save();
                            }
                            $upload = new ItemUpload();
                            $upload->upload_id = $image_id;
                            $upload->item_id = $item->id;
                            $upload->src = $image;
                            $upload->save();
                            if (!$k) {
                                $item->cover_image_id = $image_id;
                                $item->save();
                                $cat_links = CategoryLink::find()
                                    ->select([
                                        'category_id'
                                    ])
                                    ->where([
                                        'group_id' => $group->id
                                    ])
                                    ->groupBy('category_id')
                                    ->all();
                                foreach ($cat_links as $cat_link) {
                                    /* @var $cat Categories */
                                    $cat = Categories::findOne($cat_link->category_id);
                                    if ($cat->cover_image_id == null) {
                                        $cat->cover_image_id = $image_id;
                                        $cat->save();
                                    }
                                    while ($cat->pid != null) {
                                        $cat = Categories::findOne($cat->pid);
                                        if ($cat->cover_image_id == null) {
                                            $cat->cover_image_id = $image_id;
                                            $cat->save();
                                        }
                                    }
                                }
                            }
                        }
                        $k++;
                        $i++;
                    }
                }
            }
            echo $p++ . " | " . (microtime(true) - $t1) . ' | ' . $entry['article2'][0] . ' | ' . $i . "\n";
        }
        echo 'Время операции: ' . (microtime(true) - $t1) . " сек\n";
        echo 'Обработано записей: ' . $p . "\n";
        echo 'Добавлено изображений: ' . $i . "\n";
    }

    public function reposition()
    {
        $categories = Categories::find()
            ->all();
        foreach ($categories as $cat) {
            $cat->position = -1;
            $cat->sort = 0;
            $cat->save();
        }
        foreach ($categories as $cat) {
            $pos = Categories::find()
                ->select([
                    'MAX(position) as position'
                ])
                ->where([
                    'pid' => $cat->pid
                ])
                ->one()->position;
            $cat->position = $pos + 1;
            $cat->save();
        }
    }

    public function actionTest($cursor = 1)
    {
//        echo '1';
        $p = 0;
        $s = [1, 2, 3, 4, 5];
        foreach ($s as $a) {
            $p++;
            if ($p >= $cursor) {
                echo $a . "\n";
            }
        }
    }

    public function actionHappyGifts()
    {
        $array = require_once(__DIR__ . '/../uploads/HappyGifts.php');

        $i = 0;
        $s = 0;
        $count = count($array);

        foreach ($array as $arr) {

            $article = $arr[0];
//            $article = '745040.145/L';
//            $price = $arr[1];
//            $url = 'http://happygifts.ru/catalog_new/search/?q=' . urlencode($article) . '&s=%D0%9F%D0%BE%D0%B8%D1%81%D0%BA';

//            $content = file_get_contents($url);

//            $file = 'search.html';

            $old_dir = '@runtime/parser_happy_gifts';
            $old_dir = Yii::getAlias($old_dir);
//            My::checkDir($old_dir);
            $old_dir .= str_replace('/', '-', $article) . '/';
//            My::checkDir($old_dir);
//            $content = file_get_contents($old_dir.$file);

//            $new_dir = '@runtime/parser_happy_gifts/';
//            $new_dir = Yii::getAlias($new_dir);
//            My::checkDir($new_dir);
//            $new_dir .= str_replace('/', '-', $article) . '/';
//            My::checkDir($new_dir);
//
//            file_put_contents($new_dir . $file, $content);
//            chmod($new_dir . $file, 0777);

            My::removeDir($old_dir);

            echo $i . " из " . $count . "\n";
            echo $article . "\n";
            $i++;
        }

//            $pattern = '/id="item(.*?)commercial-offers-button/ims';
//            preg_match_all($pattern, $content, $matches);
//
////            $pattern = '/Артикул: (' . str_replace('.', '\.', str_replace('/', '\/', $article)) . ')</';
//            $pattern = '/Артикул: (' . str_replace('.', '\.', explode('/',$article)[0]) . ')</';
//
//            pree($matches[1]);
//
//            foreach ($matches[1] as $match) {
//                if (preg_match($pattern, $match)) $block = $match;
//            }
//            if (isset($block)) {
//                $pattern = '/href="(.*?)"/';
//                preg_match($pattern, $block, $link);
//
//                $dir = '@runtime/parser_happy_gifts/';
//                $dir = Yii::getAlias($dir);
//                My::checkDir($dir);
//                $dir .= str_replace('/', '-', $article) . '/';
//                My::checkDir($dir);
//
//                $file = 'index.html';
//
//                $content = 'http://happygifts.ru' . $link[1];
//
//                $html = file_get_contents($content);
//
//                $reg_exp = '/<h1 class="header">(.*?)<\//is';
//                preg_match($reg_exp, $html, $title);
//                $title = $title ? $title[1] : null;
//
////                if (!file_exists($dir . $file))
//                file_put_contents($dir . $file, $html);
//                chmod($dir . $file, 0777);
//
//                echo("$article\n");
//
//            } else {
//                echo "Не найдено\n";
//            }
//            break;
    }

    public function actionHappyItems()
    {
        $array = require_once(__DIR__ . '/../uploads/HappyGifts.php');

        $i = 0;
        $d = 0;
        $s = 0;
        $count = count($array);

        foreach ($array as $arr) {
            $data = [];
//            $article = $arr[0];
            $article = '745040.145/L';
            $dir = '@runtime/parser_happy_gifts/';
            $dir = Yii::getAlias($dir);
            $dir .= str_replace('/', '-', $article) . '/';

            $file = 'index.html';

            $content = file_exists($dir . $file) ? $dir . $file : '';

            if ($content) {
                $html = file_get_contents($content);

                $data['article'] = $article;

                $reg_exp = '/ELEMENT_ID=(.*?)"/is';
                preg_match($reg_exp, $html, $data['group_id']);
                $data['group_id'] = $data['group_id'] ? $data['group_id'][1] : null;

                $reg_exp = '/<span class="price">(.*?)<\//is';
                preg_match($reg_exp, $html, $data['price']);
                $data['price'] = $data['price'] ? $data['price'][1] : null;
                $data['price'] = str_replace(',', '.', preg_replace('/[^\d,]/', '', $data['price']));

                $reg_exp = '/<h1 class="header">(.*?)<\//is';
                preg_match($reg_exp, $html, $data['title']);
                $data['title'] = $data['title'] ? $data['title'][1] : null;

                $reg_exp = '/breadcrumb-title(.*?)title="(.*?)"/is';
                preg_match_all($reg_exp, $html, $data['categories']);
                $data['categories'] = $data['categories'] ? $data['categories'][2] : null;
                unset($data['categories'][array_search('Главная', $data['categories'])]);
                unset($data['categories'][array_search('Каталог', $data['categories'])]);
                $data['categories'] = Json::encode(array_values($data['categories']));

                $reg_exp = '/color-item(.*?)title="(.*?)"(.*?)id="c_(.*?)"/is';
                preg_match_all($reg_exp, $html, $data['colors_colors']);
                $data['colors_hex'] = $data['colors_colors'][4];
                $data['colors_titles'] = $data['colors_colors'][2];

                if (!empty($data['colors_hex'])) {
                    $data['color'][$data['colors_hex'][0]] = $data['colors_titles'][0];
                    $data['color'] = Json::encode($data['color']);
                }
                unset($data['colors_colors'], $data['colors_hex'], $data['colors_titles']);

                $reg_exp = '/Размер(.*?)<td>(.*?)<\/td>/is';
                preg_match($reg_exp, $html, $data['sizes']);
                $data['sizes'] = $data['sizes'] ? $data['sizes'][2] : null;

                $reg_exp = '/Материал<(.*?)<td>(.*?)<\/td>/is';
                preg_match($reg_exp, $html, $data['material']);
                $data['material'] = $data['material'] ? $data['material'][2] : null;

                $reg_exp = '/Вес изделия(.*?)<td>(.*?)<\/td>/is';
                preg_match($reg_exp, $html, $data['weight']);
                $data['weight'] = $data['weight'] ? $data['weight'][2] : null;

                $reg_exp = '/Тип нанесения<(.*?)<td>(.*?)<\/td>/is';
                preg_match($reg_exp, $html, $data['technologies']);
                $data['technologies'] = $data['technologies'] ? trim($data['technologies'][2]) : null;

                $reg_exp = '/Объем изделия(.*?)<td>(.*?)<\/td>/is';
                preg_match($reg_exp, $html, $data['vol']);
                $data['vol'] = $data['vol'] ? trim($data['vol'][2]) : null;

                $reg_exp = '/Бренд<(.*?)">(.*?)<\//is';
                preg_match($reg_exp, $html, $data['brand']);
                $data['brand'] = $data['brand'] ? trim($data['brand'][2]) : null;

                $reg_exp = '/\/catalog-images(.*?).jpg/is';
                preg_match($reg_exp, $html, $data['image_src']);
                $data['image_src'] = $data['image_src'] ? trim($data['image_src'][0]) : null;

                pre($data);
                $db = Yii::$app->getDb();
                try {
                    $db->createCommand()->insert('parser_happygifts', $data)->execute();
                    $i++;
                    echo "Inserted #$i/$count {$data['article']}\n";

                } catch (IntegrityException $e) {
                    $d++;
                    echo "Дубликат #$d/$count {$data['article']}\n";
                }
            } else {
                $s++;
                echo "Пропущенно $s/$count\n";
            }
            break;
        }

        echo "Всего $count\n";
        echo "Inserted $i\n";
        echo "Дубликатов $d\n";
        echo "Пропущенно $d\n";
    }
}
