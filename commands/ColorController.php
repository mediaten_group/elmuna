<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 06.06.2016
 * Time: 10:18
 */

namespace app\commands;


use app\tables\Colors;
use app\tables\ColorsGifts;
use app\tables\Items;
use yii;
use yii\console\Controller;

class ColorController extends Controller
{

    private $sol_array = [
        '#ffffff' => [ // Белый
            101, 102, 904, 905, 906, 907, 964, 981, 987, 989,
        ],
        '#f5f5dc' => [ // Бежевый
            114, 115, 123
        ],
        '#ffc0cb' => [ // Розовый
            137, 138, 140, 141, 147
        ],
        '#ed143d' => [ // Малиновый
            142
        ],
        '#ff0000' => [ // Красный
            145, 146, 162, 908, 925, 927, 937, 157, 155
        ],
        '#add8e6' => [ // Голубой
            200, 219, 220, 221, 233, 245, 922
        ],
        '#40e0d0' => [ // Бирюзовый
            225, 320, 321, 919
        ],
        '#0000ff' => [ // Синий
            232, 241, 244, 255, 256, 913, 928, 227
        ],
        '#006400' => [ // Темно-зеленый
            264,
            277
        ],
        '#f0e68c' => [ // Хаки
            268,
            269,
            915
        ],
        '#00ff00' => [ // Зеленый
            270, 272, 275, 276, 279, 284, 920, 929, 933, 263, 266
        ],
        '#adff2f' => [ // Зеленое яблоко
            280, 924, 282
        ],
        '#aaaaaa' => [ // Серый
            300,
            330,
            340,
            349,
            350,
            360,
            370,
            900,
            957,
        ],
        '#ffff00' => [ // Желтый
            301, 401, 302
        ],
        '#000000' => [ // Черный
            309,
            312,
            314,
            917,
            931,
            935,
            971
        ],
        '#00008b' => [ // Темно-синий
            318,
            319,
            912,
        ],
        '#888888' => [ // Темно-серый
            381,
            384,
            385,
            911,
            932,
            959
        ],
        '#4b403a' => [ // Темно-коричневый
            398, 921, 394, 396
        ],
        '#ffa500' => [ // Оранжевый
            400,
            403,
        ],
        '#ee82ee' => [ // Сиреневая
            700,
        ],
        '#800080' => [ // Фиолетовая
            710,
            712,
            720,
        ]
    ];
    private $pan_array = [
        '#ffffff' => [ // Белый
            '1c', '427', '216', '1с'
        ],
        '#f5f5dc' => [ // Бежевый
            '7500', '7504', '7535', '728', '451'
        ],
        '#ffc0cb' => [ // Розовый
            '189', '232', '211'
        ],
        '#ed143d' => [ // Малиновый
        ],
        '#ff0000' => [ // Красный
            '032', '195', '198', '199', '200', '1797', '188', '7620', '1795', '193', '1805', '425', '485', '185', '675',
            '711', '228', '208c', '499', '742', '222', '187', '201', '7652', '202', '765'
        ],
        '#add8e6' => [ // Голубой
            '2204', '7450', '300', '284', '312', '275', '288'
        ],
        '#40e0d0' => [ // Бирюзовый
            '2995', '7474', '313', '771'
        ],
        '#0000ff' => [ // Синий
            '11c', '287', '2746', '2747', '2738', '2736', '276', '2748', '660', '661', '645', '647', '659', '2945',
            '7546', '7677', '7684', '280', '2757', '289', '293', '2728', '5405', '2144', '286', '7685', '2745', '768',
            '7462', '295', '541', '275', '072', '273', '272', '294'
        ],
        '#006400' => [ // Темно-зеленый
            '3298',
        ],
        '#f0e68c' => [ // Хаки
        ],
        '#00ff00' => [ // Зеленый
            '350', '347', '375', '374', '373', '7489', '7484', '7488', '347', '348', '354', '554', '341', '3308', '349',
            '7732', '356', '376', '343', '342', '5477', '330', '560', '19-5917', '377', '334', '772', '368', '7736',
            '366', '355'
        ],
        '#adff2f' => [ // Зеленое яблоко
            '359'
        ],
        '#aaaaaa' => [ // Серый
            '429', '4c', '420', '421', '431', '8c', '4u'
        ],
        '#ffff00' => [ // Желтый
            '114c', '124', '193c', '118c', '1235', '42012', '1365', '7405', '107', '116', '108', '740', '389', '102',
            '129', '115'
        ],
        '#000000' => [ // Черный
            '122', '186', '426'
        ],
        '#00008b' => [ // Темно-синий
            '1u296', '296', '2188', '540', '533', '534', '539', '101', '282', '433'
        ],
        '#888888' => [ // Темно-серый
            '7540', '444', '2210'
        ],
        '#4b403a' => [ // Темно-коричневый
            '5 black u', '7523'
        ],
        '#ffa500' => [ // Оранжевый
            '165', '144', '021', '871', '1505', '151', '166', '774', '158', '2025', '159', '497'
        ],
        '#ee82ee' => [ // Сиреневый
            '2577'
        ],
        '#800080' => [ // Фиолетовый
            '627', '668', '2077', '2685', '268', '233'
        ]
    ];

    public function actionClear()
    {
        $sql = <<<SQL
UPDATE items SET color=NULL
WHERE color IS NOT NULL 
SQL;
        $result = Yii::$app->getDb()->createCommand($sql)->queryAll();

        print_r("Обработано записей: " . count($result) . "\n");
    }

    public function actionOptimizePantone()
    {
        $items = ColorsGifts::find()->where("pantone != ''")->all();

        foreach ($items as $item) {
            $item['pantone'] = strpbrk($item['pantone'], '0123456789') ?: $item['pantone'];
            $item->save();
        }

        print_r("Обработано записей: " . count($items) . "\n");
    }

    public function actionSetSol()
    {
        $items = Items::find()
            ->asArray()
            ->select([
                'items.id',
                'sol',
                'name'
            ])
            ->innerJoin('colors_gifts', 'items.id = colors_gifts.item_id')
            ->where([
                'and',
                'items.color IS NULL',
                "colors_gifts.sol != ''"
            ])
            ->all();

        print_r($items);

        $i = 0;

        foreach ($items as $item) {
            print_r(count($items) - $i++ . "\t");
            print_r($item['name'] . "\t" . $item['sol'] . "\n");
            foreach ($this->sol_array as $hex => $sol) {
                if (in_array($item['sol'], $sol)) {

                    $color = Colors::findOne(['hex' => $hex]);

                    if (!$color) {
                        $color = new Colors();
                        $color->hex = $hex;
                        $color->title = $hex;
                        $color->alias = substr($hex, 1);
                        $color->save();
                    }

                    $product = Items::findOne($item['id']);
                    $product->color = $hex;
                    $product->save();

                    break;
                }
            }
        }

        print_r("Обработано записей: " . count($items) . "\n");
    }

    public function actionSetPan()
    {
        $items = Items::find()
            ->asArray()
            ->select([
                'items.id',
                'pantone',
                'name'
            ])
            ->innerJoin('colors_gifts', 'items.id = colors_gifts.item_id')
            ->where([
                'and',
                'items.color IS NULL',
                "colors_gifts.pantone != ''"
            ])
            ->all();

        print_r($items);

        $i = 0;

        foreach ($items as $item) {
            print_r(count($items) - $i++ . "\t");
            print_r($item['id'] . "\t" . $item['pantone'] . "\t" . $item['name'] . "\n");
            foreach ($this->pan_array as $hex => $pans) {
                foreach ($pans as $pan) {
                    if (strpos($item['pantone'], $pan) === 0) {

                        $color = Colors::findOne(['hex' => $hex]);

                        if (!$color) {
                            $color = new Colors();
                            $color->hex = $hex;
                            $color->title = $hex;
                            $color->alias = substr($hex, 1);
                            $color->save();
                        }

                        $product = Items::findOne($item['id']);
                        $product->color = $hex;
                        $product->save();

                        break;
                    }
                }
            }
        }

        print_r("Обработано записей: " . count($items) . "\n");
    }

}