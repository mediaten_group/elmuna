<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 06.04.16
 * Time: 12:58
 */

namespace app\commands;


use app\lib\My;
use app\tables\Categories;
use yii\console\Controller;

class CatController extends Controller
{

    private $array = [
        'Сувенирная продукция' => [
            'Ручки' => [
                'Пластиковые ручки' => [],
                'Металлические ручки' => [],
                'Наборы ручек' => [],
                'Оригинальные ручки' => [],
                'Деревянные ручки' => [],
                'Футляры' => [],
            ],
            'Промо-сувениры' => [
                'Брелоки и открывалки' => [],
                'Зажигалки и пепельницы' => [],
                'Светоотражатели' => [],
                'Промопродукция' => [],
                'Антистрессы' => [],
            ],
            'Посуда и кухонные принадлежности' => [
                'Кружки' => [],
                'Наборы и посуда' => [],
                'Термосы, термокружки, фляги' => [],
                'Наборы для вина и других напитков' => [],
                'Кухонные принадлежности' => [],
            ],
            'Электроника' => [
                'Калькуляторы' => [],
                'Колонки, радио, проигрыватели' => [],
                'Компьютерные аксессуары' => [],
                'Наборы для презентаций' => [],
                'Универсальные аккумуляторы и зарядные устройства' => [],
                'Аксессуары для мобильных устройств' => [],
            ],
            'Флешки' => [
                'Пластиковые флешки' => [],
                'Металлические флешки' => [],
                'Кожаные флешки' => [],
                'Деревянные флешки' => [],
                'PVC' => [],
                'Оригинальные флешки' => [],
            ],
            'Спорт и отдых' => [
                'Дорожные аксессуары' => [],
                'Игры' => [],
                'Пледы' => [],
                'Автомобильные аксессуары' => [],
                'Спортивные товары' => [],
                'Пляжный отдых' => [],
                'Термосы и термокружки' => [],
                'Наборы для пикника' => [],
            ],
            'Часы и погодные станции' => [
                'Настольные часы' => [],
                'Песочные часы' => [],
                'Метеостанции' => [],
                'Наручные часы' => [],
                'Дорожные и карманные часы' => [],
                'Настенные часы' => [],
            ],
            'Инструменты' => [
                'Мультиинструмент и наборы инструментов' => [],
                'Фонари' => [],
                'Ножи' => [],
                'Аксессуары для автомобилей' => [],
            ],
            'Зонты и дождевики' => [
                'Складные зонты' => [],
                'Необычные и оригинальные зонты' => [],
                'Зонты-трости' => [],
            ],
            'Сумки' => [
                'Сумки для ноутбука' => [],
                'Конференц-сумка, сумки для документов' => [],
                'Спортивные сумки' => [],
                'Сумки для покупок' => [],
                'Портфели' => [],
                'Папки' => [],
                'Нессеры' => [],
                'Чехлы для телефонов и планшетов' => [],
                'Сумка-холодильник' => [],
                'Рюкзаки' => []
            ],
            'Офис' => [
                'Настольные аксессуары' => [],
                'Держатели для документов' => [],
                'Настольные наборы' => [],
                'Пресс-папье' => [],
                'Аксессуары для телефонов и компьютеров' => [],
                'Наборы для презентаций' => [],
                'Визитницы' => [],
            ],
            'Дом и быт' => [
                'Альбомы и фоторамки' => [],
                'Картины и панно' => [],
                'Пледы' => [],
                'Предметы интерьера' => [],
                'Наборы для ухода за обувью' => [],
                'Игрушки' => [],
                'Свечи' => [],
                'Текстиль' => [],
                'Зеркала и шкатулки' => [],
                'Косметические и маникюрные наборы' => [],
            ],
            'Персональные аксессуары' => [
                'Портмоне и кошельки' => [],
                'Подарочные наборы' => [],
                'Папки, блокноты' => [],
                'Визитницы' => [],
                'Портфели' => [],
                'Дорожные органайзеры' => [],
                'Зажимы для денег' => [],
                'Обложки для документов' => [],
            ],
            'Ежедневники' => [
                'Ежедневники' => [],
                'Планинги' => [],
                'Оригинальные ежедневники' => [],
                'Упаковка для ежедневников' => [],
                'Записные книжки' => [],
                'Еженедельники' => [],
            ]
        ],
        'Изделия из текстиля' => [
            'Бейсболки' => [],
            'Футболки' => [],
            'Рубашки поло' => [],
            'Толстовки' => [],
            'Пледы' => [],
            'Полотенце' => [],
            'Чехол для шампанского' => [],
        ],
        'Полиграфия' => [
            'Календари' => [],
            'Блокноты' => [],
            'Визитки' => [],
            'Буклеты' => [],
            'Флаера' => [],
            'Листовки' => [],
            'Афиша' => [],
            'Открытки' => [],
        ],
        'Наградная продукция' => [
            'Кубки' => [],
            'Медали' => [],
            'Фигуры пластиковые' => [],
            'Фигуры литые' => [],
            'Тарелки наградные' => [],
            'Награды из стекла' => [],
            'Плакетки' => [],
            'Дипломы и грамоты' => [],
        ],
        'Наружняя реклама' => [
            'Внутренняя реклама' => [
                'Офисные таблички' => [],
                'Бейджи' => [],
                'Доски информации' => [],
                'Карманы настенные' => [],
                'Подставки под меню' => [],
                'Шильды жетоны' => [],
            ],
            'Внешняя реклама' => [
                'Вывески' => [],
                'Стелы' => [],
                'Баннеры' => [],
                'Штендеры' => [],
                'Реклама на автомобиле' => [],
                'Фрезеровка' => [],
                'Плотерная резка' => [],
                'Широкоформатная печать' => [],
                'Интерьерная печать' => [],
            ]
        ],
        'Распродажа' => [],
        'Новинки' => [],
        'Эксклюзивные товары' => [
            'Сувениры из оргстекла' => [],
            'Акрилайты' => [],
            'Акриловые часы' => [],
            'Дизайнерские открытки' => [],
        ]
    ];
    private $alias = [
        'Эксклюзивные товары' => 'exclusive',
        'Новинки' => 'new',
        'Распродажа' => 'sale',
        'Наружняя реклама' => 'advertising',
        'Полиграфия' => 'polygraphy',
        'Изделия из текстиля' => 'textile',
        'Сувенирная продукция' => 'gifts',
        'Наградная продукция' => 'honors'
    ];

    public function actionIndex1()
    {
        $l1 = $this->array;
        foreach ($l1 as $cat1 => $l2) {
            $c1 = Categories::findOne([
                'title' => $cat1,
                'pid'=>null
            ]);
            if (!$c1) {
                $c1 = new Categories();
                $c1->title = $cat1;
                $c1->alias = $this->alias[$cat1];
                $c1->created_at = My::dateTime();
                $c1->status = 1;
                $c1->front_page = 1;
                $c1->save();
                $c1->sort = $c1->id;
                $c1->path = '{' . $c1->id . '}';
                $c1->save();
            }
            foreach ($l2 as $cat2 => $l3) {
                $c2 = Categories::findOne([
                    'title' => $cat2,
                    'pid'=>$c1->id
                ]);
                if (!$c2) {
                    $c2 = new Categories();
                    $c2->title = $cat2;
                    $c2->alias = My::str2url($cat2);
                    $c2->created_at = My::dateTime();
                    $c2->status = 1;
                    $c2->front_page = 1;
                    $c2->pid = $c1->id;
                    $c2->save();
                    $c2->sort = $c2->id;
                    $c2->path = '{' . $c1->id . ',' . $c2->id . '}';
                    $c2->save();
                }
                foreach ($l3 as $cat3 => $l4) {
                    $c3 = Categories::findOne([
                        'title' => $cat3,
                        'pid'=>$c2->id
                    ]);
                    if (!$c3) {
                        $c3 = new Categories();
                        $c3->title = $cat3;
                        $c3->alias = My::str2url($cat3);
                        $c3->created_at = My::dateTime();
                        $c3->status = 1;
                        $c3->front_page = 1;
                        $c3->pid = $c2->id;
                        $c3->save();
                        $c3->position = $c3->id;
                        $c3->path = '{' . $c1->id . ',' . $c2->id . ',' . $c3->id . '}';
                        $c3->save();
                    }
                }
            }
        }
    }

    public function actionIndex()
    {
        $cats = Categories::find()->all();
        foreach ($cats as $cat) {
            $cat->position = $cat->id;
            $cat->save();
        }
    }

}