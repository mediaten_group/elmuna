<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.12.2015
 * Time: 12:24
 */

namespace app\commands;

use app\components\Index;
use yii\console\Controller;

class IndexController extends Controller
{
    public function actionMarkup()
    {
        Index::markup();
    }
}
