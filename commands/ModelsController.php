<?php
/**
 * Created by PhpStorm.
 * User: dan.judex
 * Date: 09.09.2015
 * Time: 14:36
 */

namespace app\commands;


use Yii;
use yii\console\Controller;
use yii\gii\CodeFile;
use yii\gii\generators\model\Generator;

class ModelsController extends Controller
{
    public $defaultAction = 'update';

    /**
     *
     */
    public function actionUpdate()
    {
        $namespace = 'app\tables';

        $root = Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        if (!is_dir($root)) {
            mkdir($root);
        } else {
            $files = scandir($root);
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                unlink($root . '/' . $file);
                echo "remove $root/$file\n";
            }
        }

        $connection = Yii::$app->db;
        $schemas = $connection->schema->getTableSchemas();

        foreach ($schemas as $tbl) {
            $gen = new Generator;
            $gen->ns = $namespace;
            $gen->tableName = $tbl->name;
            $gen->generateLabelsFromComments = true;
            /** @var CodeFile[] $files */
            $files = $gen->generate();
            foreach ($files as $file) {
                self::saveTable($file);
                echo "Save $tbl->name\n";
            }
        }
    }

    public static function saveTable(CodeFile $file)
    {
        file_put_contents($file->path, $file->content);
    }
}
