<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 12.01.2016
 * Time: 16:03
 */

namespace app\commands;


use app\models\Items;
use app\modules\admin\models\Groups;
use app\modules\upload\models\Upload;
use app\tables\Categories;
use app\tables\CategoriesGifts;
use app\tables\CategoryLink;
use app\tables\ItemsGifts;
use app\tables\ItemUpload;
use Yii;
use yii\console\Controller;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class GiftsController extends Controller
{

    public $defaultAction = 'test';

    public function actionTest()
    {
        echo "1223\n";
    }

    public function actionCategories()
    {
        $categories = CategoriesGifts::find()->where(['level' => 1])->all();
        foreach ($categories as $cat) {
            /** @var CategoriesGifts $oldParent */
            $oldParent = CategoriesGifts::findOne($cat['parent_id']);

            /** @var Categories $newParent */
            $newParent = Categories::findOne(['alias' => $oldParent->alias]);

            $category = new Categories([
                'alias' => $cat['alias'],
                'title' => $cat['title'],
                'pid' => $newParent->id,
            ]);
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $category->save();
                $transaction->commit();
            } catch (\Exception $e) {
                throw $e;
            }
            echo $cat['title'] . "\n";
        }
    }

    public function actionC()
    {
        $categories = Categories::find()->where(['path' => null])->all();
        foreach ($categories as $c) {
            $sql = <<<SQL
UPDATE categories SET "path" = string_to_array('{$c->pid},{$c->id}',',')::int[] WHERE "id" = {$c->id}
SQL;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                Yii::$app->db->createCommand($sql)->execute();
                $transaction->commit();
            } catch (\Exception $e) {
                throw $e;
            }
            echo $c['title'] . "\n";
//            $category = Categories::findOne($c->id);
//            $category->path = [$category->id];
//            $category->save();
        }
    }

    public function actionGroups()
    {
        $categories = CategoriesGifts::find()->where(['level' => 2])->all();
        foreach ($categories as $cat) {
            /** @var CategoriesGifts $oldParent */
            $oldParent = CategoriesGifts::findOne($cat['parent_id']);

            /** @var Categories $newParent */
            $newParent = Categories::findOne(['alias' => $oldParent['alias']]);

            $group = new Groups([
                'status' => 1,
                'title' => $cat['title'],
            ]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($group->save()) {
                    $link = new CategoryLink([
                        'group_id' => $group->id,
                        'category_id' => $newParent['id'],
                    ]);
                    $link->save();
                } else {
                    throw new Exception($group->errors);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                throw $e;
            }
            echo $cat['title'] . "\n";
        }
    }

    public function actionItems()
    {
        $items = ItemsGifts::find()->asArray()->all();
        Upload::initThumbs();
        foreach ($items as $i) {
            /** @var CategoriesGifts $oldParent */
            $oldParent = CategoriesGifts::findOne($i['category_id']);

            /** @var Groups $group */
            $group = Groups::findOne(['title' => $oldParent['title']]);

            if (!$group) {
                /** @var Categories $newParent */
                $newParent = Categories::findOne(['alias' => $oldParent['alias']]);

                $group = new Groups([
                    'status' => 1,
                    'title' => $i['title'],
                ]);
            }
//if(!$i['title']) $i['title'] = ' ';
            $item = new Items();
            $data = [
                'status' => 1,
                'title' => $i['title'] ?: 'Не указано',
                'article' => $i['article'],
                'price' => (float)$i['price'],
                'sizes' => $i['sizes'],
                'material' => $i['material'],
                'weight' => $i['weight'],
                'box_size' => $i['korob_size'],
                'box_weight' => $i['korob_weight'],
                'box_vol' => $i['korob_vol'],
                'box_count' => $i['korob_count'],
            ];

            $item->load($data, '');

//            $dir = Yii::getAlias('@runtime/parser_gifts/');
//            $dir = $dir.trim($i['article']).'/1000/';
//            if(is_dir($dir)){
//                $images = array_diff(scandir($dir),['..', '.']);
//                foreach($images as $image){
//                    $id = Upload::uploadByPath($dir.$image,['screen', 'small', '100']);
//                    echo print_r($id,true)."\n";
//                    exit;
//                }
//                echo print_r($images,true)."\n";
//                exit;
//            }
//            echo trim($i['article']).'1000/'." Не робит\n";
//            exit;
//
//            $images =
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($group->isNewRecord && $group->save()) {
                    $link = new CategoryLink([
                        'group_id' => $group->id,
                        'category_id' => $newParent['id'],
                    ]);
                    $link->save();
                }

                $item->group_id = $group->id;
                if (!$item->save()) throw new Exception(Json::encode($item->errors));

                echo $i['title'] . "\n";

                $transaction->commit();
            } catch (\Exception $e) {
                throw $e;
            }
        }
    }

    public function actionTitles()
    {
        $items = Items::find()->where('"cover_image_id" IS NULL')->asArray()->count();
        echo $items . "\n";
        exit;
        $root = Yii::getAlias('@runtime/parser_gifts/');
        $j = 0;
        foreach ($items as $i) {
            $dir = $root . trim($i['article']) . '/';
            $file = $dir . 'index.html';
            if (is_dir($dir) && is_file($file)) {
                $html = file_get_contents($file);

                $data = [];

                $reg_exp = '/(<div id="j_gal".*?)<\/div><\/div><\/div><\/div>/i';
                preg_match($reg_exp, $html, $data['images']);
                $data['images'] = $data['images'] ? $data['images'][1] : null;

                $reg_exp = '/hash="(.*?)"/i';
                preg_match_all($reg_exp, $data['images'], $data['images_big']);
                $images = $data['images_big'][1] ?: '';

                if ($images) {
                    /** @var Items $item */
                    $item = Items::findOne($i['id']);
                    foreach ($images as $image) {
                        $ext = array_pop(explode('.', $image));
                        if ($ext !== 'jpg') continue;
//                        echo $ext."\n";continue;

                        $id = Upload::uploadByPath($image, ['screen', 'small', '100']);
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            $upload = new ItemUpload();
                            $upload->upload_id = $id;
                            $upload->item_id = $i['id'];
                            $upload->save();

                            if (!$item->cover_image_id) {
                                $item->cover_image_id = $id;
                                $item->save();
                            }
                            echo $i['id'] . ' ' . $id . "\n";
                            $transaction->commit();
                        } catch (\Exception $e) {
                            throw $e;
                        }
                    }
                }
                if ($images) {
//                    echo print_r($images, true) . "\n";
                    $j++;
                }
            }
//            if($j > 40) break;
        }
        echo $j . "\n";
    }

    public function actionPrice()
    {
        $items = Items::find()->asArray()->all();
        $j = 0;
        foreach ($items as $i) {
            /** @var ItemsGifts $gift */
            $gift = ItemsGifts::findOne($i['article']);
            $price = explode(' ', $gift->price);
            $price = isset($price[1]) ? $price[1] : $price[0];
            $price = (float)str_replace(',', '.', $price);

            /** @var Items $item */
            $item = Items::findOne($i['id']);
            $data['price'] = $price;
            $item->load($data, '');
            $item->save();

            echo $j . ' ' . $price . "\n";
            $j++;
//            if ($j > 30) break;
        }
    }

    public function actionImages()
    {
        $items = Items::find()->where('"cover_image_id" IS NULL')->asArray()->all();

        foreach ($items as $i) {
            $dir = Yii::getAlias('@runtime/parser_gifts/');
            $dir = $dir . trim($i['article']) . '/1000/';
            if (is_dir($dir)) {
                $images = array_diff(scandir($dir), ['..', '.']);

                echo trim($i['article']) . "\n";
                continue;
                /** @var Items $item */
                $item = Items::findOne($i['id']);
                foreach ($images as $image) {
                    $id = Upload::uploadByPath($dir . $image, ['screen', 'small', '100']);

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $upload = new ItemUpload();
                        $upload->upload_id = $id;
                        $upload->item_id = $i['id'];
                        $upload->save();

                        if (!$item->cover_image_id) {
                            $item->cover_image_id = $id;
                            $item->save();
                        }
                        $transaction->commit();
                    } catch (\Exception $e) {
                        throw $e;
                    }
                }
                echo $i['id'] . "\n";
            }
        }
    }

    public function actionCategoryImage()
    {
        $categories = Categories::find()->asArray()->all();
        foreach ($categories as $c) {
            if (!$c['cover_image_id']) {
                /** @var CategoryLink $link */
                $link = CategoryLink::findOne(['category_id' => $c['id']]);
                if ($link) {
                    /** @var Groups $group */
                    $group = Groups::findOne($link->group_id);
                    if ($group->cover_image_id) {
                        /** @var Categories $category */
                        $category = Categories::findOne($c['id']);
                        $category->cover_image_id = $group->cover_image_id;
                        $category->save();
                    }
                } else {
                    $child = Categories::findOne(['pid' => $c['id']]);
                    if ($child) {
                        /** @var CategoryLink $link */
                        $link = CategoryLink::findOne(['category_id' => $child->id]);
                        if ($link) {
                            /** @var Groups $group */
                            $group = Groups::findOne($link->group_id);
                            if ($group->cover_image_id) {
                                /** @var Categories $category */
                                $category = Categories::findOne($c['id']);
                                $category->cover_image_id = $group->cover_image_id;
                                $category->save();
                            }
                        }
                    }
                }
                echo $c['id'] . "\n";
            }
        }
    }
}