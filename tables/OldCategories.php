<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "old_categories".
 *
 * @property integer $id
 * @property string $alias
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $date
 * @property boolean $front_page
 * @property integer $level
 */
class OldCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'old_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['parent_id', 'level'], 'integer'],
            [['description'], 'string'],
            [['date'], 'safe'],
            [['front_page'], 'boolean'],
            [['alias', 'title', 'image'], 'string', 'max' => 255],
            [['alias', 'parent_id'], 'unique', 'targetAttribute' => ['alias', 'parent_id'], 'message' => 'The combination of Alias and Parent ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'parent_id' => 'Parent ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'date' => 'Date',
            'front_page' => 'Front Page',
            'level' => 'Level',
        ];
    }
}
