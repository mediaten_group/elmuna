<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "colors".
 *
 * @property integer $id
 * @property string $hex
 * @property string $title
 * @property string $alias
 *
 * @property Items[] $items
 */
class Colors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hex'], 'required'],
            [['hex'], 'string', 'max' => 7],
            [['title', 'alias'], 'string', 'max' => 255],
            [['hex'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hex' => 'Hex',
            'title' => 'Title',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['color' => 'hex']);
    }
}
