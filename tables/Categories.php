<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property integer $pid
 * @property integer $sort
 * @property string $path
 * @property integer $position
 * @property integer $cover_image_id
 * @property string $alias
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property integer $front_page
 * @property integer $status
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property double $markup
 * @property double $set_markup
 * @property string $view
 *
 * @property Categories $p
 * @property Categories[] $categories
 * @property Upload $coverImage
 * @property CategoryLink[] $categoryLinks
 * @property Groups[] $groups
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'path', 'text'], 'string'],
            [['pid', 'sort', 'position', 'cover_image_id', 'front_page', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['markup', 'set_markup'], 'number'],
            [['alias', 'meta_title'], 'string', 'max' => 50],
            [['meta_description', 'meta_keywords', 'view'], 'string', 'max' => 255],
            [['pid'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['pid' => 'id']],
            [['cover_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['cover_image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'pid' => 'Pid',
            'sort' => 'Sort',
            'path' => 'Path',
            'position' => 'Position',
            'cover_image_id' => 'Cover Image ID',
            'alias' => 'Алиас',
            'text' => 'Текст',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
            'front_page' => 'Отображать на главной',
            'status' => 'Опубликовано',
            'meta_title' => 'Заголовок для вывода',
            'meta_description' => 'Описание',
            'meta_keywords' => 'Ключевые слова',
            'markup' => 'Markup',
            'set_markup' => 'Set Markup',
            'view' => 'Отображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP()
    {
        return $this->hasOne(Categories::className(), ['id' => 'pid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['pid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoverImage()
    {
        return $this->hasOne(Upload::className(), ['id' => 'cover_image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLinks()
    {
        return $this->hasMany(CategoryLink::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['id' => 'group_id'])->viaTable('category_link', ['category_id' => 'id']);
    }
}
