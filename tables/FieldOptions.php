<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "field_options".
 *
 * @property integer $id
 * @property integer $field_id
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property Field $field
 */
class FieldOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_id', 'sort', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => Field::className(), 'targetAttribute' => ['field_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field_id' => 'Поле',
            'sort' => 'Порядок',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'active' => 'Используется',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }
}
