<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "scheme".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property integer $tech_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $status
 *
 * @property Field[] $fields
 * @property SchemeGroup $group
 * @property Technology $tech
 */
class Scheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tech_id', 'status'], 'integer'],
            [['tech_id'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchemeGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['tech_id'], 'exist', 'skipOnError' => true, 'targetClass' => Technology::className(), 'targetAttribute' => ['tech_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Группа схем',
            'name' => 'Название',
            'tech_id' => 'Технология нанесения',
            'updated_at' => 'Обновлено',
            'created_at' => 'Создано',
            'status' => 'Опубликовано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['scheme_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(SchemeGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTech()
    {
        return $this->hasOne(Technology::className(), ['id' => 'tech_id']);
    }
}
