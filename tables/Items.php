<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $title
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $price
 * @property string $intro
 * @property integer $cover_image_id
 * @property string $alias
 * @property string $article
 * @property string $color
 * @property string $sizes
 * @property string $material
 * @property string $weight
 * @property string $box_size
 * @property string $box_weight
 * @property string $box_vol
 * @property string $box_count
 * @property integer $is_bestseller
 * @property string $text
 * @property string $set_price
 * @property string $calc
 * @property string $params
 * @property string $source
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property CartItems[] $cartItems
 * @property ItemUpload[] $itemUploads
 * @property Upload[] $uploads
 * @property Colors $color0
 * @property Groups $group
 * @property Source $source0
 * @property Upload $coverImage
 * @property OrderItems[] $orderItems
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'title'], 'required'],
            [['group_id', 'status', 'cover_image_id', 'is_bestseller'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['price', 'set_price'], 'number'],
            [['alias', 'article', 'sizes', 'material', 'weight', 'box_size', 'box_weight', 'box_vol', 'box_count', 'text', 'params', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['title', 'intro', 'calc', 'source'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 7],
            [['color'], 'exist', 'skipOnError' => true, 'targetClass' => Colors::className(), 'targetAttribute' => ['color' => 'hex']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['source'], 'exist', 'skipOnError' => true, 'targetClass' => Source::className(), 'targetAttribute' => ['source' => 'alias']],
            [['cover_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['cover_image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'title' => 'Название',
            'status' => 'Опубликовано',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'price' => 'Price',
            'intro' => 'Intro',
            'cover_image_id' => 'Cover Image ID',
            'alias' => 'Алилас',
            'article' => 'Артикул',
            'color' => 'Color',
            'sizes' => 'Sizes',
            'material' => 'Material',
            'weight' => 'Weight',
            'box_size' => 'Box Size',
            'box_weight' => 'Box Weight',
            'box_vol' => 'Box Vol',
            'box_count' => 'Box Count',
            'is_bestseller' => 'Хит продаж',
            'text' => 'Текст',
            'set_price' => 'Set Price',
            'calc' => 'Calc',
            'params' => 'Params',
            'source' => 'Source',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartItems()
    {
        return $this->hasMany(CartItems::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemUploads()
    {
        return $this->hasMany(ItemUpload::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploads()
    {
        return $this->hasMany(Upload::className(), ['id' => 'upload_id'])->viaTable('item_upload', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor0()
    {
        return $this->hasOne(Colors::className(), ['hex' => 'color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource0()
    {
        return $this->hasOne(Source::className(), ['alias' => 'source']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoverImage()
    {
        return $this->hasOne(Upload::className(), ['id' => 'cover_image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['item_id' => 'id']);
    }
}
