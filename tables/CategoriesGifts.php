<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories_gifts".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $ordering
 * @property string $alias
 * @property string $title
 * @property string $image
 * @property boolean $front_page
 * @property integer $level
 * @property integer $position
 */
class CategoriesGifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_gifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'ordering', 'level', 'position'], 'integer'],
            [['front_page'], 'boolean'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'ordering' => 'Ordering',
            'alias' => 'Alias',
            'title' => 'Title',
            'image' => 'Image',
            'front_page' => 'Front Page',
            'level' => 'Level',
            'position' => 'Position',
        ];
    }
}
