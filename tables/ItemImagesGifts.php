<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "item_images_gifts".
 *
 * @property integer $id
 * @property string $article
 * @property string $name
 * @property string $title
 * @property string $alt
 * @property string $date
 */
class ItemImagesGifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_images_gifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['article'], 'string', 'max' => 11],
            [['name', 'title', 'alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'name' => 'Name',
            'title' => 'Title',
            'alt' => 'Alt',
            'date' => 'Date',
        ];
    }
}
