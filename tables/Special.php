<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "special".
 *
 * @property integer $id
 * @property string $title
 * @property integer $image
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $url
 * @property integer $status
 * @property string $text
 */
class Special extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'sort', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'url', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'image' => 'Изображение',
            'sort' => 'Sort',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'url' => 'Ссылка',
            'status' => 'Опубликовано',
            'text' => 'Текст',
        ];
    }
}
