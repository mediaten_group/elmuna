<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "parser_happygifts".
 *
 * @property integer $id
 * @property string $article
 * @property string $categories
 * @property string $brand
 * @property string $color
 * @property string $title
 * @property string $price
 * @property string $sizes
 * @property string $material
 * @property string $vol
 * @property string $weight
 * @property string $technologies
 * @property string $image_src
 * @property integer $group_id
 */
class ParserHappygifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_happygifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article'], 'required'],
            [['categories', 'technologies', 'image_src'], 'string'],
            [['group_id'], 'integer'],
            [['article'], 'string', 'max' => 20],
            [['brand', 'color', 'vol'], 'string', 'max' => 255],
            [['title', 'sizes', 'material'], 'string', 'max' => 50],
            [['price'], 'string', 'max' => 40],
            [['weight'], 'string', 'max' => 30],
            [['article'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'categories' => 'Categories',
            'brand' => 'Brand',
            'color' => 'Color',
            'title' => 'Title',
            'price' => 'Price',
            'sizes' => 'Sizes',
            'material' => 'Material',
            'vol' => 'Vol',
            'weight' => 'Weight',
            'technologies' => 'Technologies',
            'image_src' => 'Image Src',
            'group_id' => 'Group ID',
        ];
    }
}
