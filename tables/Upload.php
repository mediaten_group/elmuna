<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "upload".
 *
 * @property integer $id
 * @property string $name
 * @property string $ext
 * @property string $params
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Categories[] $categories
 * @property Clients[] $clients
 * @property ClientsUploads[] $clientsUploads
 * @property Clients[] $clients0
 * @property FieldColors[] $fieldColors
 * @property Groups[] $groups
 * @property ItemUpload[] $itemUploads
 * @property Items[] $items
 * @property Items[] $items0
 * @property TechPlaces[] $techPlaces
 * @property Technology[] $technologies
 * @property TechnologyUploads[] $technologyUploads
 * @property Technology[] $technologies0
 */
class Upload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ext'], 'required'],
            [['params'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['ext'], 'string', 'max' => 10],
            [['name', 'ext'], 'unique', 'targetAttribute' => ['name', 'ext'], 'message' => 'The combination of Name and Ext has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ext' => 'Ext',
            'params' => 'Params',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['cover_image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsUploads()
    {
        return $this->hasMany(ClientsUploads::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients0()
    {
        return $this->hasMany(Clients::className(), ['id' => 'client_id'])->viaTable('clients_uploads', ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldColors()
    {
        return $this->hasMany(FieldColors::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['cover_image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemUploads()
    {
        return $this->hasMany(ItemUpload::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['id' => 'item_id'])->viaTable('item_upload', ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems0()
    {
        return $this->hasMany(Items::className(), ['cover_image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechPlaces()
    {
        return $this->hasMany(TechPlaces::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnologies()
    {
        return $this->hasMany(Technology::className(), ['cover_image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnologyUploads()
    {
        return $this->hasMany(TechnologyUploads::className(), ['upload_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnologies0()
    {
        return $this->hasMany(Technology::className(), ['id' => 'technology_id'])->viaTable('technology_uploads', ['upload_id' => 'id']);
    }
}
