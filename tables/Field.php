<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "field".
 *
 * @property integer $id
 * @property string $name
 * @property integer $field_type_id
 * @property integer $scheme_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $sort
 * @property string $value
 * @property integer $status
 * @property integer $required
 * @property integer $separate
 *
 * @property FieldType $fieldType
 * @property Scheme $scheme
 * @property FieldOptions[] $fieldOptions
 */
class Field extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_type_id'], 'required'],
            [['field_type_id', 'scheme_id', 'sort', 'status', 'required', 'separate'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['value'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['field_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FieldType::className(), 'targetAttribute' => ['field_type_id' => 'id']],
            [['scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scheme::className(), 'targetAttribute' => ['scheme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'field_type_id' => 'Тип поля',
            'scheme_id' => 'Схема',
            'updated_at' => 'Обновлено',
            'created_at' => 'Создано',
            'sort' => 'Порядок',
            'value' => 'Параметры',
            'status' => 'Опубликовано',
            'required' => 'Обязательно',
            'separate' => 'Separate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldType()
    {
        return $this->hasOne(FieldType::className(), ['id' => 'field_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return $this->hasOne(Scheme::className(), ['id' => 'scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldOptions()
    {
        return $this->hasMany(FieldOptions::className(), ['field_id' => 'id']);
    }
}
