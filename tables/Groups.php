<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $foreign
 * @property integer $cover_image_id
 * @property string $alias
 * @property integer $scheme_group_id
 * @property integer $scheme_group_id_2
 * @property string $parser_code
 * @property string $source
 * @property integer $sort
 *
 * @property CategoriesParserGroupsLink[] $categoriesParserGroupsLinks
 * @property CategoriesParser[] $categoryParsers
 * @property CategoryLink[] $categoryLinks
 * @property Categories[] $categories
 * @property SchemeGroup $schemeGroup
 * @property SchemeGroup $schemeGroupId2
 * @property Upload $coverImage
 * @property Items[] $items
 * @property TechPlaces[] $techPlaces
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'status', 'cover_image_id', 'scheme_group_id', 'scheme_group_id_2', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['foreign', 'alias', 'parser_code'], 'string'],
            [['title', 'source'], 'string', 'max' => 255],
            [['scheme_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchemeGroup::className(), 'targetAttribute' => ['scheme_group_id' => 'id']],
            [['scheme_group_id_2'], 'exist', 'skipOnError' => true, 'targetClass' => SchemeGroup::className(), 'targetAttribute' => ['scheme_group_id_2' => 'id']],
            [['cover_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['cover_image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Заголовок',
            'status' => 'Опубликовано',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
            'foreign' => 'Foreign',
            'cover_image_id' => 'Cover Image ID',
            'alias' => 'Alias',
            'scheme_group_id' => 'Scheme Group ID',
            'scheme_group_id_2' => 'Scheme Group Id 2',
            'parser_code' => 'Parser Code',
            'source' => 'Source',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesParserGroupsLinks()
    {
        return $this->hasMany(CategoriesParserGroupsLink::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryParsers()
    {
        return $this->hasMany(CategoriesParser::className(), ['id' => 'category_parser_id'])->viaTable('categories_parser__groups__link', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLinks()
    {
        return $this->hasMany(CategoryLink::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->viaTable('category_link', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemeGroup()
    {
        return $this->hasOne(SchemeGroup::className(), ['id' => 'scheme_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemeGroupId2()
    {
        return $this->hasOne(SchemeGroup::className(), ['id' => 'scheme_group_id_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoverImage()
    {
        return $this->hasOne(Upload::className(), ['id' => 'cover_image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechPlaces()
    {
        return $this->hasMany(TechPlaces::className(), ['group_id' => 'id']);
    }
}
