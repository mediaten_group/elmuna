<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "old_items".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property integer $article
 * @property string $price
 * @property string $image
 * @property string $sizes
 * @property string $description
 * @property string $date
 */
class OldItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'old_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'article'], 'integer'],
            [['price'], 'number'],
            [['image'], 'required'],
            [['description'], 'string'],
            [['date'], 'safe'],
            [['title', 'image', 'sizes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'article' => 'Article',
            'price' => 'Price',
            'image' => 'Image',
            'sizes' => 'Sizes',
            'description' => 'Description',
            'date' => 'Date',
        ];
    }
}
