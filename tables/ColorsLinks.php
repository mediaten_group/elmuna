<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "colors_links".
 *
 * @property integer $color_id
 * @property string $column
 * @property string $pattern
 */
class ColorsLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colors_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color_id'], 'integer'],
            [['column', 'pattern'], 'required'],
            [['column', 'pattern'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'column' => 'Column',
            'pattern' => 'Pattern',
        ];
    }
}
