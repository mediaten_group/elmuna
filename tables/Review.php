<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $fio
 * @property string $email
 *
 * @property User $user
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['text'], 'required'],
            [['text', 'answer'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['fio', 'email'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'text' => 'Текст',
            'answer' => 'Ответ',
            'created_at' => 'Отправлен',
            'updated_at' => 'Updated At',
            'status' => 'Опубликован',
            'fio' => 'ФИО',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
