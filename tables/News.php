<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $intro
 * @property string $text
 * @property integer $cover_image_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $status
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['cover_image_id', 'status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['alias', 'title'], 'string', 'max' => 50],
            [['intro'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'intro' => 'Intro',
            'text' => 'Text',
            'cover_image_id' => 'Cover Image ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'status' => 'Опубликовано',
        ];
    }
}
