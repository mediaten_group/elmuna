<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "ordering".
 *
 * @property integer $id
 * @property string $order
 */
class Ordering extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordering';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
        ];
    }
}
