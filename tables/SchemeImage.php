<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "scheme_image".
 *
 * @property integer $id
 * @property integer $upload_id
 * @property string $created_at
 * @property string $updated_at
 */
class SchemeImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'upload_id' => 'Изображение',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
}
