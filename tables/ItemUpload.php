<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "item_upload".
 *
 * @property integer $upload_id
 * @property integer $item_id
 * @property integer $is_cover
 * @property string $src
 *
 * @property Items $item
 * @property Upload $upload
 */
class ItemUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_id', 'item_id'], 'required'],
            [['upload_id', 'item_id', 'is_cover'], 'integer'],
            [['src'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['upload_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['upload_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'item_id' => 'Item ID',
            'is_cover' => 'Is Cover',
            'src' => 'Src',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpload()
    {
        return $this->hasOne(Upload::className(), ['id' => 'upload_id']);
    }
}
