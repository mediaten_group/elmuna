<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "technology".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $image
 * @property string $text
 * @property boolean $main_page
 * @property integer $cover_image_id
 * @property string $updated_at
 * @property string $created_at
 * @property string $intro
 * @property integer $sort
 * @property integer $scheme_group_id
 * @property boolean $status
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property Scheme[] $schemes
 * @property SchemeGroup $schemeGroup
 * @property Upload $coverImage
 * @property TechnologyUploads[] $technologyUploads
 * @property Upload[] $uploads
 */
class Technology extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technology';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['text'], 'string'],
            [['main_page', 'status'], 'boolean'],
            [['cover_image_id', 'sort', 'scheme_group_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['alias', 'title', 'image', 'intro', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['image'], 'unique'],
            [['scheme_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchemeGroup::className(), 'targetAttribute' => ['scheme_group_id' => 'id']],
            [['cover_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['cover_image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'image' => 'Image',
            'text' => 'Text',
            'main_page' => 'Main Page',
            'cover_image_id' => 'Cover Image ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'intro' => 'Вступление',
            'sort' => 'Порядок',
            'scheme_group_id' => 'Scheme Group ID',
            'status' => 'Status',
            'meta_title' => 'Заголовок для отображения',
            'meta_description' => 'Описание для мета-тегов',
            'meta_keywords' => 'Ключевые слова',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemes()
    {
        return $this->hasMany(Scheme::className(), ['tech_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchemeGroup()
    {
        return $this->hasOne(SchemeGroup::className(), ['id' => 'scheme_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoverImage()
    {
        return $this->hasOne(Upload::className(), ['id' => 'cover_image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnologyUploads()
    {
        return $this->hasMany(TechnologyUploads::className(), ['technology_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploads()
    {
        return $this->hasMany(Upload::className(), ['id' => 'upload_id'])->viaTable('technology_uploads', ['technology_id' => 'id']);
    }
}
