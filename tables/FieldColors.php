<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "field_colors".
 *
 * @property integer $id
 * @property integer $upload_id
 * @property string $title
 * @property integer $status
 *
 * @property Upload $upload
 */
class FieldColors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field_colors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_id', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['upload_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['upload_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'upload_id' => 'Изображение',
            'title' => 'Название',
            'status' => 'Опубликовано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpload()
    {
        return $this->hasOne(Upload::className(), ['id' => 'upload_id']);
    }
}
