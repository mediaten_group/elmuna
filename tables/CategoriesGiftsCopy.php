<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories_gifts_copy".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $alias
 * @property string $title
 * @property string $cover
 * @property integer $level
 */
class CategoriesGiftsCopy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_gifts_copy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'level'], 'integer'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['cover'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'cover' => 'Cover',
            'level' => 'Level',
        ];
    }
}
