<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "items_gifts".
 *
 * @property integer $id
 * @property string $article
 * @property integer $category_id
 * @property string $brand
 * @property string $title
 * @property string $price
 * @property string $sizes
 * @property string $material
 * @property string $weight
 * @property string $korob_size
 * @property string $korob_weight
 * @property string $korob_vol
 * @property string $korob_count
 * @property string $technologies
 */
class ItemsGifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_gifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id'], 'integer'],
            [['article'], 'required'],
            [['technologies'], 'string'],
            [['article'], 'string', 'max' => 20],
            [['brand'], 'string', 'max' => 255],
            [['title', 'sizes', 'material'], 'string', 'max' => 50],
            [['price', 'korob_vol'], 'string', 'max' => 40],
            [['weight', 'korob_size', 'korob_weight', 'korob_count'], 'string', 'max' => 30],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'category_id' => 'Category ID',
            'brand' => 'Brand',
            'title' => 'Title',
            'price' => 'Price',
            'sizes' => 'Sizes',
            'material' => 'Material',
            'weight' => 'Weight',
            'korob_size' => 'Korob Size',
            'korob_weight' => 'Korob Weight',
            'korob_vol' => 'Korob Vol',
            'korob_count' => 'Korob Count',
            'technologies' => 'Technologies',
        ];
    }
}
