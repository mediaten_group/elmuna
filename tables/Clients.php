<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $sort
 * @property integer $upload_id
 *
 * @property Upload $upload
 * @property ClientsUploads[] $clientsUploads
 * @property Upload[] $uploads
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'description'], 'required'],
            [['description'], 'string'],
            [['status', 'sort', 'upload_id'], 'integer'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['upload_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upload::className(), 'targetAttribute' => ['upload_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Псевдоним',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'title' => 'Название',
            'description' => 'Описание',
            'status' => 'Опубликовано',
            'sort' => 'Sort',
            'upload_id' => 'Логотип компании',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpload()
    {
        return $this->hasOne(Upload::className(), ['id' => 'upload_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsUploads()
    {
        return $this->hasMany(ClientsUploads::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploads()
    {
        return $this->hasMany(Upload::className(), ['id' => 'upload_id'])->viaTable('clients_uploads', ['client_id' => 'id']);
    }
}
