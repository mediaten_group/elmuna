<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property string $alias
 * @property string $title
 * @property string $text
 * @property integer $status
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property boolean $is_printable
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['text'], 'string'],
            [['status'], 'integer'],
            [['is_printable'], 'boolean'],
            [['alias', 'title', 'meta_title'], 'string', 'max' => 50],
            [['meta_description', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Алиас',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'status' => 'Опубликовано',
            'meta_title' => 'Заголовок для вывода',
            'meta_description' => 'Описание',
            'meta_keywords' => 'Ключевые слова',
            'is_printable' => 'Is Printable',
        ];
    }
}
