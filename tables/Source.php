<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "source".
 *
 * @property string $alias
 * @property string $url
 * @property string $name
 * @property boolean $show
 *
 * @property Items[] $items
 */
class Source extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['show'], 'boolean'],
            [['alias', 'url', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Alias',
            'url' => 'Url',
            'name' => 'Name',
            'show' => 'Show',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['source' => 'alias']);
    }
}
