<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "category_link".
 *
 * @property integer $category_id
 * @property integer $group_id
 * @property integer $sort
 * @property integer $id
 *
 * @property Categories $category
 * @property Groups $group
 */
class CategoryLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'group_id', 'sort'], 'integer'],
            [['category_id', 'group_id'], 'unique', 'targetAttribute' => ['category_id', 'group_id'], 'message' => 'The combination of Category ID and Group ID has already been taken.'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'group_id' => 'Group ID',
            'sort' => 'Sort',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }
}
