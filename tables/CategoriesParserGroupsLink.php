<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories_parser__groups__link".
 *
 * @property integer $category_parser_id
 * @property integer $group_id
 *
 * @property CategoriesParser $categoryParser
 * @property Groups $group
 */
class CategoriesParserGroupsLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_parser__groups__link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_parser_id', 'group_id'], 'required'],
            [['category_parser_id', 'group_id'], 'integer'],
            [['category_parser_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesParser::className(), 'targetAttribute' => ['category_parser_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_parser_id' => 'Category Parser ID',
            'group_id' => 'Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryParser()
    {
        return $this->hasOne(CategoriesParser::className(), ['id' => 'category_parser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }
}
