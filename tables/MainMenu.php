<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "main_menu".
 *
 * @property string $alias
 * @property string $title
 * @property string $icon_class
 * @property integer $sort
 */
class MainMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['title'], 'string'],
            [['sort'], 'integer'],
            [['alias'], 'string', 'max' => 40],
            [['icon_class'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alias' => 'Alias',
            'title' => 'Title',
            'icon_class' => 'Icon Class',
            'sort' => 'Sort',
        ];
    }
}
