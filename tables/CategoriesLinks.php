<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories_links".
 *
 * @property integer $id
 * @property string $href
 * @property string $title
 * @property double $parser_ver
 * @property string $date
 * @property string $source
 */
class CategoriesLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['href', 'title'], 'string'],
            [['parser_ver'], 'number'],
            [['date'], 'safe'],
            [['source'], 'string', 'max' => 255],
            [['href'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'href' => 'Href',
            'title' => 'Title',
            'parser_ver' => 'Parser Ver',
            'date' => 'Date',
            'source' => 'Source',
        ];
    }
}
