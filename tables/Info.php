<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "info".
 *
 * @property string $key
 * @property string $value
 * @property string $type
 * @property string $comment
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'type'], 'required'],
            [['key', 'value', 'type', 'comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Название поля',
            'value' => 'Значение',
            'type' => 'Тип',
            'comment' => 'Комментарий',
        ];
    }
}
