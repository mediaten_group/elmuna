<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "colors_gifts".
 *
 * @property string $pantone
 * @property string $sol
 * @property string $name
 * @property string $source
 * @property integer $id
 * @property integer $item_id
 * @property integer $status
 */
class ColorsGifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colors_gifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'status'], 'integer'],
            [['pantone', 'sol', 'name', 'source'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pantone' => 'Pantone',
            'sol' => 'Sol',
            'name' => 'Name',
            'source' => 'Source',
            'id' => 'ID',
            'item_id' => 'Item ID',
            'status' => 'Status',
        ];
    }
}
