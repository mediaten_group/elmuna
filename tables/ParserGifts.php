<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "parser_gifts".
 *
 * @property integer $id
 * @property string $article
 * @property string $categories
 * @property string $brand
 * @property string $title
 * @property string $price
 * @property string $sizes
 * @property string $material
 * @property string $weight
 * @property string $korob_size
 * @property string $korob_weight
 * @property string $korob_vol
 * @property string $korob_count
 * @property string $technologies
 * @property string $images_min
 * @property string $images_big
 */
class ParserGifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_gifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['article'], 'required'],
            [['categories', 'technologies', 'images_min', 'images_big'], 'string'],
            [['article'], 'string', 'max' => 20],
            [['brand'], 'string', 'max' => 255],
            [['title', 'sizes', 'material'], 'string', 'max' => 50],
            [['price', 'korob_vol'], 'string', 'max' => 40],
            [['weight', 'korob_size', 'korob_weight', 'korob_count'], 'string', 'max' => 30],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'categories' => 'Categories',
            'brand' => 'Brand',
            'title' => 'Title',
            'price' => 'Price',
            'sizes' => 'Sizes',
            'material' => 'Material',
            'weight' => 'Weight',
            'korob_size' => 'Korob Size',
            'korob_weight' => 'Korob Weight',
            'korob_vol' => 'Korob Vol',
            'korob_count' => 'Korob Count',
            'technologies' => 'Technologies',
            'images_min' => 'Images Min',
            'images_big' => 'Images Big',
        ];
    }
}
