<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "cart_items".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $cart_id
 * @property integer $count
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $tech
 *
 * @property Cart $cart
 * @property Items $item
 */
class CartItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'cart_id', 'count'], 'required'],
            [['item_id', 'cart_id', 'count', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['tech'], 'string'],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'cart_id' => 'Cart ID',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'tech' => 'Tech',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'item_id']);
    }
}
