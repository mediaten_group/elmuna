<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "field_type".
 *
 * @property integer $id
 * @property string $type
 * @property string $description
 * @property string $updated_at
 * @property string $created_at
 * @property string $title
 *
 * @property Field[] $fields
 */
class FieldType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'created_at'], 'safe'],
            [['type', 'description', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'description' => 'Описание',
            'updated_at' => 'Обновлено',
            'created_at' => 'Создано',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['field_type_id' => 'id']);
    }
}
