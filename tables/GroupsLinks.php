<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "groups_links".
 *
 * @property integer $group_id
 * @property integer $cat_id
 * @property string $source
 * @property string $orig_cat_name
 * @property string $orig_cat_href
 * @property integer $id
 * @property double $version
 */
class GroupsLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'cat_id'], 'integer'],
            [['version'], 'number'],
            [['source', 'orig_cat_name', 'orig_cat_href'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'cat_id' => 'Cat ID',
            'source' => 'Source',
            'orig_cat_name' => 'Orig Cat Name',
            'orig_cat_href' => 'Orig Cat Href',
            'id' => 'ID',
            'version' => 'Version',
        ];
    }
}
