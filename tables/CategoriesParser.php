<?php

namespace app\tables;

use Yii;

/**
 * This is the model class for table "categories_parser".
 *
 * @property integer $id
 * @property string $source
 * @property string $title
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 * @property integer $parent_id
 *
 * @property CategoriesParser $parent
 * @property CategoriesParser[] $categoriesParsers
 * @property CategoriesParserGroupsLink[] $categoriesParserGroupsLinks
 * @property Groups[] $groups
 */
class CategoriesParser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_parser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['parent_id'], 'integer'],
            [['source', 'title', 'alias'], 'string', 'max' => 255],
            [['source', 'alias'], 'unique', 'targetAttribute' => ['source', 'alias'], 'message' => 'The combination of Source and Alias has already been taken.'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesParser::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => 'Source',
            'title' => 'Title',
            'alias' => 'Alias',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CategoriesParser::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesParsers()
    {
        return $this->hasMany(CategoriesParser::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesParserGroupsLinks()
    {
        return $this->hasMany(CategoriesParserGroupsLink::className(), ['category_parser_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['id' => 'group_id'])->viaTable('categories_parser__groups__link', ['category_parser_id' => 'id']);
    }
}
