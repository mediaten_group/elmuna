<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\Technology
 */

use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use app\modules\admin\widgets\tinymce\TinyMce;

//$this->registerJs($this->render('_form.js'));
$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'title');
echo $form->field($model, 'alias');
echo $form->field($model, 'intro')->textarea(['rows' => 3]);
echo $form->field($model, 'text')->widget(TinyMce::className(), [])->label('Текст');
echo $form->field($model, 'main_page')->checkbox();
echo $form->field($model, 'status')->checkbox()->label('Опубликовано');
echo $form->field($model, 'sort');
echo $form->field($model, 'meta_title');
echo $form->field($model, 'meta_keywords');
echo $form->field($model, 'meta_description');
echo $form->field($model, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', '100'],
    'cover' => 'cover_image_id',
    'async' => true
])->label('Лого');
?>
    <div class="form-group field-clients-works">
    <label for="name">Примеры работ</label>
<?= Upload::widget([
    'thumbs' => ['screen', 'small', '100'],
    'multiple' => true,
    'async' => true,
    'name' => 'works',
    'initialImages' => $images,
]) ?>
<?php
echo My::saveAndCloseBtn();

ActiveForm::end();
