<?php
/**
 * @var $this yii\web\View
 * @var $model
 * @var $images array
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = 'Добавить технологию нанесения';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model, 'images' => $images]) ?>
            </div>
        </div>
    </div>
</div>
