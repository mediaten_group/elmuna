<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <p>
                    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id' => [
                            'attribute' => 'id',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width: 100px'],
                            'value' => function ($data) {
                                return Html::a($data['id'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        'title' => [
                            'attribute' => 'title',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['title'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        /*[
                            'attribute' => 'main_page',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width: 100px'],
                            'value' => function ($model, $index, $widget) {
                                return Html::checkbox('main_page[]', $model['main_page'], ['value' => $index, 'disabled' => true]);
                            },
                        ],*/
                        [
                            'attribute' => 'sort',
                            'headerOptions' => ['style' => 'width: 150px']
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'headerOptions' => ['style' => 'width: 30px']
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
