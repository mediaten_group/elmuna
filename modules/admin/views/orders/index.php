<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\lib\My;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'alias' => [
                            'attribute' => 'name',
                            'label' => 'Имя',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['name'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['email'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => 'Телефон',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['phone'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'price',
                            'label' => 'Сумма',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['price'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime($data['created_at']), ['view', 'id' => $data['id']]);
                            }
                        ],
                        'status' => [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $status = '';
                                switch ($data['status']) {
                                    case \app\models\Order::NOT_READY:
                                        $status = 'Не подтвержден';
                                        break;
                                    case \app\models\Order::IN_WORK:
                                        $status = 'В работе';
                                        break;
                                    case \app\models\Order::IN_DELIVERY:
                                        $status = 'В доставке';
                                        break;
                                    case \app\models\Order::COMPLETED:
                                        $status = 'Выполнен';
                                        break;
                                    case \app\models\Order::CANCELED:
                                        $status = 'Отменен';
                                        break;
                                }
                                return $status;
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
