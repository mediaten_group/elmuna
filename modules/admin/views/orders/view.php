<?php
/**
 * @var $this yii\web\View
 * @var $order Order
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var array $status_list
 */
use app\lib\My;
use app\models\Order;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;

$this->params['h1'] = $this->title;

$js = <<<JS

$('a[role=info]').on('click',function(e) {
  e.preventDefault();
  $(this).parents('tr').next().toggle();
})

JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-lg-4">
        <div class="box">
            <div class="box-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>Заказчик</td>
                            <td><?php echo $order['user_id'] ? Html::a($order['name'], [
                                    '/admin/users/view', 'id' => $order['user_id']
                                ]) : $order['name'] ?></td>
                        </tr>
                        <tr>
                            <td>Адрес</td>
                            <td><strong><?= $order['address'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><strong><?= $order['email'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td><strong><?= $order['phone'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Дата</td>
                            <td><strong><?= My::mysql_russian_datetime($order['created_at']) ?></strong></td>
                        </tr>
                        <tr>
                            <td>Сумма заказа</td>
                            <td><strong><?= $order['price'] ?></strong> руб.</td>
                        </tr>
                        <tr>
                            <td>Комментарий</td>
                            <td><?= $order['comment'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                    <?php
                    $form = ActiveForm::begin();

                    echo $form->field($order, 'status')->dropDownList($status_list)->label('Статус');
                    echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

                    ActiveForm::end()
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'alias' => [
                            'attribute' => 'title',
                            'label' => 'Имя',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $html = $data['item_id'] ?
                                    Html::a($data['title'], ['/admin/items/update', 'id' => $data['item_id']]) :
                                    $data['title'];
                                $tech = Json::decode($data['tech']);
                                if ($tech)
                                    $html .= Html::a('+', '#', [
                                        'class' => 'btn btn-info btn-sm pull-right',
                                        'role' => 'info',
                                    ]);
                                return $html;
                            }
                        ],
                        [
                            'attribute' => 'price',
                            'label' => 'Цена за шт.',
                        ],
                        [
                            'attribute' => 'tech_price',
                            'label' => 'Цена за нанесение',
                        ],
                        [
                            'attribute' => 'count',
                            'label' => 'Тираж',
                        ],
                    ],
                    'afterRow' => function ($model, $key, $index) {
                        $html = '';
                        $item = $model;
                        $tech = Json::decode($item['tech']);
                        if ($tech) {
                            if (isset($tech['tech_place'])) {
                                $html .= Html::tag('h3', 'Место для нанесения');
                                $html .= Html::img("/uploads/images/thumbs100/$tech[tech_place]");
                            }
                            if (isset($tech['left'])) {
                                $html .= Html::tag('h3', 'Покрытие с одной стороны');
                                $html .= $tech['left']['text'];
                            }
                            if (isset($tech['right'])) {
                                $html .= Html::tag('h3', 'Покрытие с другой стороны');
                                $html .= $tech['right']['text'];
                            }
                        }

                        return Html::tag('tr',
                            Html::tag('td', $html, ['colspan' => 4]), ['style' => 'display: none']);
                    },
                ]) ?>
            </div>
        </div>
    </div>
</div>
