<?php
/**
 * @var $this yii\web\View
 * @var $order Order
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\lib\My;
use app\models\Order;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-4">
        <div class="box">
            <div class="box-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <?if ($call['user']):?>
                        <tr>
                            <td>Пользователь</td>
                            <td><?php echo Html::a($call['user']['username'], [
                                    '/admin/users/view', 'id' => $call['user']['id']
                                ]) ?></td>
                        </tr>
                        <?endif;?>
                        <tr>
                            <td>Имя</td>
                            <td><strong><?= $call['name'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td><strong><?= $call['phone'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Сообщение</td>
                            <td><strong><?= $call['text'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Дата</td>
                            <td><strong><?= My::mysql_russian_datetime($call['created_at']) ?></strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <?php
                    $form = ActiveForm::begin();

                    echo $form->field($call, 'status')->checkbox();
                    echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

                    ActiveForm::end()
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
