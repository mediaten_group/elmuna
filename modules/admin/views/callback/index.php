<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\lib\My;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Имя',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['name'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => 'Телефон',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['phone'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime($data['created_at']), ['view', 'id' => $data['id']]);
                            }
                        ],
                        'status' => [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data['status'] ? 'Обработан' : 'Не обработан';
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
