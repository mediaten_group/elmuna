<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\admin\models\Clients $model
 * @var \app\tables\ClientsUploads $images
 */

use app\modules\admin\lib\My;
use yii\bootstrap\ActiveForm;
use app\widgets\alias\GenerateAlias;
use app\modules\upload\widgets\Upload;
use app\modules\admin\widgets\tinymce\TinyMce;

?>

<div class="row">
    <div class="col-xs-12">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box">
            <div class="box-body">
                <?= $form->field($model, 'title') ?>
                <?= $form->field($model, 'alias')->widget(GenerateAlias::className(), [
                    'depends' => 'title'
                ]) ?>
                <?= $form->field($model, 'description')->textarea() ?>
                <?= $form->field($model, 'status')->checkbox() ?>
                <?= $form->field($model, '_upload[]')->widget(Upload::className(), [
                    'thumbs' => ['screen', 'small', '100'],
                    'cover' => 'upload_id',
                    'async' => true
                ])->label('Лого компании') ?>
                <div class="form-group field-clients-works">
                    <label for="name">Примеры работ</label>
                <?= Upload::widget([
                    'thumbs' => ['screen', 'small', '100'],
                    'multiple' => true,
                    'async' => true,
                    'name' => 'works',
                    'initialImages' => $images,
                ]) ?>
                </div>
            </div>
            <div class="box-footer with-border">
                <?= My::saveAndCloseBtn() ?>
            </div>
        </div>
        <?php $form->end() ?>
    </div>
</div>
