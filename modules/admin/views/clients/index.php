<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\bootstrap\Html;
use himiklab\sortablegrid\SortableGridView;
use yii\grid\ActionColumn;

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <?= Html::a('Добавить', ['/admin/clients/create'], ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="box-body">
                <?= SortableGridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'alias' => [
                            'attribute' => 'alias',
                            'label' => 'Изображение',
                            'content' => function ($data) {
                                /* @var $data \app\modules\admin\models\Clients */
                                if ($data->getUpload()->one()) {
                                    return Html::img('/uploads/images/thumbs100/' . $data->getUpload()->one()->name);
                                } else {
                                    return '';
                                }
                            }
                        ],
                        'title' => [
                            'attribute' => 'title',
                            'content' => function ($data) {
                                return Html::a($data['title'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        'description' => [
                            'attribute' => 'title',
                            'content' => function($data) {
                                return $data->description;
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{delete}'
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
