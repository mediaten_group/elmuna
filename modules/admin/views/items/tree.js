var links = <?=$links?>;
var $tree = $('#categories');
var id = $tree.attr('data-id');
$tree.jstree({
    'core' : {
        data : <?=$data?>,
        multiple : false,
        force_text: false
},
    'contextmenu':{
        'items': function($node) {
            var $tree = $("#categories"),
                tree = $tree.jstree(true),
                group_id = $tree.attr('data-id');
            return {
                "Create": {
                    "separator_before": false,
                    "separator_after": false,
                    "label": "Добавить",
                    "action": function (obj) {
                        if ($node.selected) return;

                        var ajaxData = {
                            group_id: group_id,
                            category_id: $node.id
                        };
                        $.ajax({
                            type: "POST",
                            url: "ajax?action=add",
                            data: ajaxData,
                            dataType: 'json',
                            success: function(msg){
                                if(msg.status !=='ok') return;
                                $node.selected = true;
                                var text = '<strong>'+$node.text+'</strong>';
                                tree.set_text( $node,text  );
                            }
                        });
                    }
                },
                "Remove": {
                    "separator_before": false,
                    "separator_after": false,
                    "label": "Удалить",
                    "action": function (obj) {
                        if (!$node.selected) return;
                        var ajaxData = {
                            group_id: group_id,
                            category_id: $node.id
                        };
                        $.ajax({
                            type: "POST",
                            url: "ajax?action=remove",
                            data: ajaxData,
                            dataType: 'json',
                            success: function(msg){
                                if(msg.status !=='ok') return;
                                var regex = /(<([^>]+)>)/ig;
                                var text = $node.text.replace(regex, "");
                                tree.set_text( $node, text  );
                                $node.selected = false;
                            }
                        });
                    }
                }
            };
        }
    },
    'plugins': ['types', 'contextmenu', 'wholerow', 'state']
}).on("loaded.jstree", function (e, data) {
    var tree = data.instance;
    // tree.open_all();
    links.forEach(function(el){
        var $node = tree.get_node(el);
        var text = '<strong>'+$node.text+'</strong>';
        $node.selected = true;
        tree.set_text( $node, text );
    });
});

