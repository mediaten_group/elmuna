var $title = $('input[name="Items[title]"]');
var $alias = $('input[name="Items[alias]"]');

$('.thumbnail .close').click(function () {
    var $block = $(this).parents('.block');
    var id = $block.data('id');
    var src = $block.data('src');

    $.ajax({
        url: 'delete-image?id=' + id + '&src=' + src,
        type: 'post',
        success: function (msg) {
            if (msg) {
                $block.remove();
                var icon = '<span class="glyphicon glyphicon-ok current" aria-hidden="true"></span>';
                $('[data-src="'+msg+'"] .thumbnail').append(icon);
            } else {
                $block.find('.close').remove();
            }
        }
    })
});

$('.thumbnail .define').click(function () {
    var $block = $(this).parents('.block');
    var id = $block.data('id');
    var src = $block.data('src');

    $.ajax({
        url: 'define-cover?id=' + id + '&src=' + src,
        type: 'post',
        success: function (msg) {
            if (msg == true) {
                $('.cover').removeClass('cover');
                $block.find('.thumbnail').addClass('cover');
            }
        }
    })
});

$("#refresh").click(function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: 'refresh-images?id=' + id,
        type: 'post',
        success: function (msg) {
            if (msg) {
                var missing = JSON.parse(msg);
                missing.forEach(function(val) {
                    $("[data-src = '" + val + "']").remove();
                });
            }
        }
    })
})

$title.blur(function () {
    //console.log('change');
    var alias = $alias.val();
    var title = $title.val();

    if (!title || alias) return false;

    $.ajax({
        url: 'get-alias',
        type: 'post',
        data: {title: title},
        success: function (json) {

            var obj = JSON.parse(json);
            var alias = obj.result;
            $alias.val(alias);
        }
    })
});
