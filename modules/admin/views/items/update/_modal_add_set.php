<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\AddSetForm
 * @var $item_id
 */

use app\modules\admin\lib\Lst;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->registerJs($this->render('_modal_add_set.js'));

Modal::begin([
    'id' => 'modal_add_set',
    'header' => '<h4 class="modal-title">Добавление набора</h4>',
]);

Pjax::begin(['id' => 'pjax_form_add_set', 'enablePushState' => false]);
$form = ActiveForm::begin([
    'action' => ['create-set', 'item_id' => $item_id],
    'options' => ['data-pjax' => 1],
]);
if (Yii::$app->session->getFlash('addAndClean')) {
    $this->registerJs("$('#modal_add_set').modal('hide');");
};
echo $form->field($model, 'color_id')->dropDownList(Lst::ctColors(), ['prompt' => 'Не выбранно']);
echo $form->field($model, 'price');
echo $form->field($model, 'count');
echo '<hr>';
echo '<div class="btn-toolbar">';
echo Html::submitButton('Добавить и очистить', ['name' => 'addAndClean', 'value' => 1, 'class' => 'btn btn-primary']);
echo Html::submitButton('Добавить', ['class' => 'btn btn-default']);
echo '</div>';
ActiveForm::end();
Pjax::end();

Modal::end();
