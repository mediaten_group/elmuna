<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\CtItems
 */
use app\tables\CtItemSets;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$id = Yii::$app->controller->actionParams['id'];
$dataProvider = new ActiveDataProvider([
    'query' => CtItemSets::find()->where(['item_id' => $id]),
    'pagination' => [
        'pageSize' => 5,
    ],
]);
?>
<label>Наборы товара</label>
<div class="form-group">
    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_set">Добавить набор</a>
</div>
<?php
Pjax::begin(['id' => 'pjax_grid_item_sets', 'enablePushState' => true]);
$this->registerJs($this->render('_grid_item_sets.js'));
?>
<?= GridView::widget([
    'id' => 'grid_item_sets',
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'size_id',
        'color_id',
        'price',
        'count',
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    $url = ['delete-set', 'id' => $model->id];
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'data-pjax' => 0,
                        'data-post-action' => 1,
                        'data-post-confirm' => 1,
                        'data-post-pjax-target' => 'pjax_grid_item_sets',
                    ]);
                }
            ],
        ],
    ],
]) ?>
<?php Pjax::end() ?>
