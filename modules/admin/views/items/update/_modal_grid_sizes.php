<?php
/**
 * @var $this yii\web\View
 * @var $item_id
 * @var $model
 */
use yii\bootstrap\Modal;

$this->registerJs($this->render('_modal_grid_sizes.js'));

Modal::begin([
    'id' => 'modal_grid_sizes',
    'header' => '<h4 class="modal-title">Таблица размеров</h4>',
]);

echo $this->render('modal_grid_sizes/_form.php', ['model' => $model, 'item_id' => $item_id]);
echo $this->render('modal_grid_sizes/_grid.php', ['item_id' => $item_id]);

Modal::end();
