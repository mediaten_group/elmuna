$('[data-post-action]').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    //if ($this.data('post-confirm') && !confirm('Вы уверены что хотите удалить запись')) return false;
    $.ajax({
        url: $this.attr('href'),
        type: 'post',
        success: function (msg) {
            var container = $this.data('post-pjax-target');
            if (container) {
                $.pjax.reload({container: '#' + container});
            }
        }
    });
});
