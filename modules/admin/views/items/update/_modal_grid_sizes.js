$('#pjax_modal_grid_sizes_form').on('pjax:end', function () {
    var error = $(this).find('.has-error').length;
    if (!error) {
        $.pjax.reload({container: "#pjax_modal_grid_sizes_grid"});
    }
});
