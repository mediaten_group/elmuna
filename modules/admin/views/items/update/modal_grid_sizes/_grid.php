<?php
/**
 * @var $this View
 * @var $item_id
 */
use app\tables\CtItemSizes;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

//pree(\app\models\CtItems::categoryPrices(2));
$query = CtItemSizes::find()
    ->where(['item_id' => $item_id]);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
]);

Pjax::begin(['id' => 'pjax_modal_grid_sizes_grid', 'enablePushState' => false]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'size',
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    $url = ['delete-size', 'item_id' => $model->item_id, 'size' => $model->size];
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'data-pjax' => 1,
//                        'data-post-action' => 1,
//                        'data-post-confirm' => 1,
//                        'data-post-pjax-target' => 'pjax_modal_grid_sizes_grid',
                    ]);
                }
            ],
        ],
    ],
]);
echo '<hr>';
echo '<div class="btn-toolbar">';
echo '<button class="btn btn-default pull-right" data-dismiss="modal">Закрыть</button>';
echo '</div>';
Pjax::end();
