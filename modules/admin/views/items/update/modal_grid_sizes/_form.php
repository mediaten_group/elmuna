<?php
/**
 * @var $this View
 * @var $model CtItemSizes
 * @var $item_id
 */
use app\tables\CtItemSizes;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

Pjax::begin(['id' => 'pjax_modal_grid_sizes_form', 'enablePushState' => false]);
$form = ActiveForm::begin([
    'action' => ['add-size', 'item_id' => $item_id],
    'options' => ['data-pjax' => '1'],
]);
echo $form->field($model, 'size');
echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
echo '<hr>';
ActiveForm::end();
Pjax::end();
