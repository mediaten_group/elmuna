<?php
/**
 * @var $this yii\web\View
 * @var $cover
 */
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;

$id = Yii::$app->controller->actionParams['id'];
$this->registerCss($this->render('_image_upload.css'));

echo Html::beginTag('div', ['class' => 'form-group']);
echo '<label class="control-label">Загрузка изображения</label>';
echo FileInput::widget([
    'name' => 'image',
    'pluginOptions' => [
        'language' => 'ru',
        'uploadUrl' => Url::to(['upload-image', 'item_id' => $id]),
    ],
    'options' => [
        'accept' => 'image/*',
        'multiple' => true,
    ]
]);
echo Html::endTag('div');
echo '<a class="btn btn-default fa fa-refresh" href="#" id="refresh" data-id="'.$id.'" style="float: left;"></a>';
$webRoot = Yii::getAlias('@webroot');
$web = Yii::getAlias('@web');
$thumbsPath = '/uploads/items/' . $id . '/thumbs/400';
if (is_dir($webRoot . $thumbsPath)) {
    $images = array_diff(scandir($webRoot . $thumbsPath), ['..', '.']);
    $preview = '<div class="form-group thumbnails"><div class="row">';
    foreach ($images as $image) {
        $coverCss = ($image == $cover) ? ' cover' : '';

        $preview .= '<div class="block col-lg-1 col-md-2 col-xs-3" ' .
            'data-id="' . $id . '" data-src="' . $image . '">';
        $preview .= '<div class="thumbnail' . $coverCss . '">';
//        $preview .= '<div class="set-cover"><span>Назначить обложкой</span></div>';
        if(count($images) > 1) $preview .= '<span class="glyphicon glyphicon-remove close" aria-hidden="true"></span>';

        $preview .= '<span class="glyphicon glyphicon-ok current" aria-hidden="true"></span>';

        $preview .= '<span class="glyphicon glyphicon-eye-open define" aria-hidden="true"></span>';

        $preview .= Html::img($web . $thumbsPath . '/' . $image, ['class' => 'img-responsive']);
        $preview .= '</div>';
        $preview .= '</div>';

        $coverCss = '';
    }
    echo $preview . '</div></div>';
}
