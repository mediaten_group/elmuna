<?php
/**
 * @var $this yii\web\View
 * @var \app\modules\admin\models\Groups $group
 */

use yii\bootstrap\Html;

echo Html::beginTag('div',['class'=>'btn-group']);
echo Html::a('Места нанесения',
    ['places','id'=>$group->id],
    ['class'=>'btn btn-default']);
echo Html::a('Печать с одной стороны',
    ['/admin/group-scheme/group','id'=>$group->id,'side'=>'one'],
    ['class'=>'btn btn-default']);
echo Html::a('Печать с другой стороны',
    ['/admin/group-scheme/group','id'=>$group->id,'side'=>'two'],
    ['class'=>'btn btn-default']);
echo Html::endTag('div');