<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\Items
 * @var $catList
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $model->title;
$this->params['after-h1'] = '<i>' . 'id ' . $model->id . '</i>';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
</div>
