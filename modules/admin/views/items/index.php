<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var \app\tables\Categories $category
 * @var $searchModel ItemsSearch
 * @var int $category_id
 * @var string $q
 */
use app\modules\admin\lib\Lst;
use app\modules\admin\models\ItemsSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;

$this->params['h1'] = $this->title;
if ($category) $this->title .= ' в категории ' . $category->title;

$js = <<<JS
    $('.price-change').on('change', function (e) {
        var id = $(this).attr('data-id');
        var value = $(this).val();
        var field = $(this);
        $.ajax({
            url: '/admin/items/update-attr',
            data: {id: id, value: value, attr: 'set_price'},
            success: function (msg) {
                if (msg.status == 'ok') {
                    field.parent().parent().addClass('has-success');
                    field.parent().parent().removeClass('has-error');
                } else {
                    field.parent().parent().addClass('has-error');
                    field.parent().parent().removeClass('has-success');
                    console.log(msg.data);
                }
            }
        });
    });

    $('.status-change').on('change', function (e) {
        var id = $(this).attr('data-id');
        var value = $(this).prop('checked');
        var field = $(this);
        $.ajax({
            url: '/admin/items/update-attr',
            data: {id: id, value: value, attr: 'status'},
            success: function (msg) {
                if (msg.status == 'ok') {
                    field.parent().parent().addClass('has-success');
                    field.parent().parent().removeClass('has-error');
                } else {
                    field.parent().parent().addClass('has-error');
                    field.parent().parent().removeClass('has-success');
                    console.log(msg.data);
                }
            }
        });
    })
JS;

$this->registerJs($js);

$url = Yii::$app->request->getUrl();
?>
<script>
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-6">
                        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <div class="col-xs-6 text-align-right">
                        <form class="form-inline">
                            <?php if ($category_id) : ?>
                                <input type="hidden" name="category_id" value="<?= $category_id ?>">
                            <?php endif; ?>
                            <input type="text" name="q" placeholder="Название группы" class="form-control"
                                   value="<?= $q ?>">
                            <button type="submit" class="btn btn-flat btn-info">Поиск</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'format' => 'raw',
                            'attribute' => 'id',
                            'value' => function ($data) {
                                return Html::a($data['id'], ['group', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'format' => 'raw',
                            'attribute' => 'title',
                            'value' => function ($data) {
                                return Html::a($data['title'], ['group', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'label' => 'Изображение',
                            'content' => function ($data) {
                                return Html::img("/uploads/images/thumbs100/$data[image]");
                            }
                        ],
                        [
                            'label' => 'Цена',
                            'content' => function ($data) {
                                $items = Json::decode($data['items_data']);
                                $html = '';
                                foreach ($items as $item) {
                                    $html .= Html::tag('div',
                                        Html::label($item['title']) . ' ' .
                                        Html::input('text', null, $item['set_price'],
                                            ['class' => 'form-control price-change', 'data-id' => $item['id']]),
                                        ['class' => 'form-inline form-group']);
                                }
                                return $html;
                            },
                        ],
                        [
                            'label' => 'Статус',
                            'content' => function ($data) {
                                $items = Json::decode($data['items_data']);
                                $html = '';
                                foreach ($items as $item) {
                                    $html .= Html::tag('div',
                                        Html::label($item['title'] .
                                            Html::checkbox(null, $item['status'],
                                                ['data-id' => $item['id'], 'class' => 'status-change'])),
                                        ['class' => 'checkbox']);
                                }
                                return $html;
                            }
                        ],
                        [
                            'label' => 'Количество',
                            'value' => function ($data) {
                                $items = Json::decode($data['items_data']);
                                return count($items);
                            }
                        ],
                        [
                            'attribute' => 'sort',
                            'label' => 'Порядок',
                            'content' => function ($data) {
                                $sort = $data['sort'];
                                $html = "[$sort] ";
                                $base_href = "/admin/items/move?id=$data[id]";
                                if ($sort > 1) {
                                    $html .= "<a class='fa fa-fast-backward' href='$base_href&action=fb'></a> ";
                                }
                                if ($sort > 0) {
                                    $html .= "<a class='fa fa-chevron-left' href='$base_href&action=b'></a> ";
                                }
                                $html .= "<a class='fa fa-chevron-right' href='$base_href&action=f'></a> ";
                                if ($sort < $data['sort_max']) {
                                    $html .= "<a class='fa fa-fast-forward' href='$base_href&action=ff'></a> ";
                                }
                                return $html;
                            },
                            'enableSorting' => false
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
