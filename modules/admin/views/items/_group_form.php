<?php
/**
 * @var $this yii\web\View
 * @var \app\modules\admin\models\Groups $group
 */

use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]);

echo $form->field($group, 'title')->label('Заголовок для группы товаров');

echo $form->field($group, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', '100'],
    'cover' => 'cover_image_id',
]);

echo $form->field($group, 'status')->checkbox();

$deletePath = ['delete-group', 'id' => $group->id];
echo My::buttons($deletePath);

ActiveForm::end();