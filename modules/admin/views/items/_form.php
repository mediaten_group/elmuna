<?php
/**
 * @var $this yii\web\View
 * @var Groups $group
 * @var Items $item
 * @var array $colors
 * @var \app\tables\ItemUpload $images
 * @var array $sources
 */

use app\modules\admin\lib\My;
use app\modules\admin\models\Groups;
use app\modules\admin\models\Items;
use app\modules\upload\widgets\Upload;
use app\widgets\alias\GenerateAlias;
use kartik\base\InputWidget;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]);

$closePath = $group->count['value'] === 1 ? ['index'] : ['group', 'id' => $group->id];
$deletePath = $group->count['value'] === 1 ? ['delete-group', 'id' => $group->id] : ['delete-item', 'id' => $item->id];
echo My::buttons($deletePath, $closePath);
?>

    <h4><a data-toggle="collapse" href="#main">Главные параметры</a></h4>
    <div id='main' class="collapse in">
        <?= $form->field($item, 'title')->label('Название'); ?>
        <?= $form->field($item, 'alias')->widget(GenerateAlias::className(), [
            'depends' => 'title',
        ]); ?>
        <?= $form->field($item, 'set_price')->label('Цена'); ?>
        <?= $form->field($item, 'article')->label('Артикул'); ?>
        <?= $form->field($item, 'status')->checkbox(); ?>
        <?= $form->field($item, 'is_bestseller')->checkbox(); ?>
        <?= $form->field($item, 'text')->textarea()->label('Описание'); ?>
        <?= $form->field($item, 'sizes')->label('Размеры'); ?>
        <?= $form->field($item, 'material')->label('Материал'); ?>
        <?= $form->field($item, 'weight')->label('Вес'); ?>
        <?= $form->field($item, 'source')->widget(Select2::className(),[
            'data'=>[null=>'Нет']+$sources,
        ])->label('Каталог'); ?>
    </div>
    <hr>
<?= $form->field($item, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', '100'],
    'cover' => 'cover_image_id',
    'initialImages' => $images,
    'async' => true,
    'multiple' => true,
])->label('Изображения'); ?>
    <hr>
    <h4><a data-toggle="collapse" href="#sub">Дополнительные параметры</a></h4>
    <div id="sub" class="collapse">
        <div class="sub-attr">
            <?php if (isset($item['params']['attr'])) foreach ($item['params']['attr'] as $key => $value) {
                if ($value) {
                    echo $form->field($item, "params[attr][$key]")->textarea()->label($key);
                }
            } ?>
        </div>
        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-info btn-flat " data-toggle="collapse" href="#new-form">+</a>
            </div>
            <div class="col-md-11 collapse" id="new-form">
                <div class="form-group form-inline">
                    <label for="new-sub-name">Название</label>
                    <input id="new-sub-name" class="text form-control">
                </div>
                <div class="form-group form-inline">
                    <label for="new-sub-value">Значение</label>
                    <input id="new-sub-value" class="text form-control">
                </div>
                <a id="new-sub" class="btn btn-flat btn-success">Добавить</a>
            </div>
        </div>
    </div>
    <!--    <hr>-->
<?php
$format = <<<JS
function format(state) {
    if (!state.id) return state.text; // optgroup
    return '<span class="color" style="background: ' + state.id.toLowerCase() + '"></span>' + state.text;
}
JS;
$this->registerJs($format, View::POS_HEAD);
$escape = new JsExpression("function(m) { return m; }");
?>
<?php $form->field($item, 'color')->widget(Select2::className(), [
    'language' => 'ru',
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => $colors,
    'options' => ['placeholder' => 'Выберите цвет'],
    'pluginOptions' => [
        'templateResult' => new JsExpression('format'),
        'templateSelection' => new JsExpression('format'),
        'escapeMarkup' => $escape,
        'allowClear' => true
    ],
])->label('Главный цвет'); ?>

    <hr>
    <h4><a data-toggle="collapse" href="#meta">Мета настройки</a></h4>
    <div id="meta" class="collapse">
        <?= $form->field($item, 'meta_title')->label('Заголовок для отображения'); ?>
        <?= $form->field($item, 'meta_keywords')->label('Ключевые слова'); ?>
        <?= $form->field($item, 'meta_description')->textarea()->label('Описание'); ?>
    </div>
    <hr>
<?php
echo My::buttons($deletePath, $closePath);

ActiveForm::end();

if (!$item->isNewRecord) {
    $id = $item->id;
    $coverUrl = Url::to(['/admin/items/define-cover', 'id' => $id]);
    $colorUrl = Url::to(['/admin/items/get-color', 'id' => $id]);
    $form = $form->id;

    $js = <<<JS
var id = $id;
$('.thumbs').on('click','.define',function (e) {
    e.preventDefault();
    var elem = $(this),
        thumb = elem.parents('.thumb'),
        data = {
            image_id: thumb.attr('data-id')
        };
    thumb.addClass('defined');

    $.ajax({
        type: "POST",
        url: '$coverUrl',
        data: data,
        dataType: 'json',
        success: function(msg){
            if(msg.status == 'ok'){
                $('.cover').removeClass('cover');
                thumb.addClass('cover');
            }
            thumb.removeClass('defined');
        }
    });
});
$('#get-color').click(function (e) {
    e.preventDefault();
    var elem = $(this);
    if(elem.hasClass('disabled')) return;

    elem.addClass('disabled');
    $.ajax({
        type: "POST",
        url: '$colorUrl',
        dataType: 'json',
        success: function(msg){
            if(msg.status == 'ok'){
                console.dir(msg.color);
            }
            elem.removeClass('disabled');
            $('#items-color').val(msg.color);
            $('.color-fill').css('background',msg.color);
        }
    });
});

$('#new-sub').on('click',function(e) {
    var key = $("#new-sub-name").val();
    var value = $("#new-sub-value").val();
    if (key && value) {
        var html = '<div class="form-group has-success">\
<label class="control-label">Гарантия производителя</label>\
<textarea class="form-control" name="Items[params][attr]['+key+']">'+value+'</textarea>\
<div class="help-block"></div>\
</div>'; 
        $("#new-sub-name").val('');
        $("#new-sub-value").val('');
        $('.sub-attr').append(html);
    } else {
        alert('Заполните название и значение нового атрибута');
    }
    
    console.log({key:key,value:value});
});

form = document.getElementById('$form');
form.onsubmit = function(e){
    if(window.uploading === 0) return false;
}
JS;

    $this->registerJs($js);
}
