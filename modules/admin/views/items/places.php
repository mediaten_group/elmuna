<?php
/**
 * @var \yii\web\View $this
 * @var int $id
 * @var array $images
 * @var \app\modules\admin\models\TechPlaces $model
 * @var array $all_images
 */

use yii\bootstrap\ActiveForm;
use app\modules\upload\widgets\Upload;
use app\modules\admin\lib\My;
use kartik\select2\Select2;
use yii\bootstrap\Html;

$this->params['h1'] = $this->title;

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]);

$css = <<<CSS
.thumb img {
    width:80px;
}
CSS;

$this->registerCss($css);

$js = <<<JS

$("#copy-place").on('change',function() {
  $('#copy-button').attr('href','copy-places?to=$id&from='+$(this).val());
})

JS;

$this->registerJs($js);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">

                <?php
                echo $form->field($model, '_upload[]')->widget(Upload::className(), [
                    'thumbs' => ['small', '100'],
                    'cover' => 'upload_id',
                    'initialImages' => $images,
                    'async' => true,
                    'multiple' => true,
                ]);

                echo '<div class="form-group">';
                echo Html::label('Копирование мест нанесения');
                echo Select2::widget([
                    'id' => 'copy-place',
                    'name' => 'copy',
                    'data' => $all_images,
                    'options' => [
                        'placeholder' => 'Выберите группу товаров',
                    ],
                    'addon' => [
                        'append' => [
                            'content' => '<a id="copy-button" class="btn btn-default">Скопировать</a>',
                            'asButton' => true,
                        ],
                    ]
                ]);
                echo '</div>';

                echo My::saveAndCloseBtn(['group','id'=>$id]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php

ActiveForm::end();
