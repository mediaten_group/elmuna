<?php
/**
 * @var $this yii\web\View
 * @var \app\modules\admin\models\Items $item
 * @var \app\modules\admin\models\Groups $group
 * @var \app\tables\ItemUpload $images
 * @var \app\modules\admin\models\SchemeGroup $schemeGroup
 * @var array $sources
 */
use app\modules\admin\lib\My;
use app\modules\upload\lib\Files;
use app\assets\JsTreeAsset;
use yii\helpers\Json;
use yii\helpers\Url;

$images = isset($images) ? $images : [];

$this->params['h1'] = 'Добавление товара';

if (!isset($items)) $items = null;
if (!isset($categories)) $categories = null;

if (!$items && $categories) {
    JsTreeAsset::register($this);

    $categories = Json::encode($categories);
    $links = Json::encode($links);
    $this->registerJs($this->render('tree.js', ['data' => $categories, 'links' => $links]));
}

?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['item' => $item, 'group' => $group, 'images' => $images, 'colors' => $colors, 'sources' => $sources]) ?>
            </div>
        </div>
        <?php if ($item->id) { ?>
            <div class="box">
                <div class="box-body">
                    <?= $this->render('_scheme_group_select', ['group' => $group]) ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if (!$group->isNewRecord): ?>
        <div class="col-lg-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Товары</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <?php foreach ($items as $i): ?>
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= My::thumb(Files::getPath($i->cover_image_id, '100')) ?>"
                                         alt="<?= $i->title ?>">
                                </div>
                                <div class="product-info">
                                    <a href="<?= Url::to(['update', 'id' => $i->id]) ?>"
                                       class="product-title"><?= $i->title ?>
                                        <span class="label label-warning pull-right"
                                              style="background:<?= $i->color ?> !important"><?= $i->price ?></span></a>
                                    <span class="product-description">
                          <?= $i->intro ?>
                        </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php endforeach ?>
                <div class="box-footer text-center">
                    <a href="<?= Url::to(['create', 'group_id' => $group->id]) ?>" class="uppercase">Добавить товар</a>
                </div>
            </div>
            <?php if (!$items && $categories): ?>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Категории</h3>
                    </div>
                    <div class="box-body">
                        <div id="categories" data-id="<?= $group->id ?>"></div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    <?php endif ?>
</div>
