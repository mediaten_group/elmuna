<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use app\widgets\alias\GenerateAlias;
use dosamigos\tinymce\TinyMce;
use kartik\color\ColorInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'title');

echo $form->field($model, 'url');

echo $form->field($model, 'text');

echo $form->field($model, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', 'news', '100'],
    'cover' => 'image',
])->label('Изображение');

echo $form->field($model,'status')->checkbox();

echo My::buttons(['delete', 'id' => $model->id], ['index'], $model->isNewRecord);

ActiveForm::end();
