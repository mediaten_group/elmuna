<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use himiklab\sortablegrid\SortableGridView;
use yii\bootstrap\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <p>
                    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= SortableGridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id' => [
                            'attribute' => 'Название',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['id'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        'title' => [
                            'attribute' => 'Заголовок',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['title'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        'url' => [
                            'attribute' => 'Ссылка',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['url'], $data['url']);
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
