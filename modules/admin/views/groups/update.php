<?php
/**
 * @var $this yii\web\View
 * @var \app\modules\admin\models\Groups $model
 * @var \app\modules\admin\models\Tree $categories
 */
use app\assets\JsTreeAsset;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;

$this->params['h1'] = 'Редактировать технологию нанесения';

JsTreeAsset::register($this);

$data = Json::encode($categories);
$links = Json::encode($links);

$this->registerJs($this->render('tree.js', ['data' => $data, 'links' => $links]));
//pree($data);
?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <div id="categories" data-id="<?= $model->id ?>"></div>
            </div>
        </div>
    </div>
</div>
