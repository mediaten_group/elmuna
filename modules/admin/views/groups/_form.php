<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\Technology
 */

use app\modules\admin\lib\Lst;
use app\modules\admin\lib\My;
use app\modules\admin\models\AddSetForm;
use app\tables\CtItemSizes;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

//$this->registerJs($this->render('_form.js'));
$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'title');
//echo $form->field($model, 'alias');
//echo $form->field($model, 'text')->textarea(['rows' => 6]);
echo $form->field($model, 'status')->checkbox();
//echo $form->field($model, 'imageFile')->fileInput();
echo My::saveAndCloseBtn();


ActiveForm::end();
