<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var int $id
 * @var \yii\web\View $this
 * @var int $group_id
 */
use yii\helpers\Html;
use yii\grid\GridView;

?>
<div class="box-header with-border">
    <h3 class="box-title">Схемы</h3>
</div>
<div class="box-body">
    <div>
        <div class="form-inline">
            <?= Html::a('Создать', ['/admin/scheme/create', 'pid' => $group_id], ['class' => 'btn btn-primary']) ?>
            <div class="form-group">
                <label class="sr-only" for="copyById">Скопировать по ID</label>
                <div class="input-group">
                    <div class="input-group-addon">ID</div>
                    <input type="text" class="form-control" id="copyById" placeholder="ID для копирования">
                    <?= Html::a('Скопировать', '#', ['class' => 'btn btn-success copy input-group-addon']) ?>
                </div>
            </div>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => 'ID',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['id'], ['/admin/scheme/update', 'id' => $data['id']]);
                }
            ],
            [
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['name'], ['/admin/scheme/update', 'id' => $data['id']]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{copy} {delete}',
                'headerOptions' => ['style' => 'width: 50px'],
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $id = $model['id'];
                        return <<<HTML
<a href="/admin/scheme/delete?id=$id" title="Удалить" 
  aria-label="Удалить" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post" data-pjax="0">
    <span class="glyphicon glyphicon-trash"></span>
</a>
HTML;
                    },
                    'copy' => function ($url, $model) {
                        $id = $model['id'];
                        return <<<HTML
<a href="/admin/scheme/copy?id=$id" title="Копировать" aria-label="Копировать" data-method="post" data-pjax="0">
    <span class="glyphicon glyphicon-paste"></span>
</a>
HTML;
                    },
                ]
            ],
        ],
    ]) ?>
</div>