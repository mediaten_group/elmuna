<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->params['h1'] = 'Группы схем';

use yii\helpers\Html;
use yii\grid\GridView;

?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => 'ID',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['id'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Название',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['name'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'headerOptions' => ['style' => 'width: 30px']
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

