<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\SchemeGroup
 * @var $group_id int
 */

use app\modules\admin\lib\My;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

//$this->registerJs($this->render('_form.js'));
$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'name')->label('Название');

echo $form->field($model, 'status')->checkbox();

echo My::saveAndCloseBtn();

ActiveForm::end();
