<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\admin\models\Groups $model
 * @var array $list
 * @var int $id
 * @var string $side
 */

$this->params['h1'] = 'Группы схем';

use kartik\select2\Select2;
use yii\bootstrap\Html;

$create = Html::a('Создать', ['create','id'=>$id,'side'=>$side], ['id' => 'button-create', 'class' => 'btn btn-success']);
$copy = Html::a('Скопировать', '', ['id' => 'button-copy', 'class' => 'btn btn-default']);
$use = Html::a('Использовать', '', ['id' => 'button-use', 'class' => 'btn btn-primary']);

$js = <<<JS

function changeButton(val) {
      if (val) {
        $('.input-group-btn').empty();
        $('.input-group-btn').append('$use');
        $('#button-use').attr('href','use?id='+val+'&side=$side&group_id=$id');
        $('.input-group-btn').append('$copy');
        $('#button-copy').attr('href','copy?id='+val+'&side=$side&group_id=$id');
      } else {
        $('.input-group-btn').empty();
        $('.input-group-btn').append('$create');      
      }  
}
 
 changeButton($model->id)

    $("#schemegroup-id").on('change',function() {
      changeButton($(this).val())
    })
    
JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?php
                echo Select2::widget([
                    'model' => $model,
                    'attribute' => 'id',
                    'language' => 'ru',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'data' => $list,
                    'options' => ['placeholder' => 'Выберите группу схем'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'addon' => [
                        'append' => [
                            'content' => '',
                            'asButton' => true,
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

