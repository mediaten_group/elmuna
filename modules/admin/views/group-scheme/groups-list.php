<?php
/**
 * @var \app\modules\admin\models\Groups $groups
 */

use yii\grid\GridView;
use yii\bootstrap\Html;

echo '<div class="box-header with-border">';
echo Html::tag('h3','Используется в товарах',['class'=>'box-title']);
echo '</div>';

echo '<div class="box-body">';
echo GridView::widget([
    'dataProvider' => $groups,
    'columns' => [
        [
            'attribute' => 'id',
            'label' => 'ID',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data['id'], ['/admin/items/group', 'id' => $data['id']]);
            }
        ],
        [
            'attribute' => 'title',
            'label' => 'Название',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data['title'], ['/admin/items/group', 'id' => $data['id']]);
            }
        ],
    ],
]);
echo '</div>';