<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\SchemeGroup
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $groups \app\modules\admin\models\Groups
 * @var $item_group_id int
 */

$this->params['h1'] = 'Изменить группу схем';

$js = <<<JS

updateUrl = function() {
  var url_id = $('#copyById').val();
  
  $('.copy').attr('href','/admin/scheme/copy?id='+url_id);
}

$('#copyById').change(updateUrl).keyup(updateUrl);

JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box">
            <?= $this->render('scheme-list', ['group_id' => $model->id, 'dataProvider' => $dataProvider]) ?>
        </div>
        <div class="box">
            <?= $this->render('groups-list', ['groups' => $groups]) ?>
        </div>
    </div>
</div>
