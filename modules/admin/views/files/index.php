<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 09.08.2016
 * Time: 14:52
 */

use yii\web\JsExpression;
use mihaildev\elfinder\ElFinder;

?>

<div class="box">
    <div class="box-body">
        <?= ElFinder::widget([
            'language' => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            //    'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            //    'callbackFunction' => new JsExpression('function(file, id){}'), // id - id виджета
            'frameOptions' => [
                'style' => 'width: 100%; height: 600px; border: 0'
            ]
        ]); ?>
    </div>
</div>



