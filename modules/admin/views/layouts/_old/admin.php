<?php
/**
 * @var $this yii\web\View
 * @var $content string
 */
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\lib\My;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

AdminAsset::register($this);
$this->registerJs($this->render('admin.js'));
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Url::to(['/admin']) ?>"><span>Панель управления</span></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li>
                <?= Html::a('SeZon <span class="fa fa-external-link"></span>', '/', [
                    'target' => '_blank',
                ]) ?>
            </li>
            <li>
                <?= Html::a('Gii', ['/gii/default/index']) ?>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <?= My::identify()->username ?> <i class="fa fa-caret-down"></i>
                </a>

                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <?= Html::a('Обновить модели', ['/admin/default/update-tables'], [
                            'data' => ['method' => 'post']
                        ]) ?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-sign-out fa-fw"></i> Выход', ['/site/logout'],
                            ['data-method' => 'post']) ?>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse collapse">
                <?= $this->render('_left-nav') ?>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper" style="min-height: 567px;">
        <?php if (isset($this->params['h1'])): ?>
            <div class="row">
                <div class="col-lg-12 page-header">
                    <?= isset($this->params['before-h1']) ? $this->params['before-h1'] : null ?>
                    <h1><?= $this->params['h1'] ?></h1>
                    <?= isset($this->params['after-h1']) ? $this->params['after-h1'] : null ?>
                </div>
            </div>
        <?php endif ?>
        <?= $content ?>
    </div>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
