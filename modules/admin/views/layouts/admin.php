<?php
/**
 * @var $this yii\web\View
 * @var $content string
 */

use app\modules\admin\assets\AdminAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
$this->registerJs($this->render('admin.js'));
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-red">
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <!-- Logo -->
        <?= Html::a('<span class="logo-mini"><b>A</b>PANEL</span><span class="logo-lg"><b>Admin</b>PANEL</span>',
            ['/admin/default/index'], ['class' => 'logo']) ?>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <?= $this->render('_admin-nav-top') ?>
        </nav>
    </header>
    <div class="main-sidebar">
        <!-- Inner sidebar -->
        <div class="sidebar">
            <?= $this->render('_admin-nav-left') ?>
        </div>
        <!-- /.sidebar -->
        <?php if(isset($this->params['sidebar'])) echo $this->params['sidebar'] ?>
    </div>
    <div class="content-wrapper">
        <div class="content-header">
            <h1>
                <?= (isset($this->params['header']) ? $this->params['header'] : $this->title) ?>
            </h1>

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => ['label' => 'Панель управления', 'url' => ['/admin/default/index']],
            ]) ?>

        </div>

        <div class="content body">
            <?= $content ?>
        </div>
    </div>


</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
