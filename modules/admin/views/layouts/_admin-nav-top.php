<?php
/**
 * @var $this yii\web\View
 */
use yii\helpers\Html;

?>
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <li>
            <?= Html::a('<i class="fa fa-external-link"></i> На сайт', ['/'],['target' => '_blank']) ?>
        </li>
        <li>
            <?= Html::a('Gii', ['/gii/default/index']) ?>
        </li>
        <li>
            <?= Html::a('Выход', ['/logout'],[
                'data' => ['method' => 'post']
            ]) ?>
        </li>
    </ul>
</div>
