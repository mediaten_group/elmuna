<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use app\widgets\Nav;

$_c = Yii::$app->controller->id;

echo Nav::widget([
    'items' => [
        [
            'label' => '<span>Главная</span>',
            'url' => ['/admin/default/index'],
            'visible'=> My::can('index')
        ],
        [
            'label' => '<i class="fa fa-info"></i><span>Инфо</span>', 'url' => ['/admin/info/index'],
            'active' => $_c == 'info',
            'visible'=> My::can('info')
        ],
        [
            'label' => '<i class="fa fa-list-ol"></i><span>Категории</span>', 'url' => ['/admin/categories/index'],
            'active' => $_c == 'categories',
            'visible'=> My::can('categories')
        ],
        [
            'label' => '<i class="fa fa-gift"></i><span>Товары</span>', 'url' => ['/admin/items/index'],
            'active' => $_c == 'items',
            'visible'=> My::can('items')
        ],
        [
            'label' => '<i class="fa fa-circle"></i><span>Цвета</span>', 'url' => ['/admin/colors/index'],
            'active' => $_c == 'colors',
            'visible'=> My::can('colors')
        ],
        [
            'label' => '<i class="fa fa-newspaper-o"></i><span>Новости</span>', 'url' => ['/admin/news/index'],
            'active' => $_c == 'news',
            'visible'=> My::can('news')
        ],
        [
            'label' => '<i class="fa fa-thumbs-up"></i><span>Спецпредложения</span>', 'url' => ['/admin/special/index'],
            'active' => $_c == 'special',
            'visible'=> My::can('spec')
        ],
        [
            'label' => '<i class="fa fa-file-text-o"></i><span>Страницы</span>', 'url' => ['/admin/pages/index'],
            'active' => $_c == 'pages',
            'visible'=> My::can('pages')
        ],
        [
            'label' => '<i class="fa fa-exchange"></i><span>Наши клиенты</span>', 'url' => ['/admin/clients/index'],
            'active' => $_c == 'clients',
            'visible'=> My::can('clients')
        ],
        [
            'label' => '<i class="fa fa-shopping-cart"></i><span>Заказы</span>', 'url' => ['/admin/orders/index'],
            'active' => $_c == 'orders',
            'visible'=> My::can('orders')
        ],
        [
            'label' => '<i class="fa fa-user"></i><span>Пользователи</span>', 'url' => ['/admin/users/index'],
            'active' => $_c == 'users',
            'visible'=> My::can('users')
        ],
//        [
//            'label' => '<i class="fa fa-user"></i><span>Рассылка</span>', 'url' => ['/admin/distribution/index'],
//            'active' => $_c == 'distribution',
//        ],
        [
            'label' => '<i class="fa fa-phone"></i><span>Звонки</span>', 'url' => ['/admin/callback/index'],
            'active' => $_c == 'callback',
            'visible'=> My::can('calls')
        ],
        [
            'label' => '<i class="fa fa-comments-o"></i><span>Отзывы</span>', 'url' => ['/admin/reviews/index'],
            'active' => $_c == 'reviews',
            'visible'=> My::can('reviews')
        ],
        [
            'label' => '<i class="fa fa-files-o"></i><span>Файловый менеджер</span>', 'url' => ['/admin/files'],
            'active' => $_c == 'files',
            'visible'=> My::can('files')
        ],
        [
            'label' => '<i class="fa fa-pencil-square-o"></i><span>Нанесение</span>',
            'active' => ($_c == 'technology') ||
                ($_c == 'group-scheme') ||
                ($_c == 'scheme') ||
                ($_c == 'field') ||
                ($_c == 'field-colors'),
            'items' => [
                [
                    'label' => '<i class="fa"></i><span>Технологии нанесения</span>', 'url' => ['/admin/technology/index'],
                    'active' => $_c == 'technology',
                ],
                [
                    'label' => '<i class="fa"></i><span>Группы схем</span>', 'url' => ['/admin/group-scheme/index'],
                    'active' => $_c == 'group-scheme',
                ],
                [
                    'label' => '<i class="fa"></i><span>Схемы</span>', 'url' => ['/admin/scheme/index'],
                    'active' => $_c == 'scheme',
                ],
                [
                    'label' => '<i class="fa"></i><span>Поля</span>', 'url' => ['/admin/field/index'],
                    'active' => $_c == 'field',
                ],
                [
                    'label' => '<i class="fa"></i><span>Цвета</span>', 'url' => ['/admin/field-colors/index'],
                    'active' => $_c == 'field-colors',
                ],

            ],
            'visible'=> My::can('tech')
        ],
        [
            'label' => '<i class="fa fa-recycle"></i><span>Реорганизация</span>',
            'active' => ($_c == 'reorder') ||
                ($_c == 'color-set'),
            'items' => [
                [
                    'label' => '<i class="fa"></i><span>Перенос групп</span>', 'url' => ['/admin/reorder/index'],
                    'active' => $_c == 'reorder',
                ],
                [
                    'label' => '<i class="fa"></i><span>Цвета</span>', 'url' => ['/admin/color-set/index'],
                    'active' => $_c == 'color-set',
                ]
            ],
            'visible'=> My::can('reorganisation')
        ]
//        [
//            'label' => '<i class="fa fa-magic"></i><span>Парсер</span>',
//            'active' => $_c == 'parser',
//            'items' => [
//                ['label' => '<i class="fa fa-user-secret"></i>Мистер Парсер', 'url' => '/admin/parser/do'],
//                ['label' => '<i class="fa fa-eye"></i>Посмотреть чо как там', 'url' => '/admin/parser/index'],
//                ['label' => '<i class="fa fa-bars"></i>Категории', 'url' => '/admin/parser/categories'],
//                ['label' => '<i class="fa fa-bars"></i>Товары', 'url' => '/admin/parser/items'],
//                ['label' => '<i class="fa fa-picture-o"></i>Изображения', 'url' => '/admin/parser/images'],
//            ],
//            'options' => ['class' => 'treeview'],
//        ],
    ],
    'options' => ['class' => 'sidebar-menu'],
    'encodeLabels' => false,
    'dropDownCaret' => '<i class="fa fa-angle-left pull-right"></i>',
]); /*

<ul id="left-nav" class="nav">
    <li><a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Панель управления</a></li>
    <li><a href="/admin/menu"><i class="fa fa-bars fa-fw"></i> Меню</a></li>
    <li>
        <a href="#" id="nav-toggle"><i class="fa fa-shopping-cart fa-fw"></i> Каталог <b class="caret"></b></a>
        <ul class="second nav">
            <li>
                <a href="/admin/categories"> <i class="fa fa-folder fa-fw"></i> Категории</a>
            </li>
            <li>
                <a href="/admin/items"><i class="fa fa-bars fa-fw"></i> Товары</a>
            </li>
            <li>
                <a href="/admin/colors"><i class="fa fa-adjust fa-fw"></i> Цвета</a>
            </li>
            <li>
                <a href="/admin/orders"><i class="fa fa-list-alt fa-fw"></i> Заказы</a>
            </li>
            <li>
                <a href="/admin/brands"><i class="fa fa-tag fa-fw"></i> Бренды</a>
            </li>
        </ul>
    </li>
    <li><a href="/admin/technology"><i class="fa fa-files-o fa-fw"></i> Технологии нанесения</a></li>
    <li><a href="/admin/pages"><i class="fa fa-files-o fa-fw"></i> Материалы</a></li>
    <li><a href="/admin/news"><i class="fa fa-newspaper-o fa-fw"></i> Новости</a></li>
    <li><a href="/admin/modules"><i class="fa fa-align-left fa-fw"></i> Текстовые модули</a></li>
    <li><a href="/admin/parser"><i class="fa fa-magic fa-fw" style="background: -webkit-linear-gradient(#00F6FF,#E075F3, #F72FA4);-webkit-background-clip: text;-webkit-text-fill-color: transparent;"></i> Парсер</a></li>
    <!--    <li><a href="/admin/albums"><i class="fa fa-picture-o fa-fw"></i> Альбомы</a></li>-->
</ul>
*/ ?>
