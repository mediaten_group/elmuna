<?php
/**
 * @var $list array
 * @var $this \yii\web\View
 * @var $active string
 * @var $groups_data array
 */

use kartik\select2\Select2;
use app\assets\JsTreeAsset;
use yii\bootstrap\Html;

JsTreeAsset::register($this);
$data = \yii\helpers\Json::encode($categories);
$js = <<<JS
    var selected_cat = 0;
    /* category switch */
    $('[name=cat-select]').on('change',function() {
      var val = $(this).val();
      window.location.replace("/admin/reorder?cat="+val);
    })
    /* buttons */
    $('.add-to-cat').on('click',function(e) {
      e.preventDefault();
      var data = [];
      var selected = $('#groups-table tbody tr.active');
      for (var i=0;i<selected.length;i++) {
        data.push(selected.eq(i).attr('data-key'));
      }
      $.ajax({
        url:'/admin/reorder/add',
        method:'POST',
        data:{
            data:data,
            cat:selected_cat
        }
      }).done(function() {
            var selected = $('#groups-table tbody tr.active');
            for (var i=0;i<selected.length;i++) {
                if (!selected.eq(i).children().last().children('.cat-'+selected_cat).length)
                    selected.eq(i).children().last().append('<div class="cat-'+selected_cat+'">'+selected_cat+'</div>')          
            }
      })
    })
    $('.remove-from-cat').on('click',function(e) {
      e.preventDefault();
      var data = [];
      var selected = $('#groups-table tbody tr.active');
      for (var i=0;i<selected.length;i++) {
        data.push(selected.eq(i).attr('data-key'));
      }
      $.ajax({
        url:'/admin/reorder/remove',
        method:'POST',
        data:{
            data:data,
            cat:selected_cat
        },        
      }).done(function() {
            var selected = $('#groups-table tbody tr.active');
            for (var i=0;i<selected.length;i++) {
                selected.eq(i).children().last().children('.cat-'+selected_cat).remove()          
            }
      })
    })
    /* tree */
    var tree = $('#categories');
    var id = tree.attr('data-id');
    tree.jstree({
        'core' : {
            data : $data,
            multiple : false,
            force_text: false
    },
        'contextmenu':{
            'items': function(node) {
            }
        },
        'plugins': ['types', 'contextmenu', 'wholerow','search']
    }).on("loaded.jstree", function (e, data) {
        var tree = data.instance;
        tree.open_all();
    }).on("activate_node.jstree",function(node,event) {
        selected_cat = event.node.id;
        $('.add-to-cat').removeClass('disabled');
        $('.remove-from-cat').removeClass('disabled');
    });    
    /* groups table */
    $('#groups-table').on('mousedown',function(e) {
        e.preventDefault();
        if (!e.ctrlKey && !e.shiftKey) {
            $(this).children('tbody').children('tr').removeClass('active');
        }
        $('#groups-table tbody tr').on('mousemove',function(e) {
            $(this).addClass('active');
            jQuery(document).off('mouseup');
        })
    }).on('mouseup',function(e) {
        e.preventDefault();
        $('#groups-table tbody tr').off('mousemove');
    });    
    $('#groups-table tbody tr').on('click',function(e) {
        $(this).addClass('active');
    })
    
    var to = false;
    $('#tree-search').keyup(function () {
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
      var v = $('#tree-search').val();
      tree.jstree(true).search(v);
    }, 250);
    });

JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <?= Select2::widget([
                    'data' => $list,
                    'name' => 'cat-select',
                    'options' => [
                        'placeholder' => 'Выберите категорию'
                    ],
                    'value' => $active
                ]) ?>
            </div>
            <div class="box-body">
                <table class="table table-responsive" id="groups-table">
                    <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Изображение</th>
                        <th>Текущие каталоги</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($groups_data as $item): ?>
                        <tr data-key="<?= $item['group_id'] ?>">
                            <td>
                                <?= $item['title'] ?>
                            </td>
                            <td>
                                <?php if ($item['image_name']): ?>
                                    <img src="/uploads/images/thumbs100/<?= $item['image_name'] ?>">
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $item['cats'] ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <div class="btn-group">
                    <?= Html::a('Добавить', '#', ['class' => 'btn btn-success add-to-cat disabled btn-flat', 'role' => 'button']) ?>
                    <?= Html::a('Удалить', '#', ['class' => 'btn btn-danger remove-from-cat disabled btn-flat', 'role' => 'button']) ?>
                </div>
                <div class="box-tools pull-right form-inline">
                    <input id="tree-search" class="form-control" placeholder="Категория">
                </div>
            </div>
            <div class="box-body">
                <div id="categories"></div>
            </div>
        </div>
    </div>
</div>