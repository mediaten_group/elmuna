<?php
/**
 * @var \app\modules\admin\models\Scheme $model
 * @var int $pid
 * @var \app\tables\FieldType $type
 * @var string $fieldForm
 * @var \yii\web\View $this
 */

$this->params['h1'] = 'Добавить новую схему';

?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <?= $this->render('_form', ['model' => $model, 'type' => $type, 'fieldForm' => $fieldForm]) ?>
        </div>
    </div>
</div>
