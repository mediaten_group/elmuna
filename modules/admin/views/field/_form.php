<?php
/**
 * @var $this \yii\web\View
 * @var $model \app\modules\admin\models\Field
 * @var $type array
 * @var $fieldForm string
 */

use app\modules\admin\lib\My;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use himiklab\sortablegrid\SortableGridAsset;

$js = <<<JS

$(".todo-list").sortable({
  placeholder: "sort-highlight",
  handle: ".handle",
  forcePlaceholderSize: true,
  zIndex: 999999,
  revert:true, 
  update: function( event, ui ) {}
}).disableSelection();

$("#add-option").on('click',function(e) {
  e.preventDefault();
  var id = $(this).attr('data-id'),
      ul = $(this).parent().prev().children('ul');
  $.ajax({
    method: 'POST',
    url:'/admin/field/create-option',
    data:{id:id}    
  })
    .done(function(msg) {
      var form = '<li data-id="'+msg+'">'+
                    '<div class="form-inline">'+
                        '<span class="handle ui-sortable-handle">'+
                            '<i class="fa fa-ellipsis-v"></i> '+
                            '<i class="fa fa-ellipsis-v"></i>'+
                        '</span>'+                    
                        '<input class="form-control" name="value[value]['+msg+']" placeholder="Значение">'+
                        '<input class="form-control" name="value[price]['+msg+']" placeholder="Увеличение стоимости">'+
                        '<button type="button" class="btn btn-danger pull-right delete-option">'+
                            '<div class="glyphicon glyphicon-trash"></div>'+
                        '</button>'+
                    '</div>'+
                '</li>';
      ul.append(form);
    })
  
  
})

$('.todo-list').on('click','.delete-option',function(e) {
  e.preventDefault();
  var li = $(this).parents('li'),
      id = li.attr('data-id');
  $.ajax({
    method: 'POST',
    url: '/admin/field/delete-option',
    data:{id:id}
  })
    .done(function() {
      li.remove();      
    })  
})

$('.image-checkbox').on('change',function(e){
    e.preventDefault();
    var checked = !$(this).prop('checked'); 
    $(this).parents('div').children('input').attr('disabled',checked)
})

function checkArea(){
    var h_max = parseFloat($('#area-height_max').val()),
        h_min = parseFloat($('#area-height_min').val()),
        w_max = parseFloat($('#area-width_max').val()),
        w_min = parseFloat($('#area-width_min').val()),
        error = 0;
    if (h_max < h_min) {
        error++;
        $('.field-area-height_max').addClass('has-error');
        $('.field-area-height_min').addClass('has-error');
    } else {
        $('.field-area-height_max').removeClass('has-error');
        $('.field-area-height_min').removeClass('has-error');
    }
    if (w_max < w_min) {
        error++;
        $('.field-area-width_max').addClass('has-error');
        $('.field-area-width_min').addClass('has-error');
    } else {
        $('.field-area-width_max').removeClass('has-error');
        $('.field-area-width_min').removeClass('has-error');
    }
    if (error) {
        $('button[type=submit]').attr('disabled',true)
    } else {
        $('button[type=submit]').attr('disabled',false)
    }
}

$('#area-height_max').on('input',checkArea);
$('#area-height_min').on('input',checkArea);
$('#area-width_max').on('input',checkArea);
$('#area-width_min').on('input',checkArea);

JS;

SortableGridAsset::register($this);
$this->registerJs($js);

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo '<div class="box-body">';

echo $form->field($model, 'name');

echo $form->field($model, 'field_type_id')->widget(Select2::className(), [
    'language' => 'ru',
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => $type,
    'options' => ['placeholder' => 'Выберите тип'],
    'pluginOptions' => [
        'allowClear' => true,
        'disabled' => true,
    ],
]);

echo $fieldForm;

echo $form->field($model, 'separate')->checkbox()->label('Отдельно от остальных вычислений');

echo $form->field($model, 'status')->checkbox();

echo $form->field($model, 'required')->checkbox();

echo '</div><div class="box-footer">';

if ($model->id)
    echo My::buttons(['/admin/field/delete', 'id' => $model->id], ['/admin/scheme/update', 'id' => $model->scheme_id]);
else
    echo My::saveAndCloseBtn(['/admin/scheme/update', 'id' => $model->scheme_id]);

echo '</div>';

ActiveForm::end();