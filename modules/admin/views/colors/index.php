<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'attribute' => 'Название',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['title'], ['update', 'hex' => $data['hex']]);
                    }
                ],
                'hex' => [
                    'attribute' => 'Цвет',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a(Html::tag('span', null, [
                            'class' => 'color',
                            'style' => 'background-color: ' . $data['hex'],
                        ]), ['update', 'hex' => $data['hex']]);
                    }
                ]
            ],
        ]) ?>
            </div>
        </div>
    </div>
</div>
