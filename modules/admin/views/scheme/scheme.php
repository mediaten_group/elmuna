<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var int $group_id
 */
use yii\helpers\Html;
use yii\grid\GridView;

?>
<div class="box-header with-border">
    <h3 class="box-title"><a href="/admin/group-scheme/update?id=<?= $group_id ?>">Схемы в этой группе</a></h3>
</div>
<div class="box-body">
    <p>
        <?= Html::a('Добавить', ['create', 'pid' => $group_id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => 'ID',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['id'], ['update', 'id' => $data['id']]);
                }
            ],
            [
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['name'], ['update', 'id' => $data['id']]);
                }
            ],
        ],
    ]) ?>
</div>