<?php
/**
 * @var \app\modules\admin\models\Scheme $model
 * @var int $pid
 * @var array $technology
 * @var array $fieldForm
 */

$this->params['h1'] = 'Добавить новую схему';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form-scheme', ['model' => $model,'technology' => $technology]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('scheme', ['group_id' => $model->group_id, 'dataProvider' => $dataProvider]) ?>
            </div>
        </div>
    </div>
</div>
