<?php
/**
 * @var \yii\web\View $this
 * @var array $fields
 */
use yii\bootstrap\Html;
use himiklab\sortablegrid\SortableGridView;

?>
<div class="box-header with-border">
    <h3 class="box-title">Поля</h3>
</div>
<div class="box-body">
    <p>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Добавить
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php foreach ($fields as $fid=>$field) {
                echo '<li>';
                echo Html::a($field,['/admin/field/create','scheme_id'=>$id,'type_id'=>$fid]);
                echo '</li>';
            } ?>
        </ul>
    </div>
    </p>
    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => 'ID',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['id'], ['/admin/field/update', 'id' => $data['id']]);
                }
            ],
            [
                'attribute' => 'name',
                'label' => 'Название',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['name'], ['/admin/field/update', 'id' => $data['id']]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{copy} {delete}',
                'headerOptions' => ['style' => 'width: 50px'],
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $id = $model['id'];
                        return <<<HTML
<a href="/admin/field/delete?id=$id" title="Удалить" 
  aria-label="Удалить" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post" data-pjax="0">
    <span class="glyphicon glyphicon-trash"></span>
</a>
HTML;
                    },
                    'copy' => function ($url, $model) {
                        $id = $model['id'];
                        return <<<HTML
<a href="/admin/field/copy?id=$id" title="Копировать" aria-label="Копировать" data-method="post" data-pjax="0">
    <span class="glyphicon glyphicon-paste"></span>
</a>
HTML;
                    },
                ]
            ],
        ],
    ]) ?>
</div>