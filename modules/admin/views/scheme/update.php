<?php
/**
 * @var \app\modules\admin\models\Scheme $model
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var array $technology
 * @var \yii\data\ActiveDataProvider $fieldsProvider
 * @var array $fields
 * @var \yii\web\View $this
 */

$this->params['h1'] = 'Изменить схему';

?>
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form-scheme', ['model' => $model, 'technology' => $technology]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <?php if ($model->id) { ?>
            <div class="box">
                    <?= $this->render('field',
                        ['id' => $model->id, 'dataProvider' => $fieldsProvider, 'fields' => $fields]) ?>
            </div>
        <?php } ?>
        <div class="box">
            <?= $this->render('scheme', ['group_id' => $model->group_id, 'dataProvider' => $dataProvider]) ?>
        </div>
    </div>
</div>
