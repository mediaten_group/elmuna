<?php
/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\models\
 * @var $technology array
 */

use app\modules\admin\lib\My;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Html;

//$this->registerJs($this->render('_form.js'));

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'name');

echo $form->field($model, 'tech_id')->widget(Select2::className(),[
    'language' => 'ru',
    'theme' => Select2::THEME_BOOTSTRAP,
    'data' => $technology,
    'options' => ['placeholder' => 'Выберите технологию'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);

echo $form->field($model, 'status')->checkbox();

echo My::saveAndCloseBtn(['/admin/group-scheme/update','id'=>$model->group_id]);

ActiveForm::end();
