<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\assets\AdminLteAsset;
use yii\grid\GridView;

$this->params['h1'] = $this->title;

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',['depends' => AdminLteAsset::className()]);
$this->registerJsFile(Yii::getAlias('@web/plugins/morris/morris.min.js'),['depends' => AdminLteAsset::className()]);
$this->registerCssFile(Yii::getAlias('@web/plugins/morris/morris.css'),['depends' => AdminLteAsset::className()]);
$this->registerJs("
var donut = new Morris.Donut({
                        element: 'result',
                        resize: true,
                        colors: ['#3c8dbc', '#d2d6de', '#00a65a'],
                        data: [
                            {label: 'Левел 2', value: ".$data['level2']."},
                            {label: 'Левел 0', value: ".$data['level0']."},
                            {label: 'Левел 1', value: ".$data['level1']."},
                        ],
                        hideHover: 'auto'
                    });
");

$this->params['sidebar'] = '<div class="box"><div class="chart" id="result" style="height: 200px; position: relative;"></div></div>'
?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                ]) ?>
            </div>
        </div>
    </div>
</div>