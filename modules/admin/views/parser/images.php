<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\assets\AdminLteAsset;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;

?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id',
                        'article',
                        'name' => [
                            'attribute' => 'Изображение',
                            'format' => 'raw',
                            'value' => function ($data) {
//                                return Html::a($data['title'], ['update', 'hex' => $data['hex']]);
                                return Html::a(Html::tag('div', Html::img('/uploads/pgifts/'.$data['article'].'/thumbs/'.$data['name']), ['class'=>'image']),'/uploads/pgifts/'.$data['article'].'/'.$data['name']);
                            }
                        ],
                        'date',
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>