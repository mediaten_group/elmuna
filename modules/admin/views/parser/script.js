var $textarea = $('#textarea'),
    $button = $('#launch'),
    $stop = $('#stop'),
    $status = $('#status'),
    $result = $('#result'),
    $overlay = $('.overlay'),
    $current = $('#current'),
    $count = $('#count'),
    parsing = false,
    status_ok = 0,
    status_bad_0 = 0,
    status_bad_1 = 0,
    status_bad_2 = 0;

$button.click(function (e) {
    var array = [],
        string,
        j = 0,
        row,
        result;
    e.preventDefault();
    parsing = true;
    $overlay.fadeIn();
    string = $textarea.val();
    var regex = /'(.*?)'/g;

    while ((row = regex.exec(string)) != null) {
        array.push(row[1])
    }

    $count.html(array.length);

    array.forEach(function (item, i, arr) {

        $.ajax({
            type: "post",
            url: "/admin/parser/ajax",
            data: {article: item},
            success: function (msg) {
                j++;
                $current.html(j);
                msg = $.parseJSON(msg);
                console.log(msg.status);
                switch (msg.status) {
                    case 'ok':
                        $status.append('<p class="ok">' + msg.data + ' - Успешно</p>');
                        status_ok++;
                        break;
                    case 'bad':
                        $status.append('<p class="error">' + msg.data + ' - ' + msg.message + '</p>');
                        if(msg.number == 0) status_bad_0++;
                        if(msg.number == 1) status_bad_1++;
                        break;
                    case 'error':
                        alert('Произошла неведомая ошибка!');
                        break;
                    default:
                        alert('Не сканало :(')
                }
                $("html, body").animate({ scrollTop: $(document).height() }, 0);

                if(array.length == j || parsing == false) {
                    $overlay.fadeOut();
                    parsing = false;
                    $('.chart-responsive').show();
                    var donut = new Morris.Donut({
                        element: 'result',
                        resize: true,
                        colors: ["#3c8dbc", "#f39c12", "#f56954", "#00a65a", "#d2d6de"],
                        data: [
                            {label: "Уже есть", value: status_bad_0},
                            {label: "Не найдено", value: status_bad_1},
                            {label: "Ошибка сервера", value: status_bad_2},
                            {label: "Успешно", value: status_ok},
                            {label: "Прервано", value: (array.length - j)}
                        ],
                        hideHover: 'auto'
                    });
                    $("html, body").animate({ scrollTop: 0 }, 0);
                    return true;
                }
            },
            error: function(){
                j++;
                status_bad_2++;
                $current.html(j);
                $status.append('<p class="error">' + item + ' - Ошибка сервера</p>');
                if(array.length == j) {
                    $overlay.fadeOut();
                    parsing = false;
                }
                $("html, body").animate({ scrollTop: $(document).height() }, 0);
                if(array.length == j || parsing == false) {
                    $overlay.fadeOut();
                    parsing = false;
                    $('.chart-responsive').show();
                    var donut = new Morris.Donut({
                        element: 'result',
                        resize: true,
                        colors: ["#3c8dbc", "#f39c12", "#f56954", "#00a65a", "#d2d6de"],
                        data: [
                            {label: "Уже есть", value: status_bad_0},
                            {label: "Не найдено", value: status_bad_1},
                            {label: "Ошибка сервера", value: status_bad_2},
                            {label: "Успешно", value: status_ok},
                            {label: "Прервано", value: (array.length - j)}
                        ],
                        hideHover: 'auto'
                    });
                    $("html, body").animate({ scrollTop: 0 }, 0);
                    return true;
                }
            }
        });
    });
});

$stop.click(function() {
    parsing = false;
    console.log(parsing);
});