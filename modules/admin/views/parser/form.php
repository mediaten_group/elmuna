<?php

use yii\helpers\Html;

?>
<h1><i class="fa fa-magic fa-fw"
       style="background: -webkit-linear-gradient(#0FC3F7,#E075F3, #F72FA4);-webkit-background-clip: text;-webkit-text-fill-color: transparent;"></i> <?= $this->title; ?>
</h1>

<div class="row">
    <div class="col-md-6">
        <?=Html::beginForm('','post',['class'=>'form-inline'])?>
            <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                <div class="input-group">
                    <div class="input-group-addon">$</div>
                    <input type="text" class="form-control" name="article" placeholder="Артикул">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Спарсздить!</button>
        <?= Html::endForm()?>
    </div>
</div>