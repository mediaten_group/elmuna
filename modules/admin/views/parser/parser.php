<?php

use app\assets\AdminLteAsset;

$this->registerJs($this->render('script.js'));
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',['depends' => AdminLteAsset::className()]);
$this->registerJsFile(Yii::getAlias('@web/plugins/morris/morris.min.js'),['depends' => AdminLteAsset::className()]);
$this->registerCssFile(Yii::getAlias('@web/plugins/morris/morris.css'),['depends' => AdminLteAsset::className()]);
$this->params['header'] = '<i class="fa fa-magic fa-fw"
       style="background: -webkit-linear-gradient(#0FC3F7,#E075F3, #F72FA4);-webkit-background-clip: text;-webkit-text-fill-color: transparent;"></i> ' . $this->title;
?>
<div class="row">
    <div class="col-sm-4">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <textarea id="textarea" rows="20" class="form-control" placeholder="Артикулы"></textarea>
                </div>
                <button type="button" class="btn btn-primary" id="launch">Спарсздить</button>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Результат</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body chart-responsive" style="display: none;">
                <div class="chart" id="result" style="height: 400px; position: relative;"></div>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-sm-8">
        <pre id="status"></pre>
    </div>
</div>
<div class="overlay">
    <div class="count"><span id="current">0</span> из <span id="count">0</span></div>
    <a href="#" id="stop"><i class="fa fa-hand-paper-o"></i> СТОП</a>

    <?/*<svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44" stroke="#fff">
        <g fill="none" fill-rule="evenodd" stroke-width="2">
            <circle cx="22" cy="22" r="19.9155">
                <animate attributeName="r" begin="0s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1"
                         keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite"/>
                <animate attributeName="stroke-opacity" begin="0s" dur="1.8s" values="1; 0" calcMode="spline"
                         keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite"/>
            </circle>
            <circle cx="22" cy="22" r="16.458">
                <animate attributeName="r" begin="-0.9s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1"
                         keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite"/>
                <animate attributeName="stroke-opacity" begin="-0.9s" dur="1.8s" values="1; 0" calcMode="spline"
                         keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite"/>
            </circle>
        </g>
    </svg>*/?>
</div>