<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var $links
 */
use app\models\Tree;
use app\widgets\TreeView\TreeView;
use kartik\tree\models\TreeQuery;
use yii\grid\GridView;
use yii\helpers\Html;

/** @var TreeQuery $tree */
$tree = Tree::find();

echo TreeView::widget([
    'query' => $tree->addOrderBy('root, lft'),
    'headingOptions' => ['label' => Yii::t('app', 'Категории')],
]);

return

    $this->params['h1'] = $this->title;

$params = Yii::$app->controller->actionParams;

$current = $this->params['current'];

$array = [];
$array['id'] = [
    'attribute' => 'id',
    'format' => 'raw',
    'headerOptions' => ['style' => 'width: 100px'],
    'value' => function ($data) {
        return Html::a($data['id'], ['update', 'id' => $data['id']]);
    }
];
$array[] = ['attribute' => 'level'];
$array['title'] = [
    'attribute' => 'title',
    'format' => 'raw',
    'value' => function ($data) {
        $current = $this->params['current'];

        if ($current['level'] == 2) {
            $link = ['update', 'id' => $data['id']];
        } else {
            $link = ['index', 'id' => $data['id']];
        }

        return Html::a($data['title'], $link);
    }
];
if ($current['level'] >= 1) $array[] = [
    'format' => 'raw',
    'header' => 'Добавить товар',
    'value' => function ($model) {
        $html = '<a href="/admin/items/create?category_id=' . $model['id'] . '" title="Добавить товар" aria-label="Добавить товар" data-pjax="0"><span class="glyphicon glyphicon-plus-sign"></span> Добавить</a>';
        return $html;
    }
];
$array[] = [
    'header' => 'Редактировать',
    'class' => 'yii\grid\ActionColumn',
    'template' => '{update}',
    'headerOptions' => ['style' => 'width: 150px']
];
$array[] = [
    'header' => 'Удалить',
    'class' => 'yii\grid\ActionColumn',
    'template' => '{delete}',
    'headerOptions' => ['style' => 'width: 30px']
];
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <p>
                    <?= Html::a('Добавить', ['create', 'parent_id' => $current['id']], ['class' => 'btn btn-success']) ?>
                    <?= ($current['level'] >= 1) ? Html::a('Редактировать', ['update', 'id' => $current['id']],
                        ['class' => 'btn btn-success']) : null ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $array
                ]) ?>
            </div>
        </div>
    </div>
</div>
