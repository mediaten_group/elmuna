<?php
/**
 * @var \app\modules\admin\models\Categories $categories
 */

use app\assets\JsTreeAsset;
use yii\helpers\Url;

JsTreeAsset::register($this);

$data = \yii\helpers\Json::encode($categories);
$url = Url::to(['categories/ajax']);
$update = Url::to(['categories/update','id'=>'']);
$items = Url::to(['items/index','category_id'=>'']);
//pre($data);
$js = <<<JS
$('#categories').jstree({
'core' : {
    'data' : $data,
    "check_callback" : true
} ,
"contextmenu":{
    "items": function(node) {
        var tree = $("#categories").jstree(true);
        return {
            "Create": {
                "separator_before": false,
                "separator_after": false,
                "label": "Создать",
                "action": function (obj) {
                    node = tree.create_node(node);
                }
            },
            "Edit": {
                "separator_before": false,
                "separator_after": false,
                "label": "Редактировать",
                "action": function (obj) {
                    window.location.href = '$update'+node.id;
                }
            },
            "Item": {
                "separator_before": false,
                "separator_after": false,
                "label": "Товары",
                "action": function (obj) {
                    window.location.href = '$items'+node.id;
                }
            },
//            "Rename": {
//                "separator_before": false,
//                "separator_after": false,
//                "label": "Переименовать",
//                "action": function (obj) {
//                    tree.edit(node);
//                }
//            },
            "Remove": {
                "separator_before": false,
                "separator_after": false,
                "label": "Удалить",
                "action": function (obj) {
                    tree.delete_node(node);
                }
            }
        };
    }
},
    'plugins': ['types', 'dnd', 'contextmenu', 'wholerow', 'state']
    }).bind("move_node.jstree", function (e, data) {
        var ajaxData = {
            id: data.node.id,
            position: data.position,
            old_position: data.old_position,
            parents: data.node.parents,
            parent: data.parent,
            old_parent: data.old_parent
        };
            $.ajax({
                type: "POST",
                url: "$url?action=move",
                data: ajaxData,
                success: function(msg){

                }
            });
        }).bind("create_node.jstree", function (e, data) {
        var ajaxData = {
            position: data.position,
            parent: data.parent,
            parents: data.node.parents,
            title: data.node.text
        };
            $.ajax({
                type: "POST",
                url: "$url?action=create",
                data: ajaxData,
                dataType: 'json',
                success: function(msg){
                    if(msg.status != 'ok') return;
                    var id = msg.id;
                    window.location.href = '$update'+id;
                    //set_id(obj,id);
                    $('#categories').jstree(true).set_id(data.node,id);
                }
            });
        }).bind("rename_node.jstree", function (e, data) {
        var ajaxData = {
            title: data.text,
            id: data.node.id
        };
            $.ajax({
                type: "POST",
                url: "$url?action=rename",
                data: ajaxData,
                dataType: 'json',
                success: function(msg){

                }
            });
        }).bind("delete_node.jstree", function (e, data) {
        var ajaxData = {
            id: data.node.id
        };
            $.ajax({
                type: "POST",
                url: "$url?action=delete",
                data: ajaxData,
                dataType: 'json',
                success: function(msg){

                }
            });
        });
JS;
$this->registerJs($js);
?>

<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <div id="categories"></div>
            </div>
        </div>
    </div>
</div>
