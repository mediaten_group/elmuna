<?php
/**
 * @var $this yii\web\View
 * @var $model app\modules\admin\models\Categories
 * @var $catList
 * @var $level1
 * @var $level2
 */

use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use app\widgets\alias\GenerateAlias;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

echo $form->field($model, 'title');
echo $form->field($model, 'alias')->widget(GenerateAlias::className(), [
    'depends' => 'title',
]);
echo $form->field($model, 'text')->widget(TinyMce::className(), [
    'options' => ['rows' => 10],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            'advlist autolink lists link charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste'
        ],
        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
    ]
])->label('Текст');

echo $form->field($model, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', '100'],
    'cover' => 'cover_image_id',
])->label('Лого');

echo $form->field($model, 'front_page')->checkbox();
echo $form->field($model, 'status')->checkbox();
echo $form->field($model, 'meta_title');
echo $form->field($model, 'meta_description');
echo $form->field($model, 'meta_keywords');
echo $form->field($model, 'set_markup');
echo $form->field($model, 'view');
echo \yii\bootstrap\Html::tag('p','0 - показ товаров; 1 - показ подкатегорий; строка - псевдоним страницы');

echo My::saveAndCloseBtn();

ActiveForm::end();
