<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var array $roles
 */
use app\lib\My;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
$p = [];
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => 'id',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['id'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'username',
                            'label' => 'Логин',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['username'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'label' => 'Имя',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $name = ArrayHelper::getValue(ArrayHelper::getValue($data['userProfiles'], 0), 'name');
                                return $name ? Html::a($name, ['view', 'id' => $data['id']]) : 'Не указано';
                            }
                        ],
                        [
                            'label' => 'Email',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $email = ArrayHelper::getValue(ArrayHelper::getValue($data['userProfiles'], 0), 'email');
                                return $email ? Html::a($email, ['view', 'id' => $data['id']]) : Html::a($data['email'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'label' => 'Телефон',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $phone = ArrayHelper::getValue(ArrayHelper::getValue($data['userProfiles'], 0), 'phone');
                                return $phone ? Html::a($phone, ['view', 'id' => $data['id']]) : 'Не указано';
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Зарегистрирован',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime(My::dateTime($data['created_at'])), ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'label' => 'Роль',
                            'content' => function ($data) use ($roles) {
                                return Html::dropDownList('role', $data['item_name'], $roles,['disabled'=>true,'class'=>'form-control']);
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
