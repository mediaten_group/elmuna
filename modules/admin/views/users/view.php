<?php
/**
 * @var $this yii\web\View
 * @var $user Order
 * @var $orders yii\data\ActiveDataProvider
 * @var $reviews yii\data\ActiveDataProvider
 */
use app\lib\My;
use app\models\Order;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-4">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Информация</h3>
            </div>
            <div class="box-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>Логин</td>
                            <td><strong><?= $user['username'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><strong><?= $user['email'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Имя</td>
                            <td>
                                <strong><?= ArrayHelper::getValue(ArrayHelper::getValue($user['userProfiles'], 0, 'Не указано'), 'name', 'Не указано') ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td>
                                <strong><?= ArrayHelper::getValue(ArrayHelper::getValue($user['userProfiles'], 0, 'Не указано'), 'phone', 'Не указано') ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td>
                                <strong><?= ArrayHelper::getValue(ArrayHelper::getValue($user['userProfiles'], 0, 'Не указано'), 'address', 'Не указано') ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>Зарегистрирован</td>
                            <td><strong><?= My::mysql_russian_datetime(My::dateTime($user['created_at'])) ?></strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Заказы</h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $orders,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime($data['created_at']), ['/admin/orders/view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'price',
                            'label' => 'Сумма заказа',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['price'].' руб.', ['/admin/orders/view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data['status'] ? 'Выполнено' : 'Не выполнено';
                            }
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Отзывы</h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $reviews,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime($data['created_at']), ['/admin/reviews/view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'fio',
                            'label' => 'ФИО',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['fio'], ['/admin/reviews/view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['email'], ['/admin/reviews/view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data['status'] ? 'Опуликован' : 'Не опубликован';
                            }
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
