<?php
/**
 * @var $this yii\web\View
 * @var Info $model
 */
use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use app\tables\Info;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;

$types = [
    'input' => 'Небольшой текст',
    'textarea' => 'Большой текст',
    'TinyMCE' => 'Текстовый редактор',
    'checkbox' => 'Дискретное',
    'image' => 'Изображение',
];

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

if($model->isNewRecord) {
    echo $form->field($model, 'key')->label('Название поля');
} else {
    echo $form->field($model, 'key')->label('Название поля')->textInput(['disabled'=>'disabled']);
    switch($model->type) {
        case 'input':
            echo $form->field($model, 'value')->label($model->comment);
            break;
        case 'textarea':
            echo $form->field($model, 'value')->textarea(['rows'=>8])->label($model->comment);
            break;
        case 'TinyMCE':
            echo $form->field($model, 'value')->widget(TinyMce::className(), [
                'options' => ['rows' => 8],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste'
                    ],
                    'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
                ]
            ])->label($model->comment);
            break;
        case 'checkbox':
            echo $form->field($model, 'value')->checkbox()->label($model->comment);
            break;
        case 'image':
            echo $form->field($model, '_upload[]')->widget(Upload::className(), [
            'thumbs' => ['screen', 'small', '100'],
            'cover' => 'value',
        ])->label('Изображение');
            break;
    }
}
echo '<hr>';

echo $form->field($model, 'comment')->label('Комментарий');
echo $form->field($model, 'type')->dropDownList($types);

echo My::buttons(['delete', 'key' => $model->key], ['index'], $model->isNewRecord);

ActiveForm::end();
