<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;

$types = [
    'input' => 'Небольшой текст',
    'textarea' => 'Большой текст',
    'TinyMCE' => 'Текстовый редактор',
    'checkbox' => 'Дискретное',
    'image' => 'Изображение',
];

?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'key',
                    'label' => 'Название',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['key'], ['update', 'key' => $data['key']]);
                    }
                ],
                [
                    'attribute' => 'comment',
                    'label' => 'Поле',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['comment'], ['update', 'key' => $data['key']]);
                    }
                ],
                [
                    'attribute' => 'type',
                    'label' => 'Тип',
                    'format' => 'raw',
                    'value' => function ($data) use ($types) {
                        return $types[$data['type']];
                    }
                ]
            ],
        ]) ?>
            </div>
        </div>
    </div>
</div>
