<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <a href="<?=Url::to(['create'])?>" class="btn btn-primary">Создать</a>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'alias' => [
                            'attribute' => 'Алиас',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['alias'], ['update', 'alias' => $data['alias']]);
                            }
                        ],
                        'title' => [
                            'attribute' => 'Название',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['title'], ['update', 'alias' => $data['alias']]);
                            }
                        ],
                        'status' => [
                            'attribute' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data['status'] ? 'Опубликовано' : 'Не опубликовано';
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
