<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use app\widgets\alias\GenerateAlias;
use dosamigos\tinymce\TinyMce;
use kartik\color\ColorInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'title')->label('Заголовок');

echo $form->field($model, 'alias')->textInput(/*['readonly' => true]*/)->label('Алиас');

//echo $form->field($model, 'intro')->label('Вступление');

echo $form->field($model, 'text')->widget(TinyMce::className(), [
    'options' => ['rows' => 12],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            'advlist autolink lists link charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste'
        ],
        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        'extended_valid_elements' => "script[charset|defer|language|src|type]",
    ]
])->label('Текст');

//echo $form->field($model, '_upload[]')->widget(Upload::className(), [
//    'thumbs' => ['screen', 'small', 'news', '100'],
//    'cover' => 'cover_image_id',
//])->label('Изображение');

echo $form->field($model, 'status')->checkbox();

echo $form->field($model, 'meta_title');

echo $form->field($model, 'meta_description');

echo $form->field($model, 'meta_keywords');

echo $form->field($model, 'is_printable')->checkbox();

echo My::saveAndCloseBtn(['index']);

ActiveForm::end();
