<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'attribute' => 'Название',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['title'], ['update', 'id' => $data['id']]);
                    }
                ],
                'title' => [
                    'attribute' => 'Заголовок',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['title'], ['update', 'id' => $data['id']]);
                    }
                ],
                'status' => [
                    'attribute' => 'Статус',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data['status'] ? 'Опубликовано' : 'Не опубликовано';
                    }
                ]
            ],
        ]) ?>
            </div>
        </div>
    </div>
</div>
