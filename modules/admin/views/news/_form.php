<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use app\modules\admin\lib\My;
use app\modules\upload\widgets\Upload;
use app\widgets\alias\GenerateAlias;
use dosamigos\tinymce\TinyMce;
use kartik\color\ColorInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]);

echo $form->field($model, 'title')->label('Заголовок');

echo $form->field($model, 'alias')->widget(GenerateAlias::className(), [
    'depends' => 'title',
])->label('Алиас');

echo $form->field($model, 'intro')->label('Вступление');

echo $form->field($model, 'text')->widget(TinyMce::className(), [
    'options' => ['rows' => 8],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            'advlist autolink lists link charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste'
        ],
        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
    ]
])->label('Текст');

echo $form->field($model, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['screen', 'small', 'news', '100'],
    'cover' => 'cover_image_id',
])->label('Изображение');

echo $form->field($model, 'status')->checkbox();

echo My::buttons(['delete', 'id' => $model->id], ['index'], $model->isNewRecord);

//echo Html::beginTag('div', ['form-group']);
//echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
//    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
//]);
//echo Html::endTag('div');
ActiveForm::end();
