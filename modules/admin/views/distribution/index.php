<?php
/**
 * @var \app\models\forms\DistributionForm $model
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;

$this->params['h1'] = '';

?>

<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?php

                $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                    ]
                ]);
                echo $form->field($model, 'subject')->label('Тема');

                echo $form->field($model, 'body')->widget(TinyMce::className(), [
                    'options' => ['rows' => 8],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            'advlist autolink lists link charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table contextmenu paste'
                        ],
                        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
                    ]
                ]);

                echo Html::submitButton('Разослать');

                ActiveForm::end();

                ?>
            </div>
        </div>
    </div>
</div>




