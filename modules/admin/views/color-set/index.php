<?php
/**
 * @var $list array
 * @var $this \yii\web\View
 * @var $active string
 * @var $groups_data array
 * @var $model \app\modules\admin\models\ColorsGifts
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\modules\admin\models\Items;
use app\widgets\alias\GenerateAlias;
use kartik\color\ColorInput;
use yii\grid\GridView;

$js = <<<JS
    var selected_cat = 0;
    /* sort switch */
    $('[name=sort-select]').on('change',function() {
      var val = $(this).val();
      window.location.replace("/admin/color-set?sort="+val);
    })
    /* modals */
    $('[href="#new"]').on('click',function(e) {
      e.preventDefault();
      $('#new').modal('show');
    })
    $('[href="#select"]').on('click',function(e) {
      e.preventDefault();
      $('#select').modal('show');
    })
    /* table clicks */
    $('.grid-view').on('click','tbody tr',function(e) {
      e.preventDefault();
      $('[href="#select"]').removeClass('disabled');
      $('.grid-view tr.active').removeClass('active');
      $('#colorslinks-color_id').val($(this).attr('data-key'));
      $(this).addClass('active');
    });
    $('#groups-table').on('click','tbody tr',function(e) {
      e.preventDefault();
      $('[href="#remove"]').removeClass('disabled');
      $('#groups-table tr.active').removeClass('active');
      $(this).addClass('active');
    });
    
JS;

$this->registerJs($js);

?>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <?= Select2::widget([
                    'data' => $list,
                    'name' => 'sort-select',
                    'options' => [
                        'placeholder' => 'Выберите сортировку'
                    ],
                    'value' => $active,
                    'hideSearch' => true
                ]) ?>
            </div>
            <div class="box-body">
                <table class="table table-responsive" id="groups-table">
                    <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Pantone</th>
                        <th>Sol's</th>
                        <th>Изображение</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($groups_data as $item): ?>
                        <tr data-key="<?= $item['id'] ?>">
                            <td>
                                <?= $item['name'] ?>
                            </td>
                            <td>
                                <?= $item['pantone'] ?>
                            </td>
                            <td>
                                <?= $item['sol'] ?>
                            </td>
                            <td>
                                <?= Html::img('/uploads/images/thumbs100/' . Items::actionGetCover($item['item_id'])) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <div class="btn-group">
                    <?= Html::a('Создать новый цвет', '#new', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Выбрать указанный', '#select', ['class' => 'btn btn-primary disabled']) ?>
                    <?= Html::a('Нет цвета', '#remove', ['class' => 'btn btn-warning disabled']) ?>
                </div>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'title',
                        'hex' => [
                            'attribute' => 'hex',
                            /** @var $data \app\tables\Colors */
                            'content' => function ($data) {
                                return Html::tag('span', null, [
                                    'class' => 'color',
                                    'style' => 'background-color: ' . $data['hex'],
                                ]);
                            }
                        ]
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="new-label">
    <div class="modal-dialog" role="document">
        <div class="box box-success">
            <?php $form = ActiveForm::begin() ?>
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="new-label">Создать новый цвет</h4>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'hex')->widget(ColorInput::className()) ?>
                <?= $form->field($model, 'title')->label('Заголовок') ?>
                <?= $form->field($model, 'alias')->widget(GenerateAlias::className(), [
                    'depends' => 'title'
                ])->label('Псевдоним') ?>
                <?= $form->field($model, 'column')->dropDownList($list)->label('Колонка') ?>
                <?= $form->field($model, 'pattern')->label('Шаблон') ?>
            </div>
            <div class="box-footer">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div class="modal fade" id="select" tabindex="-1" role="dialog" aria-labelledby="select-label">
    <div class="modal-dialog" role="document">
        <div class="box box-primary">
            <?php $form = ActiveForm::begin() ?>
            <div class="box-header with-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="select-label">Использовать выбранный цвет</h4>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'color_id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'column')->dropDownList($list)->label('Колонка') ?>
                <?= $form->field($model, 'pattern')->label('Шаблон') ?>
            </div>
            <div class="box-footer">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
