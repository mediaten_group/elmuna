<?php
/**
 * @var $this yii\web\View
 * @var $review Review
 * @var $user User
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use app\modules\admin\lib\My;
use app\tables\Review;
use app\tables\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin() ?>
        <div class="box">
            <div class="box-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>Пользователь</td>
                            <td><?php echo isset($user['userProfiles']['name']) ? Html::a($user['userProfiles']['name'], [
                                    '/admin/users/view', 'id' => $user['id']
                                ]) : Html::a($user['username'], [
                                    '/admin/users/view', 'id' => $user['id']]) ?></td>
                        </tr>
                        <tr>
                            <td>ФИО</td>
                            <td><strong><?= $review['fio'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><strong><?= $review['email'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Оставлен</td>
                            <td><strong><?= \app\lib\My::mysql_russian_datetime($review['created_at']) ?></strong></td>
                        </tr>
                        <tr>
                            <td>Текст</td>
                            <td><?= $review['text'] ?></td>
                        </tr>
                        <tr>
                            <td>Ответ</td>
                            <td><?= $form->field($review, 'answer')->textarea(['rows' => 8])->label(false) ?></td>
                        </tr>
                        <tr>
                            <td>Статус</td>
                            <td><?= $form->field($review, 'status')->checkbox() ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <?= My::buttons(['delete', 'id' => $review->id]) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
