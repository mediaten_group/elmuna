<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\lib\My;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'fio',
                            'label' => 'ФИО',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['fio'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['email'], ['view', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'Текст',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return mb_strimwidth($data['text'], 0, 200, "...");
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Отправлен',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a(My::mysql_russian_datetime($data['created_at']), ['view', 'id' => $data['id']]);
                            }
                        ],
                        'status' => [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return $data['status'] ? 'Опубликован' : 'Не опубликован';
                            },
                            'headerOptions' => [
                                'style' => 'min-width: 121px'
                            ]
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
