<?php
/**
 *
 * @var \app\modules\admin\models\FieldColors $model
 */

use yii\bootstrap\ActiveForm;
use app\modules\upload\widgets\Upload;
use app\modules\admin\lib\My;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

echo $form->field($model, 'title');

echo $form->field($model, 'status')->checkbox();

echo $form->field($model, '_upload[]')->widget(Upload::className(), [
    'thumbs' => ['25'],
    'cover' => 'upload_id',
]);

echo My::saveAndCloseBtn();

ActiveForm::end();