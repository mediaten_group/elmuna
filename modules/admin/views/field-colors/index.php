<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\admin\models\FieldColors;

?>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <p>
                    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'label' => 'ID',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['id'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Название',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['title'], ['update', 'id' => $data['id']]);
                            }
                        ],
                        [
                            'attribute' => 'upload_id',
                            'label' => 'Изображение',
                            'format' => 'raw',
                            'value' => function($data){
                                return FieldColors::getColor($data['upload_id']);
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'headerOptions' => ['style' => 'width: 30px']
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

