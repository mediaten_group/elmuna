<?php
/**
 * @var $model \app\modules\admin\models\FieldColors
 */

?>

<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <div class="box-body">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
        </div>
    </div>
</div>