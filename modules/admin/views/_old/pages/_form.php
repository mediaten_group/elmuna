<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin();

echo $form->field($model, 'title');
echo $form->field($model, 'text')->widget(TinyMce::className(), [
    'options' => ['rows' => 20],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]);
echo $form->field($model, 'meta_keywords')->textarea(['rows' => 3]);
echo $form->field($model, 'meta_description')->textarea(['rows' => 3]);
echo Html::beginTag('div', ['form-group']);
echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
]);
echo Html::endTag('div');
ActiveForm::end();
