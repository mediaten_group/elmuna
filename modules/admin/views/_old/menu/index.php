<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'format' => 'raw',
                    'attribute' => 'id',
                    'value' => function ($data) {
                        return Html::a($data['id'], ['update', 'id' => $data['id']]);
                    }
                ],
                'url' => [
                    'format' => 'raw',
                    'attribute' => 'url',
                    'value' => function ($data) {
                        $v = Html::a($data['url'], ['update', 'id' => $data['id']]);
                        $v .= Html::a('<span class="glyphicon glyphicon-link"></span>',
                            $data['url'],
                            ['class' => 'pull-right', 'title' => 'Открыть на сайте']);
                        return $v;
                    }
                ],
                'label',
                'element_title' => [
                    'format' => 'raw',
                    'attribute' => 'element_title',
                    'value' => function ($data) {
                        $icon = $data['icon'] ? '<span class="icon ' . $data['icon'] . '"></span>' : '';
                        $title = $data['element_title'] ? $data['element_title'] : $data['element_id'];
                        return $data['element_id'] ?
                            Html::a($icon . $title,
                                ['/admin/' . $data['type_controller'] . '/update', 'id' => $data['element_id']],
                                ['title' => 'Редактировать элемент']) : null;
                    }
                ],
                'ordering',
                'date',
                [
                    'class' => 'yii\grid\ActionColumn', 'template' => '{delete}'
                ],
            ],
        ]) ?>
    </div>
</div>
