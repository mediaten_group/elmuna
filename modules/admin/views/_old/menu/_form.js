$('#routes-type_id').change(function () {
    var type_id = $(this).val();
    $.ajax({
        url: 'elements-list',
        data: {type_id: type_id},
        success: function (html) {
                $('#routes-element_id').html($(html));
        }
    })
});
