<?php
/**
 * @var $this yii\web\View
 * @var $model Routes
 */
use app\modules\admin\lib\Lst;
use app\modules\admin\models\Routes;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJs($this->render('_form.js'));

$form = ActiveForm::begin();

echo $form->field($model, 'label');
echo $form->field($model, 'alias');
echo $form->field($model, 'text');

echo $form->field($model, 'parent')
    ->dropDownList(Lst::routeParents($model->url), ['prompt' => 'Не выбранно']);

echo $form->field($model, 'type_id')->dropDownList(Lst::types(), ['prompt' => 'Не выбранно']);
echo $form->field($model, 'element_id')->dropDownList(Lst::elements(), ['prompt' => 'Не выбранно']);
echo $form->field($model, 'controller');
echo $form->field($model, 'ordering');

echo Html::beginTag('div', ['form-group']);
echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
]);
echo Html::endTag('div');

ActiveForm::end();
