<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'format' => 'raw',
                    'attribute' => 'id',
                    'value' => function ($data) {
                        return Html::a($data['id'], ['update', 'id' => $data['id']]);
                    }
                ],
                'title' => [
                    'format' => 'raw',
                    'attribute' => 'title',
                    'value' => function ($data) {
                        return Html::a($data['title'], ['update', 'id' => $data['id']]);
                    }
                ],
                'date',
                [
                    'class' => 'yii\grid\ActionColumn', 'template' => '{delete}',
                ],
            ],
        ]) ?>
    </div>
</div>
