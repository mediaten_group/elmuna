<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use app\modules\admin\lib\Lst;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['h1'] = $this->title;

?>
<div class="row">
    <div class="col-lg-12">
        <?
        $form = ActiveForm::begin();
        echo $form->field($model, 'type')->dropDownList(Lst::modulesTypes(), ['prompt' => 'Выберете позицию']);
        echo $form->field($model, 'alias');
        echo $form->field($model, 'text');
        echo Html::beginTag('div', ['form-group']);
        echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]);
        echo Html::endTag('div');
        ActiveForm::end();
        ?>
    </div>
</div>
