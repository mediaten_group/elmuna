<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin();

echo $form->field($model, 'title');
echo $form->field($model, 'text');
echo Html::beginTag('div', ['form-group']);
echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
]);
echo Html::endTag('div');
ActiveForm::end();
