<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use app\modules\admin\models\Modules;
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
$modules = Modules::allModules();
?>
    <div class="row">
        <div class="col-lg-12">
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'alias' => [
                        'format' => 'raw',
                        'attribute' => 'alias',
                        'value' => function ($data) {
                            return Html::a($data['alias'], ['update', 'alias' => $data['alias']]);
                        },
                    ],
                    'title',
                    'type',
                    'delete' => [
                        'format' => 'raw',
                        'label' => 'Удалить',
                        'value' => function ($data) {
                            return $data['deleted'] ? Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                                'delete', 'alias' => $data['alias'],
                            ], ['data' => [
                                'method' => 'post',
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                            ]]) : '';
                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>
<? /*
<div class="row">
    <? foreach ($modules as $mod): ?>
        <div class="col-lg-12">
            <div class="form-group">
                <label for="mod<?= $mod['alias']; ?>"><?= $mod['title']; ?></label>
                <input type="text" class="form-control" id="mod<?= $mod['alias']; ?>"
                       placeholder="<?= $mod['title']; ?>" value="<?= $mod['text']; ?>">
            </div>
        </div>
    <? endforeach; ?>
</div>

*/ ?>