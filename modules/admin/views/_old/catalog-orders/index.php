<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a($data['id'], ['update', 'id' => $data['id']]);
                    }
                ],
                'name',
                'phone',
                'date',
            ],
        ]) ?>
    </div>
</div>
