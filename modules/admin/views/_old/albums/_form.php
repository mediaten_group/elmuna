<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin();

echo $form->field($model, 'title');
echo $form->field($model, 'folder');
echo $form->field($model, 'cover');
echo $form->field($model, 'date');
echo $form->field($model, 'popup_width');
echo $form->field($model, 'popup_height');
echo $form->field($model, 'mini_width');
echo $form->field($model, 'mini_height');
echo $form->field($model, 'meta_keywords')->textarea(['rows' => 3]);
echo $form->field($model, 'meta_description')->textarea(['rows' => 3]);
echo Html::beginTag('div', ['class' => 'form-group']);
if ($model->isNewRecord) {
    echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
} else {
    echo Html::submitButton('Сохранить закрыть', [
        'class' => 'btn btn-success',
        'name' => 'submit',
        'value' => 'save_close',
    ]);
    echo Html::submitButton('Сохранить', [
        'style' => 'margin-left: 15px', 'class' => 'btn btn-success',
        'name' => 'submit',
        'value' => 'save',
    ]);
}
echo Html::endTag('div');

ActiveForm::end();
