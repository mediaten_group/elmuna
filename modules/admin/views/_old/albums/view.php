<?php
/**
 * @var $this yii\web\View
 * @var $model Albums
 */

use app\modules\admin\models\Albums;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;

$js = <<<JS
$('.fancyBox').fancybox();
JS;

$this->registerJs($js);
$params = json_decode($model->params, JSON_UNESCAPED_UNICODE);

$this->params['h1'] = $model->title;
$webRoot = Yii::getAlias('@app/uploads/albums/' . $model->folder);
$web = Yii::getAlias('@web/uploads/albums/' . $model->folder);
if (!is_dir($webRoot)) mkdir($webRoot);
$images = array_diff(scandir($webRoot), ['.', '..']);

echo FileInput::widget([
    'name' => 'file',
    'options' => ['accept'=>'image/*', 'multiple' => true],
    'pluginOptions' => [
        'uploadExtraData' => [
            'folder' => $model->folder,
        ],
        'uploadUrl' => Url::to(['images-upload']),
    ]
]);

echo Html::beginTag('div', ['id' => 'album-images']);
foreach ($images as $name) {
    echo $this->render('_image', ['folder' => $model->folder, 'name' => $name, 'params' => $params]);
}
echo Html::endTag('div');
