<?php
/**
 * @var $this yii\web\View;
 * @var $params
 * @var $folder
 * @var $name
 */
use app\modules\admin\models\Albums;
use yii\helpers\Url;

$mini = Albums::makeThumb($folder, $name, 'mini', $params['mini_width'], $params['mini_height']);
$popup = Albums::makeThumb($folder, $name, 'popup', $params['popup_width'], $params['popup_height'], 80);
?>
<div class="image">
    <a class="fancyBox" href="<?= $popup ?>" rel="group">
        <img src="<?= $mini ?>" alt=""/>
    </a>
    <a href="<?= Url::to(['delete-image', 'album' => $folder, 'name' => $name]) ?>" data-method="post">
        <span class="glyphicon glyphicon-remove"></span>
    </a>
</div>