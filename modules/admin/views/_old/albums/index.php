<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->params['h1'] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id' => [
                    'format' => 'raw',
                    'attribute' => 'id',
                    'value' => function ($data) {
                        $v = Html::a($data['id'], ['view', 'id' => $data['id']]);
                        return $v;
                    }
                ],
                'title' => [
                    'format' => 'raw',
                    'attribute' => 'title',
                    'value' => function ($data) {
                        $v = Html::a($data['title'], ['view', 'id' => $data['id']]);
                        $span = Html::tag('span', '', [
                            'class' => 'pull-right glyphicon glyphicon-edit',
                            'title' => 'Редактировать альбом',
                        ]);
                        $v .= Html::a($span, ['update', 'id' => $data['id']]);
                        return $v;
                    }
                ],
                'date',
                [
                    'class' => 'yii\grid\ActionColumn', 'template' => '{delete}'
                ],
            ],
        ]) ?>
    </div>
</div>
