<?php

namespace app\modules\admin\models;

use app\modules\admin\lib\My;
use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;


class CtBrands extends \app\tables\CtBrands
{
    public $imageFile;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['imageFile'], 'file', 'extensions' => 'png, jpg, jpeg, bmp'];

        return $rules;
    }

}