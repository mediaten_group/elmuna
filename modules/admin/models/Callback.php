<?php

namespace app\modules\admin\models;


use /** @noinspection PhpMethodOrClassCallIsNotCaseSensitiveInspection */
    Yii;
use yii\helpers\ArrayHelper;

class Callback extends \app\tables\Callback
{
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'status' => 'Статус',
        ]);
    }
}
