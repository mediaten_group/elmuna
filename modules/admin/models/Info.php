<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 18.01.2016
 * Time: 12:56
 */

namespace app\modules\admin\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class Info extends \app\tables\Info
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
        ];
    }
}