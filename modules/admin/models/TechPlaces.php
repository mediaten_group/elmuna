<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.03.16
 * Time: 14:36
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class TechPlaces extends \app\tables\TechPlaces
{

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'async' => true,
            ],
            DateTimeBehavior::className(),
        ];
    }

}