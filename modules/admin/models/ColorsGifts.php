<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.04.16
 * Time: 16:01
 */

namespace app\modules\admin\models;


class ColorsGifts extends \app\tables\ColorsGifts
{

    public static function getCatalog($sort)
    {
        $query = ColorsGifts::find()
            ->asArray();
        if ($sort == 'pantone') {
            $query
                ->where([
                    'not',
                    ['pantone' => ''],
                ])
                ->andWhere([
                    'or',
                    ['status' => null],
                    ['status' => '0']
                ])
                ->orderBy('pantone ASC');
        }
        if ($sort == 'sol') {
            $query->
            where([
                'not',
                ['sol' => '']
            ])
                ->andWhere([
                    'or',
                    ['status' => null],
                    ['status' => '0']
                ])
                ->orderBy('sol ASC');
        }
        if ($sort == 'name') {
            $query->
            where([
                'not',
                ['name' => '']
            ])
                ->andWhere([
                    'or',
                    ['status' => null],
                    ['status' => '0']
                ])
                ->orderBy('name ASC');
        }
        return $query->all();
    }

}