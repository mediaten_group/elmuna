<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 12.04.16
 * Time: 13:31
 */

namespace app\modules\admin\models;


use yii\helpers\ArrayHelper;

class GroupsLinks extends \app\tables\GroupsLinks
{

    public static function getCatList()
    {
        $list = GroupsLinks::find()
            ->asArray()
            ->select([
                'orig_cat_name'
            ])
            ->indexBy('orig_cat_href')
            ->column();
        $arr = [];
        foreach ($list as $key => &$item) {
            $groups = GroupsLinks::find()
                ->where([
                    'orig_cat_href' => $key
                ])
                ->all();
            $count = 0;
            foreach ($groups as $group) {
                $p = CategoryLink::find()
                    ->where([
                        'group_id'=>$group['group_id']
                    ])
                    ->count();
                if ($p) $count++;
            }
            $arr[$key] = (count($groups)-$count);
            $item = $item . '. Групп: ' . count($groups).'. Не привязано: '.(count($groups)-$count);
        }
        arsort($arr);
        $list = array_merge($arr,$list);
        return $list;
    }

    public static function getGroupsList($cat_href)
    {
        $groups = GroupsLinks::find()
            ->asArray()
            ->select([
                'group_id',
                'title',
                'upload.name as image_name'
            ])
            ->where([
                'orig_cat_href' => $cat_href
            ])
            ->leftJoin('groups', 'groups.id = groups_links.group_id')
            ->leftJoin('upload', 'upload.id = groups.cover_image_id')
            ->all();
        foreach ($groups as $key=>&$group) {
            $cats = CategoryLink::find()
                ->asArray()
                ->select([
                    'categories.id'
                ])
                ->where([
                    'group_id' => $group['group_id']
                ])
                ->leftJoin('categories', 'categories.id = category_link.category_id')
                ->column();
            $group['cats'] = '';
            $count = 0;
            foreach ($cats as $cat) {
                $count++;
//                $group['cats'] .= Categories::getFullName($cat) . '<br>';
                $group['cats'] .= "<div class='cat-$cat'>$cat</div>";
            }
            $group['count'] =$count;
        }
        ArrayHelper::multisort($groups,['count'],[SORT_ASC]);
        return $groups;
    }

}