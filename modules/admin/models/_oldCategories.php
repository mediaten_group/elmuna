<?php

namespace app\modules\admin\models;

use app\modules\admin\lib\My;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class _oldCategories extends \app\tables\Categories
{
    public $imageFile;
    public $lvl1;
    public $lvl2;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['imageFile'], 'file', 'extensions' => 'png, jpg, jpeg, bmp'];
        $rules[] = [['lvl1','lvl2'], 'safe'];

        return $rules;
    }

    public function attributeLabels()
    {
        $parent = parent::attributeLabels();

        $parent['imageFile'] = 'Файл изображения';

        return $parent;
    }

    public function uploadImage()
    {
        $image = UploadedFile::getInstance($this, 'imageFile');
        if (!$image) return false;

        $web = Yii::getAlias('@webroot');
        $root = $web . '/uploads/categories/' . $this->id;

        My::checkDir($root);
        My::checkDir($root . '/thumbs');

        if ($this->image) {
            @unlink($root . '/' . $this->image);
            @unlink($root . '/thumbs/' . $this->image);
        }

        $name = My::uniqueName($root, $image->extension);

//        $size = Image::getImagine()->open($path . '/' . $name)->getSize();
//        $h = $size->getHeight();
//        $w = $size->getWidth();
//        $sizes = My::enteredSize($w, $h, 440, 160);
//        Image::thumbnail($path . '/' . $name, $sizes[0], $sizes[1])->save($path . '/thumbs/' . $name, ['quality' => 100]);
        Image::thumbnail($image->tempName, 200, 150)->save($root . '/thumbs/' . $name, ['quality' => 95]);

        $image->saveAs($root . '/' . $name);

        $this->image = $name;

        return true;
    }

    public function beforeSave($insert)
    {
        $this->created_at = My::dateTime();
        $this->uploadImage();

        if($this->lvl2) $this->pid = $this->lvl2;
        if($this->lvl1 && !$this->lvl2) $this->pid = $this->lvl1;

        $level = self::getCurrentLevel($this->pid)['level'];
        $this->level = (int) $level + 1;

        /*
        if (isset($file->name)) {

            $file->saveAs('uploads/' . $file->name);
            $this->cover = $file->name;

        } else {
//            $this->image = isset($this->oldAttributes['image']) ? $this->oldAttributes['image'] : '';
        }
*/
        return true;
    }

    public static function getLink($id)
    {
        return (new Query())
            ->select([
                'id',
                'title',
            ])
            ->from('categories')
            ->where(['id' => $id])
            ->one();
    }


    public static function getPid($pid)
    {
        return (new Query())
            ->select([
                'pid',
            ])
            ->from('categories')
            ->where(['id' => $pid])
            ->column()[0];
    }

    /**
     * @param $id
     * @return array|bool
     */
    public static function getCurrentLevel($id)
    {
        return (new Query())
            ->select([
                'id',
                'title',
                'level',
                'pid',
            ])
            ->from('categories')
            ->where(['id' => $id])
            ->one();
    }

    public static function getCatList()
    {
        $array = (new Query())
            ->select([
                'id',
                'title as name',
            ])
            ->from('categories')
            ->where(['level' => 1])
            ->all();

        return ArrayHelper::map($array, 'id', 'name');
    }

    public static function getSubCatList($cat_id = null)
    {
        if(!$cat_id) return [];
        $array = (new Query())
            ->select([
                'id',
                'title as name',
            ])
            ->from('categories')
            ->where(['pid' => $cat_id])
            ->all();
        pree($array);
        return $array;
    }


    public static function getCatTree()
    {
        $list = (new Query())
            ->select([
                'title AS text',
                'id',
                'parent_id',
            ])
            ->from('categories_gifts')
            ->where(['front_page' => 1])
            ->orderBy('parent_id, id')
            ->all();
        $tree = array();
        foreach ($list as $row) {
            if ($row['parent_id'] === null) $row['parent_id'] = 0;
            $tree[(int)$row['parent_id']][] = $row;
        }
//        echo '<pre>';
//        print_r(Categories::treePrint($tree, 0));
//        echo '</pre>';
        return Categories::treePrint($tree, 0);
    }

    public static function treePrint($tree, $pid = 0, $id = false)
    {
        if (empty($tree[$pid]))
            return false;

        $html = [];
        $i = 0;
        foreach ($tree[$pid] as $row) {
            $html[$i]['id'] = $row['id'];
            $html[$i]['text'] = $row['text'];
//            $html[$i]['alias'] = $row['alias'];
//            $html[$i]['image'] = $row['image'];
//            $html[$i]['level'] = $row['level'];

            if (isset($tree[$row['id']])) {

                $html[$i]['children'] = Categories::treePrint($tree, $row['id'], $id);
            }
            $i++;
        }

        return $html;
    }
}
