<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.04.16
 * Time: 17:27
 */

namespace app\modules\admin\models;

use app\tables\Colors;
use yii\base\Exception;
use yii;

class ColorsLinks extends \app\tables\ColorsLinks
{

    public $hex;
    public $title;
    public $alias;

    public function beforeSave($insert)
    {
        if (!$this->color_id) {
            $color = new Colors();
            $color->hex = $this->hex;
            $color->title = $this->title;
            $color->alias = $this->alias;
            if ($color->save()) {
                $this->color_id = $color->id;
                return true;
            } else {
                pree($color->errors);
                return false;
            }
        } else {
            return true;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $list = ColorsGifts::find()
            ->where("$this->column like '%$this->pattern%'")
            ->all();
        /* @var $color_gifts ColorsGifts */
        foreach ($list as $color_gifts) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $color_gifts->status = 1;
                if (!$color_gifts->save())
                    throw new Exception($color_gifts->errors);
                $color = Colors::findOne($this->color_id);
                if (!$color)
                    throw new Exception('Нет цвета ' . $this->color_id);
                $item = Items::findOne($color_gifts->item_id);
                if (!$item)
                    throw new Exception('Нет предмета ' . $color_gifts->item_id);
                $item->color = $color->hex;
                if (!$item->save())
                    throw new Exception($item->errors);
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

}