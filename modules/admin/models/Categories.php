<?php

namespace app\modules\admin\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use app\components\Index;

class Categories extends \app\tables\Categories
{
//    public $text;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['title', 'required'];
        return $rules;
    }

    public static function getCatTree()
    {
        $list = self::find()
            ->select([
//                'CONCAT(title, \' - \', position) AS text',
                "(id || ': ' || title) AS text",
                'id',
                'pid',
                'markup',
                'set_markup'
            ])
            ->asArray()
            ->orderBy('pid, position')
            ->all();
        $tree = array();
        foreach ($list as $row) {
            if ($row['pid'] === null) $row['pid'] = 0;
            $tree[(int)$row['pid']][] = $row;
        }

        return self::treePrint($tree, 0);
    }

    public static function treePrint($tree, $pid = 0, $id = false)
    {
        if (empty($tree[$pid]))
            return false;

        $html = [];
        $i = 0;
        foreach ($tree[$pid] as $row) {
            $row['set_markup'] ? $markup = 'set_markup' : $markup = 'markup';
            $row[$markup] ? $text = $row['text'] . ' (<i class="fa fa-percent" aria-hidden="true"></i>
                                <span class=' . $markup . '>' . $row[$markup] . '</span>)' : $text = $row['text'];
            $html[$i]['id'] = $row['id'];
            $html[$i]['text'] = $text;
//            $html[$i]['alias'] = $row['alias'];
//            $html[$i]['icon'] = "";
//            $html[$i]['level'] = $row['level'];

            if (isset($tree[$row['id']])) {

                $html[$i]['children'] = Categories::treePrint($tree, $row['id'], $id);
            }
            $i++;
        }

        return $html;
    }


    public static function actionCreate($post)
    {
        $position = $post['position'];
        $pid = $post['parent'];
        $title = $post['title'];
        $parents = $post['parents'];
        array_pop($parents);
        array_reverse($parents);

        $model = new self;
        $model->pid = $pid;
        $model->position = $position;
        $model->title = $title;
        $model->path = self::toString($parents);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$model->save()) throw new Exception;
            Yii::$app->db->createCommand('UPDATE categories SET "path" = "path" || ARRAY[:id]::INT[]
WHERE "id" = :id', [':id' => $model->id])->execute();
            $transaction->commit();
            return ['status' => 'ok', 'id' => $model->id];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function actionRename($post)
    {
        $id = ArrayHelper::getValue($post, 'id');
        $title = ArrayHelper::getValue($post, 'title', '');

        $model = self::findOne($id);
        if (!$model) return ['error' => 'Не найдено ' . $id];

        $model->load($post, '');
        if ($model->save()) return ['status' => 'ok', 'title' => $title];

        return ['error' => $model->errors];
    }

    public static function actionDelete($id)
    {
        $model = self::findOne($id);
        if (!$model) return ['error' => 'Не найдено ' . $id];
        if ($model->pid == null) return ['error' => 'Нельзя удалять с верхнего уровня ' . $id];

        if ($model->delete()) return ['status' => 'ok', 'id' => $model->id];

        return ['error' => $model->errors];
    }

    public static function actionMove($post)
    {
        $id = (int)$post['id'];

        $model = self::findOne($id);
        if (!$model) return ['error' => 'Не найдено ' . $id];

        $old_position = $post['old_position'];
        $position = $post['position'];
        $old_pid = $post['old_parent'];
        $pid = $post['parent'];

        if ($pid === '#') $pid = 'NULL';
        if ($old_pid === '#') $old_pid = 'NULL';

        $parents = $post['parents'];
        array_pop($parents);
        $parents = array_reverse($parents);
//        $parents[] = $id;
        $parents = implode(',', $parents);


        if (self::changePosition($id, $pid, $position, $old_pid, $old_position, $parents))
            return ['id' => $id];

        return ['error' => $model->errors];
    }

    public static function changePosition($id, $pid, $pos, $old_pid, $old_pos, $parents)
    {
        $sql = [];
//        exit();
        if ($pid === $old_pid) {
            if ($pos > $old_pos) {
                if ($pid == 'NULL') {
                    $sql[] = <<<SQL
UPDATE categories SET "position" = "position" - 1
WHERE "pid" IS NULL AND "position" >= $old_pos AND "position" <= $pos AND "id" != $id
SQL;
                } else {
                    $sql[] = <<<SQL
UPDATE categories SET "position" = "position" - 1
WHERE "pid" = $pid AND "position" >= $old_pos AND "position" <= $pos AND "id" != $id
SQL;
                }
            } else {
                if ($pid == 'NULL') {
                    $sql[] = <<<SQL
UPDATE categories SET "position" = "position" + 1
WHERE "pid" IS NULL AND "position" >= $pos AND "position" <= $old_pos AND "id" != $id
SQL;
                } else {
                    $sql[] = <<<SQL
UPDATE categories SET "position" = "position" + 1
WHERE "pid" = $pid AND "position" >= $pos AND "position" <= $old_pos AND "id" != $id
SQL;
                }
            }
            $sql[] = <<<SQL
UPDATE categories SET "position" = $pos WHERE "id" = $id
SQL;
        } else {
            if ($old_pid == 'NULL') {
                $sql[] = <<<SQL
UPDATE categories SET "position" = "position" - 1
WHERE "pid" IS NULL AND "position" >= $old_pos AND "id" != $id
SQL;
            } else {
                $sql[] = <<<SQL
UPDATE categories SET "position" = "position" - 1
WHERE "pid" = $old_pid AND "position" >= $old_pos AND "id" != $id
SQL;
            }
            if ($pid == 'NULL') {
                $sql[] = <<<SQL
UPDATE categories SET "position" = "position" + 1
WHERE "pid" IS NULL AND "position" >= $pos AND "id" != $id
SQL;
            } else {
                $sql[] = <<<SQL
UPDATE categories SET "position" = "position" + 1
WHERE "pid" = $pid AND "position" >= $pos AND "id" != $id
SQL;
            }
            /** Пересчет parents всех вложенных элемента */
//            $length = count($parents);
            $sql[] = <<<SQL
UPDATE categories
SET "path" = string_to_array('$parents',',')::int[] ||
    "path"[(SELECT array_length("path",1) FROM categories WHERE "id" = $id) : array_length("path",1)]
WHERE "path" && ARRAY[$id]
SQL;

            /** Обновление переносимого элемента */
            $path = $pid === 'NULL' ? "string_to_array('$id',',')::int[]" :
                "array_append((SELECT \"path\" FROM categories WHERE \"id\" = $pid), $id)";
            $sql[] = <<<SQL
UPDATE categories
SET "position" = $pos,
    "pid" = $pid,
    "path" = $path
WHERE "id" = $id
SQL;
//            pree($sql);

        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($sql as $s) {
                Yii::$app->db->createCommand($s)->execute();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            throw $e;
        }

        return ['status' => 'ok'];
    }

    public static function toString($array)
    {
        return '{' . implode(',', $array) . '}';
    }
    
    public static function getFullName($id)
    {
        $name = '';
        do {
            $cat = Categories::findOne($id);
            $name = ' > ' . $cat->title . $name;
            $id = $cat->pid;
        } while ($cat->pid);
        return $name;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Index::markup();
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'set_markup' => 'Наценка',
        ]);
    }

}
