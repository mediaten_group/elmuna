<?php

namespace app\modules\admin\models;

use app\modules\admin\lib\My;
use yii\db\Query;

class ItemImages extends \app\tables\ItemImages
{
    public function beforeSave()
    {
        $this->date = My::dateTime();
        return true;
    }

    public static function names($id)
    {
        return (new Query())
            ->select(['name'])
            ->where(['item_id' => $id])
            ->from('item_images')
            ->all();
    }
}
