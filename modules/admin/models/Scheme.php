<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 25.02.16
 * Time: 17:00
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class Scheme extends \app\tables\Scheme
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public static function duplicate($id, $group_id = 0)
    {
        $old_scheme = Scheme::findOne($id);
        $new_scheme = new Scheme();
        $new_scheme->attributes = $old_scheme->attributes;
        $new_scheme->group_id = ($group_id) ? $group_id : $old_scheme->group_id;
        $new_scheme->save();

        $fields = Field::find()->where(['scheme_id' => $old_scheme->id])->all();
        foreach ($fields as $field) {
            Field::duplicate($field->id, $new_scheme->id);
        }
        return $new_scheme;
    }

}