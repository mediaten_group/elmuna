<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 11:40
 */

namespace app\modules\admin\models;

use app\modules\upload\behaviors\UploadBehavior;
use app\modules\upload\models\Upload;
use yii\bootstrap\Html;
use yii\helpers\Json;
use yii;

class FieldColors extends \app\tables\FieldColors
{

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
        ];
    }

    public static function getColor($id)
    {
        $image = Upload::findOne($id);
        return Html::img("/uploads/images/thumbs25/" . $image['name']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $sql = <<<SQL
SELECT
  field.id  
FROM
  field
LEFT JOIN
  field_type ON field_type.id = field.field_type_id
WHERE
  field_type.type = 'color'
AND 
  jsonb_exists((field.value -> 'select_data'), :value)
SQL;

            $cmd = Yii::$app->db
                ->createCommand($sql);
            $cmd->bindValue('value', $this->id, yii\db\mssql\PDO::PARAM_STR);
            $fields = $cmd->queryAll();

            foreach ($fields as $field) {
                $f = Field::findOne($field['id']);
                $value = Json::decode($f->value);
                unset($value['price'][$this->id]);
                unset($value['select_data'][array_search($this->id, $value['select_data'])]);
                $new = [];
                foreach ($value['select_data'] as $v) {
                    $new[] = $v;
                }
                $value['select_data'] = $new;
                $f->value = Json::encode($value);
                $f->save();
            }

            return true;
        } else {
            return false;
        }
    }

}