<?php

namespace app\modules\admin\models;

use app\tables\CtItemSets;
use Yii;
use yii\base\Model;

class AddSetForm extends CtItemSets
{
    public function createSet($id)
    {
        $item = new CtItemSets;
        $item->item_id = $id;
        $item->size_id = $this->size_id;
        $item->color_id = $this->color_id;
        $item->count = $this->count;
        $item->price = $this->price;

        $item->save();

        return true;
    }
}