<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.03.16
 * Time: 18:25
 */

namespace app\modules\admin\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use himiklab\sortablegrid\SortableGridBehavior;

class Special extends \app\tables\Special
{

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

}