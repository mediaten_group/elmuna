<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 14.04.16
 * Time: 18:03
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use app\tables\ClientsUploads;
use himiklab\sortablegrid\SortableGridBehavior;
use yii\helpers\ArrayHelper;

class Clients extends \app\tables\Clients
{

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public function addImages($images)
    {
        $ids = ArrayHelper::getValue($images, '_upload');
        if ($ids) {
            foreach ($ids as $id) {
                $model = new ClientsUploads();
                $model->upload_id = $id;
                $model->client_id = $this->id;
                $model->save();
            }
        }
        return TRUE;
    }

}