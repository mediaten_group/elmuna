<?php

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\components\Index;
use app\modules\upload\behaviors\UploadBehavior;
use app\modules\upload\models\Upload;
use app\tables\ItemUpload;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Items
 * @package app\modules\admin\models
 * @property Groups $_group
 * @property $_upload
 */
class Items extends \app\models\Items
{
    private $_group;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'async' => true,
//                'beforeUpload' => 'beforeUpload',
            ],
            DateTimeBehavior::className(),
        ];
    }

    public static function actionGetCover($id)
    {
        $item = Items::findOne($id);
        if ($item->cover_image_id) {
            $upload = Upload::findOne($item->cover_image_id);
            return $upload->name;
        }
        return false;
    }

    public function beforeValidate()
    {
//        dd($this->_group->id);
        if (!$this->group_id) $this->group_id = $this->_group->id;
        if ($this->color == '') $this->color = null;

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (!$this->cover_image_id && $this->_upload) {
            $this->cover_image_id = $this->_upload[0];
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (key_exists('set_price',$changedAttributes)) {
            Index::markup();
        }

        $uploads = Upload::find()
            ->asArray()
            ->select([
                'upload.id AS upload_id',
                '(' . $this->id . ') AS item_id',
            ])
            ->where([
                'upload.id' => $this->_upload,
                'item_upload.upload_id' => null,
            ])
            ->leftJoin('item_upload', 'item_upload.upload_id = upload.id')
            ->all();

        if ($uploads) {
            Yii::$app->db->createCommand()->batchInsert(ItemUpload::tableName(), ['upload_id', 'item_id'], $uploads)->execute();
        }
    }

    public function populateGroup(Groups $group)
    {
        $this->_group = $group;
    }
//
//    public function beforeUpload($upload)
//    {
//        echo $param;
//        exit;
//    }
}
