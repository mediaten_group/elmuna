<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 04.08.2016
 * Time: 9:56
 */

namespace app\modules\admin\models;


class CategoriesParserGroupsLink extends \app\tables\CategoriesParserGroupsLink
{


    public static function getItems($category_id)
    {
        $items = [];
        if ($category_id) {
            $sql = <<<SQL
SELECT
  "groups"."id"                                   AS group_id,
  "groups".title                                AS title,
  upload."name"                                 AS image_name,
  string_agg(
  '<div class="cat-' || category_link.category_id::varchar || '">' ||
  category_link.category_id::varchar ||
  '</div>', '') AS cats
FROM categories_parser__groups__link
  INNER JOIN groups ON categories_parser__groups__link.group_id = "groups"."id"
  LEFT JOIN category_link ON "groups"."id" = category_link.group_id
  LEFT JOIN upload ON groups.cover_image_id = upload.id
WHERE categories_parser__groups__link.category_parser_id = $category_id
GROUP BY "groups"."id", upload."name"
SQL;
            $items =\Yii::$app->getDb()->createCommand($sql)->queryAll();
        }

        return $items;

    }
}