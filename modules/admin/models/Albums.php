<?php

namespace app\modules\admin\models;

use app\lib\My;
use Yii;
use yii\imagine\Image;


class Albums extends \app\tables\Albums
{
    public $popup_width;
    public $popup_height;

    public $mini_width;
    public $mini_height;

    public function beforeSave()
    {
        $app = Yii::getAlias('@app');
        $webRoot = Yii::getAlias('@webroot');
        $root = '/uploads/albums';

        $params['popup_width'] = $this->popup_width ?: 1200;
        $params['popup_height'] = $this->popup_height ?: 720;

        $params['mini_width'] = $this->mini_width ?: 200;
        $params['mini_height'] = $this->mini_height ?: 150;

        if ($this->isNewRecord) {
            if (!is_dir($app . $root . '/' . $this->folder)) mkdir($app . $root . '/' . $this->folder);
            if (!is_dir($webRoot . $root . '/' . $this->folder)) mkdir($webRoot . $root . '/' . $this->folder);
        }

        $this->params = json_encode($params, JSON_UNESCAPED_UNICODE);
        $this->date = My::dateTime();

        return true;
    }

    public function afterFind()
    {
        $params = json_decode($this->params, JSON_UNESCAPED_UNICODE);

        $this->popup_width = isset($params['popup_width']) && $params['popup_width'] ? $params['popup_width'] : 1200;
        $this->popup_height = isset($params['popup_height']) && $params['popup_height'] ? $params['popup_height'] : 720;

        $this->mini_width = isset($params['mini_width']) && $params['mini_width'] ? $params['mini_width'] : 200;
        $this->mini_height = isset($params['mini_height']) && $params['mini_height'] ? $params['mini_height'] : 150;

    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['popup_width', 'popup_height', 'mini_width', 'mini_height'], 'integer', 'min' => 0];

        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['mini_width'] = 'Ширина миниатюры';
        $labels['mini_height'] = 'Высота миниатюры';

        $labels['popup_width'] = 'Ширина изображения';
        $labels['popup_height'] = 'Высота изображения';

        return $labels;
    }

    public static function makeThumb($folder, $name, $type, $params)
    {
        $width = $params[$type . '_width'];
        $height = $params[$type . '_height'];

        $app = Yii::getAlias('@app');
        $web = Yii::getAlias('@web');
        $webRoot = Yii::getAlias('@webroot');

        $root = '/uploads/albums/' . $folder;
        if (!is_dir($webRoot . $root)) mkdir($webRoot . $root);

        $type = '/' . $type;
        if (!is_dir($webRoot . $root . $type)) mkdir($webRoot . $root . $type);

        $name = '/' . $name;
        if (is_file($webRoot . $root . $type . $name)) {
            return $web . $root . $type . $name;
        }

        if (is_file($app . $root . $name)) {
            Image::thumbnail($app . $root . $name, $width, $height)
                ->save($webRoot . $root . $type . $name, ['quality' => 95]);
            return $web . $root . $type . $name;
        }

        return true;
    }

    public static function imageSrc($folder, $type, $name)
    {
        $web = Yii::getAlias('@web');
        $root = '/uploads/albums/' . $folder;
        $type = '/' . $type;
        $name = '/' . $name;

        return $web . $root . $type . $name;
    }
}
