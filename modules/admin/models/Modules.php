<?php

namespace app\modules\admin\models;

use yii\db\Query;

class Modules extends \app\tables\Modules
{
    public function beforeSave () {
        if($this->isNewRecord) $this->deleted = 1;
        return true;
    }

    public static function allModules()
    {
        return (new Query())
            ->select([
                'modules.*'
            ])
            ->from('modules')
            ->all();
    }


    public static function getTypes()
    {
        return (new Query())
            ->distinct()
            ->select([
                'modules.type'
            ])
            ->where(['not', ['modules.type' => '']])
            ->from('modules')
            ->all();
    }
}