<?php

namespace app\modules\admin\models;

use app\modules\admin\lib\My;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ItemsSearch extends Model
{
    public $title;
    public $category;
    public $article;

    public function rules()
    {
        return [
            [['article', 'category', 'title'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Groups::find()
            ->asArray()
            ->select([
                'groups.id',
                'groups.title',
                'groups.status',
                'COUNT(items.id) as count',
            ])
        ->leftJoin('items','items.group.id = groups.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        My::sortAttr($dataProvider, 'category');

//        if (!($this->load($params) && $this->validate())) {
//            return $dataProvider;
//        }
//
//        $query->filterWhere([
//            'and',
//            ['items.article' => $this->article],
//            ['items.category_id' => $this->category],
//            ['like', 'items.title', $this->title],
//        ]);

        return $dataProvider;
    }
}