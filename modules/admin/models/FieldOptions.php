<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 26.02.16
 * Time: 18:18
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use himiklab\sortablegrid\SortableGridBehavior;
use yii\helpers\Json;

class FieldOptions extends \app\tables\FieldOptions
{
    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }

    public static function duplicate($from_id, $to_id)
    {
        $from = FieldOptions::find()
            ->where([
                'field_id' => $from_id
            ])
            ->orderBy('sort')
            ->all();

        $field = Field::findOne($to_id);
        $value = Json::decode($field['value']);
        $new_value = [];

        foreach ($from as $option) {
            $f = new FieldOptions();
            $f->attributes = $option->attributes;
            $f->field_id = $to_id;
            $f->save();

            $new_value['sort'][] =  $f->id;
            $new_value['price'][$f->id] = $value['price'][$option->id];
            $new_value['value'][$f->id] = $value['value'][$option->id];

        }

        $field->value = Json::encode($new_value);
        $field->save();

        return $field;
    }
}