<?php

namespace app\modules\admin\models;

use app\lib\My;
use app\lib\Route;
use app\tables\Types;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Routes extends \app\tables\Routes
{
    public $alias;
    public $parent;

    public function rules()
    {
        $parent = parent::rules();
        $parent[] = [['alias'], 'required'];
        $parent[] = [['parent'], 'string'];
        return $parent;
    }

    public function afterFind()
    {
        if ($this->url == '/') {
            $this->alias = '/';
        } else {
            $parts = explode('/', $this->url);
            $this->parent = join('/', array_slice($parts, 0, -1));
            $this->alias = end($parts);
        }
    }

    public function beforeValidate()
    {
        if (!$this->isNewRecord) {
            if ($this->parent == $this->oldAttributes['url']) return false;
        }

        if ($this->alias == '/') {
            $this->url = '/';
        } else {
            $this->alias = str_replace('/', '', $this->alias);
            $this->url = $this->parent . '/' . $this->alias;
        }

        if (!$this->text) $this->text = null;

        return true;
    }

    public function beforeSave()
    {
        if ($this->type_id) {
            /** @var $type Types */
            $type = Types::findOne($this->type_id);
            if ($type == null) return false;
            $this->element_title = Route::elementTitle($type->table, $this->element_id);
        }

        $this->lvl = count(explode('/', $this->url)) - 1;

        if ($this->parent) {
            $parent = Routes::findOne(['url' => $this->parent]);
            if ($parent == null) return false;
            $breadcrumbs = [];
            if ($parent->breadcrumbs) {
                $breadcrumbs = json_decode($parent->breadcrumbs, JSON_UNESCAPED_UNICODE);
            }
            $breadcrumbs[] = $parent->label;
            $this->breadcrumbs = json_encode($breadcrumbs, JSON_UNESCAPED_UNICODE);
        }

        $this->date = My::dateTime();

        return true;
    }

    public function attributeLabels()
    {
        $parent = parent::attributeLabels();
        $parent['alias'] = 'Псевдоним';
        $parent['parent'] = 'Родительский пункт';
        return $parent;
    }

    public static function elements($table)
    {
        $array = (new Query())
            ->select(['id', 'title'])
            ->from($table)
            ->orderBy(['date' => SORT_DESC])
            ->all();

        return ArrayHelper::map($array, 'id', 'title');
    }
}
