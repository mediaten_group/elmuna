<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 25.02.16
 * Time: 17:00
 */

namespace app\modules\admin\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class SchemeGroup extends \app\tables\SchemeGroup
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public static function duplicate($id)
    {
        $old_group = SchemeGroup::findOne($id);
        $new_group = new SchemeGroup();
        $new_group->attributes = $old_group->attributes;
        $new_group->save();

        $schemes = Scheme::find()->where(['group_id'=>$id])->all();
        foreach($schemes as $scheme){
            Scheme::duplicate($scheme->id, $new_group->id);
        }
        return $new_group;
    }
    
}