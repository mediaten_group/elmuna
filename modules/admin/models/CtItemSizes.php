<?php

namespace app\modules\admin\models;


class CtItemSizes extends \app\tables\CtItemSizes
{
    public function beforeSave()
    {
        $this->size = mb_strtoupper($this->size);
        return true;
    }
}
