<?php

namespace app\modules\admin\models;

use app\lib\My;
use Yii;
use yii\helpers\ArrayHelper;

class Pages extends \app\tables\Pages
{
    /*
    public function beforeSave($insert)
    {
        $this->date = My::dateTime();
        return true;
    }
    */

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'is_printable' => 'Есть версия для печати',
        ]);
    }
}
