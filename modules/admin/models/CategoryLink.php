<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 01.12.2015
 * Time: 16:52
 */

namespace app\modules\admin\models;


use yii\helpers\Json;
use yii\web\BadRequestHttpException;

class CategoryLink extends \app\tables\CategoryLink
{
    public static function addLink($data)
    {
        if (!isset($data['category_id'], $data['group_id'])) return ['status' => 'error', 'message' => 'Недостаточно параметров'];

        $link = new self;
        $link->category_id = $data['category_id'];
        $link->group_id = $data['group_id'];
        if ($link->save()) {
            return ['status' => 'ok'];
        } else {
            throw new BadRequestHttpException(Json::encode($link->errors));
        }
    }

    public static function removeLink($data)
    {
        if (!isset($data['category_id'], $data['group_id'])) return ['status' => 'error', 'message' => 'Недостаточно параметров'];

        $link = self::find()->where([
            'category_id' => $data['category_id'],
            'group_id' => $data['group_id']
        ])->one();
        if (!$link) return ['status' => 'error', 'message' => 'Не найдено'];

        if ($link->delete()) {
            return ['status' => 'ok'];
        } else {
            return $link->errors;
        }
    }

    public static function add($cat, $groups)
    {
        $errors = [];
        foreach ($groups as $group) {
            $link = self::find()->where([
                'category_id' => $cat,
                'group_id' => $group
            ])->one();
            if (!$link) {
                $link = new self;
                $link->category_id = $cat;
                $link->group_id = $group;
                if (!$link->save()) {
                    $errors[] = $link->errors;
                }
            }
            $group_rec = Groups::findOne($group);
            $group_rec->status = 1;
            if ($group_rec->isAttributeChanged('status')) {
                $group_rec->save();
            }
            $items = Items::findAll(['group_id'=>$group]);
            foreach ($items as $item) {
                $item->status = 1;
                if ($item->isAttributeChanged('status')) {
                    $item->save();
                }
            }
        }
        if (!$errors)
            return true;
        else
            return $errors;
    }

    public static function remove($cat, $groups)
    {
        $errors = [];
        foreach ($groups as $group) {
            $link = self::find()->where([
                'category_id' => $cat,
                'group_id' => $group
            ])->one();
            if ($link) {
                if (!$link->delete()) {
                    $errors[] = $link->errors;
                }
            }
        }
        if (!$errors)
            return true;
        else
            return $errors;
    }

}