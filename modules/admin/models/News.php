<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 18.01.2016
 * Time: 12:56
 */

namespace app\modules\admin\models;



use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class News extends \app\tables\News
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }
}