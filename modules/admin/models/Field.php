<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 26.02.16
 * Time: 18:18
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use himiklab\sortablegrid\SortableGridBehavior;

class Field extends \app\tables\Field
{
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public static function duplicate($id, $scheme_id = 0)
    {
        $old_field = Field::findOne($id);
        $new_field = new Field();
        $new_field->attributes = $old_field->attributes;
        $new_field->scheme_id = ($scheme_id) ? $scheme_id : $old_field->scheme_id;
        $new_field->save();

        if ($new_field->getFieldType()->one()->type == 'select') {
            FieldOptions::duplicate($old_field->id,$new_field->id);
        }

        return $new_field;
    }
}