<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 27.11.2015
 * Time: 13:36
 */

namespace app\modules\admin\models;


use app\behaviors\DateTimeBehavior;
use app\modules\admin\lib\My;
use app\modules\upload\behaviors\UploadBehavior;
use app\modules\upload\models\Upload;
use yii\db\ActiveQuery;
use yii\web\UploadedFile;

/**
 * Class Groups
 * @package app\modules\admin\models
 * @property Items $_item
 * @property array $count
 */
class Groups extends \app\tables\Groups
{
    private $_item;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (!$this->created_at) $this->created_at = My::dateTime();
        $this->updated_at = My::dateTime();

        if ($this->_upload) {
            $this->cover_image_id = $this->_upload[0];
            if (!$this->isNewRecord && $this->oldAttributes['cover_image_id']) {
                Upload::remove($this->oldAttributes['cover_image_id']);
            }
        }

        if (isset($this->_item->title)) {
            $this->title = $this->_item->title;
        }
        if (isset($this->_item->cover_image_id)) {
            $this->cover_image_id = $this->_item->cover_image_id;
        }
        if (isset($this->_item->status)) {
            $this->status = $this->_item->status;
        }

        return true;
    }

    /**
     * @param int $category_id
     * @param string $q
     * @return ActiveQuery $this
     */
    public static function getItemsByGroups($category_id = null,$q = null)
    {
        $where = [];
        $w = '';
        if ($category_id) {
            $where = "categories.\"path\" && string_to_array('$category_id', ',') :: int[]";
        }
        $items_data = <<<SQL
SELECT jsonb_agg(
  jsonb_build_object('id', items.id, 'title', items.title, 'status', items.status, 'set_price', items.set_price)
)
FROM items
WHERE items.group_id = groups.id
SQL;

        return self::find()
            ->asArray()
            ->select([
                'groups.id',
                'groups.title',
                "($items_data) AS items_data",
                'groups.sort',
                "(select max(sort) from groups limit 1) as sort_max",
                'upload.name AS image'
            ])
            ->where($where)
            ->andFilterWhere(['ilike','groups.title',$q])
            ->leftJoin('category_link', 'category_link.group_id = groups.id')
            ->leftJoin('categories', 'categories.id = category_link.category_id')
            ->leftJoin('upload','upload.id = groups.cover_image_id')
            ->groupBy('groups.id, upload.name')
            ->orderBy('sort ASC, id DESC');
    }

    /**
     * @param Items $item
     */
    public function populateItem(Items $item)
    {
        $this->_item = $item;
    }

    public function getCount()
    {
        $group = self::tableName();
        $item = Items::tableName();

        return $this->hasOne(self::className(), ['id' => 'id'])
            ->asArray()
            ->select([
                "[[$group]].[[id]]",
                "COUNT([[$item]].[[id]]) AS value",
            ])
            ->leftJoin($item, "[[$item]].[[group_id]] = [[$group]].[[id]]")
            ->groupBy("[[$group]].[[id]]");
    }
}
