<?php

namespace app\modules\admin\models;

use app\lib\RebuildOrdering;
use app\modules\admin\lib\My;
use app\tables\CategoriesGifts;
use Yii;
use yii\db\Query;


class Parser extends \app\tables\ParserGifts
{

    public static function saveResults($data)
    {
        $db = Yii::$app->getDb();
        $keys = array_keys($data);
//        pree($keys);
        $db->createCommand()->insert('parser_gifts', $data)->execute();
//        $db->createCommand()->batchInsert('parser_gifts', $keys, $data)->execute();
    }

    public static function addCategory($data, $parent)
    {
        if (CategoriesGifts::find()->select(['id'])->where(['alias' => $data['alias']])->one()) return false;

        $parent_id = CategoriesGifts::find()->select(['id'])->where(['alias' => $parent])->one();
        $data['parent_id'] = $parent_id['id'];
        $db = Yii::$app->getDb();

        $db->createCommand()->insert('categories_gifts', $data)->execute();
        return TRUE;
    }

    public static function addImage($name, $article)
    {
        $data['name'] = $name;
        $data['article'] = $article;
        $data['date'] = My::dateTime();
        $db = Yii::$app->getDb();

        $db->createCommand()->insert('item_images_gifts', $data)->execute();
    }

    public static function addItem($data)
    {
        $db = Yii::$app->getDb();

        $db->createCommand()->insert('items_gifts', $data)->execute();
    }

    public static function getCategoryId($alias)
    {
        return (new Query())
            ->select([
                'id'
            ])
            ->where([
                'alias' => $alias
            ])
            ->from('categories_gifts')
            ->one();
    }

    public static function updateCategories($id, $image)
    {
        $db = Yii::$app->getDb();

        $db->createCommand()->update('categories_gifts', ['cover' => $image], ['id' => $id])->execute();
    }

    public static function getImage($category_id)
    {
        return (new Query())
            ->select([
                'items_gifts.article',
                'items_gifts.id',
                'item_images_gifts.name',
            ])
            ->where([
                'items_gifts.category_id' => $category_id,
            ])
            ->andWhere(['not', ['items_gifts.id' => null]])
            ->leftJoin('item_images_gifts', 'item_images_gifts.article = items_gifts.article')
            ->from(['items_gifts'])
            ->all();
    }

    public static function rebuildOrdering()
    {
        $rows = (new Query)
            ->select(['id', 'pid' => 'parent_id'])
            ->from('categories_gifts')
            ->orderBy(['parent_id' => SORT_ASC])
            ->all();

        $array = [];
        foreach ($rows as $row) {
            $array[$row['pid']][] = $row['id'];
        }
//        pree($array);

        $rebuild = new RebuildOrdering;
        $rebuild->array = $array;
        $rebuild->rebuild($array);
        $res = array_flip($rebuild->res);

        $q = '';
        foreach($res as $key => $value){
            $q .= "UPDATE categories_gifts SET ordering='$value' WHERE id='$key'; ";
        }
        Yii::$app->getDb()->createCommand($q)->execute();
//        pree($res);
    }

    public static function rebuildLevel()
    {
        $query = new Query;
        $query->select('id, level, parent_id')
            ->from('categories_gifts')
            ->orderBy(['parent_id' => SORT_ASC]);
        $rows = $query->all();
        $array = [];
        foreach ($rows as $val) {
            $array[$val['id']] = $val;
        }
        $q = '';
        $i = 0;
        foreach ($array as $k => $v) {
            if ($v['parent_id'] == 0) {
                $array[$k]['level_new'] = 0;
                $q .= "UPDATE categories_gifts SET level='0' WHERE id='$k'; ";
            } else {
                $newLevel = isset($array[$v['parent_id']]) ? $array[$v['parent_id']]['level_new'] + 1 : 0;
                $array[$k]['level_new'] = $newLevel;
                $q .= "UPDATE categories_gifts SET level='$newLevel' WHERE id='$k';";
            }
            $i++;
        }
//        pree($q);
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($q);
        $command->execute();
    }
}