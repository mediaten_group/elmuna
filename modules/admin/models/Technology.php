<?php

namespace app\modules\admin\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;
use app\tables\TechnologyUploads;
use yii;
use yii\helpers\ArrayHelper;

class Technology extends \app\tables\Technology
{
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

    public function addImages($images)
    {
        $ids = ArrayHelper::getValue($images, '_upload');
        if ($ids) {
            foreach ($ids as $id) {
                $model = new TechnologyUploads();
                $model->upload_id = $id;
                $model->technology_id = $this->id;
                $model->save();
            }
        }
        return TRUE;
    }
}