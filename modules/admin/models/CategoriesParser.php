<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 04.08.2016
 * Time: 9:25
 */

namespace app\modules\admin\models;


class CategoriesParser extends \app\tables\CategoriesParser
{

    public static function getList()
    {
        $sql = <<<SQL
SELECT 
id, source, title, parent_id
FROM categories_parser cp1
GROUP BY cp1.id        
HAVING (SELECT COUNT(*)
        FROM categories_parser cp2
        WHERE cp2.parent_id = cp1.id) = 0
ORDER BY source,cp1.id
SQL;
        $items = \Yii::$app->getDb()->createCommand($sql)->queryAll();

        $list = [];

        foreach ($items as &$item) {
            while ($item['parent_id']) {
                $parent = self::findOne($item['parent_id']);
                $item['parent_id'] = $parent->parent_id;
                $item['title'] = "$parent->title ⇒ $item[title]";
            }
            $list[$item['id']] = "$item[source] ⇒ $item[title]";
        }
        asort($list);

        return $list;
    }

}