<?php

namespace app\modules\admin\models;

use app\modules\admin\lib\My;

class CtOrders extends \app\tables\CtOrders
{
    public function beforeSave()
    {
        $this->date = $this->date ?: My::dateTime();
        return true;
    }

    public function checkout()
    {
        return true;
    }
}
