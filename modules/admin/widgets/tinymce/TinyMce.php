<?php

namespace app\modules\admin\widgets\tinymce;

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 31.03.16
 * Time: 15:42
 */
use yii\bootstrap\Html;
use app\modules\admin\assets\TinyMceAsset;
use yii\widgets\InputWidget;

class TinyMce extends InputWidget
{

    public $language;
    public $clientOptions = [];
    public $triggerSaveOnBeforeValidateForm = true;

    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $view = $this->getView();

        TinyMceAsset::register($view);

        $id = $this->options['id'];

        $js = <<<JS

tinymce.init({
  selector: '#$id',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    // '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    '//www.tinymce.com/css/codepen.min.css',
    '/css/style.css'
  ],
  extended_valid_elements: 'span[*],script[*]',
  language: 'ru',
});

JS;
        $view->registerJs($js);

    }

}