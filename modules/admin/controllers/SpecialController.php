<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.03.16
 * Time: 18:27
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\Special;
use himiklab\sortablegrid\SortableGridAction;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii;
use yii\db\Exception;
use yii\helpers\Json;

class SpecialController extends Controller
{

    public $title = 'Спецпредложения';

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Special::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;
        
        $model = Special::find()->orderBy('sort');
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Special */
        $model = new Special();
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }
        
        return $this->render('create', [
            'model'=>$model
        ]);
    }
    
    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Special */
        $model = Special::findOne($id);
        if ($model->load($req->post()) && $model->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        return $this->render('create', [
            'model'=>$model
        ]);        
    }

    public function actionDelete($id)
    {
        /** @var Special $news */
        $model = Special::findOne($id);

        if (!$model->delete()) throw new Exception(Json::encode($news->errors));

        return $this->redirect('index');
    }

}