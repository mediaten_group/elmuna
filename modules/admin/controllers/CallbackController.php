<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.01.2016
 * Time: 15:35
 */

namespace app\modules\admin\controllers;


use app\lib\My;
use app\modules\admin\models\Callback;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class CallbackController extends Controller
{
    public $title = 'Звонки';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Callback::find()->asArray()->with('user');

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'status' => SORT_ASC
                ],
                'attributes' => [
                    'created_at', 'status'
                ]
            ],
        ]);

        $this->view->params['breadcrumbs'][] = ['label' => $this->title];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $req = Yii::$app->request;

        /** @var $call Callback */
        $call = Callback::find()->where(['id' => $id])->with('user')->one();

        if ($call->load($req->post()) && $call->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url' => Url::to(['/admin/callback/index'])];
        $this->view->params['breadcrumbs'][] = ['label' => $call->name];

        $this->view->title = 'Звонок ' . $call->name;

        return $this->render('view', [
            'call' => $call,
        ]);
    }
}