<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 11:39
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\FieldColors;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Url;
use Yii;

class FieldColorsController extends Controller
{

    public $title = 'Цвета нанесения';

    public function actionIndex()
    {
        Url::remember();

        $this->view->title = $this->title;

        $query = FieldColors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model FieldColors */
        $model = new FieldColors();

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model FieldColors */
        $model = FieldColors::findOne($id);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    public function actionDelete($id)
    {
        /** @var FieldColors $model */
        $model = FieldColors::findOne($id);
        if ($model->delete())
            return $this->redirect('index');

        return $model->errors;

    }
    
}