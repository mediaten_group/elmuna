<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 27.11.2015
 * Time: 12:32
 */

namespace app\modules\admin\controllers;


use app\modules\admin\lib\My;
use app\modules\admin\models\Categories;
use app\modules\admin\models\CategoryLink;
use app\modules\admin\models\Groups;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class GroupsController extends Controller
{
    public $title = 'Группы';

    public function actionIndex($id = null)
    {
        Url::remember();

        $this->view->title = $this->title;

//        $categories = Categories::getCatTree();

        $groups = Groups::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $groups,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
//            'categories' => $categories,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $this->view->params['breadcrumbs'][] = 'Добавить группу товаров';

        $req = Yii::$app->request;

        /** @var $model Groups */
        $model = new Groups;

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjax($action)
    {
        $req = Yii::$app->request;
        if (!$req->isAjax) throw new NotFoundHttpException;

        Yii::$app->response->format = Response::FORMAT_JSON;

        switch ($action) {
            case 'add':
                CategoryLink::addLink($req->post());

                break;
            case 'remove':
                CategoryLink::removeLink($req->post());

                break;
        }

        return false;
    }

    public function actionUpdate($id)
    {
        $this->view->title = 'Редактировать группу';

        $req = Yii::$app->request;

        /** @var $model Groups */
        $model = Groups::findOne($id);

        $categories = Categories::getCatTree();

        if ($model->load($req->post()) && $model->save() && !$req->post('btn-save')) {
            return $this->redirect('index');
        }

        $links = CategoryLink::find()->asArray()->select(['category_id'])->where(['group_id' => $model->id])->column();

        return $this->render('update', [
            'model' => $model,
            'categories' => $categories,
            'links' => $links,
        ]);
    }

    public function actionDelete($id)
    {
        /** @var Groups $model */
        $model = Groups::findOne($id);
        if ($model->delete())
            return $this->redirect('index');

        return $model->errors;
    }

}