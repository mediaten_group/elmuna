<?php

namespace app\modules\admin\controllers;

use app\modules\admin\lib\My;
use app\modules\admin\models\Categories;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CategoriesController extends Controller
{
    public $title = 'Категории';

    public function actionIndex($id = null)
    {
        Url::remember();

        $categories = Categories::getCatTree();

        $this->view->params['h1'] = $this->view->title = $this->view->params['breadcrumbs'][] = $this->title;

        return $this->render('index', [
            'categories' => $categories,
        ]);

        $links = ['homeLink' => ['label' => 'Категории', 'url' => ['index']]];

        $models = Categories::find()->where(['pid' => $id])->asArray();

        $this->view->params['current'] = Categories::getCurrentLevel($id);
        $this->view->params['breadcrumbs'] = My::getLinks($this->view->params['current']['id']);
        $this->view->title = $this->title;

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'level' => SORT_ASC,
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'links' => $links,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

//        $this->view->params['breadcrumbs'][] = 'Добавить категорию';

        $req = Yii::$app->request;
        /** @var $model Categories */
        $model = new Categories;
        $catList = Categories::getCatList();
        $parent = Categories::getCurrentLevel($req->get('pid'));
        $this->view->params['breadcrumbs'] = My::getLinks($req->get('pid'), 'Добавить');

        if ($parent['level'] == 2) {
            $model->lvl2 = $parent['id'];
            $model->lvl1 = Categories::getPid($model->lvl2);
        }
        if ($parent['level'] == 1) {
            $model->lvl1 = $parent['pid'];
        }

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
            'catList' => $catList,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Categories */
        $model = Categories::findOne($id);
        if (!$model) throw new NotFoundHttpException;

        if (!$model->alias) $model->title = null;

//        $this->view->params['breadcrumbs'] = My::getLinks($model->id, 'Редактировать');

        if ($model->load($req->post()) && $model->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Categories::deleteAll(['id' => $id]);
        return $this->redirect('index');
    }

    public function actionSubCat()
    {
        $req = Yii::$app->request;
        $parents = $req->post('depdrop_parents');
        if (!isset($parents[0]) || !$parents[0]) return Json::encode(['output' => '', 'selected' => '']);

        $out = Categories::getSubCatList($parents[0]);
        if (!$out) $out = '';
        // the getSubCatList function will query the database based on the
        // cat_id and return an array like below:
        // [
        //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
        //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
        // ]
        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function actionProd()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            $subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                $data = Categories::getProdList($cat_id, $subcat_id);
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */

                echo Json::encode(['output' => $data['out'], 'selected' => $data['selected']]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionAjax($action)
    {
        $req = Yii::$app->request;
        if (!$req->isAjax) throw new NotFoundHttpException;
        
        Yii::$app->response->format = Response::FORMAT_JSON;

        switch ($action) {
            case 'create':
                return Categories::actionCreate($req->post());

                break;

            case 'rename':
                return false;
                return Categories::actionRename($req->post());

                break;

            case 'delete':
                return Categories::actionDelete($req->post('id'));

                break;

            case 'move':
                return Categories::actionMove($req->post());

                break;
        }

        return false;
    }
}
