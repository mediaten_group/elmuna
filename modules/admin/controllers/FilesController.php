<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 09.08.2016
 * Time: 14:52
 */

namespace app\modules\admin\controllers;


use yii\web\Controller;

class FilesController extends Controller
{

    public function actionIndex()
    {
        $this->view->title = 'Файловый менеджер';
        return $this->render('index', []);
    }

}