<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 18.01.2016
 * Time: 12:53
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;

class NewsController extends Controller
{
    public $title = 'Новости';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = News::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model News */
        $model = new News;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model News */
        $model = News::findOne($id);

        if ($model->load($req->post()) && $model->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        /** @var News $news */
        $news = News::findOne($id);

        if (!$news->delete()) throw new Exception(Json::encode($news->errors));

        return $this->redirect('index');
    }
}