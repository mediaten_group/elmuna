<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 25.02.16
 * Time: 16:47
 */

namespace app\modules\admin\controllers;

use app\lib\FieldForm;
use app\modules\admin\models\Field;
use app\modules\admin\models\Scheme;
use app\modules\admin\models\Technology;
use app\tables\FieldType;
use himiklab\sortablegrid\SortableGridAction;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;

class SchemeController extends Controller
{

    public $title = 'Схемы';

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Field::className(),
            ],
        ];
    }

    public function actionIndex($id = null)
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Scheme::find()
            ->asArray()
            ->select([
                'scheme.id',
                'scheme.name as name',
                'technology.title as tech_name',
                'scheme_group.name as group_name',
            ])
            ->leftJoin('scheme_group', 'scheme.group_id = scheme_group.id')
            ->leftJoin('technology', 'scheme.tech_id = technology.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate($pid)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Scheme */
        $model = new Scheme();
        $model->group_id = $pid;
        if ($model->load($req->post()) && $model->save()) {
            if ($req->post('btn-close')) return $this->redirect(['/admin/group-scheme/update', 'id' => $pid]);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $models = Scheme::find()->where(['group_id' => $pid])->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $models,
        ]);
        $techs = Technology::find()->asArray()->select('title')->indexBy('id')->column();

        return $this->render('create', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'pid' => $pid,
            'technology' => $techs,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;
//        dd($req->post());
        /** @var $model Scheme */
        $model = Scheme::findOne($id);
        if ($model->load($req->post()) && $model->save()) {
            if ($req->post('btn-close')) return $this->redirect(['/admin/group-scheme/update', 'id' => $model->group_id]);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $models = Scheme::find()->where(['group_id' => $model->group_id])->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $models,
        ]);
        $techs = Technology::find()->asArray()->select('title')->indexBy('id')->column();

        $fields = Field::find()->where(['scheme_id'=>$id])->asArray();
        $fieldsProvider = new ActiveDataProvider([
            'query' => $fields,
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_ASC,
                ],
            ],
        ]);
        $fieldsType = FieldType::find()->select(['title'])->asArray()->indexBy('id')->column();

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'technology' => $techs,
            'fieldsProvider' => $fieldsProvider,
            'fields' => $fieldsType,
        ]);

    }

    public function actionCopy($id)
    {
        $new = Scheme::duplicate($id);
        return $this->redirect(['update', 'id' => $new->id]);
    }

    public function actionDelete($id)
    {
        /** @var Scheme $model */
        $model = Scheme::findOne($id);
        if ($model->delete())
            return $this->redirect(['/admin/group-scheme/update','id'=>$model->group_id]);

        return $model->errors;
    }

}