<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 26.02.16
 * Time: 18:33
 */

namespace app\modules\admin\controllers;

use app\lib\FieldForm;
use app\modules\admin\models\Field;
use app\modules\admin\models\FieldOptions;
use app\tables\FieldColors;
use app\tables\FieldType;
use himiklab\sortablegrid\SortableGridAction;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;


class FieldController extends Controller
{

    public $title = 'Поля';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $query = Field::find()
            ->asArray()
            ->select([
                'field.id',
                'name',
                'field_type.title'
            ])
            ->leftJoin('field_type','field_type.id = field.field_type_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate($scheme_id,$type_id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Field */
        $model = new Field();
        $model->scheme_id = $scheme_id;
        $model->field_type_id = $type_id;
        if ($model->load($req->post()) && $model->save() && FieldForm::saveForm($model->id,$req->post('value'))) {
            if ($req->post('btn-close')) return $this->redirect(['/admin/scheme/update', 'id' => $model->scheme_id]);
            return $this->redirect(['/admin/scheme/update', 'id' => $model->scheme_id]);
        }

        $type = FieldType::find()->asArray()->select(['title'])->indexBy('id')->column();
        $fieldForm = new FieldForm();
        $fieldForm = $fieldForm->getBlankForm($type_id);

        return $this->render('create', [
            'model' => $model,
            'type' => $type,
            'fieldForm' => $fieldForm,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Field */
        $model = Field::findOne($id);
        if ($model->load($req->post()) && $model->save() && (FieldForm::saveForm($id,$req->post('value')))) {
            if ($req->post('btn-close')) return $this->redirect(['/admin/scheme/update', 'id' => $model->scheme_id]);
            return $this->redirect(['/admin/scheme/update', 'id' => $model->scheme_id]);
        }

        $type = FieldType::find()->asArray()->select(['title'])->indexBy('id')->column();
        $fieldForm = new FieldForm();
        $fieldForm = $fieldForm->getForm($id);

        return $this->render('create', [
            'model' => $model,
            'type' => $type,
            'fieldForm' => $fieldForm,
        ]);

    }

    public function actionDelete($id)
    {
        /** @var Field $model */
        $model = Field::findOne($id);
        $scheme = $model->scheme_id;
        if ($model->delete()) {
            return $this->redirect(['/admin/scheme/update','id'=>$scheme]);
        }

        return $model->errors;
    }

    public function actionCopy($id)
    {
        $new = Field::duplicate($id);
        return $this->redirect(['update','id'=>$new->id]);
    }
    
    public function actionCreateOption()
    {
        $req = Yii::$app->request;
        $id = $req->post('id');
        /** @var $model FieldOptions */
        $model = new FieldOptions();
        $model->field_id = $id;
        if ($model->save())
            return $model->id;
        else
            return FALSE;
    }
    
    public function actionDeleteOption()
    {
        $req = Yii::$app->request;
        $id = $req->post('id');
        if(FieldOptions::findOne($id)->delete())
            return TRUE;
        else
            return FALSE;
    }

}