<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.01.2016
 * Time: 15:35
 */

namespace app\modules\admin\controllers;


use app\lib\My;
use app\tables\Review;
use app\tables\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;

class ReviewsController extends Controller
{
    public $title = 'Отзывы';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Review::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'status' => SORT_ASC
                ],
                'attributes' => [
                    'created_at', 'status'
                ]
            ],
        ]);

        $this->view->params['breadcrumbs'][] = ['label' => $this->title];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $req = Yii::$app->request;

        /** @var $review Review */
        $review = Review::findOne($id);

        if ($review->load($req->post()) && $review->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        $user = User::find()->select(['id', 'username'])->where(['id' => $review->user_id])->with('userProfiles')->one();

        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url' => Url::to(['/admin/reviews/index'])];
        $this->view->params['breadcrumbs'][] = ['label' => 'Просмотр отзыва'];

        $this->view->title = 'Просмотр отзыва';

        return $this->render('view', [
            'user' => $user,
            'review' => $review,
        ]);
    }

    public function actionDelete($id)
    {
        /** @var Review $review */
        $review = Review::findOne($id);

        if (!$review->delete()) throw new Exception(Json::encode($review->errors));

        return $this->redirect('index');
    }
}