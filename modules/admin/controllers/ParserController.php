<?php

namespace app\modules\admin\controllers;

use app\modules\admin\lib\My;
use app\modules\admin\models\Parser;
use app\tables\CategoriesGifts;
use app\tables\ItemImagesGifts;
use app\tables\ItemsGifts;
use app\tables\ParserGifts;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class ParserController extends Controller
{
    public $title = 'Мистер Парсер';
    public static $message = [];

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = $this->title;

        $models = ParserGifts::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDo()
    {
        $this->view->title = $this->title;

        return $this->render('parser');
    }

    public function actionAjax()
    {
        $req = Yii::$app->request;
        $article = $req->post('article');

        $message = self::parse(trim($article));
        if (!$message) {
            $message = ['status' => 'error'];
        }

        return json_encode($message);
    }

    public function actionCategories()
    {
//        $source = Parser::find()->select(['categories'])->all();
//        $i = 0;
//        foreach ($source as $s) {
//            $array = json_decode($s['categories']);
//            $prev = null;
//            if ($array) {
//                foreach ($array as $arr) {
//                    $data['alias'] = end(explode('/', $arr[0]));
//                    if ($data['alias']) {
//                        $data['title'] = $arr[1];
//                        $data['level'] = array_search($arr, $array);
////                    echo $level . ' ' . $alias . ' ' . $title . ' Parent: ' . $prev . '<br>';
//                        Parser::addCategory($data, $prev);
//                        $prev = $data['alias'];
//                    }
//                }
//            }
//            $i++;
////            if ($i > 20) break;
//        }

//        $source = CategoriesGifts::find()->where(['level' => 0, 'cover' => null])->all();

//        $dir = '@webroot/uploads/cgifts/';
//        $dir = Yii::getAlias($dir);
//        My::checkDir($dir);

        /*foreach ($source as $s) {
            if (!$s['cover']) {
                $id = $s['id'];

                $path = $dir . $id . '/';
                My::checkDir($path);


                $image = CategoriesGifts::find()->where(['parent_id' => $id])->andWhere(['not', ['cover' => null]])->one();

                if (is_file($path = $dir . $image['id'] . '/'.$image['cover'])) {
                    copy($path = $dir . $image['id'] . '/'.$image['cover'], $path . $image['cover']);
                    chmod($path . $image['cover'], 0777);
                    Parser::updateCategories($id, $image['cover']);
                    break;
                }
//                $image = [];
//                $images = Parser::getImage($id);
//                foreach ($images as $i) {
//                    if ($i['name']) {
//                        $image['article'] = $i['article'];
//                        $image['name'] = $i['name'];
//                        $item_image = Yii::getAlias('@webroot/uploads/pgifts/' . $image['article']) . '/thumbs/' . $image['name'];
//                        if (is_file($item_image)) {
//                            copy($item_image, $path . $image['name']);
//                            chmod($path . $image['name'], 0777);
//                            Parser::updateCategories($id, $image['name']);
//                            break;
//                        }
//                    }
//                }
            }
        } */
//        Parser::rebuildOrdering();
        $data['level2'] = CategoriesGifts::find()->where(['level' => 2])->count();
        $data['level1'] = CategoriesGifts::find()->where(['level' => 1])->count();
        $data['level0'] = CategoriesGifts::find()->where(['level' => 0])->count();
        $model = CategoriesGifts::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('categories', [
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    public function actionItems()
    {
        $source = Parser::find()->all();

//        $runtime = '@runtime/parser_gifts/';
//        $runtime = Yii::getAlias($runtime);
//
//        $uploads = '@webroot/uploads/pgifts/';
//        $uploads = Yii::getAlias($uploads);
//        My::checkDir($uploads);

//        foreach ($source as $s) {
//            if ($s['images_min']) {
//                $images = json_decode($s['images_min']);
//
//                if(preg_match('/.*?(\<).*?/i', $s['images_big'])) {
//                    $images_big = null;
//                } else {
//                    $images_big = json_decode($s['images_big']);
//                }
//
//                $runtime_dir = $runtime . $s['article'] . '/';
//
//                $uploads_dir = $uploads . $s['article'] . '/';
//
//                if (is_dir($runtime_dir) && !is_dir($uploads_dir)) {
//                    $uploads_dir_thumbs = $uploads . $s['article'] . '/thumbs/';
//                    My::checkDir($uploads_dir);
//                    My::checkDir($uploads_dir_thumbs);
//
//                    $i = 1;
//                    $j = 0;
//                    if (is_array($images)) {
//                        foreach ($images as $img) {
//                            $new_filename = My::uniqueName($uploads_dir);
//                            $old_filename = $i . '_280.jpg';
//                            if (is_file($runtime_dir . "280/" . $old_filename)) {
//                                copy($runtime_dir . "280/" . $old_filename, $uploads_dir_thumbs . $new_filename);
//                                chmod($uploads_dir_thumbs . $new_filename, 0777);
//                            } else {
//                                $file = file_get_contents($img);
//                                file_put_contents($uploads_dir_thumbs . $new_filename, $file);
//                                chmod($uploads_dir_thumbs . $new_filename, 0777);
//                            }
//                            $old_filename = $i . '_1000.jpg';
//                            if (is_file($runtime_dir . "1000/" . $old_filename)) {
//                                copy($runtime_dir . "1000/" . $old_filename, $uploads_dir . $new_filename);
//                                chmod($uploads_dir . $new_filename, 0777);
//                            } else {
//                                if(isset($images_big[$j])) {
//                                    $file = file_get_contents($images_big[$j]);
//                                    file_put_contents($uploads_dir . $new_filename, $file);
//                                    chmod($uploads_dir . $new_filename, 0777);
//                                }
//                            }
//                            Parser::addImage($new_filename, $s['article']);
//                            $i++;
//                            $j++;
//                        }
//                    }
//                }
//            }
//        }

//        foreach ($source as $s) {
//            if($s['id']) {
//                $data = $s->attributes;
//                unset($data['categories']);
//                unset($data['images_min']);
//                unset($data['images_big']);
//                $data['category_id'] = Parser::getCategoryId(end(explode('/', end(json_decode($s['categories']))[0])))['id'];
//                Parser::addItem($data);
//            }
//        }
        $model = ItemsGifts::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('items', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImages()
    {
        $model = ItemImagesGifts::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('images', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $article
     * @return array
     */
    public static function parse($article)
    {
        $explode = explode('.', $article);
        if ($explode && count($explode) > 1) {
            $end = array_pop($explode);
            if (iconv_strlen($end) > 2) {
                $end = substr($end, 0, 2);
                $explode[] = $end;
                $article = implode('.', $explode);
            }
        }

        if (ParserGifts::findOne($article)) return ['status' => 'bad', 'message' => 'уже есть', 'data' => $article, 'number' => 0];

        $link = file_get_contents('http://gifts.ru/id?p_p_id=Header_WAR_LogrusRuntimeportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&_Header_WAR_LogrusRuntimeportlet_logrus_ajax=autoSearch&autosearch=' . $article . '&_=1439365489454');

        $h = json_decode($link);

        if (!$h) {
            $data['article'] = $article;
            Parser::saveResults($data);
            $message = ['status' => 'bad', 'message' => 'Не удалось получить товар по артикулу', 'data' => $article, 'number' => 1];
            return $message;
        }

        $reg_exp = '/href="(.*?)"/i';

        preg_match($reg_exp, $h->results[0]->items[0]->html, $link);

        $id = array_pop(explode('/', $link[1]));

        $dir = '@runtime/parser_gifts/';
        $dir = Yii::getAlias($dir);
        My::checkDir($dir);

        $dir .= $article . '/';
        My::checkDir($dir);

        $file = 'index.html';


        $content = file_exists($dir . $file) ? $dir . $file : 'http://gifts.ru' . $link[1];

        $html = file_get_contents($content);

        if (!file_exists($dir . $file)) file_put_contents($dir . $file, $html);
        chmod($dir . $file, 0777);

        $data = [];

        $data['id'] = $id;

        $data['article'] = $article;

        $reg_exp = '/<h1.*?>(.*?)<\/h1>/i';
        preg_match($reg_exp, $html, $data['title']);
        $data['title'] = $data['title'] ? $data['title'][1] : null;

        $reg_exp = '/<span class=\"amount\">(.*?)<\/span>/i';
        preg_match($reg_exp, $html, $data['price']);
        $data['price'] = $data['price'] ? strip_tags($data['price'][1]) : null;

        $reg_exp = '/Размеры: (.*?)</i';
        preg_match($reg_exp, $html, $data['sizes']);
        $data['sizes'] = $data['sizes'] ? strip_tags($data['sizes'][1]) : null;

        $reg_exp = '/Материал: (.*?)</i';
        preg_match($reg_exp, $html, $data['material']);
        $data['material'] = $data['material'] ? strip_tags($data['material'][1]) : null;

        $reg_exp = '/Вес \(1 шт\.\): (.*?)</i';
        preg_match($reg_exp, $html, $data['weight']);
        $data['weight'] = $data['weight'] ? $data['weight'][1] : null;

        $reg_exp = '/Размеры коробки: (.*?)</i';
        preg_match($reg_exp, $html, $data['korob_size']);
        $data['korob_size'] = $data['korob_size'] ? $data['korob_size'][1] : null;

        $reg_exp = '/Вес коробки: (.*?)</i';
        preg_match($reg_exp, $html, $data['korob_weight']);
        $data['korob_weight'] = $data['korob_weight'] ? $data['korob_weight'][1] : null;

        $reg_exp = '/Объем коробки: (.*?)</i';
        preg_match($reg_exp, $html, $data['korob_vol']);
        $data['korob_vol'] = $data['korob_vol'] ? $data['korob_vol'][1] : null;

        $reg_exp = '/Количество в коробке: (.*?)</i';
        preg_match($reg_exp, $html, $data['korob_count']);
        $data['korob_count'] = $data['korob_count'] ? $data['korob_count'][1] : null;

        $reg_exp = '/itm-logo"><a href="http:\/\/gifts\.ru\/catalog\/brand-(.*?)"/i';
        preg_match($reg_exp, $html, $data['brand']);
        $data['brand'] = $data['brand'] ? $data['brand'][1] : null;

        $reg_exp = '/<a itemprop="item" href="(.*?)"/i';
        preg_match_all($reg_exp, $html, $data['categories_alias']);
        $data['categories_alias'] = $data['categories_alias'] ? $data['categories_alias'][1] : null;

        $reg_exp = '/<span itemprop="name">(.*?)<\/span>/i';
        preg_match_all($reg_exp, $html, $data['categories_title']);
        $data['categories_title'] = $data['categories_title'] ? $data['categories_title'][1] : null;

        $data['categories'] = [];
        $j = 0;
        foreach ($data['categories_title'] as $cat) {
            $data['categories'][] = [$data['categories_alias'][$j], $cat];
            $j++;
        }

        $reg_exp = '/Виды нанесения: (.*?)<\/li/i';
        preg_match($reg_exp, $html, $data['technology']);
        $data['technology'] = $data['technology'] ? $data['technology'][1] : null;

        $reg_exp = '/a class.*?">(.*?)<\/a>/i';
        preg_match_all($reg_exp, $data['technology'], $data['technology_names']);
        $data['technology_names'] = $data['technology_names'] ? $data['technology_names'][1] : null;

        $reg_exp = '/" href="(.*?)">/i';
        preg_match_all($reg_exp, $data['technology'], $data['technology_pathes']);
        $data['technology_pathes'] = $data['technology_pathes'] ? $data['technology_pathes'][1] : null;

        $data['technologies'] = [];
        $j = 0;
        foreach ($data['technology_names'] as $tech) {
            $data['technologies'][] = [$data['technology_pathes'][$j], $tech];
            $j++;
        }
        $data['technologies'] = $data['technologies'] ? $data['technologies'] : '';

        $reg_exp = '/(<div id="j_gal".*?)<\/div><\/div><\/div><\/div>/i';
        preg_match($reg_exp, $html, $data['images']);
        $data['images'] = $data['images'] ? $data['images'][1] : null;

        $reg_exp = '/img src="(.*?\.jpg)"/i';
        preg_match_all($reg_exp, $data['images'], $data['images_min']);
        $data['images_min'] = $data['images_min'][1] ? $data['images_min'][1] : '';

//        $dir_280 = $dir . '280/';
//        My::checkDir($dir_280);
//        $i = 1;
//        foreach ($data['images_min'] as $img) {
//            $content = file_get_contents($img);
//            file_put_contents($dir_280 . $i . '_280.jpg', $content);
//            chmod($dir_280 . $i . '_280.jpg', 0777);
//            $i++;
//        }

        $reg_exp = '/hash="(.*?\.jpg)"/i';
        preg_match_all($reg_exp, $data['images'], $data['images_big']);
        $data['images_big'] = $data['images_big'][1] ? $data['images_big'][1] : '';

//        $dir_1000 = $dir . '1000/';
//        My::checkDir($dir_1000);
//        $i = 1;
//        foreach ($data['images_big'] as $img) {
//            $content = file_get_contents($img);
//            file_put_contents($dir_1000 . $i . '_1000.jpg', $content);
//            chmod($dir_1000 . $i . '_1000.jpg', 0777);
//            $i++;
//        }

        $data['categories'] = json_encode($data['categories']);
        $data['technologies'] = json_encode($data['technologies']);
        $data['images_min'] = json_encode($data['images_min']);
        $data['images_big'] = json_encode($data['images_big']);

        unset($data['images']);
        if (!$data['brand']) unset($data['brand']);
        unset($data['technology']);
        unset($data['technology_names']);
        unset($data['technology_pathes']);
        unset($data['technology']);
        unset($data['categories_alias']);
        unset($data['categories_title']);

        Parser::saveResults($data);
        $message = ['status' => 'ok', 'data' => $article];
        return $message;
    }
}