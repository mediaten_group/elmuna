<?php

namespace app\modules\admin\controllers;

use app\modules\admin\lib\My;
use app\modules\admin\models\Categories;
use app\modules\admin\models\CategoryLink;
use app\modules\admin\models\Groups;
use app\modules\admin\models\ItemImages;
use app\modules\admin\models\Items;
use app\modules\admin\models\Source;
use app\modules\admin\models\TechPlaces;
use app\modules\upload\lib\ColorsOfImage;
use app\modules\upload\models\Upload;
use app\tables\Colors;
use app\tables\ItemUpload;
use dosamigos\transliterator\TransliteratorHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ItemsController extends Controller
{
    public $title = 'Товары';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-image' => ['post'],
                    'get-alias' => ['post'],
//                    'get-color' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($category_id = null, $q = null)
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Groups::getItemsByGroups($category_id, $q);

//        pree($models->limit(50)->all());

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            'pagination' => [
                'pageSize' => 50
            ]
        ]);

//        $searchModel = new ItemsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->params['breadcrumbs'] = ['Товары'];

        $category = $category_id ? Categories::findOne($category_id) : null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
            'category_id' => $category_id,
            'q' => $q
//            'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate($group_id = null)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var Items $item */
        $item = new Items;

        if ($group_id === null) {
            $group = new Groups();
        } else {
            $group = Groups::findOne($group_id);
            if (!$group) throw new NotFoundHttpException;
        }

        $sources = Source::find()->asArray()->select(['name'])->indexBy('alias')->column();

//        $image = Yii::$app->getModule();

        $items = Items::find()->where(['group_id' => $group_id])->andWhere(['not', ['id' => $item->id]])->all();

        if ($item->load($req->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($group->isNewRecord) {
                    $group->populateItem($item);
                    if (!$group->save()) throw new Exception('Group not save', $group->errors);
                }

                $item->populateGroup($group);

                if (!$item->save()) throw new Exception('Item not save', $item->errors);

                $transaction->commit();

                if ($req->post('btn-close')) return $this->redirect(['update', 'id' => $item->id]);

                return $this->redirect(['group', 'id' => $item->group_id]);
            } catch (Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $colors = Colors::find()->asArray()->all();

        return $this->render('create', [
            'item' => $item,
            'items' => $items,
            'group' => $group,
            'colors' => $colors,
            'sources' => $sources
        ]);
    }

    public function actionCreateSet($item_id)
    {
        $req = Yii::$app->request;
        $item = Items::findOne($item_id);
        if ($item === null) return false;

        $model = new AddSetForm();
        $model->load($req->post());
        $model->createSet($item_id);

        if ($req->post('addAndClean')) {
            $model = new AddSetForm();
            Yii::$app->session->setFlash('addAndClean');
        }

        return $this->render('update/_modal_add_set', [
            'model' => $model,
            'item_id' => $item_id,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var Items $item */
        $item = Items::findOne($id);

        /** @var Groups $group */
        $group = Groups::find()->with('count')->where(['id' => $item->group_id])->one();

        if ($item->load($req->post())) {
            if (!$item->source) $item->source = null;
            if ($item->save()) {
                if ($group->count['value'] === 1) {
                    $group->populateItem($item);
                    $group->save();
                }
                if ($req->post('btn-close')) {
                    return $this->redirect($req->post('btn-close'));
                }
                return $this->redirect(['update', 'id' => $item->id]);
            }
        }

        $items = Items::find()->where(['group_id' => $item->group_id])->andWhere(['not', ['id' => $item->id]])->all();

        $categories = $links = null;
        if (!$items) {
            $categories = Categories::getCatTree();
            $links = CategoryLink::find()->asArray()->select(['category_id'])->where(['group_id' => $group->id])->column();
        }

        $images = ItemUpload::find()
            ->asArray()->select(['upload.*'])
            ->where(['item_upload.item_id' => $id])
            ->leftJoin('upload', 'upload.id = item_upload.upload_id')
            ->all();

        $colors = ArrayHelper::map(Colors::find()->select(['hex', 'title'])->asArray()->all(), 'hex', 'title');
        $sources = Source::find()->asArray()->select(['name'])->indexBy('alias')->column();

        return $this->render('create', [
            'item' => $item,
            'items' => $items,
            'group' => $group,
            'images' => $images,
            'categories' => $categories,
            'links' => $links,
            'colors' => $colors,
            'sources' => $sources
        ]);
    }

    public function actionGroup($id)
    {
        $req = Yii::$app->request;

        /** @var Groups $group */
        $group = Groups::findOne($id);
        if (!$group) throw new NotFoundHttpException;

        $items = Items::find()->where(['group_id' => $id])->all();

        if (count($items) === 1) return $this->redirect(['update', 'id' => $items[0]->id]);

        if ($group->load($req->post())) {
            if ($group->save()) {
//                Yii::warning('save');
                if ($req->post('btn-close')) return $this->redirect($req->post('btn-close'));
            } else {
                Yii::warning($group->errors);
            }
        }

        $categories = Categories::getCatTree();

        $links = CategoryLink::find()->asArray()->select(['category_id'])->where(['group_id' => $group->id])->column();

        return $this->render('group', [
            'group' => $group,
            'items' => $items,
            'categories' => $categories,
            'links' => $links,
        ]);
    }

    public function actionGetColor($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Items $model */
        $model = Items::findOne($id);
        if (!$model) throw new BadRequestHttpException('Запись не найдена');

        /** @var Upload $image */
        $image = Upload::findOne($model->cover_image_id);
        if (!$image) throw new BadRequestHttpException('Изображение не найдено');

        $path = Yii::getAlias('@webroot/uploads/images/thumbs/');
        $image = new ColorsOfImage($path . $image->name, 5, 1);
        $color = $image->getProminentColors();

        if (count($color[0] == 7)) {
            $model->color = $color[0];
            $model->save();
        }

        return ['status' => 'ok', 'color' => $color[0]];
    }

    public function actionDeleteItem($id)
    {
        /** @var $model Items */
        $model = Items::findOne($id);
        if ($model === null) return false;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model->delete();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect(['group', 'id' => $model->group_id]);
    }

    public function actionDeleteGroup($id)
    {
        /** @var $group Groups */
        $group = Groups::findOne($id);
        if ($group === null) return false;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $group->delete();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->redirect('index');
    }

    public function actionDeleteSet($id)
    {
        /** @var $set ItemSets */
        $set = ItemSets::findOne($id);
        if ($set === null) return false;

        return $set->delete();
    }

    public function actionDefineCover($id)
    {
        $req = Yii::$app->request;
        if (!$req->isAjax) throw new NotFoundHttpException;

        $res = Yii::$app->response;
        $res->format = Response::FORMAT_JSON;

        /** @var Items $model */
        $model = Items::findOne($id);
        if ($model === null) return ['status' => 'error', 'message' => 'Запись не найдена'];

        $image = Upload::findOne($req->post('image_id'));
        if (!$image) return ['status' => 'error', 'message' => 'Изображение не найдено'];

        $model->cover_image_id = $image->id;
        if ($model->save()) return ['status' => 'ok', 'message' => 'Обложка изменена'];
        return ['status' => 'error', 'message' => $model->errors];
    }

    public function actionDeleteImage($id, $src)
    {
        /** @var $model Items */
        $model = Items::findOne($id);
        if ($model === null) return false;

        $latest = ItemImages::find()->where(['item_id' => $model->id])->orderBy(['date' => SORT_DESC])->one();
        if (!$latest) return false;

        /** @var $image ItemImages */
        $image = ItemImages::findOne(['item_id' => $model->id, 'name' => $src]);
        if ($image) $image->delete();

        if ($model->image == $src) {
            /** @var $latest ItemImages */
            $model->image = $latest;
            $model->save();
        }

        if (!My::removeImage(Yii::getAlias('@webroot/uploads/items/' . $model->id), $src)) return false;
        return $model->image;
    }

    public function actionUploadImage($item_id)
    {
        $file = UploadedFile::getInstanceByName('image');

        if (!My::validateImageFile($file))
            return json_encode(['error' => 'Не верный формат файла']);

        /** @var $model Items */
        $model = Items::find()->where(['id' => $item_id])->one();
        if ($model === null) return json_encode(['error' => 'Товара не существует']);

        /** Подготовка ФС */
        $itemsDir = Yii::getAlias('@webroot/uploads/items');
        if (!My::checkDir($itemsDir))
            return json_encode(['error' => 'Нет прав на создание папки itemsDir']);

        $itemDir = $itemsDir . '/' . $model->id;
        if (!My::checkDir($itemDir))
            return json_encode(['error' => 'Нет прав на создание папки itemDir']);

        $itemThumbsDir = $itemDir . '/thumbs';
        if (!My::checkDir($itemThumbsDir))
            return json_encode(['error' => 'Нет прав на создание папки itemThumbsDir']);

        $itemThumbs400Dir = $itemThumbsDir . '/400';
        if (!My::checkDir($itemThumbs400Dir))
            return json_encode(['error' => 'Нет прав на создание папки itemThumbs400Dir']);

        /** Получаем размер изображение */
//        $image = Image::getImagine()->open($file->tempName);
//        $width = $image->getSize()->getWidth();
//        $height = $image->getSize()->getHeight();

        /**
         * Сохранение изображения и нарезка миниатюр
         */
        $name = My::uniqueName($itemDir, $file->extension);

//
//        $size = My::enteredSize($width, $height, 400, 300);
//        Image::thumbnail($image->tempName, $size[0], $size[1])->save($itemThumbs400Dir . '/' . $name);
        Image::thumbnail($file->tempName, 200, 150)->save($itemThumbs400Dir . '/' . $name);
        $file->saveAs($itemDir . '/' . $name);

        $image = new ItemImages();
        $image->name = $name;
        $image->item_id = $model->id;
        $image->save();

        if (!$model->image) {
            $model->image = $name;
            $model->save();
        }

        return true;
    }

    public function actionRefreshImages($id)
    {
        $webRoot = Yii::getAlias('@webroot');
        $web = Yii::getAlias('@web');
        $thumbsPath = '/uploads/items/' . $id . '/thumbs/400';
        $files = array_diff(scandir($webRoot . $thumbsPath), ['..', '.']);
        $images = ItemImages::names($id);
        $out = array();
        foreach ($images as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $str) {
                    $out[] = $str;
                }
            }
        }
        $missing = array_values(array_diff(array_values($files), array_values($out)));
        foreach ($missing as $m) {
            My::removeImage(Yii::getAlias('@webroot/uploads/items/' . $id), $m);
        }
        return json_encode($missing);
    }

    public function actionGetAlias()
    {
        $req = Yii::$app->request;
        $title = trim($req->post('title'));
        $alias = json_encode(['result' => TransliteratorHelper::process($title)]);
        $alias = strtolower($alias);
        $alias = str_replace(' ', '-', $alias);
        $alias = preg_replace('/[\.,]/', '', $alias);
        return $alias;
    }

    public function actionAddSize($item_id)
    {
        $item = Items::findOne($item_id);
        if ($item === null) return json_encode(['error' => 'item false']);

        $size = new CtItemSizes();
        if ($size->load(Yii::$app->request->post())) {
            $size->item_id = $item->id;
            if ($size->save()) {
                $size = new CtItemSizes();
            };
        }

        return $this->render('update/modal_grid_sizes/_form', [
            'item_id' => $item->id,
            'model' => $size,
        ]);
    }

    public function actionDeleteSize($item_id, $size)
    {
        $item = Items::findOne($item_id);
        if ($item === null) return json_encode(['error' => 'item false']);

        /** @var $size CtItemSizes */
        $size = CtItemSizes::findOne(['item_id' => $item_id, 'size' => $size]);
        $size->delete();

        return $this->render('update/modal_grid_sizes/_grid', [
            'item_id' => $item_id,
        ]);
    }

    public function actionSubCat()
    {
        $req = Yii::$app->request;
        $parents = $req->post('depdrop_parents');
        if (!isset($parents[0]) || !$parents[0]) return Json::encode(['output' => '', 'selected' => '']);

        $out = Categories::getSubCatList($parents[0]);
        if (!$out) $out = '';
        // the getSubCatList function will query the database based on the
        // cat_id and return an array like below:
        // [
        //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
        //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
        // ]
        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function actionAjax($action)
    {
        $req = Yii::$app->request;
        if (!$req->isAjax) throw new NotFoundHttpException;

        Yii::$app->response->format = Response::FORMAT_JSON;

        switch ($action) {
            case 'add':
                return CategoryLink::addLink($req->post());

                break;
            case 'remove':
                return CategoryLink::removeLink($req->post());

                break;
        }

        return false;
    }

    public function actionPlaces($id)
    {
        $images = TechPlaces::find()
            ->asArray()
            ->select(['upload.*'])
            ->where(['tech_places.group_id' => $id])
            ->leftJoin('upload', 'upload.id = tech_places.upload_id')
            ->all();
        $model = new TechPlaces();
        $all_images = TechPlaces::find()
            ->asArray()
            ->select([
                'groups.title',
                'groups.id',
            ])
            ->where([
                'not',
                ['groups.id' => $id]
            ])
            ->leftJoin('groups', 'groups.id = tech_places.group_id')
            ->leftJoin('upload', 'upload.id = tech_places.upload_id')
            ->all();
        if ($all_images) {
            $all_images = ArrayHelper::map($all_images, 'id', 'title');
        }
        $request = Yii::$app->request->post('TechPlaces');

        if (isset($request['_upload'])) {
            foreach ($request['_upload'] as $upload) {
                $model = TechPlaces::findOne(['group_id' => $id, 'upload_id' => $upload]);
                if (!$model) {
                    $model = new TechPlaces();
                    $model->group_id = $id;
                    $model->upload_id = $upload;
                    $model->save();
                }
            }
            return $this->redirect(['places', 'id' => $id]);
        }

        Url::remember();
        $this->view->title = $this->title;

        return $this->render('places', [
            'id' => $id,
            'images' => $images,
            'model' => $model,
            'all_images' => $all_images,
        ]);
    }

    public function actionCopyPlaces($to, $from)
    {
        $places = TechPlaces::find()
            ->asArray()
            ->where([
                'group_id' => $from
            ])
            ->all();
        foreach ($places as $place) {
            $model = TechPlaces::findOne(['group_id' => $to, 'upload_id' => $place['upload_id']]);
            if (!$model) {
                $model = new TechPlaces();
                $model->group_id = $to;
                $model->upload_id = $place['upload_id'];
                $model->save();
            }
        }
        return $this->redirect(['places', 'id' => $to]);
    }

    public function actionMove($id, $action)
    {
        $model = Groups::findOne($id);

        switch ($action) {
            case 'fb':
                if ($model->sort) {
                    $model->sort = 0;
                }
                break;
            case 'b':
                if ($model->sort > 0) {
                    $model->sort--;
                }
                break;
            case 'f':
                $model->sort++;
                break;
            case 'ff':
                $max = Groups::find()->asArray()->select('MAX(sort) as max')->where("id != $id")->one();
                $max = $max['max'];
                $model->sort = $max + 1;
        }

        if ($model->isAttributeChanged('sort')) {
            $model->save();
        }

        return $this->redirect(Url::previous());
    }

    public function actionUpdateAttr($id, $value, $attr)
    {
        $model = Items::findOne($id);
        Yii::$app->response->format = 'json';
        if (!$model) return ['status' => 'error', 'data' => 'Товар не найден'];
        $old_value = $model[$attr];
        if ($value == 'true') $value = 1;
        if ($value == 'false') $value = 0;
        $model[$attr] = $value;
        if ($value<0 || !$model->save()) return ['status' => 'error', 'data' => $model->errors, 'value' => $old_value];
        return ['status' => 'ok'];
    }

}
