<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.03.16
 * Time: 18:45
 */

namespace app\modules\admin\controllers;


use app\models\forms\DistributionForm;
use yii\web\Controller;
use yii;

class DistributionController extends Controller
{

    public $title = 'Рассылка'; 

    public function actionIndex() 
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;
        
        $model = new DistributionForm();

        if (($model->load($req->post())) && ($model->distribute())) {
            return $this->redirect('/admin/distribution/index');
        }

        return $this->render('index',[
            'model' => $model,
        ]);
        
    }
    
}