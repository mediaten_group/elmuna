<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 14.04.16
 * Time: 18:00
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\Clients;
use app\tables\ClientsUploads;
use himiklab\sortablegrid\SortableGridAction;
use yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ClientsController extends Controller
{

    private $title = 'Наши клиенты';

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Clients::className(),
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->view->title = $this->title;
        $this->view->params['breadcrumbs'] = [$this->title];
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $query = Clients::find()->orderBy('sort');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $req = Yii::$app->request;
        $images = [];

        /** @var $model Clients */
        $model = new Clients();
        if ($model->load($req->post())) {
            if (isset($req->post('Clients')['_upload'][0]))
                $model->upload_id = $req->post('Clients')['_upload'][0];
            if ($model->save()) {
                $model->addImages($req->post('works'));
                if ($req->post('btn-close')) return $this->redirect(['/admin/clients/index']);
                return $this->redirect(['/admin/clients/update', 'id' => $model->id]);
            }
        }

        return $this->render('form', [
            'model' => $model,
            'images'=>$images
        ]);
    }

    public function actionUpdate($id)
    {
        $req = Yii::$app->request;
        $images = ClientsUploads::find()
            ->asArray()
            ->select(['upload.*'])
            ->where(['clients_uploads.client_id' => $id])
            ->leftJoin('upload', 'upload.id = clients_uploads.upload_id')
            ->all();

        /** @var $model Clients */
        $model = Clients::findOne($id);
        if ($model->load($req->post())) {
            if (isset($req->post('Clients')['_upload'][0]))
                $model->upload_id = $req->post('Clients')['_upload'][0];
            if ($model->save()) {
                $model->addImages($req->post('works'));
                if ($req->post('btn-close')) return $this->redirect(['/admin/clients/index']);
                return $this->redirect(['/admin/clients/update', 'id' => $model->id]);
            }
        }

        return $this->render('form', [
            'model' => $model,
            'images' => $images,
        ]);

    }

    public function actionDelete($id)
    {
        /** @var Clients $model */
        $model = Clients::findOne($id);
        if ($model->delete())
            return $this->redirect('index');

        return $model->errors;
    }

}