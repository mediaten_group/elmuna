<?php

namespace app\modules\admin\controllers;

use app\lib\My;
use app\modules\admin\models\Albums;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class AlbumsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-image' => ['post'],
                ],
            ],
        ];
    }

    public $title = 'Альбомы';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;
        /** @var $models Albums */
        $models = Albums::find()->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        Url::remember();
        $this->view->title = $this->title;

        /** @var $models Albums */
        $model = Albums::findOne($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionImagesUpload()
    {
        /** @var $file UploadedFile */
        $file = UploadedFile::getInstanceByName('file');
        if ($file == null) return json_encode(['error' => 'file null']);

        $req = Yii::$app->request;
        $folder = $req->post('folder');

        /** @var $album Albums */
        $album = Albums::findOne(['folder' => $folder]);
        if ($album == null) return json_encode(['error' => 'album null']);
        $params = json_decode($album->params, JSON_UNESCAPED_UNICODE);

        $app = Yii::getAlias('@app');
        $root = '/uploads/albums';
        $folder = '/' . $req->post('folder');
        $name = My::uniqueName($app . $root . $folder, $file->getExtension());

        $file->saveAs($app . $root . $folder . '/' . $name);
        Albums::makeThumb($folder, $name, 'mini', $params);
        Albums::makeThumb($folder, $name, 'popup', $params);

        return true;
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;

        /** @var $model Albums */
        $model = new Albums;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;

        /** @var $model Albums */
        $model = Albums::findOne($id);
        if ($model == null) throw new NotFoundHttpException('Страницы не существует');

        if ($model->load($req->post()) && $model->save()) {
            switch ($req->post('submit')) {

                case 'save_close':
                    return $this->redirect(['index']);
                    break;
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDeleteImage($album, $name)
    {
        $app = Yii::getAlias('@app');
        $webRoot = Yii::getAlias('@webroot');
        $root = '/uploads/albums/' . $album;
        $name = '/' . $name;

        @unlink($app . $root . $name);
        @unlink($webRoot . $root . '/popup' . $name);
        @unlink($webRoot . $root . '/mini' . $name);

        return $this->goBack();
    }

    public function actionDelete($id)
    {
        /** @var $model Albums */
        $model = Albums::findOne(['id' => $id]);
        /** @var $type Types */
        $type = Types::findOne(['table' => 'albums']);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            /** @var $route Routes */
            $route = Routes::findOne(['type_id' => $type->id, 'element_id' => $model->id]);
            if ($route) {
                $route->type_id = null;
                $route->element_id = null;
                $route->element_title = null;
                if (!$route->save()) throw new Exception('route save false');
            }

            if (!$model->delete()) throw new Exception('model delete false');

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $this->redirect('index');
    }
}
