<?php

namespace app\modules\admin\controllers;

use app\lib\My;
use app\modules\admin\models\Routes;
use app\tables\Types;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class MenuController extends Controller
{
    public $title = 'Меню';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $query = Routes::find()
            ->asArray()
            ->select([
                'routes.id',
                'routes.url',
                'routes.label',
                'routes.element_id',
                'routes.element_title',
                'routes.ordering',
                'routes.date',
                'types.icon AS icon',
                'types.controller AS type_controller',
            ])
            ->leftJoin('types', 'types.id = routes.type_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionElementsList($type_id)
    {
        /** @var $type Types */
        $type = Types::findOne($type_id);
        if ($type == null) return false;
        return My::arrToOpt(Routes::elements($type->table));
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;

        /** @var $model Routes */
        $model = new Routes();
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;

        /** @var $model Routes */
        $model = Routes::findOne($id);
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Routes::deleteAll(['id' => $id]);
        return $this->redirect('index');
    }
}
