<?php

namespace app\modules\admin\controllers;

use app\tables\CtOrders;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class CatalogOrdersController extends Controller
{
    public $title = 'Заказы';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = CtOrders::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model CtOrders */
        $model = CtOrders::findOne($id);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        CtOrders::deleteAll(['id' => $id]);
        return $this->redirect('index');
    }
}
