<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\CtBrands;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class CatalogBrandsController extends Controller
{

    public $title = 'Категории';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = CtBrands::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_DESC,
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model CtBrands */
        $model = CtBrands::findOne($id);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}