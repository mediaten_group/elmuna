<?php

namespace app\modules\admin\_old\controllers;

use app\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class NewsController extends Controller
{
    public $title = 'Новости';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $models = News::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model News */
        $model = new News;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model News */
        $model = News::findOne($id);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        News::deleteAll(['id' => $id]);
        return $this->redirect('index');
    }
}
