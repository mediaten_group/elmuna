<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Modules;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ModulesController extends Controller
{
    public $title = 'Текстовые модули';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $models = Modules::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['alias' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Modules */
        $model = new Modules;
        $types = $model->getTypes();
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
            'types' => $types,
        ]);
    }


    public function actionUpdate($alias)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model News */
        $model = Modules::findOne($alias);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($alias)
    {
        /** @var $model Modules */
        $model = Modules::findOne($alias);
        if ($model== null) throw new NotFoundHttpException;
        if (!$model->deleted) throw new BadRequestHttpException;
        $model->delete();

        return $this->redirect('index');
    }
}
