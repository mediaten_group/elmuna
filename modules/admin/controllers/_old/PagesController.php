<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Pages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class PagesController extends Controller
{
    public $title = 'Материалы';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $models = Pages::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Pages */
        $model = new Pages;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Pages */
        $model = Pages::findOne($id);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Pages::deleteAll(['id' => $id]);
        return $this->redirect('index');
    }
}
