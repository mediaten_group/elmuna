<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Technology;
use app\tables\TechnologyUploads;
use yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\admin\models\SchemeGroup;
use yii\web\NotFoundHttpException;

class TechnologyController extends Controller
{

    public $title = 'Технологии нанесения';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Technology::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;
        $images = [];

        $req = Yii::$app->request;
        /** @var $model Technology */
        $model = new Technology();
        if ($model->load($req->post())) {
            if (isset($req->post('Technology')['_upload'][0])) {
                $model->cover_image_id = $req->post('Technology')['_upload'][0];
            }
            if ($model->save()) {
                $model->addImages($req->post('works'));
                if ($req->post('btn-close')) return $this->redirect(['/admin/technology/index']);
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'images' => $images,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;
        $images = TechnologyUploads::find()
            ->asArray()
            ->select(['upload.*'])
            ->where(['technology_uploads.technology_id' => $id])
            ->leftJoin('upload', 'upload.id = technology_uploads.upload_id')
            ->all();

        $req = Yii::$app->request;
        /** @var $model Technology */
        $model = Technology::findOne($id);

        if ($model->load($req->post())) {            
            if (isset($req->post('Technology')['_upload'][0])) {
                $model->cover_image_id = $req->post('Technology')['_upload'][0];
            }            
            if ($model->save()) {
                $model->addImages($req->post('works'));
                if ($req->post('btn-close')) return $this->redirect(['/admin/technology/index']);
                return $this->redirect(['/admin/technology/update', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'images' => $images,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Technology::findOne($id);
        if (!$model) throw new NotFoundHttpException;
        $model->delete();
        return $this->redirect(['index']);
    }
}