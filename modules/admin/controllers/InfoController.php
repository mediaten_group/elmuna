<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 02.02.2016
 * Time: 13:44
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\Info;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;

class InfoController extends Controller
{
    public $title = 'Инфо';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Info::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['key' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Info */
        $model = new Info;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect(['update', 'key' => $model->key]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($key)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Info */
        $model = Info::findOne($key);

        if ($model->load($req->post()) && $model->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($key)
    {
        /** @var Info $news */
        $news = Info::findOne($key);

        if (!$news->delete()) throw new Exception(Json::encode($news->errors));

        return $this->redirect('index');
    }
}