<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.04.16
 * Time: 18:54
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\Categories;
use app\modules\admin\models\CategoriesParser;
use app\modules\admin\models\CategoriesParserGroupsLink;
use app\modules\admin\models\CategoryLink;
use yii;
use app\modules\admin\models\GroupsLinks;
use yii\web\Controller;

class ReorderController extends Controller
{

    public function beforeAction($action)
    {
        $this->view->title = 'Массовое перемещение';
        return parent::beforeAction($action);
    }

    public function actionIndex($cat = null)
    {
        $list = CategoriesParser::getList();

//        $data = GroupsLinks::getGroupsList($cat);
        $data = CategoriesParserGroupsLink::getItems($cat);

        $categories = Categories::getCatTree();
        return $this->render('index', [
            'list' => $list,
            'groups_data' => $data,
            'active' => $cat,
            'categories' => $categories
        ]);
    }

    public function actionAdd()
    {
        $req = Yii::$app->request;
        $data = $req->post('data');
        $cat_id = $req->post('cat');
        return CategoryLink::add($cat_id,$data);
    }

    public function actionRemove()
    {
        $req = Yii::$app->request;
        $data = $req->post('data');
        $cat_id = $req->post('cat');
        return CategoryLink::remove($cat_id,$data);
    }

}