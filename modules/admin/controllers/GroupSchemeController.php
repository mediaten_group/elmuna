<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 25.02.16
 * Time: 16:47
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\Groups;
use app\modules\admin\models\Scheme;
use app\modules\admin\models\SchemeGroup;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii;

class GroupSchemeController extends Controller
{

    public $title = 'Группы схем';

    public function actionIndex($id = null)
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = SchemeGroup::find()
            ->asArray()
            ->select([
                'scheme_group.id',
                'scheme_group.name',
            ])
            ->groupBy('scheme_group.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($id, $side)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model SchemeGroup */
        $model = new SchemeGroup();

        if ($model->load($req->post()) && $model->save()) {
            $group = Groups::findOne($id);
            if ($side == 'one')
                $group->scheme_group_id = $model->id;
            elseif ($side == 'two')
                $group->scheme_group_id_2 = $model->id;
            $group->save();

            if ($req->post('btn-close')) return $this->redirect(['/admin/items/group', 'id' => $id]);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUse($id, $side, $group_id)
    {
        $model = Groups::findOne($group_id);
        if ($side == 'one')
            $model->scheme_group_id = $id;
        elseif ($side == 'two')
            $model->scheme_group_id_2 = $id;
        $model->save();

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionUpdate($id)
    {
        $this->view->title = $this->title;
        $req = Yii::$app->request;
        /** @var $model SchemeGroup */
        $model = SchemeGroup::findOne($id);
        if ($model->load($req->post()) && $model->save()) {
            if ($req->post('btn-close')) return $this->redirect(['/admin/group-scheme']);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $models = Scheme::find()->where(['group_id' => $id])->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $models,
        ]);

        $groups = Groups::find()
            ->where([
                'or',
                ['scheme_group_id' => $id],
                ['scheme_group_id_2' => $id],
            ])
            ->asArray();
        $dataGroups = new ActiveDataProvider([
            'query' => $groups,
        ]);

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'groups' => $dataGroups,
        ]);

    }

    public function actionCopy($id, $side, $group_id)
    {
        $current = SchemeGroup::findOne($id);
        $new = SchemeGroup::duplicate($id);
        $group = Groups::findOne($group_id);
        if ($side == 'one')
            $group->scheme_group_id = $new->id;
        else
            $group->scheme_group_id_2 = $new->id;
        $group->save();
        return $this->redirect(['update', 'id' => $new->id, 'group_id' => $group_id]);
    }

    public function actionGroup($id, $side)
    {
        $this->view->title = $this->title;

        if ($side == 'one')
            $group_id = Groups::findOne($id)->scheme_group_id;
        else
            $group_id = Groups::findOne($id)->scheme_group_id_2;
        if ($group_id == null) {
            $model = new SchemeGroup();
        } else {
            $model = SchemeGroup::findOne($group_id);
        }

        $list = array();
        $list['Одна сторона'] = Groups::find()
            ->asArray()
            ->select([
                "(scheme_group.name || ': ' || groups.title || ' - 1') as title",
            ])
            ->where([
                'not',
                ['scheme_group_id' => null]
            ])
            ->indexBy('scheme_group_id')
            ->innerJoin('scheme_group', 'scheme_group.id = groups.scheme_group_id')
            ->column();
        $list['Другая сторона'] = Groups::find()
            ->asArray()
            ->select([
                "(scheme_group.name || ': ' || groups.title || ' - 2') as title",
            ])
            ->where([
                'not',
                ['scheme_group_id_2' => null]
            ])
            ->indexBy('scheme_group_id_2')
            ->innerJoin('scheme_group', 'scheme_group.id = groups.scheme_group_id_2')
            ->column();

        return $this->render('group', [
            'model' => $model,
            'list' => $list,
            'id' => $id,
            'side' => $side,
        ]);

    }

    public function actionDelete($id)
    {
        /** @var SchemeGroup $model */
        $model = SchemeGroup::findOne($id);
        if ($model->delete())
            return $this->redirect('index');

        return $model->errors;
    }

}