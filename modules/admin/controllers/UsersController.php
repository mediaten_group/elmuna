<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.01.2016
 * Time: 15:35
 */

namespace app\modules\admin\controllers;

use app\lib\My;
use app\models\Order;
use app\tables\AuthItem;
use app\tables\OrderItems;
use app\tables\Review;
use app\tables\User;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class UsersController extends Controller
{
    public $title = 'Пользователи';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = User::find()
            ->asArray()
            ->select([
                'user.*',
                'auth_assignment.item_name'
            ])
            ->leftJoin('{{auth_assignment}}', '{{auth_assignment}}.[[user_id]]::int = {{user}}.[[id]]')
            ->with('userProfiles');

        $roles = [null=>'Пользователь']+AuthItem::find()
            ->asArray()
            ->select(['description'])
            ->where(['type' => 1])
            ->indexBy('name')
            ->column();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'status' => SORT_ASC
                ],
                'attributes' => [
                    'created_at', 'status'
                ]
            ],
        ]);

        $this->view->params['breadcrumbs'][] = ['label' => $this->title];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'roles' => $roles
        ]);
    }

    public function actionView($id)
    {
        $req = Yii::$app->request;

        /** @var $user User */
        $user = User::find()->asArray()->where(['id' => $id])->with('userProfiles')->one();
        if (!$user) throw new Exception('Пользователь не найден');

        $ordersQuery = Order::find()->where(['user_id' => $id]);
        $reviewsQuery = Review::find()->where(['user_id' => $id])->orderBy('created_at');

        $orders = new ActiveDataProvider([
            'query' => $ordersQuery,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'updated_at' => SORT_DESC,
                ],
            ],
        ]);
        $reviews = new ActiveDataProvider([
            'query' => $reviewsQuery,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'updated_at' => SORT_DESC,
                ],
            ],
        ]);
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url' => Url::to(['/admin/users/index'])];
        $this->view->params['breadcrumbs'][] = ['label' => $user['username']];

        $this->view->title = 'Пользователь - ' . $user['username'];

        return $this->render('view', [
            'orders' => $orders,
            'user' => $user,
            'reviews' => $reviews,
        ]);
    }
}