<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.01.2016
 * Time: 15:35
 */

namespace app\modules\admin\controllers;


use app\lib\My;
use app\models\Mail;
use app\models\Order;
use app\tables\OrderItems;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class OrdersController extends Controller
{
    public $title = 'Заказы';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Order::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'status' => SORT_ASC
                ],
                'attributes' => [
                    'created_at', 'status'
                ]
            ],
        ]);

        $this->view->params['breadcrumbs'][] = ['label' => $this->title];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $req = Yii::$app->request;

        /** @var $order Order */
        $order = Order::findOne($id);
        $items = OrderItems::find()->where(['order_id' => $id]);

        $status_list = [
            Order::NOT_READY => 'Не подтвержден',
            Order::IN_WORK => 'В работе',
            Order::IN_DELIVERY => 'В доставке',
            Order::COMPLETED => 'Выполнен',
            Order::CANCELED => 'Отменен'
        ];

        if ($order->load($req->post())) {
            if ($order->save()) {
                    Mail::statusChange($order->email,$order);
                if ($req->post('btn-close')) {
                    return $this->redirect($req->post('btn-close'));
                }
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $items,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                    'price' => SORT_ASC,
                ],
            ],
        ]);
        $this->view->params['breadcrumbs'][] = ['label' => $this->title, 'url' => Url::to(['/admin/orders/index'])];
        $this->view->params['breadcrumbs'][] = ['label' => My::mysql_russian_datetime($order->created_at)];

        $this->view->title = 'Заказ ' . My::mysql_russian_datetime($order->created_at);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'order' => $order,
            'status_list'=>$status_list
        ]);
    }
}