<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.04.16
 * Time: 18:54
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\CategoryLink;
use app\modules\admin\models\ColorsLinks;
use app\tables\Colors;
use yii;
use yii\web\Controller;
use app\modules\admin\models\ColorsGifts;
use yii\data\ActiveDataProvider;

class ColorSetController extends Controller
{

    public function beforeAction($action)
    {
        $this->view->title = 'Массовое перемещение';
        return parent::beforeAction($action);
    }

    public function actionIndex($sort = 'pantone')
    {
        $list = [
            'pantone' => 'Каталог цветов Pantone',
            'sol' => 'Каталог цветов Sol\'s',
            'name' => 'Имя',
        ];
        $data = ColorsGifts::getCatalog($sort);
        $colors = Colors::find()
            ->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $colors,
            'pagination' => false,
        ]);
        $model = new ColorsLinks();
        $req = Yii::$app->request;

        if ($model->load($req->post())) {
            if (!$model->color_id) {
                $model->hex = $req->post('ColorsLinks')['hex'];
                $model->title = $req->post('ColorsLinks')['title'];
                $model->alias = $req->post('ColorsLinks')['alias'];
            }
            if ($model->save()) {
                return $this->refresh();
            }
        }

        return $this->render('index', [
            'groups_data' => $data,
            'list' => $list,
            'active' => $sort,
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAdd()
    {
        $req = Yii::$app->request;
        $data = $req->post('data');
        $cat_id = $req->post('cat');
        return CategoryLink::add($cat_id, $data);
    }

    public function actionRemove()
    {
        $req = Yii::$app->request;
        $data = $req->post('data');
        $cat_id = $req->post('cat');
        return CategoryLink::remove($cat_id, $data);
    }

}