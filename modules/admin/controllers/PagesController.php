<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.01.2016
 * Time: 15:35
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\Pages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class PagesController extends Controller
{
    public $title = 'Страницы';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Pages::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['alias' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Pages */
        $model = new Pages;
        if ($model->load($req->post()) && $model->save()) {
            if ($req->post('btn-close')) return $this->redirect($req->post('btn-close'));
            return $this->redirect(['update', 'alias' => $model->alias]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($alias)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;

        /** @var $model Pages */
        $model = Pages::findOne($alias);

        if ($model->load($req->post()) && $model->save() && $req->post('btn-close')) {
            return $this->redirect($req->post('btn-close'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}