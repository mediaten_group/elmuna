<?php

namespace app\modules\admin\controllers;

use app\modules\admin\lib\My;
use Yii;
use yii\filters\VerbFilter;
use yii\gii\CodeFile;
use yii\gii\generators\model\Generator;
use yii\helpers\Url;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update-tables' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        Url::remember();
        return $this->render('index');
    }

    public function actionUpdateTables()
    {
        $connection = Yii::$app->db;
        $schemas = $connection->schema->getTableSchemas();

        My::cleanDir('@runtime/tables');

        foreach ($schemas as $tbl) {
            $gen = new Generator;
            $gen->ns = 'app\tables';
            $gen->tableName = $tbl->name;
            $gen->generateLabelsFromComments = true;
            /** @var CodeFile[] $files */
            $files = $gen->generate();
            foreach ($files as $file) {
                $file->save();
            }
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }
}
