<?php

namespace app\modules\admin\controllers;

use app\tables\Colors;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;

class ColorsController extends Controller
{
    public $title = 'Цвета';

    public function actionIndex()
    {
        Url::remember();
        $this->view->title = $this->title;

        $models = Colors::find()->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Colors */
        $model = new Colors;
        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($hex)
    {
        $this->view->title = $this->title;

        $req = Yii::$app->request;
        /** @var $model Colors */
        $model = Colors::findOne(['hex' => $hex]);

        if ($model->load($req->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($hex)
    {
        Colors::deleteAll(['hex' => $hex]);
        return $this->redirect('index');
    }
}
