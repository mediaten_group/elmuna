<?php

namespace app\modules\upload\lib;

use app\modules\upload\models\Upload;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: igor
 * Date: 08.12.2015
 * Time: 11:20
 */
class Files
{
    public static function uniqueName($path, $extension = 'jpg', $length = 8)
    {
        if ($extension=='tif') {
            $extension = 'png';
        }
        for ($i = 0; $i < 10; $i++) {
            $name = Yii::$app->security->generateRandomString($length) . '.' . $extension;
            if (!is_file($path . '/' . $name)) return $name;
        }

        return '';
    }

    public static function checkDir($path)
    {
        if (is_dir($path)) return true;
        mkdir($path);
        chmod($path, 0777);

        return true;
    }

    public static function checkDirs($array)
    {
        foreach ($array as $path) {
            self::checkDir($path);
        }
    }

    public static function removeImage($itemDir, $name)
    {
        $thumbs = glob($itemDir . '*', GLOB_MARK);

        foreach ($thumbs as $thumb) {
            if (!is_file($thumb)) {
                self::removeImage($thumb, $name);
            } else {
                if (basename($thumb) == $name) {
                    unlink($thumb);
                }
            }
        }

        $count = count(glob($itemDir . '*'));

        /** Осталась последняя папка thumbs */
        if ($count < 1) {
            self::removeDir($itemDir);
        }

        return true;
    }

    public static function removeDir($dir)
    {
        if ($objects = glob($dir . '*', GLOB_MARK)) {
            foreach ($objects as $obj) {
                is_dir($obj) ? self::removeDir($obj) : unlink($obj);
            }
        }
        if (is_dir($dir)) {
            rmdir($dir);
        }

        return true;
    }

    public static function getPath($id, $thumb = '')
    {
        /** @var Upload $image */
        $image = Upload::findOne($id);
        if (!$image) return false;

        $thumbs = Upload::$_thumbs;

        $dir = array_key_exists($thumb, $thumbs) ? $thumbs[$thumb]['dir'] : '';

        return Yii::getAlias('@web/uploads/images/'.$dir.$image->name);
    }
}