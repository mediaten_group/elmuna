<?php
/**
 * @var \app\tables\Upload $images
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Фотографии';
?>
<div class="row">
    <div class="col-lg-3">
        <a href="<?= Url::to(['/upload/images/index', 'filter' => 'unused']) ?>">Не используемые</a>
    </div>
</div>
<div class="row">
    <?php foreach ($images as $img) : ?>
        <div class="col-lg-3">
            <div class="box">
                <div class="image">
                    <img src="<?= My::thumb('/uploads/images/thumbs/' . $img['name']) ?>">
                </div>
                <div class="colors">
                    <a href="#"><i class="ion-trash-a"></i></a>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
