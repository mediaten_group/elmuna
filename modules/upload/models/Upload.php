<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.12.2015
 * Time: 17:10
 */

namespace app\modules\upload\models;


use app\modules\upload\lib\Files;
use app\modules\upload\Module;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\imagine\Image;
use yii\web\UploadedFile;

class Upload extends \app\tables\Upload
{
    public static $_thumbs;

    public function setThumbs($data)
    {
        self::$_thumbs = $data;
    }

    public static function getThumbs()
    {
        return self::$_thumbs;
    }
//    public function init()
//    {
//        self::$_thumbs = $this->thumbs;
//    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeValidate()
    {
        if (!$this->name) $this->name = Yii::$app->security->generateRandomString();

        return parent::beforeValidate();
    }

    public static function findUnused()
    {

    }

    public static function upload($thumbs, $filename = 'file')
    {
        $module_thumbs = self::$_thumbs;

        $thumbs = array_intersect_key($module_thumbs, array_flip($thumbs));

        $file = UploadedFile::getInstanceByName($filename);
        if (!$file) return null;

        $root = Yii::getAlias('@webroot/uploads/images/');
        $originalDir = $root . 'original/';
        $dirs = [$root, $originalDir];

        foreach ($thumbs as $thumb) {
            $dirs[] = $root . $thumb['dir'];
        }

        Files::checkDirs($dirs);

        $name = Files::uniqueName($root, $file->extension);
        if (!$name) throw new Exception('Имя изображения не задано');

        $image = Image::getImagine()->open($file->tempName);

        
        
        foreach ($thumbs as $thumb) {
            $mode = ($thumb['mode'] == 'outbound') ? ManipulatorInterface::THUMBNAIL_OUTBOUND : ManipulatorInterface::THUMBNAIL_INSET;
            $image->thumbnail(new Box($thumb['x'], $thumb['y']), $mode)->save($root . $thumb['dir'] . $name, ['quality' => $thumb['q']]);
        }

        $file->saveAs($originalDir . $name);

        $model = new self;
        $model->name = $name;
        $model->ext = $file->extension;
        $model->params = json_encode(['width' => $image->getSize()->getWidth(), 'height' => $image->getSize()->getWidth()]);

        if (!$model->save()) {
            throw new Exception('Model not save', $model->errors);
        }
        return ['result' => 1, 'id' => $model->id];
    }

    public static function uploadByPath($path, $thumbs = [])
    {
        $module_thumbs = self::$_thumbs;

        $thumbs = array_intersect_key($module_thumbs, array_flip($thumbs));

//        $file = file_get_contents($path);
//        if (!$file) return null;

        $image = Image::getImagine()->open($path);

        $path_parts = pathinfo($path);
        $ext = $path_parts['extension'];

        $root = Yii::getAlias('@webroot/uploads/images/');
        $originalDir = $root . 'original/';
        $dirs = [$root, $originalDir];

        foreach ($thumbs as $thumb) {
            $dirs[] = $root . $thumb['dir'];
        }

        Files::checkDirs($dirs);

        $name = Files::uniqueName($root, $ext);
        if (!$name) throw new Exception('Имя изображения не задано');

        foreach ($thumbs as $thumb) {
            $mode = ($thumb['mode'] == 'outbound') ? ManipulatorInterface::THUMBNAIL_OUTBOUND : ManipulatorInterface::THUMBNAIL_INSET;
            $image->thumbnail(new Box($thumb['x'], $thumb['y']), $mode)->save($root . $thumb['dir'] . $name, ['quality' => $thumb['q']]);
        }

        $image->save($originalDir . $name);

        $model = new self;
        $model->name = $name;
        $model->ext = $ext;
        $model->params = json_encode(['width' => $image->getSize()->getWidth(), 'height' => $image->getSize()->getWidth()]);

        if (!$model->save()) {
            throw new Exception('Model not save', $model->errors);
        }
        return $model->id;
    }

    public static function remove($id)
    {
        $model = self::findOne($id);
        if (!$model) return ['status' => 'error', 'message' => 'file not load'];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$model->delete()) throw new Exception('Изображение не удалилось');
            Files::removeImage(Yii::getAlias('@webroot/uploads/images/'), $model->name);

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return ['status' => 'ok', 'id' => $id];
    }

    public static function initThumbs()
    {
        self::$_thumbs = [
            'screen' => ['x' => 1920, 'y' => 1080, 'q' => 95, 'mode' => 'inset', 'dir' => ''],
            'small' => ['x' => 400, 'y' => 1000, 'q' => 95, 'mode' => 'inset', 'dir' => 'thumbs/'],
            '100' => ['x' => 100, 'y' => 100, 'q' => 95, 'mode' => 'outbound', 'dir' => 'thumbs100/'],
        ];
    }
}
