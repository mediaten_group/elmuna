<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 12.01.2016
 * Time: 12:05
 */

namespace app\modules\upload\controllers;


use app\modules\upload\models\Upload;
use yii\web\Controller;

class ImagesController extends Controller
{
//    public $layout = 'admin';

    public function actionIndex($filter = null)
    {
        if($filter == 'unused') {
            $images = Upload::findUnused();
        } else {
            $images = Upload::find()->asArray()->orderBy('created_at')->all();
        }

        return $this->render('index',[
            'images' => $images,
        ]);
    }
}