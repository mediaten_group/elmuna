<?php

namespace app\modules\upload\controllers;


use app\modules\upload\lib\Files;
use app\modules\upload\models\Upload;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

Class ActionController extends Controller
{
    public function actionUpload()
    {
        $req = Yii::$app->request;
        $res = Yii::$app->response;
        $res->format = Response::FORMAT_JSON;

        $thumbs = $req->post('thumbs');
        $thumbs = explode(',', $thumbs);

        return Upload::upload($thumbs);
    }

    public function actionRemove()
    {
        $req = Yii::$app->request;
        $res = Yii::$app->response;
        $res->format = Response::FORMAT_JSON;

        $id = $req->post('id');

        return Upload::remove($id);
    }
}
