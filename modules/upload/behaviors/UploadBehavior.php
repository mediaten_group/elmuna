<?php

namespace app\modules\upload\behaviors;

use app\modules\upload\models\Upload;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Class UploadBehavior
 * @package app\modules\upload\behaviors
 * @property ActiveRecord $owner
 */
class UploadBehavior extends Behavior
{
    public $_upload;
    public $_upload_params;

    public $async = false;

//    public $beforeUpload;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @param ActiveRecord $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $validator = Validator::createValidator('safe', $owner, '_upload');
        $owner->getValidators()->append($validator);
        $validator = Validator::createValidator('safe', $owner, '_upload_params');
        $owner->getValidators()->append($validator);
    }

    public function beforeSave()
    {

        $params = $this->owner['_upload_params'];

        if ($this->async || !isset($params['thumbs'])) return true;

        $thumbs = explode(',', $params['thumbs']);

        $image = Upload::upload($thumbs);

        if(!$image) return true;

        if($this->owner[$params['cover']]) Upload::remove($this->owner[$params['cover']]);

        $this->owner[$params['cover']] = $image['id'];

//        if ($this->beforeUpload) {
//            if (method_exists($this->owner, $this->beforeUpload)) {
//                $this->owner->{$this->beforeUpload}($this);
//            }
//        }
//
//        $file = UploadedFile::getInstance($this->owner, '_file');
//        pree(123);
//        \Yii::warning('123');
//        exit();
        return true;
    }
}
