<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 04.12.2015
 * Time: 10:37
 */

namespace app\modules\upload\widgets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class UploadAsset extends AssetBundle
{
    public function init()
    {
        if (YII_ENV_DEV) {
            $this->publishOptions['forceCopy'] = true;
        }

        $this->sourcePath = '@app/modules/upload/widgets/dist';
        $this->js = ['upload.js'];
        $this->css = ['upload.css'];
        $this->depends = [JqueryAsset::className()];
    }
}
