<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 03.12.2015
 * Time: 17:57
 */

namespace app\modules\upload\widgets;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\InputWidget;

class Upload extends InputWidget
{
//    public $moduleName = 'upload';
    public $thumbs = [];
    public $initialImages = [];
    public $cover = '';
    public $async = false;
    public $multiple = false;
    public $required = false;

    public function init()
    {
        $this->view->registerAssetBundle(UploadAsset::className());
//        $this->view->registerJs($this->render('upload.js'));
    }

    public function run()
    {
        $this->inputJs();
        $id = $this->getId();
        if (isset($this->model)) {
            $formName = $this->model->formName();
        } else {
            $formName = $this->name;
        }

        $cover = $this->cover ?: '';

        $thumbs = '';
        if ($this->initialImages) {
            if (is_array($this->initialImages)) {
                foreach ($this->initialImages as $image) {
                    $thumbs .= $this->renderThumb($image, $this->model[$cover]);
                }
            }
        } elseif ($this->cover) {
            $image_field = $this->cover;
            $image = \app\modules\upload\models\Upload::findOne($this->model[$image_field]);
            if ($image) $thumbs .= $this->renderThumb($image, $this->model[$image_field]);
        }

        $params = ['id' => $id,'required' => $this->required];
//        if ($this->async) $params['multiple'] = 'multiple';
        if ($this->multiple) $params['multiple'] = 'multiple';

        $html = Html::fileInput('file', null, $params);
        $html .= Html::tag('div', $thumbs, ['id' => $id . 'result', 'class' => 'thumbs row']);

        if (is_array($this->model['_upload'])) {
            foreach ($this->model['_upload'] as $key => $value) {
                $html .= Html::input('hidden', $formName . '[_upload][]', $value);
            }
        }

        $html .= Html::hiddenInput($formName . '[_upload_params][async]', $this->async);
        $html .= Html::hiddenInput($formName . '[_upload_params][multiple]', $this->multiple);
        if ($this->cover) $html .= Html::hiddenInput($formName . '[_upload_params][cover]', $this->cover);
        if (!$this->async) $html .= Html::hiddenInput($formName . '[_upload_params][thumbs]', implode(',', $this->thumbs));
        $html .= Html::hiddenInput($formName . '[_upload_params][name]', $formName,['class'=>'name-helper']);
        return $html;
    }

    public function inputJs()
    {
//        $name = preg_replace('/\[\]/', '', $this->attribute);
        $remove_url = Url::to(['/upload/action/remove']);

        $js = <<<JS
$('.thumbs').on('click','.remove',function (e) {
    e.preventDefault();
    var elem = $(this),
        thumb = elem.parents('.thumb'),
        data = {
            id: thumb.attr('data-id')
        };
    thumb.addClass('removed');

    $.ajax({
        type: "POST",
        url: '$remove_url',
        data: data,
        dataType: 'json',
        success: function(msg){
            if(msg.status == 'ok'){
                thumb.parent().remove();
            } else {
                thumb.removeClass('removed');
            }
        }
    });
});
JS;
        $this->view->registerJs($js);
        if ($this->async) {
            $id = $this->getId();
            if ($this->model) {
                $formName = $this->model->formName();
            } else {
                $formName = $this->name;
            }
            $upload_url = Url::to(['/upload/action/upload']);
            if (!in_array('100', $this->thumbs)) $this->thumbs[] = '100';
            $thumbs = implode(',', $this->thumbs);
            $multiple = ($this->multiple)?1:0;
            $js = "$('#$id').upload({url: '$upload_url',thumbs: '$thumbs','id': $id,'formName': '$formName', 'multiple': $multiple})";
            $this->view->registerJs($js);
        }
    }

    public function renderThumb($image, $cover = '')
    {
        $html = Html::beginTag('div', ['class' => 'col-lg-2 col-sm-3']);
        $cover_class = ($cover && $cover === $image['id']) ? ' cover' : '';
        $class = (!$this->async) ? ' once' : '';
        $html .= Html::beginTag('div', ['class' => 'thumb' . $class . $cover_class, 'data-id' => $image['id']]);
        if ($this->cover) {
            $html .= '<i class="fa fa-check defined"></i>';
            $html .= '<i class="fa fa-picture-o define"></i>';
        }
        $html .= '<i class="fa fa-remove remove"></i>';

        if(array_key_exists('100',$this->thumbs)) {
            $dir = 'thumbs100';
        } else {
            $thumbs = \app\modules\upload\models\Upload::getThumbs();
            $dir = $thumbs[end($this->thumbs)]['dir'];
        }
        
        $html .= Html::img("/uploads/images/$dir" . $image['name']);
        $html .= Html::endTag('div');
        $html .= Html::endTag('div');
        return $html;
    }
}
