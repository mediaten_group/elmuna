(function ($) {
    window.loading = 0;
    var methods = {
            options: {
                url: '/upload/action/upload',
                formName: '',
                thumbs: '',
                id: '',
                multiple: false
            },
            init: function (options) {
                methods.options = options;
                this.on('change', methods.change);
            },
            change: function (e) {
                e.preventDefault();
                $(this.files).each(function (key, file) {
                    var data = {
                            thumbs: methods.options.thumbs
                        },
                        id = $(e.target).attr('id'),
                        div = document.getElementById(id + 'result'),
                        thumb = document.createElement('div'),
                        col = document.createElement('div');
                    // Удаляем старое если не multiple
                    if (!methods.options.multiple){
                        $(div).empty();
                        $('input[data-widget='+id+']').remove();
                    }
                    // Создаём обвязку
                    col.classList.add('col-lg-3');
                    col.classList.add('col-sm-3');
                    thumb.classList.add('thumb');
                    thumb.classList.add('new');
                    thumb.classList.add('is-uploading');
                    col.appendChild(thumb);
                    div.appendChild(col);

                    // Если браузер поддерживает, показываем изображение
                    if (window.File && window.FileReader && window.FileList && window.Blob) {
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            var dataUri = event.target.result,
                                img = new Image();

                            img.onload = function () {
                                thumb.appendChild(img);
                            };
                            img.src = dataUri;
                        };
                        reader.readAsDataURL(file);
                    }

                    // Загружаем изображение
                    methods.upload(file, methods.options.url, data, thumb, function (json, elem) {
                        var obj = JSON.parse(json);

                        // Если ошибка, добавляем класс ошибки на миниатюру
                        if (obj.status && obj.status == 'error') elem.classList.add('error');

                        // Добавляем hidden input во передачи данных в модель
                        var name = $(e.target).parent().children('.name-helper').val();
                        $(e.target).after('<input data-widget="'+id+'" type="hidden" name="' + name + '[_upload][]" value="' + obj['id'] + '">');
                    });
                });
            },
            upload: function (file, url, data, elem, callback) {
                if (typeof elem === 'function') {
                    callback = elem;
                    elem = null;
                }

                var http = new XMLHttpRequest();

                // Создаём ползунок загрузки
                var loader = document.createElement('span');
                loader.classList.add('loader');
                if (elem) {
                    elem.appendChild(loader);
                }

                // Связываем размер полузнка с процентом загрузки
                http.upload.onprogress = function (event) {
                    var percent = (event.loaded / event.total) * 100;
                    loader.style.width = percent + '%';
                };

                // Выполнить callback после успешной загрузки
                if (http.upload && http.upload.addEventListener) {
                    http.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            if (elem) {
                                elem.classList.remove(('is-uploading'));
                            }
                            window.uploading = false;
                            callback(this.response, elem);
                        } else {
                            if (this.readyState == 4 && (this.status == 500 || this.status == 400)) {
                                elem.classList.add(('error'));
                                window.uploading--;
                            }
                        }
                    }
                }

                // Подготовка данных для отправки запроса
                var form = new FormData();
                form.append('_csrf', $('meta[name=csrf-token]').attr('content'));
                form.append('file', file);

                if (data) {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            form.append(key, data[key]);
                        }
                    }
                }

                http.open('POST', url);
                window.uploading++;
                http.send(form);
            }
        }
        ;

    $.fn.upload = function (method) {

        // логика вызова метода
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.upload');
        }
    };

})
(jQuery);
