<?php
/**
 * @var \app\tables\Callback $request
 * @var \app\models\User $user
 */
?>

<div class="callback-request">

    <p>Оформлен звонок<?= $user ? " от пользователя $user->email" : '' ?> <?= $request->created_at ?></p>

    <p><strong>Имя:</strong> <?= $request->name ?></p>

    <p><strong>Телефон:</strong> <?= $request->phone ?></p>

    <?php if ($request->text) : ?>
        <p><strong>Сообщение:</strong> <?= $request->text ?></p>
    <?php endif; ?>

</div>
