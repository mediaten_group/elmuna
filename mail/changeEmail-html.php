<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user \app\models\User */
$code = base64_encode("$user->email;$user->auth_key");
$host = Yii::$app->request->hostInfo;
$activateLink = "$host/change-email?code=$code";
?>
<div class="change-email">
    <p>Здравствуйте, <?= Html::encode($user->username) ?>,</p>

    <p>Перейдите по следующей ссылке для того чтобы подтвердить ваш новый email:</p>

    <p><?= Html::a(Html::encode($activateLink), $activateLink) ?></p>
</div>