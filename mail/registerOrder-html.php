<?php
/**
 * @var $this yii\web\View
 * @var $order array
 */
use yii\helpers\Html;
use yii\helpers\Json;

?>
<div class="register-order">

    <p>Здравствуйте,</p>

    <p>Заказ от <?= $order['date'] ?> успешно оформлен.</p>

    <p>Указанные товары:</p>

    <table border="1">
        <thead>
        <tr>
            <th>Имя</th>
            <th>Цена за шт.</th>
            <th>Тираж</th>
            <th>Итого</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['title'] ?></td>
                <td><?= ($item['price'] + $item['tech_price']) ?></td>
                <td><?= $item['count'] ?></td>
                <td><?= $item['count'] * ($item['price'] + $item['tech_price']) ?></td>
            </tr>
            <tr>
                <td colspan="4">
                    <?php
                    if ($item['tech_price'] && $item['tech']) {
                        $tech = Json::decode($item['tech']);
                        if ($tech) {
                            if (isset($tech['tech_place'])) {
                                echo Html::tag('h3', 'Место для нанесения');
                                echo Html::img("/uploads/images/thumbs100/$tech[tech_place]");
                            }
                            if (isset($tech['left'])) {
                                echo Html::tag('h3', 'Покрытие с одной стороны');
                                echo $tech['left']['text'];
                            }
                            if (isset($tech['right'])) {
                                echo Html::tag('h3', 'Покрытие с другой стороны');
                                echo $tech['right']['text'];
                            }
                        }
                    }
                    ?>
                </td>
            </tr>

        <?php endforeach ?>
        </tbody>
    </table>

    <p>Общая сумма: <?= $order['price'] ?> руб.</p>

</div>