<?php

/**
 * @var $this yii\web\View
 * @var $order array
 */

?>
<div class="complete-order">

    <p>Здравствуйте,</p>

    <p>Заказ от <?= $order['date'] ?> успешно выполнен.</p>

    <p>Указанные товары:</p>

    <ol>
        <?php foreach ($order['items'] as $item) : ?>

            <li><?=$item['name']. ': '. $item['price'] .' руб. за ' . $item['count'] .' шт.'?></li>

        <?php endforeach; ?>
    </ol>
    
    <p>Общая сумма: <?=$order['price']?> руб.</p>
    
</div>