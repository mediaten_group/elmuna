<?php

/**
 * @var $this yii\web\View
 * @var \app\tables\Order $order
 */

?>
<div class="complete-order">

    <p>Здравствуйте,</p>

    <p>У вашего заказа №<?= $order->id ?> изменился статус на
        <?php
        switch ($order->status) {
            case \app\models\Order::NOT_READY:
                echo 'Не подтвержден';
                break;
            case \app\models\Order::IN_WORK:
                echo 'В работе';
                break;
            case \app\models\Order::IN_DELIVERY:
                echo 'В доставке';
                break;
            case \app\models\Order::COMPLETED:
                echo 'Выполнен';
                break;
            case \app\models\Order::CANCELED:
                echo 'Отменен';
                break;
        }
        $order->status
        ?>
        .
    </p>

    <p>Заказ от <?= $order->created_at ?> на сумму <?= $order->price ?> руб.</p>

</div>