<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 03.02.2016
 * Time: 12:28
 */

namespace app\assets;


use yii\web\AssetBundle;

class MagnificPopupAsset extends AssetBundle
{
    public $sourcePath = '@bower/magnific-popup/dist';

    public $js = [
        'jquery.magnific-popup.min.js',
        '/js/magnific-popup-init.js',
    ];
    public $css = [
        'magnific-popup.css',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}