<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 27.01.2016
 * Time: 13:23
 */

namespace app\assets;


use yii\web\AssetBundle;

class JsCookieAsset extends AssetBundle
{
    public $sourcePath = '@bower/yii2-js-cookie/src';

    public $js = [
        'js.cookie.js',
    ];
}