<?php
/**
 * Created by PhpStorm.
 * User: dan.judex
 * Date: 29.09.2015
 * Time: 10:20
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminPluginsAsset extends AssetBundle
{
    public $sourcePath = '@bower/adminlte/plugins';

    public $publishOptions = [
        'only' => [
            'slimScroll/*',
        ],
    ];

    public $js = [
        'slimScroll/jquery.slimscroll.min.js',
    ];
}
