<?php
namespace app\assets;

use yii\web\AssetBundle;

class IonIconsAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/ion-icons';

    public $css = [
        'css/ionicons.min.css',
    ];
}
