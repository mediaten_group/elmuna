<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminLteAsset extends AssetBundle
{
    public $sourcePath = '@bower/adminlte/dist';

    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/skin-red.css'
    ];

    public $js = [
        'js/app.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesomeAsset',
        'app\assets\AdminPluginsAsset',
    ];
}
