<?php

namespace app\assets;

use yii\web\AssetBundle;

class OwlCarouselAsset extends AssetBundle
{
    public $sourcePath = '@app/web/plugins/owl.carousel.2.0.0-beta.2.4/';

    public $css = [
        'assets/owl.carousel.css',
        'assets/owl.theme.default.min.css'
    ];

    public $js = [
        'owl.carousel.min.js',
    ];

}
