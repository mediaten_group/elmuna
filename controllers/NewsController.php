<?php

namespace app\controllers;

use app\models\News;
use yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Pages;

class NewsController extends Controller
{
    public $title = 'Новости';

    public function actionIndex()
    {
        $query = News::get();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->view->title = $this->title;
        $list = Pages::getMenuAbout1();
        $list2 = Pages::getMenuAbout2();
        $main = Pages::getMainAbout();

        Url::remember();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'list'=>$list,
            'list2'=>$list2,
            'main'=>$main
        ]);
    }

    public function actionView($id, $alias)
    {
        /** @var News $model */
        $model = News::getOne(['id' => $id, 'alias' => $alias, 'status' => 1]);
        if (!$model) throw new NotFoundHttpException;

        $links[] = ['label' => 'Новости', 'url' => Url::to(['/news'])];
        $links[] = ['label' => $model->title];

        $this->view->title = $model->title;
        $list = Pages::getMenuAbout1();
        $list2 = Pages::getMenuAbout2();
        $main = Pages::getMainAbout();

        Url::remember();

        return $this->render('view', [
            'model' => $model,
            'links' => $links,
            'list'=>$list,
            'list2'=>$list2,
            'main'=>$main
        ]);
    }
}