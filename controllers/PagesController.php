<?php

namespace app\controllers;

use app\models\Pages;
use yii;
use yii\helpers\Url;
use yii\web\Controller;

class PagesController extends Controller
{
    public function actionIndex($alias)
    {
        $model = Pages::findOne(['alias' => $alias, 'status' => 1]);
        $list = [];
        $list2 = [];
        $main = [];
        $bread = Pages::getBreadcrumbs($model);
        if (in_array($alias, ['about', 'reviews', 'vacancy', 'news'])) {
            $list = Pages::getMenuAbout1();
            $list2 = Pages::getMenuAbout2();
            $main = Pages::getMainAbout();
        } else
        if (in_array($alias, ['work', 'contract', 'payment', 'requisites', 'delivery'])) {
            $list = Pages::getMenuWork();
            $main = Pages::getMainWork();
        } else {
            $list = Pages::getOtherMenus($model->title, $alias);
        }
        return $this->render('page', [
            'model' => $model,
            'list' => $list,
            'list2' => $list2,
            'main'=>$main,
            'bread'=>$bread
        ]);
    }
    
    public function actionPrint($alias)
    {
        $model = Pages::findOne(['alias' => $alias, 'status' => 1]);
        echo $model->text;
    }

    public function actionBuy()
    {
        $model = Pages::findOne(['alias' => 'buy', 'status' => 1]);
        return $this->render('page', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        $model = Pages::findOne(['alias' => 'about', 'status' => 1]);
        return $this->render('about', [
            'model' => $model,
        ]);
    }

    public function actionDelivery()
    {
        $model = Pages::findOne(['alias' => 'delivery', 'status' => 1]);
        return $this->render('work', [
            'model' => $model,
        ]);
    }

    public function actionContacts()
    {
        $model = Pages::findOne(['alias' => 'contacts', 'status' => 1]);
        return $this->render('contacts', [
            'model' => $model,
        ]);
    }

    public function actionRequirements()
    {
        $model = Pages::findOne(['alias' => 'requirements', 'status' => 1]);
        return $this->render('requirements', [
            'model' => $model,
        ]);
    }

    public function actionSitemap()
    {
        $model = Pages::findOne(['alias' => 'sitemap', 'status' => 1]);
        return $this->render('sitemap', [
            'model' => $model,
        ]);
    }

    public function actionWork()
    {
        $model = Pages::findOne(['alias' => 'work', 'status' => 1]);
        return $this->render('work', [
            'model' => $model,
        ]);
    }

    public function actionClients()
    {
        $model = Pages::findOne(['alias' => 'clients', 'status' => 1]);
        return $this->render('about', [
            'model' => $model,
        ]);
    }

    public function actionVacancy()
    {
        $model = Pages::findOne(['alias' => 'vacancy', 'status' => 1]);
        return $this->render('about', [
            'model' => $model,
        ]);
    }

    public function actionContract()
    {
        $model = Pages::findOne(['alias' => 'contract', 'status' => 1]);
        return $this->render('work', [
            'model' => $model,
        ]);
    }

    public function actionPayment()
    {
        $model = Pages::findOne(['alias' => 'payment', 'status' => 1]);
        return $this->render('work', [
            'model' => $model,
        ]);
    }

    public function actionRequisites()
    {
        $model = Pages::findOne(['alias' => 'requisites', 'status' => 1]);
        return $this->render('work', [
            'model' => $model,
        ]);
    }
}
