<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 04.02.2016
 * Time: 12:04
 */

namespace app\controllers;


use app\models\Categories;
use app\models\Groups;
use app\models\Items;
use yii\web\Controller;
use yii;
use app\components\Info;

class SearchController extends Controller
{
    public function actionIndex($q)
    {
        $pgq = pg_escape_string($q);
        $groups = Groups::search($pgq,Info::get('load_once'));
        $categories = Categories::search($pgq);
        $eq =  htmlspecialchars($q);
        $this->view->params['q'] = $eq;
        return $this->render('index', [
            'groups' => $groups,
            'categories' => $categories,
            'q' => $eq
        ]);
    }

    public function actionNextPage()
    {
        $req = Yii::$app->request->post();
        $q = $req['q'];
        $page = $req['page'];
        $limit = Info::get('load_next');
        $offset = (int)Info::get('load_once') + (int)$limit * (int)$page;
        $groups = Groups::search($q,$limit,$offset);
        $html = '';

        foreach($groups as $group){
            $html.=$this->renderPartial('_item', ['group' => $group]);
        }

        return $html;
    }
}