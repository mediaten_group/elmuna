<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 28.01.2016
 * Time: 12:13
 */

namespace app\controllers;


use app\components\Blocks;
use app\models\forms\UserDataForm;
use app\models\Order;
use app\models\User;
use app\models\UserProfile;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

class ProfileController extends Controller
{
    public $title = 'Личный кабинет';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $user_id = Yii::$app->user->getId();

        /** @var UserProfile $profile */
        $profile = UserProfile::findOne(['user_id' => $user_id]);
        if (!$profile) throw new Exception('Профиль отсутствует');

//        Blocks::set([
//            'aside' => [
//                'profile',
//            ]
//        ], true);
        Blocks::set([
            'aside' => false
        ]);

        if ($profile->load(Yii::$app->request->post())) {
            if($profile->save()) {
                Yii::$app->session->setFlash('user_profile_form', true);
            }
        }

        /** @var UserDataForm $user_data */
        $user_data_form = new UserDataForm();
        if ($user_data_form->load(Yii::$app->request->post())) {
            if ($user_data_form->submit()) {
                Yii::$app->session->setFlash('user_data_form', true);
                $user_data_form = new UserDataForm();
            }
        }
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        $data = ['username' => $user->username, 'email' => $user->email];

        $user_data_form->load($data, '');

        $orders = Order::getOrders(Yii::$app->getUser()->getId());

        $this->view->title = $this->title;
        $this->view->params['mainBlock'] = true;

        return $this->render('index', [
            'profile' => $profile,
            'user_data_form' => $user_data_form,
            'orders' => $orders,
        ]);
    }

    public function actionEdit()
    {

    }
}