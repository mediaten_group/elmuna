<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.12.2015
 * Time: 12:24
 */

namespace app\controllers;


use app\models\Categories;
use app\models\Items;
use app\tables\Groups;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class GroupController extends Controller
{
    public function actionView($id)
    {
        /** @var Groups $group */
        $group = Groups::find()
            ->where([
                'groups.id' => $id,
                'items.status' => 1])
            ->innerJoin('items', 'items.group_id = groups.id')
            ->one();
        if (!$group) throw new NotFoundHttpException('Группа не найдена');

        /** @var Categories $category */
        $category = Categories::findByGroup($group->id);
        if (!$category) throw new NotFoundHttpException('Категория не найдена');

        $links = Categories::linksById($category['id']);
        $this->view->params['catalog'] = $links[0]['alias'];
        if (!is_array($links) && count($links) !== count(explode(',', $category['path']))) throw new NotFoundHttpException('Категории не совпадают');

        $previous = '';
        $breadcrumbs = [];
        foreach ($links as $l) {
            $breadcrumbs[] = ['label' => $l['title'], 'url' => Url::to(['/catalog/' . $previous . $l['alias']])];
            $previous .= $l['alias'] . '/';
        }
        $breadcrumbs[] = ['label' => $group->title];

        /** @var Categories $categories */
        $categories = Categories::getLeftBar($category['path']);
        if (!$categories) throw new NotFoundHttpException('Категория не найдена');

        $list = Categories::treePrint($categories, $links[0]['id']);

        $items = Items::getByGroup($group->id);

        return $this->render('view', [
            'group' => $group,
            'category' => $category,
            'categories' => $categories,
            'links' => $breadcrumbs,
            'previous' => $links[0]['alias'],
            'list' => $list,
            'items' => $items,
            'parents' => $links
        ]);
    }
}