<?php

namespace app\controllers;

use app\models\forms\ReviewForm;
use app\models\User;
use app\models\UserProfile;
use app\tables\Review;
use yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Pages;

class ReviewsController extends Controller
{
    public $title = 'Отзывы';

    public function actionIndex()
    {
        $this->view->title = $this->title;

        $user = Yii::$app->user;
        $id = $user->getId();
        $review = new ReviewForm(['user_id' => $id]);
        if ($id) {
            $user_profile = UserProfile::findOne(['user_id'=>$id]);
            $review->email = $user_profile->email;
            $review->fio =$user_profile->name;
        }

        if ($review->load(Yii::$app->request->post()) && $review->save()) {
            Yii::$app->session->setFlash('reviewForm', true);

            $review = new ReviewForm(['user_id' => $user->getId()]);
        }

        $query = Review::find()->asArray()->where(['status' => 1]);
        $bread = [
            [
                'label'=>'О компании',
                'url'=>'/about'
            ],
            [
                'label'=>'Отзывы',
            ]
        ];
        $list = Pages::getMenuAbout1();
        $list2 = Pages::getMenuAbout2();
        $main = Pages::getMainAbout();

        $reviews = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'review' => $review,
            'reviews' => $reviews,
            'bread'=>$bread,
            'list'=>$list,
            'list2'=>$list2,
            'main'=>$main
        ]);
    }
}