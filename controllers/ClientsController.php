<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.04.16
 * Time: 10:54
 */

namespace app\controllers;

use yii;
use app\models\Clients;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use app\models\Pages;

class ClientsController extends Controller
{

    public function actionIndex()
    {
        $sql = Clients::getClients();
        $dataProvider = new SqlDataProvider([
            'sql'=>$sql,
            'pagination'=>[
                'pageSize'=>10,
            ],
        ]);
        $bread = [
            [
                'label'=>'О компании',
                'url'=>'/about'
            ],
            [
                'label'=>'Наши клиенты',
            ]
        ];
        $list = Pages::getMenuAbout1();
        $list2 = Pages::getMenuAbout2();
        $main = Pages::getMainAbout();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'list'=>$list,
            'list2'=>$list2,
            'main'=>$main,
            'bread'=>$bread
        ]);
    }

}