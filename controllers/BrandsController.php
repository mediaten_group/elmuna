<?php

namespace app\controllers;

use app\models\CtItems;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class BrandsController extends Controller
{
    public function actionIndex()
    {
        Url::remember();
        $brands = CtItems::brands();

        return $this->render('index', [
            'brands' => $brands,
        ]);
    }

    public function actionOne($alias)
    {
        Url::remember();
        $req = Yii::$app->request;

        $brand = CtItems::brand($alias);
        if($brand == null) throw new NotFoundHttpException;

        $items = CtItems::brandItems($brand['id']);


        return $this->render('items', [
            'brand' => $brand,
            'items' => $items,
        ]);
    }

}