<?php

namespace app\controllers;

use app\components\Cart;
use app\models\CartItems;
use app\models\forms\OrderForm;
use app\models\User;
use app\models\UserProfile;
use app\modules\admin\models\CtOrders;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CartController extends Controller
{
    public $title = 'Корзина';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove-item' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        Url::remember();

        $this->view->title = $this->title;

        $cart = Cart::getCart();

        $items = $cart ? Cart::getItems($cart) : [];

        if (!$items) return $this->render('clear');

        $order = new OrderForm();

        if ($order->load(Yii::$app->request->post())) {
            $order->comment = pg_escape_string($order->comment);
            $order->comment = htmlspecialchars($order->comment);
            if (!$order->save() || !Cart::addOrder($order->id)) throw new Exception(Json::encode($order->errors));
            return $this->render('clear');
        }
        if (!Yii::$app->user->isGuest) {
            /** @var User $user */
            $user = Yii::$app->getUser()->getIdentity();
            $user_data = ['email' => $user->email];

            /** @var UserProfile $profile */
            $profile = UserProfile::getInfo($user->id);

            $order->load($profile, '');
            $order->load($user_data, '');
        }

//        dd($items);
        return $this->render('cart', [
            'cart' => $cart,
            'items' => $items,
            'order' => $order,
        ]);
    }

    public function actionCheckout()
    {
        $cart = Cart::render();
        if (!$cart) return $this->redirect(['index']);

        $req = Yii::$app->request;
        $model = new CtOrders();

        if ($model->load($req->post())) {
            if ($model->checkout()) {
                Yii::$app->session->setFlash('checkout');
            }
        }

        return $this->render('checkout', [
            'model' => $model,
            'cart' => $cart,
        ]);
    }

    public function actionClear()
    {
        unset(Yii::$app->session['cart']);
        return json_encode(['result' => 'ok']);
    }

    public function actionRemoveItem()
    {
        $req = Yii::$app->request;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = $req->post('id');
        if (!$id) return ['status' => 'error', 'message' => 'Нет ID'];

        /** @var CartItems $item */
        $item = CartItems::findOne($id);
        if (!$item) return ['status' => 'error', 'message' => 'Не найден товар'];

        if (!$item->delete()) return ['status' => 'error', 'message' => $item->errors];

        $cart = Cart::getCartByCookie();

        return ['status' => 'ok', 'count' => $cart->count, 'price' => $cart->price];
    }

    public function actionRowCount($key, $way)
    {
        $cart = Yii::$app->session['cart'];

        if (!isset($cart['items'])) return json_encode(['error' => 'cart empty']);
        if (!isset($cart['items'][$key])) return json_encode(['error' => 'key not found']);

        $count = $cart['items'][$key]['count'];

        if ($way) {
            $count++;
        } else {
            if ($count <= 0) return json_encode(['error' => 'count 0']);
            $count--;
        }

        $cart['items'][$key]['count'] = $count;
        Yii::$app->session['cart'] = $cart;

        return json_encode(['result' => 'ok']);
    }

    public function actionCount($item_id, $size = null)
    {
        $cart = Yii::$app->session['cart'];
        if (!isset($cart['items'])) return json_encode(['error' => 'cart empty']);

        if ($size) {
            if (isset($cart['items'][$item_id][$size])) {
                $cart['items'][$item_id][$size]++;
            } else {
                return json_encode(['error' => 'item_unset']);
            }
        } else {
            if (isset($cart['items'][$item_id])) {
                $cart['items'][$item_id][$size]++;
            } else {
                return json_encode(['error' => 'item_unset']);
            }
        }

        Yii::$app->session['cart'] = $cart;

        return json_encode([
            'result' => 'OK',
            'log' => $cart,
        ]);
    }

    public function actionCartHover()
    {
        $cart = Cart::getCart();

        $items = $cart ? Cart::getItems($cart) : [];
        if (!$items) {
            return '';
        } else {
            return $this->renderPartial('cart-hover', [
                'items' => $items,
            ]);
        }
    }

    public function actionCheckValid()
    {
        $not_valid = array();

        $cart = Cart::getCart();
        $items = $cart ? Cart::getItems($cart) : [];
        foreach ($items as $item) {
            if ($item['tech_price'] === 'Ошибка') {
                $not_valid[] = $item['cartItemId'];
            }
        }

        return Json::encode($not_valid);
    }

    public function actionItemQuantity()
    {
        $req = Yii::$app->request;
        $cart_item_id = $req->post('item_id');
        $val = $req->post('val');
        $a = Cart::itemQuantity($cart_item_id, $val);
        return json_encode($a);
    }

}