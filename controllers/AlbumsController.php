<?php

namespace app\controllers;

use app\tables\Albums;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class AlbumsController extends Controller
{
    public function actionIndex($elem_id)
    {
        Url::remember();
        $model = Albums::find()->asArray()->where(['id' => $elem_id])->one();

        return $this->render('albums', [
            'model' => $model,
        ]);
    }
}
