<?php

namespace app\controllers;

use app\models\CtItems;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class ShopController extends Controller
{
    public function actionIndex()
    {
        Url::remember();
        $models = CtItems::categories();

        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionOne($alias)
    {
        Url::remember();
        $req = Yii::$app->request;

        $category = CtItems::category($alias);
        if ($category == null) throw new NotFoundHttpException;

//        $filter = [
//            'and',
//            ['ct_items.color_hex' => $req->get('color')],
//            ['ct_item_sizes.size' => $req->get('size')],
//            ['>=', 'ct_items.price', $req->get('min')],
//            ['<=', 'ct_items.price', $req->get('max')],
//        ];
//
        $items = CtItems::categoryItems($category['id']);
//
//        $sizes = CtItems::categorySizes($category['id']);
//        $prices = CtItems::categoryPrices($category['id']);
//        $colors = CtItems::categoryColors($category['id']);


        return $this->render('items', [
            'category' => $category,
            'items' => $items,
//            'sizes' => $sizes,
//            'prices' => $prices,
//            'colors' => $colors,
        ]);
    }
}
