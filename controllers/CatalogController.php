<?php

namespace app\controllers;

use app\components\Info;
use app\lib\Links;
use app\lib\My;
use app\models\Categories;
use app\models\Colors;
use app\models\forms\FilterForm;
use app\models\Pages;
use yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;
use yii\data\SqlDataProvider;


class CatalogController extends Controller
{
    public function actionIndex($category = null)
    {
        Url::remember();

        $uri = explode('/', trim(preg_replace('/\/+/', '/', $category), '/'));
        if (!isset($uri[0])) {
            return $this->render('404');
        }
        if ($uri[0] == "next-page") {
            return $this->nextPage();
        }
        if (!isset($uri[1])) {
            return $this->main($uri);
        };

        return $this->catalog($uri);
    }

    public function actionOne($alias)
    {
        Url::remember();
        $req = Yii::$app->request;

        $category = CtItems::category($alias);
        if ($category == null) throw new NotFoundHttpException;

//        $filter = [
//            'and',
//            ['ct_items.color_hex' => $req->get('color')],
//            ['ct_item_sizes.size' => $req->get('size')],
//            ['>=', 'ct_items.price', $req->get('min')],
//            ['<=', 'ct_items.price', $req->get('max')],
//        ];
//
        $items = CtItems::categoryItems($category['id']);
//
//        $sizes = CtItems::categorySizes($category['id']);
//        $prices = CtItems::categoryPrices($category['id']);
//        $colors = CtItems::categoryColors($category['id']);


        return $this->render('items', [
            'category' => $category,
            'items' => $items,
//            'sizes' => $sizes,
//            'prices' => $prices,
//            'colors' => $colors,
        ]);
    }

    public function actionCatalog()
    {
        Url::remember();

        $uri = My::getUri();
        if (!isset($uri[1])) throw new NotFoundHttpException;
        if (!isset($uri[2])) return $this->catalog($uri);
        return 'работает';
    }

    public function main($uri)
    {
        /** @var Categories $current */
        $current = Categories::oneByAlias(end($uri));
        if (!$current) throw new NotFoundHttpException('Категория не найдена');
        $this->view->params['catalog'] = $current->alias;

        $categories = Categories::getChilds($current['path']);
        if (!$categories) return $this->catalog($uri, true);

        Url::remember();

        return $this->render('index', [
            'current' => $current,
            'categories' => $categories,
        ]);
    }

    public function catalog(Array $uri, $show_items = false)
    {
//        print_r('check');

        $parents = Categories::links($uri);
        $this->view->params['catalog'] = $parents[0]['alias'];
        if (count($parents) !== count($uri)) throw new NotFoundHttpException;

        /** @var Categories $current */
        $current = Categories::oneByAlias(['alias' => end($uri)]);

        $page = false;

        if ($current->alias == 'imennye-lozhki') {
            $page = Pages::findOne('imena');
        }

        $list = [];
        if (!$show_items) {
            $categories = Categories::getLeftBar($current['path']);

            if (!$categories) throw new NotFoundHttpException('Категория не найдена');
//            pree($parents[0]['id']);
            $list = Categories::treePrint($categories, $parents[0]['id']);
        }

        $filters = new FilterForm();
        $filters->loadFilter(Yii::$app->request->get());
        $links = [];
        $previous = '';
        $poly = false;
        foreach ($parents as $p) {
            $links[] = ['label' => $p['title'], 'url' => Url::to(['/catalog/' . $previous . $p['alias']])];
            $previous .= $p['alias'] . '/';
            if (in_array($p['id'], $current['idNoFilter'])) {
                $filters = [];
            }
            if ($p['alias'] == 'polygraphy') {
                $poly = true;
            }
        }

        $last_link = array_pop($links);
        unset($last_link['url']);
        $links[] = $last_link;

        $previous = $parents[0]['alias'];

        Url::remember();

        if ($current->view == '0') {
            $groups = Categories::getItems($current['id'], $filters, 0, Info::get('load_once'));
            $colors = Colors::getInCategory($current->id);
            $material_list = Categories::getMaterials($current['id']);
            $color_list = Categories::getColors($current['id']);
            return $this->render('catalog', [
                'current' => $current,
                'groups' => $groups,
                'links' => $links,
                'parents' => $parents,
                'previous' => $previous,
                'categories' => $list,
                'colors' => $colors,
                'filters' => $filters,
                'material_list' => $material_list,
                'color_list' => $color_list,
                'page' => $page,
                'poly' => $poly
            ]);
        } elseif ($current->view == '1') {
            $categories = Categories::getChilds($current['id']);
//            print_r(Yii::$app->ur
            return $this->render('sub_category', [
                'current' => $current,
                'categories' => $categories,
                'previous' => $previous,
                'parents' => $parents,
                'menu' => $list,
                'links' => $links,
            ]);
        } else {
            $page = Pages::findOne($current->view);
            return $this->render('page', [
                'current' => $current,
                'links' => $links,
                'parents' => $parents,
                'previous' => $previous,
                'categories' => $list,
                'page' => $page,
            ]);
        }
    }

    public function nextPage()
    {

        $req = Yii::$app->request->post();
        $current = $req['id'];
        $page = $req['page'];
        $filters = new FilterForm();
        if (isset($req['filter'])) {
            $filters->loadFilter($req['filter']);
        }
        $limit = Info::get('load_next');
        $offset = (int)Info::get('load_once') + (int)$limit * (int)$page;
        $groups = Categories::getItems($current, $filters, $offset, $limit);
        $html = '';

        foreach ($groups as $group) {
            $html .= $this->renderPartial('_item', ['group' => $group]);
        }

        return $html;
    }

//    public function actionCatalog($level1, $level2, $level3 = false)
//    {
//        Url::remember();
//
//        $category = Categories::one($level3 ? $level3 : $level2);
//        $categories = Categories::byAlias($level1);
//        if (!$categories) throw new NotFoundHttpException('categories not found');
//
//        $childs = Categories::byAlias($level2);
//        if (!$childs) throw new NotFoundHttpException('childs not found');
//
//        $items = Categories::items($category['id'], $category['level']);
//
//        $links = [];
//        $link1 = Categories::getLink($level1, 1);
//        $links[] = ['label' => $link1['title'], 'url' => ['/catalog/index', 'alias' => $link1['alias']]];
//        $link2 = Categories::getLink($level2, 2);
//        $links[] = ['label' => $link2['title'], 'url' => ['/catalog/catalog', 'level1' => $link1['alias'], 'level2' => $link2['alias']]];
//        if ($level3) {
//            $link3 = Categories::getLink($level3, 3);
//            $links[] = ['label' => $link3['title'], 'url' => ['/catalog/catalog', 'level1' => $link1['alias'], 'level2' => $link2['alias'], 'level3' => $link3['alias']]];
//        }
//
//        $lastLink = array_pop($links);
//        array_pop($lastLink);
//        $links[] = $lastLink;
//
//        return $this->render('catalog', [
//            'category' => $category,
//            'categories' => $categories,
//            'level1' => $level1,
//            'level2' => $level2,
//            'childs' => $childs,
//            'items' => $items,
//            'links' => $links,
//        ]);
//
//    }
}
