<?php

namespace app\controllers;

use app\components\Blocks;
use app\components\Cart;
use app\models\Categories;
use app\models\forms\PasswordResetRequestForm;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\forms\LoginForm;
use app\models\User;
use app\tables\CategoriesGifts;
use app\tables\UserProfile;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        Url::remember();

        $mainCategories = Categories::getCatTree();

        Blocks::set([
            'bottom' => [
                'technology',
                'gifts',
            ]
        ]);

        $this->view->params['mainBlock'] = true;

        return $this->render('index', [
            'categories' => $mainCategories,
        ]);
    }

    public function actionAjax()
    {
        $req = Yii::$app->request;

        if (!$req->isAjax) throw new NotFoundHttpException;

        $id = $req->post('id');
        $parent = $req->post('parent');
        $position = $req->post('position');

        if (!$id || !$parent || !$position) throw new ErrorException($id . ' ' . $parent . ' ' . $position);

        /** @var CategoriesGifts $model */
        $model = CategoriesGifts::findOne($id);

        if (!$model) throw new ErrorException($id);

        $model->parent_id = $parent;
        $model->position = $position;
        $model->save();

        return '����������';
    }

    public function actionLogin()
    {
        $this->layout = 'clean';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Cart::merge();
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        $url = Url::previous();

        $cookies = Yii::$app->response->cookies;
        if ((Cart::getCookie())) {
            $cookies->remove('cart');
        }

        Yii::$app->user->logout();
        return $this->goBack($url);
    }

    public function actionSignup()
    {
        $req = Yii::$app->request->post();
        $model = new SignupForm();
        if ($model->load($req)) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    Cart::merge();
                    $profile = new UserProfile();
                    $profile->user_id = Yii::$app->getUser()->getId();
                    $profile->email = $model->email;
                    $profile->save();
                    Yii::$app->session->setFlash('just-registered');
                    return $this->goHome();
                }
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionActivateEmail($code)
    {
        $code = base64_decode($code);
        $parts = explode(';', $code);
        $email = isset($parts[0]) ? $parts[0] : null;
        $key = isset($parts[1]) ? $parts[1] : null;
        if ($email == null || $key == null) return $this->render('activate-email', ['message' => 'Неверный код активации', 'code' => 0]);
        $user = User::findOne(['email' => $email]);
        if ($user->auth_key === $key) {
            if ($user->activate != 1) {
                $user->activate = 1;
                $user->save();
                return $this->render('activate-email', ['message' => 'Email успешно активирован', 'code' => 0]);
            } else {
                return $this->render('activate-email', ['message' => 'Email уже активирован', 'code' => 1]);
            }
        } else {
            return $this->render('activate-email', ['message' => 'Неверный код активации', 'code' => 2]);
        }
    }

    public function actionChangeEmail($code)
    {
        $code = base64_decode($code);
        $parts = explode(';', $code);
        $email = isset($parts[0]) ? $parts[0] : null;
        $key = isset($parts[1]) ? $parts[1] : null;
        if ($email == null || $key == null) return $this->render('activate-email', ['message' => 'Неверный код активации', 'code' => 0]);
        $user = User::findOne(['email' => $email]);
        if ($user->auth_key === $key) {
            if ($user->activate != 1) {
                $user->activate = 1;
                $user->save();
                return $this->render('activate-email', ['message' => 'Email успешно активирован', 'code' => 0]);
            } else {
                return $this->render('activate-email', ['message' => 'Email уже активирован', 'code' => 1]);
            }
        } else {
            return $this->render('activate-email', ['message' => 'Неверный код активации', 'code' => 2]);
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте вашу почту для дальнейших инструкций.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем сбросить пароль для указанной почты.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль был сохранен.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionDownload($name)
    {
        $file = Yii::getAlias('@webroot') . '/files/' . $name;

        if (file_exists($file)) {
            if (ob_get_level()) {
                ob_end_clean();
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
        else {
            $session = Yii::$app->session;
            $session->setFlash('error', 'File not found.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
}
