<?php

namespace app\controllers;

use app\components\Cart;
use app\lib\field\Area;
use app\lib\field\cart\FloatField;
use app\lib\field\Checkbox;
use app\lib\field\Color;
use app\lib\field\Number;
use app\lib\field\Select;
use app\models\Categories;
use app\models\Groups;
use app\models\Items;
use app\models\TechPlaces;
use app\tables\Field;
use app\tables\FieldColors;
use app\models\Scheme;
use app\tables\FieldOptions;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ItemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-to-cart' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $category = Items::category($id);


        $links = [];
        if ($category['level'] == 3) {
            $level3 = $category;
            $level2 = Categories::oneById($category['parent_id']);
            $level1 = Categories::oneById($level2['parent_id']);
            $links[] = ['label' => $level1['title'], 'url' => ['/catalog/index', 'alias' => $level1['alias']]];
            $links[] = ['label' => $level2['title'], 'url' => ['/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $level2['alias']]];
            $links[] = ['label' => $level3['title'], 'url' => ['/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $level2['alias'], 'level3' => $level3['alias']]];
        } else {
            $level2 = $category;
            $level1 = Categories::oneById($level2['parent_id']);
            $links[] = ['label' => $level1['title'], 'url' => ['/catalog/index', 'alias' => $level1['alias']]];
            $links[] = ['label' => $level2['title'], 'url' => ['/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $level2['alias']]];
        }

        $item = Items::one($id);

        $links[] = ['label' => $item['title']];

        $categories = Categories::byAlias($level1['alias']);
        $childs = Categories::byAlias($level2['alias']);

        $recent = Items::recent($item['category_id']);

        /*
        $recent = CtItems::recentItems($item['category_id'], 3, $id);

        if ($item === null) throw new NotFoundHttpException;

        $images = CtItemImages::find()
            ->asArray()
            ->where(['item_id' => $item['id']])
            ->orderBy(['date' => SORT_DESC])
            ->all();

        /*$sizes = CtItemSizes::find()
            ->asArray()
            ->select(['size'])
            ->where([
                'item_id' => $item['id'],
            ])
            ->orderBy(['size' => SORT_ASC])
            ->all();

        return $this->render('index', [
            'item' => $item,
            'images' => $images,
            'recent' => $recent,
        ]);*/
        return $this->render('index', [
            'links' => $links,
            'category' => $category,
            'item' => $item,
            'level1' => $level1,
            'level2' => $level2,
            'categories' => $categories,
            'childs' => $childs,
            'recent' => $recent,
        ]);
    }

    public function actionView($id, $alias = '')
    {
        /** @var Items $item */
        $item = $alias ? Items::oneByCondition(['id' => $id, 'alias' => $alias, 'status' => 1]) :
            Items::oneByCondition(['id' => $id, 'status' => 1]);
        if (!$item) throw new NotFoundHttpException('Товар не найден');

        $images = Items::getImages($item['id']);
//dd($images);
        /** @var Groups $group */
        $group = Groups::findOne(['id' => $item['group_id'], 'status' => 1]);
        if (!$group) throw new NotFoundHttpException('Группа не найдена');

        /** @var Items $groupItems */
        $groupItems = Items::find()
            ->asArray()
            ->select([
                'items.id',
                'items.color',
                'items.alias',
                'upload.name as image',
                'items.params'
            ])
            ->where(['items.group_id' => $group->id, 'items.status' => 1])
            ->andWhere(['not', ['items.id' => $id]])
            ->innerJoin('upload','upload.id = items.cover_image_id')
            ->all();

//        Yii::$app->session->remove('cart');

        /** @var Categories $category */
        $category = Categories::findByGroup($group->id);
        if (!$category) throw new NotFoundHttpException('Категория не найдена');

        $always_tech = false;

        $recent = Items::getRecent($category['id'], 6, $item['group_id']);

        $links = Categories::linksById($category['id']);
        if (!is_array($links) && count($links) !== count(explode(',', $category['path']))) throw new NotFoundHttpException('Категории не совпадают');
        $this->view->params['catalog'] = $links[0]['alias'];
        $previous = '';
        $breadcrumbs = [];
        foreach ($links as $l) {
            $breadcrumbs[] = ['label' => $l['title'], 'url' => Url::to(['/catalog/' . $previous . $l['alias']])];
            $previous .= $l['alias'] . '/';

            if (in_array($l['id'], Categories::getIdNoFilter())) {
                $always_tech = true;
            }
        }
        $breadcrumbs[] = ['label' => $item['title']];

        /** @var Categories $categories */
        $categories = Categories::getLeftBar($category['path']);
        if (!$categories) throw new NotFoundHttpException('Категория не найдена');

        $list = Categories::treePrint($categories, $links[0]['id']);

        $this->view->title = $item['title'];

        Url::remember();

        $scheme = Scheme::getGroupScheme($group->scheme_group_id, $group->scheme_group_id_2);
        $tech_places = TechPlaces::getPlaces($group->id);

        return $this->render('view', [
            'item' => $item,
            'group' => $group,
            'recent' => $recent,
            'images' => $images,
            'groupItems' => $groupItems,
            'category' => $category,
            'categories' => $categories,
            'links' => $breadcrumbs,
            'previous' => $links[0]['alias'],
            'list' => $list,
            'scheme' => $scheme,
            'tech_places' => $tech_places,
            'parents' => $links,
            'is_flash' => Groups::is_flash($item['group_id']),
            'always_tech' => $always_tech
        ]);
    }

    public function actionAddToCart()
    {
        $req = Yii::$app->request;
//        print_r($_POST);
        if (!$req->isAjax) throw new NotFoundHttpException;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = (int)$req->post('id');
        $count = (int)$req->post('count');
        $tech = $req->post('tech');
//        print_r($cart);

        /* Валидация переданных данных нанесения */

        $left = array();
        $right = array();
        $errors = [];
        if (isset($tech['left'])) {
            foreach ($tech['left'] as $key => $field) {
                if ($field) {
                    $left[$key] = $field;
                    $f = Field::findOne($key);
                    if (!$f) {
                        $errors[] = "Одного из полей не существует";
                    } else {
                        switch ($f->getFieldType()->one()->type) {
                            case 'area':
                                Area::checkTech($f, $field, $errors);
                                break;
                            case 'color':
                                Color::checkTech($f, $field, $errors);
                                break;
                            case 'number':
                                Number::checkTech($f, $field, $errors);
                                break;
                            case 'select':
                                Select::checkTech($f, $field, $errors);
                                break;
                            case 'checkbox':
                                Checkbox::checkTech($f, $field, $errors);
                                break;
                        }
                    }

                }
            }
            $tech['left'] = $left;
        }
        if (isset($tech['right'])) {
            foreach ($tech['right'] as $key => $field) {
                if ($field) {
                    $right[$key] = $field;
                    $f = Field::findOne($key);
                    if (!$f) {
                        $errors[] = "Одного из полей не существует";
                    } else {
                        switch ($f->getFieldType()->one()->type) {
                            case 'area':
                                Area::checkTech($f, $field, $errors);
                                break;
                            case 'color':
                                Color::checkTech($f, $field, $errors);
                                break;
                            case 'number':
                                Number::checkTech($f, $field, $errors);
                                break;
                            case 'select':
                                Select::checkTech($f, $field, $errors);
                                break;
                            case 'checkbox':
                                Checkbox::checkTech($f, $field, $errors);
                                break;
                        }
                    }

                }
            }
            $tech['right'] = $right;
        }
        $errors = implode('<br>', $errors);

        /* Добавление провалидированных данных в корзину*/
        $cart = Cart::getCartByCookie(null, true);

        /** @var Items $item */
        $item = Items::findOne(['id' => $id, 'status' => 1]);
        if (!$item) throw new NotFoundHttpException;

        if ($cart->isNewRecord && !$cart->save()) throw new Exception(Json::encode($cart->errors));

        if (!$errors)
            Cart::addItem($cart->id, $item->id, $count, $tech);
        $hash = Cart::getHash(['id' => $cart->id, 'key' => $cart->key]);

        Cart::setByCartId($cart->id);
        if ($errors)
            return ['status' => 'fail', 'errors' => $errors, 'tech' => $tech, 'hash' => $hash];
        else
            return ['status' => 'ok', 'price' => Cart::getPrice(), 'count' => Cart::getCount(), 'hash' => $hash, 'tech' => $tech, 'cart' => $cart->id];
    }


}
