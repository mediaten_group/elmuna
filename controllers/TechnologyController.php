<?php

namespace app\controllers;

use app\models\Technology;
use app\components\Info;
use app\tables\TechnologyUploads;
use MongoDB\Collection;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\lib\My;
use yii\web\NotFoundHttpException;

class TechnologyController extends Controller
{
    public function actionIndex()
    {
        $uri = My::getUri();
        $current = new Technology();
        $current->id = 0;
        $current->title = 'Технологии нанесения';
        $this->view->title = $current->title;
        $current->alias = end($uri);
//        $technologies = Technology::all();
        $technologies = Technology::find()
            ->asArray()
            ->select([
                'technology.id',
                'technology.alias',
                'technology.title',
                '(SELECT upload."name" FROM upload WHERE upload.id = technology.cover_image_id) AS src',
            ])
            ->where('status')
            ->orderBy('sort')
            ->all();
        Url::remember();

        return $this->render('index', [
            'technologies' => $technologies,
            'current' => $current,
        ]);
    }

    public function actionView($alias)
    {
        $uri = $uri = My::getUri();
        /** @var Technology $current */
        $current = Technology::oneByAlias(end($uri));
        if (!$current || !$current->status) {
            throw new NotFoundHttpException;
        }

        $parent = new Technology();
        $parent['id'] = 0;
        $parent['title'] = 'Технологии нанесения';
        $parent['alias'] = $uri[0];

        $technologies = Technology::all();
        $images = TechnologyUploads::find()
            ->asArray()
            ->select([
                'name'
            ])
            ->where([
                'technology_id' => $current->id
            ])
            ->innerJoin('upload', 'technology_uploads.upload_id = upload.id')
            ->column();
        $main_image = $current->coverImage->name;

        $sum = Info::get('min_sum');
        $this->view->title = $current->meta_title ?: $current->title;

        Url::remember();

        return $this->render('view', [
            'technologies' => $technologies,
            'current' => $current,
            'parent' => $parent,
            'sum' => $sum,
            'images' => $images,
            'main_image' => $main_image
        ]);
    }
}