<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 16.12.2015
 * Time: 11:54
 */

namespace app\widgets;


use yii\base\Widget;
use yii\helpers\Html;

class LeftNav extends Widget
{
    public $list;
    public $current;
    public $previous = '';
    public $category = [];
    public $type;

    public function run()
    {
        $html = '';
        $cl = 'main';
        if ($this->category) {
            if ($this->type == 'pages') {
                if ($this->current == $this->category['alias']) {
                    $cl .= ' active';
                }
            } elseif ($this->current == $this->category['id']) {
                $cl .= ' active';
            }
        }
        if ($this->category) {
            if ($this->type == 'pages') {
                $html .= Html::a('<span>'.$this->category['title'].'</span>', ['/' . $this->category['alias']], ['class' => $cl]);
            } else {
                $html .= Html::a('<span>'.$this->category['title'].'</span>', ['/catalog/' . $this->category['alias']], ['class' => $cl]);
            }
        }
        $html .= Html::tag('div', $this->renderList($this->list, $this->previous), [
            'class' => 'left-nav'
        ]);
        return $html;
    }

    public function init()
    {

    }

    public function renderList(Array $list, $previous = '')
    {
        $html = '';
        $active = [];

        $html .= Html::beginTag('ul', ['class' => 'list-unstyled']);
        foreach ($list as $l) {
            if ($this->type == 'pages') {
                if ($l['alias'] == $this->current) $active = ['class' => 'active'];
            } else {
                if ($l['id'] == $this->current) $active = ['class' => 'active'];
            }
            $html .= Html::beginTag('li', $active);
            if ($this->type == 'pages') {
                $html .= Html::a($l['title'], ['/' . $previous . $l['alias']]);
            } else {
                $html .= Html::a($l['title'], ['/catalog/' . $previous . $l['alias']]);
            }
            $html .= Html::endTag('li');
            if ($this->type == 'pages') {
                if ($l['alias'] == $this->current) $active = [];
            } else {
                if ($l['id'] == $this->current) $active = [];
            }
            if (isset($l['children']) && $l['children']) $html .= $this->renderList($l['children'], $previous . $l['alias'] . '/');
        }
        $html .= Html::endTag('ul');

        return $html;
    }
}