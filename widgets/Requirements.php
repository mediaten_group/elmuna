<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 13.04.16
 * Time: 15:46
 */

namespace app\widgets;


use yii\base\Widget;

class Requirements extends Widget
{

    public function run()
    {
        $html = "<a href='/requirements' class='requirements'><i class='ion-alert-circled'></i>Требования к макетам</a>";
        return $html;
    }

    public function init()
    {

    }

}