<?php
namespace app\widgets\mainSlider;

use app\lib\My;
use drmabuse\slick\SlickWidget;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class MainSlider extends Widget
{
    public $title;
    public $alias;
    public $items;

    public function run()
    {
        $id = $this->getId();
        SlickWidget::widget([
            'container' => '#' . $id . ' .items',
            'settings' => My::slickSettings(),
        ]);

        $items = '';
        $count = 0;
        foreach ($this->items as $item) {
            $childs = '';
            if (isset($item['children'])) {
                $i = 0;
                $count++;
                foreach ($item['children'] as $c) {
                    $childs .= Html::tag('li', Html::a($c['title'],
                        ['/catalog/' . $this->alias . '/' . $item['alias'] . '/' . $c['alias']]));
                    $i++;
                    if ($i > 3) break;
                }
                $img = Html::img(My::thumb('/uploads/images/thumbs/'.$item['image']));
                $a = Html::a('Посмотреть все...', ['/catalog/index', 'category' => $this->alias . '/' . $item['alias']]);
                $items .= <<<HTML
    <div class="card effect__hover item">
        <div class="card__front">
            <div class="parent">
                <div class="image">$img</div>
            </div>
            <div class="title">{$item['title']}</div>
        </div>
        <div class="card__back">
            <div class="title"><h4>{$item['title']}</h4></div>
                <ul class="list-unstyled">
                    $childs
                </ul>
            $a
        </div>
    </div>
HTML;
            } else {
                $img = Html::img(My::thumb('/uploads/images/thumbs/'.$item['image']));
                $url = Url::to(['/catalog/index', 'category' => $this->alias . '/' . $item['alias']]);
                $items .= <<<HTML
                    <div class="card item">
                        <a href="$url">
                            <div class="card__front">
                                <div class="parent">
                                    <div class="image">$img</div>
                                </div>
                                <div class="title">{$item['title']}</div>
                            </div>
                        </a>
                    </div>
HTML;
            }
        }
        $a = Html::a($this->title, ['/catalog/index', 'category' => $this->alias]);

        return <<<HTML
<div id="$id" class="slider">
    <div class="header">$a</div>
    <div class="items">
        $items
    </div>
</div>
HTML;
    }
}
