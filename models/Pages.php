<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 13.04.16
 * Time: 16:38
 */

namespace app\models;


class Pages extends \app\tables\Pages
{

    public static function getMenuAbout1()
    {
        $a = [
            [
                'title' => 'Наши клиенты',
                'alias' => 'clients'
            ],
            [
                'title' => 'Отзывы',
                'alias' => 'reviews'
            ],
            [
                'title' => 'Вакансии',
                'alias' => 'vacancy'
            ],
        ];
        return $a;
    }

    public static function getMenuAbout2()
    {
        $a = [
            [
                'title' => 'Новости',
                'alias' => 'news'
            ],
        ];
        return $a;
    }

    public static function getMenuWork()
    {
        $a = [
            [
                'title' => 'Заключение договора',
                'alias' => 'contract'
            ],
            [
                'title' => 'Оплата',
                'alias' => 'payment'
            ],
            [
                'title' => 'Реквизиты ООО и ИП',
                'alias' => 'requisites'
            ],
            [
                'title' => 'Доставка',
                'alias' => 'delivery'
            ],
        ];
        return $a;
    }

    public static function getMainAbout()
    {
        $a = [
            'title'=>'О компании',
            'alias'=>'about'
        ];
        return $a;
    }

    public static function getMainWork()
    {
        $a = [
            'title'=>'Условия работы',
            'alias'=>'work'
        ];
        return $a;
    }

    public static function getBreadcrumbs($model)
    {
        /** @var $model Pages */

        $bread = [];
        if (in_array($model->alias, ['clients', 'reviews', 'vacancy', 'news'])) {
            $bread = [
                [
                    'label'=>'О компании',
                    'url'=>'/about'
                ],
                [
                    'label'=>$model->title,
                ]
            ];
        } elseif (in_array($model->alias, ['contract', 'payment', 'requisites', 'delivery'])) {
            $bread = [
                [
                    'label'=>'Условия работы',
                    'url'=>'/work'
                ],
                [
                    'label'=>$model->title,
                ]
            ];
        } else {
            $bread = [
                [
                    'label'=>$model->title,
                ]
            ];
        }
        return $bread;
    }

    public static function getOtherMenus($title,$alias)
    {
        $a = [
            [
                'title' => $title,
                'alias' => $alias
            ],
        ];
        return $a;
    }

}