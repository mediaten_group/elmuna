<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 02.03.16
 * Time: 15:04
 */

namespace app\models;

use app\tables\FieldColors;
use app\tables\FieldOptions;
use app\tables\Upload;
use yii;
use yii\helpers\Json;

class Scheme extends \app\tables\Scheme
{

    //Передается id группы схем
    //Получается массив
    /*
array => [
    scheme_id       // id схемы
    tech_name       // название нанесения
    fields => [     // поля
        sort => [   // порядок
            0       // id поля
            1       // массив цветов
            2       // массив значений
            3       // цена
            4       // название поля
            5       // тип поля
            6       // обязательность
        ]
    ]
]
     */
    public static function getScheme($id)
    {
        $sql = <<<SQL
SELECT  
  scheme.id AS scheme_id,  
  technology.title AS tech_name,
  technology.alias AS tech_link,
  jsonb_object_agg(field.sort, string_to_array(field.id::text, ',') || 
      image.colors::text || 
      field.value::text || 
      (field."value" -> 'price')::text || 
      field.name::text || 
      field_type.type::text || 
      field.required::text ||
      field.separate::text) AS fields
FROM
  scheme
LEFT JOIN technology ON scheme.tech_id = technology.id
LEFT JOIN field ON scheme.id = field.scheme_id
LEFT JOIN (
  SELECT
    "a"."id" AS field_id,
    jsonb_object_agg (
      field_colors."id",
      upload."name"
    ) AS colors
  FROM
    (
      SELECT
        field."id",
        jsonb_array_elements_text (
          field."value" -> 'select_data'
        ) :: INT AS color_id
      FROM
        field
      WHERE
        field."value" -> 'select_data' IS NOT NULL
    ) AS "a"
  LEFT JOIN field_colors ON field_colors."id" = "a".color_id
  LEFT JOIN upload ON upload."id" = field_colors.upload_id
  WHERE field_colors.status = B'1'
  GROUP BY
    "a"."id"
) AS image ON image.field_id = field."id"
LEFT JOIN field_type ON field.field_type_id = field_type.id
WHERE 
  field.status = B'1'
AND 
  scheme.status = B'1'
AND 
  scheme.group_id = $id
GROUP BY
  scheme.id,
  technology.title,
  technology.alias
SQL;
        $scheme = Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
//        pree($scheme);
        if ($scheme) {
            $scheme = Scheme::check_fields($scheme);
        }
        return $scheme;

    }


    // Передаются id правой и левой групп схем
    // Получается массив схем
    public static function getGroupScheme($left, $right)
    {
        $scheme = [];
        if ($left) {
            $s = Scheme::getScheme($left);
            if ($s) {
                $scheme['left'] = $s;
            }
        }
        if ($right) {
            $s = Scheme::getScheme($left);
            if ($s) {
                $scheme['right'] = $s;
            }
        }
        if ($scheme)
            return $scheme;
        else
            return false;
    }

    //Получается группа схем
    //Отдается группа схем, в которой убраны неправильные схемы и поля
    public static function check_fields($scheme_group)
    {
        foreach ($scheme_group as &$scheme) {
            $fields = Json::decode($scheme['fields']);
            foreach ($fields as $key => &$field) {
                $field['value'] = Field::check_field($field);
                if ($field['value']) {
                    $field['value'] = Json::encode($field['value']);
                } else {
                    unset($fields[$key]);
                }
            }
            $scheme['fields'] = Json::encode($fields);
        }
        return $scheme_group;
    }

    public static function getPrice($tech)
    {
        $tech_price = 0;

        $tech = yii\helpers\Json::decode($tech);

        if ($tech['scheme_left'] != 'NaN') {
            $price = Field::getPrice($tech['left']);
            if ($price['status'] == 'error')
                return 'Ошибка';
            $tech_price += (float)$price['data'];
        }

        if ($tech['scheme_right'] != 'NaN') {
            $price = Field::getPrice($tech['right']);
            if ($price['status'] == 'error')
                return 'Ошибка';
            $tech_price += (float)$price['data'];
        }

        return $tech_price;
    }

    public static function getPriceFields($tech)
    {

        $tech = yii\helpers\Json::decode($tech);

        $priceFields = array();

        if ($tech) {
            if ($tech['scheme_left'] != 'NaN') {
                $price = Field::getPriceFields($tech['left']);
                if ($price['status'] == 'error') {
                    return 'Ошибка';
                } else {
                    $priceFields['left']['text'] = $price['data'];
                }
                if (isset($tech['image_left']))
                    $priceFields['left']['image'] = [
                        'type' => 'image',
                        'image' => Upload::findOne($tech['image_left'])->name,
                    ];
            }

            if ($tech['scheme_right'] != 'NaN') {
                $price = Field::getPriceFields($tech['right']);
                if ($price['status'] == 'error') {
                    return 'Ошибка';
                } else {
                    $priceFields['right']['text'] = $price['data'];
                }
                if (isset($tech['image_right']))
                    $priceFields['right']['image'] = [
                        'type' => 'image',
                        'image' => Upload::findOne($tech['image_right'])->name,
                    ];
            }
        }

        return $priceFields;
    }

    public static function getSchemeName($tech)
    {
        $tech = Json::decode($tech);
        $tech_name = '';
        if ($tech) {
            if ($tech['scheme_left'] != 'NaN') {
                $scheme = Scheme::findOne($tech['scheme_left']);
                if ($scheme) {
                    $tech_name = $scheme->getTech()->one()->title;
                }
            }
            if ($tech['scheme_right'] != 'NaN') {
                $scheme = Scheme::findOne($tech['scheme_right']);
                if ($scheme) {
                    $tech_name .= '<br>' . $scheme->getTech()->one()->title;
                }
            }
        }

        return $tech_name;
    }

}