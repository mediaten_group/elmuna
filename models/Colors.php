<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 28.12.2015
 * Time: 12:55
 */

namespace app\models;


class Colors extends \app\tables\Colors
{
    public static function getInCategory($cid)
    {
        return self::find()
            ->asArray()
            ->select([
                'colors.*'
            ])
            ->distinct()
            ->where(
                "categories.[[path]] && string_to_array(:cid,',') :: integer[]",
                [':cid' => $cid]
            )
            ->leftJoin('items', 'items.color = colors.hex')
            ->leftJoin('groups', 'groups.id = items.group_id')
            ->leftJoin('category_link', 'category_link.group_id = groups.id')
            ->leftJoin('categories', 'categories.id = category_link.category_id')
            ->all();
    }
}