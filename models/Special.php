<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 14.03.16
 * Time: 10:23
 */

namespace app\models;


class Special extends \app\tables\Special
{

    public static function getSpecial() 
    {
        $special = Special::find()
            ->asArray()
            ->select([
                'title',
                'upload.name as image',
                'url',
                'text'
            ])
            ->where([
                'status' => "1"
            ])
            ->leftJoin('upload','upload.id = special.image')
            ->orderBy('sort DESC')
            ->one();
        return $special;
    }
    
}