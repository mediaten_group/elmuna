<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.04.16
 * Time: 12:55
 */

namespace app\models;

use yii;

class Clients extends \app\tables\Clients
{

    public static function getClients()
    {
        $sql = <<<SQL
SELECT
	"public".clients.title,
	"public".clients.description,
	(
		SELECT
			upload."name"
		FROM
			upload
		WHERE
			upload."id" = "public".clients.upload_id
	) AS logo,
	jsonb_agg("public".upload. NAME) AS works
FROM
	"public".clients
LEFT JOIN "public".clients_uploads ON "public".clients_uploads.client_id = "public".clients."id"
LEFT JOIN "public".upload ON "public".clients_uploads.upload_id = "public".upload."id"
WHERE
	"public".clients.status = B'1'
GROUP BY
	"public".clients."id"
ORDER BY 
    "public".clients.sort
SQL;

        return $sql;

    }

}