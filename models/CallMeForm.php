<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CallMeForm extends Model
{
    public $name;
    public $phone;

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            ['phone', 'filter', 'filter' => function ($value) {
                return trim(htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8'));
            }],
        ];
    }

    public function sendMail()
    {
        $adminEmail = Yii::$app->params['adminEmail'];
        $adminName = Yii::$app->params['adminName'];

        $body = <<<HTML
<b>Имя : </b> $this->name<br>
<b>Телефон : </b> $this->phone<br>
HTML;

        return Yii::$app->mailer->compose()
            ->setTo($adminEmail)
            ->setFrom([$adminEmail => $adminName])
            ->setSubject('Заказ звонка с сайта')
            ->setHtmlBody($body)
            ->send();
    }
}
