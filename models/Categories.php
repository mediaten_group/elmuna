<?php
/**
 * Created by PhpStorm.
 * User: dan.judex
 * Date: 05.10.2015
 * Time: 10:28
 */

namespace app\models;

use app\lib\My;
use app\models\forms\FilterForm;
use Yii;
use yii\db\Query;
use yii\helpers\Json;

class Categories extends \app\tables\Categories
{
    public $idNoFilter = [6431, 6413, 6451];

    public static function getIdNoFilter()
    {

        return [6431, 6413, 6451];
    }

    public static function mainMenu()
    {
        return self::find()->asArray()
            ->select(['alias', 'name', 'lvl'])
            ->where(['on_front_page' => 1, 'lvl' => 0])->all();
    }

    public static function frontPage()
    {
        return self::find()->asArray()
            ->select(['id', 'alias', 'name', 'image'])
            ->where(['on_front_page' => 1])
            ->orderBy('root, lft')
            ->all();
    }

    public static function links(Array $uri)
    {
        return self::find()
            ->asArray()
            ->select(['id', 'alias', 'title'])
            ->where(['alias' => $uri, 'status' => 1])
            ->orderBy('path')
            ->all();
    }

    public static function getList($maxDepth = 3)
    {
        $sql = <<<SQL
SELECT
	"categories".title,
	"categories".id,
	"categories".alias,
	"categories".pid,
	"categories".position,
	"upload".name AS "image"
FROM
	(
		SELECT DISTINCT
			UNNEST (categories."path") AS category_id
		FROM
			categories
-- 		INNER JOIN category_link ON category_link.category_id = categories."id"
        WHERE categories.status = 1
	) AS list
LEFT JOIN categories ON categories. ID = category_id
LEFT JOIN upload ON upload. ID = categories.cover_image_id
WHERE array_length("path", 1) <= :maxDepth AND categories.status = 1 AND categories.front_page = 1
ORDER BY categories.pid NULLS FIRST, "position"
SQL;

        return Yii::$app->db
            ->createCommand($sql, [
                ':maxDepth' => $maxDepth
            ])
            ->queryAll();
    }

    public static function linksById($id)
    {
        $sql = <<<SQL
SELECT
	categories."id",
	categories."alias",
	categories."title",
	categories."path"
FROM
	(
		SELECT DISTINCT
			UNNEST (categories."path") AS category_id
		FROM
			categories
		INNER JOIN category_link ON category_link.category_id = categories."id"
		WHERE
			categories."id" = $id
	) AS list
LEFT JOIN categories ON categories."id" = list.category_id
WHERE
	categories.status = 1
ORDER BY categories."path"
SQL;
        return Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
    }

    public static function findByGroup($id)
    {
        $sql = <<<SQL
SELECT
    categories."id",
    categories."title",
    categories."alias",
    array_to_string(categories."path", ',') AS "path"
FROM
    categories
INNER JOIN category_link ON category_link.category_id = categories."id"
INNER JOIN groups ON category_link.group_id = groups."id"
WHERE
    groups."id" = $id AND
    categories."status" = 1
SQL;
        return Yii::$app->db
            ->createCommand($sql)
            ->queryOne();
    }

    public static function getCatTree()
    {
        $list = self::getList();

        $tree = array();
        foreach ($list as $row) {
            if ($row['pid'] === null) $row['pid'] = 0;
            $tree[(int)$row['pid']][] = $row;
        }
        return Categories::treePrint($tree);
    }

    public static function treePrint($tree, $pid = 0, $id = false)
    {
        if (empty($tree[$pid]))
            return;

        $html = [];
        $i = 0;

//        dd($tree);
        foreach ($tree[$pid] as $row) {
            foreach ($row as $key => $value) {
                $html[$i][$key] = $value;

            }
//            $html[$i]['id'] = $row['id'];
//            $html[$i]['title'] = $row['title'];
//            $html[$i]['alias'] = $row['alias'];
//            $html[$i]['image'] = $row['image'];
//            $html[$i]['level'] = $row['level'];
            if (isset($tree[$row['id']])) {

                $html[$i]['children'] = Categories::treePrint($tree, $row['id'], $id);
            }

            $i++;
        }

        return $html;
    }

    public static function oneByAlias($alias)
    {
        return self::find()
            ->select([
                '*',
                'array_to_string("public".categories."path", \',\') AS "path"'
            ])
            ->where(['alias' => $alias, 'status' => 1])
            ->one();
    }

    public static function getChilds($path)
    {
        $sql = <<<SQL
SELECT
    categories."title",
    categories."alias",
    categories."path",
    categories."id",
    (SELECT upload."name" FROM upload WHERE upload.id= categories.cover_image_id) AS src
FROM
    (
		SELECT DISTINCT
			"unnest" ("public".categories."path") AS "id"
		FROM
			"public".categories
-- 		INNER JOIN "public".category_link ON "public".category_link.category_id = "public".categories."id"
		WHERE
			"categories".pid IS NOT NULL
		AND "categories".status = 1
	) AS list
	INNER JOIN categories ON categories."id" = list."id"
WHERE "categories".pid IS NOT NULL
		AND ARRAY [ "categories"."pid" ] && string_to_array('$path', ',') :: INT []
ORDER BY
	categories."pid",
	categories."position",
	categories."title"
SQL;

        return Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
    }

    public static function getLeftBar($path)
    {
        $sql = <<<SQL
SELECT
	"categories"."id",
	"categories"."title",
	"categories"."pid",
	"categories"."alias",
	"categories"."position"
FROM
	(
		SELECT DISTINCT
			"unnest" ("public".categories."path") AS "id"
		FROM
			"public".categories
-- 		INNER JOIN "public".category_link ON "public".category_link.category_id = "public".categories."id"
		WHERE
			"categories".pid IS NOT NULL
		AND "categories".status = 1
	) AS list
INNER JOIN categories ON categories."id" = list."id"
WHERE "categories".pid IS NOT NULL
		AND ARRAY [ "categories"."pid" ] && string_to_array('$path', ',') :: INT []
		AND status = 1
ORDER BY
	categories."pid",
	categories."position",
	categories."title"
SQL;
        $list = Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
        $tree = array();
        foreach ($list as $row) {
            if ($row['pid'] === null) $row['pid'] = 0;
            $tree[(int)$row['pid']][] = $row;
        }
        return $tree;
    }

    public static function getItems($id, $filter, $offset = 0, $limit = 0)
    {
//        pree($filter);
        $priceFrom = '';
        $priceTo = '';
        $material = '';
        $color = '';
        $order = '';
        $limitT = '';
        if ($filter) {
            if (!$filter->type) $filter->type = 'min_price';
            $priceFrom = $filter->priceFrom ? "AND items.price >= {$filter->priceFrom}" : '';
            $priceTo = $filter->priceTo ? "AND items.price <= {$filter->priceTo}" : '';
            $material = $filter->material ? "AND items.material like '%{$filter->material}%'" : '';
            $color = $filter->color ? "AND items.color = '{$filter->color}'" : '';
            $order = 'ORDER BY';
            $order .= " $filter->type";
            $order .= $filter->sort ? " DESC" : " ASC";
            $limitT = '';
            if ($limit)
                $limitT .= 'LIMIT ' . $limit . ' ';
            if ($offset)
                $limitT .= 'OFFSET ' . $offset;
        } else {
            $order = 'ORDER BY groups.sort ASC, min_price ASC';
        }
        $sql = <<<SQL
SELECT
	"public".groups."id",
	"public".groups.title,
	"public".groups.created_at,
	MIN(items.price) as min_price,
  (SELECT upload."name" FROM upload WHERE upload.id= groups.cover_image_id) AS src,
(SELECT json_agg(i)
  FROM (
    SELECT
			items.id,
			items.alias,
			items.title,
			items.price,
			items.article,
			items.color,
			(SELECT upload.name FROM upload WHERE upload.id = items.cover_image_id) AS src,
			items.params
		FROM items WHERE items."group_id" = groups."id" AND items."status" = 1
  ) i
) AS "item",
	"public".groups.status
FROM
	"public".categories
LEFT JOIN "public".category_link ON "public".category_link.category_id = "public".categories."id"
LEFT JOIN "public".groups ON "public".category_link.group_id = "public".groups."id"
INNER JOIN "public".items ON "public".items.group_id = "public".groups."id"
WHERE
	"public".categories."path" && string_to_array('$id', ',') :: INT []
-- AND "public".groups.status = 1
AND "public".items.status = 1
$priceFrom
$priceTo
$material
$color
GROUP BY groups.id
$order
$limitT
SQL;
//        return $sql;
        return Yii::$app->db
            ->createCommand($sql)
            ->query();
    }

    public static function getMaterials($id)
    {
        $sql = <<<SQL
SELECT
	"public".items.material
FROM
	"public".items
INNER JOIN "public".groups ON "public".items.group_id = "public".groups."id"
INNER JOIN "public".category_link ON "public".category_link.group_id = "public".groups."id"
INNER JOIN "public".categories ON "public".category_link.category_id = "public".categories."id"
WHERE
	"public".categories."path" && string_to_array('$id', ',') :: INT []
AND "public".groups.status = 1
GROUP BY
	"public".items.material
SQL;

        $list = Yii::$app->db
            ->createCommand($sql)
            ->query();
        $array = [
            null => 'Выберите материал'
        ];
//        pree($array);
        foreach ($list as $item) {
            $a = preg_split('/; /', $item['material']);
            foreach ($a as $m) {
                $array[$m] = $m;
            }
        }
        $array[null] ='Выберите материал';
        return $array;
    }

    public static function getColors($id)
    {
        $sql = <<<SQL
SELECT
	"public".items.color
FROM
	"public".items
INNER JOIN "public".groups ON "public".items.group_id = "public".groups."id"
INNER JOIN "public".category_link ON "public".category_link.group_id = "public".groups."id"
INNER JOIN "public".categories ON "public".category_link.category_id = "public".categories."id"
WHERE
	"public".categories."path" && string_to_array('$id', ',') :: INT []
AND "public".groups.status = 1
GROUP BY
	"public".items.color
SQL;

        $list = Yii::$app->db
            ->createCommand($sql)
            ->query();
        $array = [];
        foreach ($list as $item) {
            $color = $item['color'];
            if ($color == null) {
                $array[] = 'Выберите цвет';
            } else {
                $array[$color] = "<span class='color' style='background-color: $color'></span>";
            }
        }
        return $array;
    }


    public static function search($q)
    {

        $sql = <<<SQL
SELECT
	categories."id",
	categories."title",
	categories."alias",
  (SELECT upload."name" FROM upload WHERE upload.id= categories.cover_image_id) AS src,
(SELECT json_agg(i)
  FROM (
    SELECT
			cat."id",
			cat."alias",
			cat."title"
		FROM
			categories AS cat
		WHERE
			categories."path" && ARRAY[cat."id"]
		ORDER BY
			cat."path"
		) i
) AS "path"
FROM
	categories
WHERE
	LOWER(categories."title" ) LIKE LOWER('%$q%')
AND
	categories."pid" IS NOT null
AND categories.status = 1
SQL;
        return Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
    }

    public static function parser($title, $href, $parser_ver, $level, $source, $pid, $parent)
    {
        $base_cat = Categories::findOne([
            'title' => 'Сайт gifts.ru',
            'alias' => 'parse-gifts-ru'
        ]);
        $cat_link = CategoriesLinks::findOne([
            'source' => $source,
            'href' => $href
        ]);
        if ($cat_link) {
            $cat = Categories::findOne($cat_link->cat_id);
        } else {
            $cat = new Categories();
            $cat->title = $title;
            $cat->sort = 0;
            $cat->alias = My::str2url($cat->title);
            $cat->text = '';
            $cat->created_at = My::dateTime();
            $cat->front_page = 1;
            $cat->status = 1;
            $p_string = null;
            if ($pid) {
                $cat->pid = $pid;
                $p_string = $base_cat->id . ',' . $pid . ',';
            } else {
                $cat->pid = $base_cat->id;
                $p_string = $base_cat->id . ',';

            }
            $count = Categories::find()
                ->where([
                    'pid' => $cat->pid
                ])
                ->count();
            if ($count) {
                $pos = Categories::find()
                    ->select([
                        'MAX(position) as position'
                    ])
                    ->where([
                        'pid' => $cat->pid
                    ])
                    ->one()->position;
            } else {
                $pos = -1;
            }
            $cat->position = $pos + 1;
            $cat->save();
            $cat->path = '{' . $p_string . $cat->id . '}';
            $cat->save();
            CategoriesLinks::doRecord($href, $title, $level, $parser_ver, $source, $cat->id, $parent);
        }
        return $cat;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (key_exists('alias', $changedAttributes)) {
            $not_unique = Categories::find()
                ->where([
                    'and',
                    "id != $this->id",
                    "alias = '$this->alias'"])
                ->count();
            if ($not_unique) {
                $this->alias = $this->alias . "-$this->id";
                if (!$this->save()) {
                    print_r($this->errors);
                    exit();
                }
            }
        }
    }

}

