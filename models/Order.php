<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 28.01.2016
 * Time: 18:27
 */

namespace app\models;


use app\behaviors\DateTimeBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class Order extends \app\tables\Order
{

    const NOT_READY = 0;
    const IN_WORK = 2;
    const IN_DELIVERY = 3;
    const COMPLETED = 1;
    const CANCELED = -1;

    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (!$this->status) $this->status = 0;
        return true;
    }

    public static function getOrders($user_id)
    {
        $orders = self::find()
            ->asArray()
            ->select([
                'order.*',
            ])
            ->where([
                'order.user_id' => $user_id,
            ])
            ->joinWith(['orderItems' => function ($query) {
                /** @var ActiveQuery $query */
                $query
                    ->select([
                        'items.id',
                        '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS cover',
                        'order_items.*',
                    ])
                    ->leftJoin('items', 'items.id = order_items.item_id');
            }])
            ->orderBy('order.created_at desc')
            ->all();

//        foreach ($orders as &$order) {
//            foreach ($order['orderItems'] as &$item) {
//
//            }
//        }




        return $orders;
    }
}