<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 23.03.16
 * Time: 17:59
 */

namespace app\models;


use app\lib\My;
use app\models\GroupsParser;

class ItemsParser extends \app\tables\ItemsParser
{

    public static function doItem($title, $price, $color_panetone, $sizes, $material, $weight, $box_size, $box_weight,
                                  $box_vol, $box_count, $description, $article, $parser_version, $source,
                                  $category, $group_title = null)
    {
        $item = ItemsParser::findOne(['article' => $article]);
        if ($item) {
            if ($item->parser_version < $parser_version) {                
            } else {
                $item->title = $title;
                $item->price = $price;
                $item->color_panetone = $color_panetone;
                $item->sizes = $sizes;
                $item->material = $material;
                $item->weight = $weight;
                $item->box_size = $box_size;
                $item->box_weight = $box_weight;
                $item->box_vol = $box_vol;
                $item->box_count = $box_count;
                $item->description = $description;
                $item->parser_version = $parser_version;
                $item->date = My::dateTime();
                $item->source = $source;
                $group = ($group_title)?$group_title:$title;
                $item->group_id = GroupsParser::doGroup($category,$group,$parser_version,$source,$item->group_id);
                $item->save();
            }
        } else {
            $item = new ItemsParser();
            $item->title = $title;
            $item->price = $price;
            $item->color_panetone = $color_panetone;
            $item->sizes = $sizes;
            $item->material = $material;
            $item->weight = $weight;
            $item->box_size = $box_size;
            $item->box_weight = $box_weight;
            $item->box_vol = $box_vol;
            $item->box_count = $box_count;
            $item->description = $description;
            $item->parser_version = $parser_version;
            $item->date = My::dateTime();
            $item->source = $source;
            $group = ($group_title)?$group_title:$title;
            $item->group_id = GroupsParser::doGroup($category,$group,$parser_version,$source);
            $item->save();
        }
        return $item->id;
    }

}