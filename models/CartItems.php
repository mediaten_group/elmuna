<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 13:13
 */

namespace app\models;


use app\behaviors\DateTimeBehavior;

class CartItems extends \app\tables\CartItems
{
    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }
}