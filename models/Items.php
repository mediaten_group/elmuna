<?php

namespace app\models;

use yii\db\Query;
use yii;
use yii\helpers\Json;

class Items extends \app\tables\Items
{
    public $src;

    public static function mainPage()
    {
        return (new Query())
            ->select([
                'ct_items.id',
                'ct_items.title',
                'ct_items.alias',
                'ct_items.price',
                'ct_items.color_hex',
                'ct_items.description',
                'IF(cover_image IS NULL, latest_image, cover_image) AS cover',
                'ct_categories.title AS category',
                'ct_categories.alias AS category_alias',
            ])
            ->from('ct_items')
            ->leftJoin('ct_categories', 'ct_categories.id = ct_items.category_id')
            ->where(['ct_categories.main_page' => 1])
            ->orderBy('ct_categories.ordering')
            ->all();
    }

    public static function item($id, $alias)
    {
        return (new Query())
            ->select([
                'ct_items.*',
                'IF(cover_image IS NULL, latest_image, cover_image) AS cover',
                'ct_categories.title AS category_title',
                'ct_categories.alias AS category_alias',
            ])
            ->from('ct_items')
            ->leftJoin('ct_categories', 'ct_categories.id = ct_items.category_id')
            ->where(['ct_items.id' => $id, 'ct_items.alias' => $alias])
            ->one();
    }

    public static function oneByCondition(Array $condition)
    {
        return self::find()
            ->asArray()
            ->select([
                'items.*',
                '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS src',
                'source.url as source_url',
                'source.name as source_name',
                'source.show as source_show'
            ])
            ->leftJoin('source','source.alias = items.source')
            ->where($condition)
            ->one();
    }

    public static function categories()
    {
        return (new Query())
            ->from('ct_categories')
            ->all();
    }

    public static function getByGroup($group_id)
    {
        return self::find()->select([
            'items.*',
            '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS src',
        ])
            ->asArray()
            ->where([
                'items.group_id' => $group_id,
                'items.status' => 1,
            ])
            ->all();
    }

    public static function mainPageCategories()
    {
        return (new Query())
            ->select([
                'ct_categories.title',
                'ct_categories.alias',
            ])
            ->where(['ct_categories.main_page' => 1])
            ->from('ct_categories')
            ->all();
    }

    public static function category($id)
    {
        return (new Query())
            ->select([
                'items.category_id',
                'categories.id',
                'categories.title',
                'categories.alias',
                'categories.level',
                'categories.parent_id',
            ])
            ->from('items')
            ->leftJoin('categories', 'categories.id = items.category_id')
            ->where(['items.id' => $id])
            ->one();
    }

    public static function parent_category($id)
    {
        return (new Query())
            ->select([
                'categories.*',
            ])
            ->from('categories')
            ->where(['id' => $id])
            ->one();
    }

    public static function one($id)
    {
        return (new Query())
            ->select([
                'items.*',
            ])
            ->where(['items.id' => $id])
            ->from('items')
            ->one();
    }

    public static function recent($category_id)
    {
        return (new Query())
            ->select([
                'items.id',
                'items.title',
                'items.image',
            ])
            ->where(['items.category_id' => $category_id])
            ->from('items')
            ->all();
    }

    public static function categoryItems($category_id = 0, $filter = [])
    {
        return (new Query())
            ->select([
                'ct_items.id',
                'ct_items.title',
                'ct_items.alias',
                'ct_items.price',
                'ct_items.description',
                'ct_items.color_hex',
                'IF(cover_image, cover_image, latest_image) AS cover',
            ])
            ->from('ct_items')
            ->leftJoin('ct_item_sizes', 'ct_item_sizes.item_id = ct_items.id')
            ->where(['ct_items.category_id' => $category_id])
            ->andFilterWhere($filter)
            ->groupBy('ct_items.id')
            ->orderBy('ct_items.price desc')
            ->all();
    }

    public static function recentItems($category_id = 0, $limit = 4, $id)
    {
        return (new Query())
            ->select([
                'ct_items.id',
                'ct_items.title',
                'ct_items.alias',
                'ct_items.price',
                'ct_items.description',
                'IF(cover_image IS NULL, latest_image, cover_image) AS cover'
            ])
            ->from('ct_items')
            ->where([
                'and',
                ['ct_items.category_id' => $category_id],
                ['not in', 'id', $id],
            ])
            ->orderBy('rand()')
            ->limit($limit)
            ->all();
    }

    public static function getBestsellers($limit = 5)
    {
        return self::find()
            ->select([
                'items.*',
                '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS cover'
            ])
            ->asArray()
            ->where(['is_bestseller' => 1, 'status' => 1])
            ->limit($limit)
            ->orderBy('random()')
            ->all();
    }

    public static function categorySizes($category_id)
    {
        return (new Query())
            ->distinct()
            ->select('ct_item_sizes.size')
            ->from('ct_item_sizes')
            ->leftJoin('ct_items', 'ct_items.id = ct_item_sizes.item_id')
            ->where(['ct_items.category_id' => $category_id])
            ->all();
    }

    public static function categoryColors($category_id)
    {
        return (new Query())
            ->distinct()
            ->select([
                'ct_items.color_hex',
                'ct_colors.color',
                'ct_colors.hex',
            ])
            ->from('ct_items')
            ->leftJoin('ct_colors', 'ct_colors.hex = ct_items.color_hex')
            ->where([
                'and',
                ['ct_items.category_id' => $category_id],
                ['not', ['ct_items.color_hex' => null]],
            ])
            ->orderBy(['ct_colors.color' => SORT_ASC])
            ->all();
    }

    public static function categoryPrices($category_id)
    {
        return (new Query())
            ->select([
                'MIN(ct_items.price) AS min',
                'MAX(ct_items.price) AS max',
            ])
            ->from('ct_items')
            ->where([
                'ct_items.category_id' => $category_id,
            ])
            ->one();
    }

    public static function brands()
    {
        return (new Query())
            ->select(['ct_brands.*'])
            ->from('ct_brands')
            ->orderBy('ct_brands.sort')
            ->all();
    }

    public static function brand($alias)
    {
        return (new Query())
            ->select(['ct_brands.*'])
            ->from('ct_brands')
            ->where([
                'ct_brands.alias' => $alias,
            ])
            ->one();
    }

    public static function brandItems($id, $limit = 10, $offset = 0)
    {
        return (new Query())
            ->select([
                'ct_items.id',
                'ct_items.title',
                'ct_items.alias',
                'ct_items.price',
                'ct_items.description',
                'IF(cover_image IS NULL, latest_image, cover_image) AS cover',
            ])
            ->from('ct_items')
            ->where(['ct_items.brand_id' => $id])
            ->orderBy('ct_items.date')
            ->all();
    }

    public static function getRecent($cid, $limit = 4,$current = null)
    {
        return $cid ? self::find()
            ->asArray()
            ->select([
                'items.*',
                '(SELECT upload.name FROM upload WHERE upload.id = items.cover_image_id) AS cover',
            ])
            ->where([
                'categories.id' => $cid,
                'items.status' => 1,
                'groups.status' => 1,
            ])
            ->andFilterWhere(['not',['groups.id'=>$current]])
            ->leftJoin('groups', 'groups.id = items.group_id')
            ->leftJoin('category_link', 'category_link.group_id = groups.id')
            ->leftJoin('categories', 'categories.id = category_link.category_id')
            ->limit($limit)
            ->orderBy('random()')
            ->all() : [];
    }

    public static function getImages($id)
    {
        return self::find()
            ->asArray()
            ->select([
                'upload.name AS src'
            ])
            ->where(['items.id' => $id])
            ->andWhere([
                'not',
                'items.cover_image_id = upload.id'
            ])
            ->leftJoin('item_upload', 'item_upload.item_id = items.id')
            ->leftJoin('upload', 'upload.id = item_upload.upload_id')
            ->all();
    }

    public static function getExclusive()
    {
        $id = Categories::findOne([
            'alias' => 'exclusive'
        ])->id;
        $sql = <<<SQL
SELECT
	"public".categories.*,
	"public".upload.name	
FROM
	"public".items
INNER JOIN "public".groups ON "public".items.group_id = "public".groups."id"
INNER JOIN "public".category_link ON "public".category_link.group_id = "public".groups."id"
INNER JOIN "public".categories ON "public".category_link.category_id = "public".categories."id"
LEFT JOIN "public".upload ON "public".upload.id = "public".categories.cover_image_id
WHERE
	"categories".pid IS NOT NULL
AND ARRAY [ "categories"."pid" ] && string_to_array('$id', ',') :: INT []
GROUP BY 
    "public".categories.id,
    "public".upload.name

SQL;

        return Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
    }

    public static function getSize($item)
    {
        $data = explode(', ', $item['title']);
        $size = end($data);
        return $size;
    }

    public static function getVolume($item)
    {
        $params = $item['params'];
        if (!is_array($params)) {
            $params = Json::decode($params);
        }
        $size = $params['attr']['Объем'];
        return $size;
    }

    public function beforeValidate()
    {
        if (is_array($this->params)) {
            $this->params = Json::encode($this->params);
        }
        return parent::beforeValidate();
    }


    public function afterFind()
    {
        parent::afterFind();
        if (!is_array($this->params)) {
            $this->params = Json::decode($this->params);
        }
    }
}