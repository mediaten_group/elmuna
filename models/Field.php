<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 16.03.16
 * Time: 18:19
 */
namespace app\models;

use app\lib\field\Area;
use app\lib\field\Checkbox;
use app\lib\field\Color;
use app\lib\field\Number;
use app\lib\field\Select;
use app\tables\FieldColors;
use app\tables\FieldOptions;
use yii;
use yii\helpers\Json;

class Field extends \app\tables\Field
{

    // Принимается поле
    // Отдается его значение после проверки
    // Возвращается FALSE если поле полностью не прошло проверку
    public static function check_field($field)
    {
        $value = Json::decode($field[2]);
        switch ($field[5]) {
            case 'area':
                $value = Field::check_area($value);
                break;
            case 'color':
                if ($field[1]) {
                    $value = Field::check_color($value);
                } else {
                    $value = FALSE;
                }
                break;
            case 'checkbox':
                break;
            case 'select':
                $value = Field::check_select($value);
                break;
        }

        if (!$value) {
            return FALSE;
        } else {
            return $value;
        }
    }

    // Принимается значение поля типа area
    // Если максимальная ширина меньше минимальной ширины
    // Или если максимальная высота меньше минимальной высоты
    // Возвращается FALSE
    // Иначе возвращается его обычное значение
    public static function check_area($value)
    {
        if ($value['width_max'] < $value['width_min'])
            return FALSE;
        if ($value['height_max'] < $value['height_min'])
            return FALSE;
        return $value;
    }

    // Принимается значение поля типа color
    // Для каждого указанного в select_data цвета
    // Ищется, имеется ли цвет в таблице в принципе и если он опубликован
    // Если такого цвета не находится, он убирается
    // Если не осталось цветом, то возвращается FALSE
    // Иначе возвращаются цвета, которые прошли проверку
    public static function check_color($value)
    {
        foreach ($value['select_data'] as $sort => $key) {
            $color = FieldColors::findOne(['id' => $key, 'status' => 1]);
            if (!$color) {
                unset($value['select_data'][$sort]);
                unset($value['price'][$key]);
            }
        }
        if (!$value['select_data'])
            return FALSE;
        else
            return $value;
    }

    // Принимается значение поля типа select
    // Для каждого указанного в sort поля
    // Ищется, имеется ли опция в таблице опций
    // Если такой опции нет, она убирается
    // Если не осталось опций, то возвращается FALSE
    // Иначе возвращаются опции, которые прошли проверку
    public static function check_select($value)
    {
        foreach ($value['sort'] as $sort => $key) {
            $option = FieldOptions::findOne($key);
            if (!$option) {
                unset($value['sort'][$sort]);
                unset($value['price'][$key]);
                unset($value['value'][$key]);
            }
        }
        if (!$value['sort'])
            return FALSE;
        else
            return $value;
    }

    public static function getPriceArea($value, $field)
    {
        if (($value['height_min'] <= $field[0]) && ($value['height_max'] >= $field[0])
            && ($value['width_min'] <= $field[1]) && ($value['width_max'] >= $field[1])
        ) {
            return $value['price'] * $field[2];
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPriceSelect($value, $field)
    {
        $option = FieldOptions::findOne($field);
        if (($option) && (in_array($field, $value['sort']))) {
            return $value['price'][$field];
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPriceCheckbox($value)
    {
        return $value['price'];
    }

    public static function getPriceColor($value, $field)
    {
        $color = FieldColors::findOne(['id' => $field, 'status' => 1]);
        if ($value['select_data']) {
            if (($color) && (in_array($field, $value['select_data']))) {
                return $value['price'][$field];
            } else {
                return 'ОШИБКА';
            }
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPrice($tech)
    {
        $techPrice = 0;
        $i = 0;
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];
        foreach ($tech as $id => $f) {
            $field = Field::find()
                ->asArray()
                ->select([
                    'field.*',
                    'field_type.id as type_id',
                    'field_type.type as type',
                ])
                ->where([
                    'field.id' => $id,
                    'status' => 1
                ])
                ->leftJoin('field_type', 'field_type_id = field_type.id')
                ->one();
            if ($field) {
                switch ($field['type']) {
                    case 'area':
                        list($n_priceSumm, $n_priceMult, $n_priceSep, $n_errors) = Area::getTechPrice($field, $f);
                        $priceSumm += $n_priceSumm;
                        $priceMult *= $n_priceMult;
                        $priceSep += $n_priceSep;
                        $errors += $n_errors;
                        break;
                    case 'number':
                        list($n_priceSumm, $n_priceMult, $n_priceSep, $n_errors) = Number::getTechPrice($field, $f);
                        $priceSumm += $n_priceSumm;
                        $priceMult *= $n_priceMult;
                        $priceSep += $n_priceSep;
                        $errors += $n_errors;
                        break;
                    case 'checkbox':
                        list($n_priceSumm, $n_priceMult, $n_priceSep, $n_errors) = Checkbox::getTechPrice($field, $f);
                        $priceSumm += $n_priceSumm;
                        $priceMult *= $n_priceMult;
                        $priceSep += $n_priceSep;
                        $errors += $n_errors;
                        break;
                    case 'color':
                        list($n_priceSumm, $n_priceMult, $n_priceSep, $n_errors) = Color::getTechPrice($field, $f);
                        $priceSumm += $n_priceSumm;
                        $priceMult *= $n_priceMult;
                        $priceSep += $n_priceSep;
                        $errors += $n_errors;
                        break;
                    case 'select':
                        list($n_priceSumm, $n_priceMult, $n_priceSep, $n_errors) = Select::getTechPrice($field, $f);
                        $priceSumm += $n_priceSumm;
                        $priceMult *= $n_priceMult;
                        $priceSep += $n_priceSep;
                        $errors += $n_errors;
                        break;
                }
            }
        }
        $techPrice = ($priceMult * $priceSumm) + $priceSep;
        if ($errors) {
            return ['status' => 'error', 'data' => $errors];
        }
        return ['status' => 'ok', 'data' => $techPrice];
    }

    public static function getPriceFieldArea($tech, $field)
    {
        $value = $tech['value'];
        if (($value['height_min'] <= $field[0]) && ($value['height_max'] >= $field[0])
            && ($value['width_min'] <= $field[1]) && ($value['width_max'] >= $field[1])
        ) {
            return [
                'name' => $tech['name'],
                'width' => $field[1],
                'height' => $field[0],
                'area' => $field[2],
                'price' => $value['price'] * $field[2],
                'type' => $tech['type'],
            ];
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPriceFieldSelect($tech, $field)
    {
        $value = $tech['value'];
        $option = FieldOptions::findOne($field);
        if (($option) && (in_array($field, $value['sort']))) {
            return [
                'name' => $tech['name'],
                'option' => $value['value'][$field],
                'price' => $value['price'][$field],
                'type' => $tech['type'],
            ];
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPriceFieldCheckbox($tech)
    {
        $value = $tech['value'];
        return [
            'name' => $tech['name'],
            'price' => $value['price'],
            'type' => $tech['type'],
        ];
    }

    public static function getPriceFieldColor($tech, $field)
    {
        $value = $tech['value'];
        $color = FieldColors::findOne(['id' => $field, 'status' => 1]);
        if ($value['select_data']) {
            if (($color) && (in_array($field, $value['select_data']))) {
                return [
                    'name' => $tech['name'],
                    'color' => $color->getUpload()->one()->name,
                    'price' => $value['price'][$field],
                    'type' => $tech['type'],
                ];
            } else {
                return 'ОШИБКА';
            }
        } else {
            return 'ОШИБКА';
        }
    }

    public static function getPriceFields($tech)
    {
        $priceFields = [];
        $text = '';
        $errors = [];
        foreach ($tech as $id => $left) {
            $field = Field::find()
                ->asArray()
                ->select([
                    'field_type.type',
                    'field.*',
                ])
                ->where(['field.id' => $id])
                ->leftJoin('field_type', 'field_type_id = field_type.id')
                ->one();
            if ($field) {
                $field['value'] = yii\helpers\Json::decode($field['value']);
                switch ($field['type']) {
                    case 'area':
                        list($n_text,$n_errors) = Area::getTechName($field,$left);
                        $text .= $n_text;
                        $errors += $n_errors;
                        break;
                    case 'select':
                        list($n_text,$n_errors) = Select::getTechName($field,$left);
                        $text .= $n_text;
                        $errors += $n_errors;
                        break;
                    case 'checkbox':
                        list($n_text,$n_errors) = Checkbox::getTechName($field,$left);
                        $text .= $n_text;
                        $errors += $n_errors;
                        break;
                    case 'color':
                        list($n_text,$n_errors) = Color::getTechName($field,$left);
                        $text .= $n_text;
                        $errors += $n_errors;
                        break;
                    case 'number':
                        list($n_text,$n_errors) = Number::getTechName($field,$left);
                        $text .= $n_text;
                        $errors += $n_errors;
                        break;
                }
            }
        }
        if ($errors) {
            return ['status' => 'error', 'data' => $errors];
        }
        return ['status' => 'ok', 'data' => $text];
    }

    public static function getPriceNumber($field, $value)
    {
        if ($field['min'] && $field['min'] > $value) {
            return 'ОШИБКА';
        }
        if ($field['max'] && $field['max'] < $value) {
            return 'ОШИБКА';
        }
        return $value * $field['price'];
    }

}