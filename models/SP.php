<?php

namespace app\models;

use app\tables\Catalogs;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SP extends Catalogs
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id', 'pos', 'code', 'count'], 'integer'],
            [['model', 'des', 'ru_name', 'en_name', 'table'], 'string'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Catalogs::find()->asArray()->joinWith('category');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'and',
//            ['id' => $this->id],
//            ['model' => $this->model],
//            ['pos' => $this->pos],
//            ['like', 'table', $this->table],
//            ['like', 'ru_name', $this->ru_name],
//            ['like', 'en_name', $this->en_name],
//            ['like', 'code', $this->code],
//            ['like', 'des', $this->des],
//        ]);

        return $dataProvider;
    }
}