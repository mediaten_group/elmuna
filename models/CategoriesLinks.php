<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 23.03.16
 * Time: 15:37
 */

namespace app\models;

use app\lib\My;
use app\models\Categories;
use yii\helpers\Json;

class CategoriesLinks extends \app\tables\CategoriesLinks
{
    public static function doRecord($href, $title, $parser_ver, $source)
    {
        $cat = CategoriesLinks::findOne(['title' => $title, 'parent' => $parent]);
        if ($cat) {
        } else {
            $cat = new CategoriesLinks();
            $cat->href = $href;
            $cat->title = $title;
            $cat->parser_ver = $parser_ver;
            $cat->source = $source;
            $cat->date = My::dateTime();
            $cat->save();
        }
        return $cat;
    }
}