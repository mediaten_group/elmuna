<?php
namespace app\models\forms;

use app\models\User;
use app\models\UserProfile;
use Yii;
use yii\base\Model;
use app\models\Mail;

/**
 * User data form
 */
class UserDataForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $old_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
//            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'Этот логин занят.'],
            ['username', 'validateUsername'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'validateEmail'],
//            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'Этот Email занят.'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Пароли не совпадают"],
            ['old_password', 'required'],
            ['old_password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['username'] = 'Логин';
        $labels['email'] = 'Email';
        $labels['password'] = 'Пароль';
        $labels['password_repeat'] = 'Подтверждение пароля';
        $labels['old_password'] = 'Старый пароль';
        return $labels;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var User $user */
            $user = Yii::$app->getUser()->getIdentity(Yii::$app->getUser()->getId());

            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Неверный пароль.');
            }
        }
    }

    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var User $user */

            if (User::find()->where([$attribute => $this->$attribute])
                ->andWhere(['not', ['id' => Yii::$app->user->getId()]])->exists()) {
                $this->addError($attribute, 'Такой логин занят');
            }
        }
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var User $user */

            if (User::find()->where([$attribute => $this->$attribute])
                ->andWhere(['not', ['id' => Yii::$app->user->getId()]])->exists()) {
                $this->addError($attribute, 'Такой email занят');
            }
        }
    }

    public function submit()
    {
        if (!$this->validate()) {
            return false;
        }
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity(Yii::$app->getUser()->getId());
        $old_email = $user->email;
        $user->username = $this->username;
        $user->email = $this->email;
        if ($this->password) {
            $user->setPassword($this->password);
            $user->generateAuthKey();
        }
        if ($user->email != $old_email){
            $user->activate = 0;
            $profile = UserProfile::findOne(['user_id'=>$user->id]);
            $profile->email = $user->email;
            $profile->save();
        }

        if ($user->save()) {
            if ($user->email != $old_email) {
                Mail::changeEmail($user,$user->email);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }
}