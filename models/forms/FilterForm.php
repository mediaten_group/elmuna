<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 11.02.2016
 * Time: 16:27
 */

namespace app\models\forms;


use yii\base\Model;
use yii\helpers\ArrayHelper;

class FilterForm extends Model
{
    public $priceFrom;
    public $priceTo;
    public $color;
    public $type;
    public $sort;
    public $material;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['priceFrom', 'number', 'min' => 0, 'max' => 999999, 'message' => 'Длина поля должна быть от 0 до 999999 символов.'];
        $rules[] = ['priceTo', 'number', 'min' => 0, 'max' => 999999, 'message' => 'Длина поля должна быть от 0 до 999999 символов.'];
//        $rules[] = ['material', 'exist', 'targetClass' => '\app\tables\Materials', 'message' => 'Такого цвета нет'];
//        $rules[] = ['color', 'exist', 'targetClass' => '\app\tables\Colors', 'message' => 'Такого цвета нет'];
        $rules[] = [['sort'],'number'];
        $rules[] = [['type'],'string'];
        $rules[] = [['material','color'],'string'];
        return $rules;
    }

    public function loadFilter($filter)
    {
        $data = [];
//        $data['material'] = ArrayHelper::getValue($filter, 'material');
        $data['color'] = ArrayHelper::getValue($filter, 'color');
        $data['priceFrom'] = ArrayHelper::getValue($filter, 'priceFrom');
        $data['priceTo'] = ArrayHelper::getValue($filter, 'priceTo');
        $data['sort'] = ArrayHelper::getValue($filter, 'sort');
        $data['type'] = ArrayHelper::getValue($filter, 'type');
        $data['material'] = ArrayHelper::getValue($filter, 'material');

        return $this->load($data,'');
    }
}