<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 28.01.2016
 * Time: 16:14
 */

namespace app\models\forms;

use app\lib\My;
use app\models\Mail;
use app\tables\Callback;
use app\tables\Info;
use Yii;

class CallbackForm extends Callback
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['name', 'phone'], 'required'];
        $rules[] = ['name', 'string', 'min' => 3, 'max' => 25, 'message' => 'Длина имени должна быть от 3 до 25 символов.'];
        $rules[] = ['phone', 'match', 'pattern' => '/^((8|\+[\d]{1,3})[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'Введите корректный номер телефона.'];
        $rules[] = ['text', 'string', 'max' => 500, 'message' => 'Максимальная длина сообщения - 500 символов.'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Имя';
        $labels['phone'] = 'Телефон';
        $labels['text'] = 'Сообщение';
        return $labels;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if ($insert) {
            $this->text = htmlspecialchars($this->text);
        }

        if (!$this->created_at) $this->created_at = My::dateTime();
        if (!$this->status) $this->status = 0;
        if (!Yii::$app->user->isGuest) $this->user_id = Yii::$app->user->getId();

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $email = Info::findOne(['key'=>'email'])->value;
            Mail::sendCallbackRequest($email,$this);
        }
    }
}