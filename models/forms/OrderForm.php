<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 28.01.2016
 * Time: 16:14
 */

namespace app\models\forms;

use app\models\Order;

class OrderForm extends Order
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['name', 'email', 'phone'], 'required'];
        $rules[] = ['name', 'string', 'min' => 3, 'max' => 25, 'message' => 'Длина имени должна быть от 3 до 25 символов.'];
        $rules[] = ['address', 'string', 'max' => 255, 'message' => 'Адрес не должен превышать 255 символов.'];
        $rules[] = ['email', 'email', 'message' => 'Введите корректный Email.'];
        $rules[] = ['phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'Введите корректный номер телефона.'];
        $rules[] = ['comment','string','max'=>1000,'message'=>'Комментарий не может быть длиннее 1000 символов'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Имя';
        $labels['address'] = 'Адрес';
        $labels['email'] = 'Email';
        $labels['phone'] = 'Телефон';
        $labels['comment'] = 'Комментарий';
        return $labels;
    }
}