<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 03.02.2016
 * Time: 14:35
 */

namespace app\models\forms;

use app\behaviors\DateTimeBehavior;

class ReviewForm extends \app\tables\Review
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['fio', 'text'], 'required'];
        $rules[] = [['fio', 'text', 'email'], 'filter', 'filter' => 'strip_tags'];
        $rules[] = [['fio', 'text', 'email'], 'filter', 'filter' => 'trim'];
        $rules[] = ['fio', 'string', 'min' => 3, 'max' => 255];
        $rules[] = ['text', 'string', 'max' => 1000, 'message' => 'Текст не должен превышать 1000 символов.'];
        $rules[] = ['email', 'email'];
        $rules[] = ['email', 'string', 'max' => '255'];
        return $rules;
    }

    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }

}