<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 15.03.16
 * Time: 18:47
 */

namespace app\models\forms;


use yii\base\Model;
use app\models\Mail;

/*
* @property string $subject
* @property string $body
*/

class DistributionForm extends Model
{

    public $subject;
    public $body;

    public function rules()
    {
        return [
            ['subject', 'string', 'max' => 255],
            ['body', 'string']
        ];
    }

    public function distribute()
    {
        return Mail::distribution($this->subject, $this->body);
    }

    public function attributeLabels()
    {
        return [
            'subject' => 'Тема',
            'body' => 'Текст',
        ];
    }

}