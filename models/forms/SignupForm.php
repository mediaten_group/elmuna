<?php
namespace app\models\forms;

use app\models\User;
use yii\base\Model;
use Yii;
use app\models\Mail;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
//            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'Этот логин занят.'],
            ['username', 'validateUsername'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'validateEmail'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Пароли не совпадают"],
        ];
    }

    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var User $user */
            $u = strtolower($this->$attribute);
            if (User::find()->where(["LOWER($attribute)" => $u])
                ->andWhere(['not', ['id' => Yii::$app->user->getId()]])->exists()) {
                $this->addError($attribute, 'Такой логин занят');
            }
        }
    }
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var User $user */
            $u = strtolower($this->$attribute);
            if (User::find()->where(["LOWER($attribute)" => $u])
                ->andWhere(['not', ['id' => Yii::$app->user->getId()]])->exists()) {
                $this->addError($attribute, 'Такая почта занята');
            }
        }
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        Mail::activateEmail($user,$this->email);

        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }
}