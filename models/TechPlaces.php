<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.03.16
 * Time: 16:56
 */

namespace app\models;


class TechPlaces extends \app\tables\TechPlaces
{

    public static function getPlaces($id)
    {
        $places = TechPlaces::find()
            ->asArray()
            ->select([
                'upload.id',
                'upload.name'
            ])
            ->where([
                'tech_places.group_id' => $id
            ])
            ->leftJoin('upload','upload.id = tech_places.upload_id')
            ->all();

        return $places;
    }

}