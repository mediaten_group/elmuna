<?php

namespace app\models\_old;

use yii\db\Query;

class Categories extends \app\tables\Categories
{

    public static function mainMenu()
    {
        return (new Query())
            ->select([
                'categories.alias',
                'categories.title',
            ])
            ->from('categories')
            ->where(['pid' => null, 'level' => 0])
            ->limit(5)
            ->all();
    }

    public static function main()
    {
        return (new Query())
            ->select(['categories.*'])
            ->from('categories')
            ->where(['pid' => null])
            ->all();
    }

    public static function getCatTree($where)
    {
        $list = (new Query())
            ->select([
                'title AS text',
                'id',
                'parent_id',
            ])
            ->from('categories_gifts')
            ->where(['front_page' => 1])
            ->orderBy('parent_id, id')
            ->all();
        $tree = array();
        foreach ($list as $row) {
            if ($row['parent_id'] === null) $row['parent_id'] = 0;
            $tree[(int)$row['parent_id']][] = $row;
        }
//        echo '<pre>';
//        print_r(Categories::treePrint($tree, 0));
//        echo '</pre>';
        return Categories::treePrint($tree, 0);
    }

    public static function treePrint($tree, $pid = 0, $id = false)
    {
        if (empty($tree[$pid]))
            return FALSE;

        $html = [];
        $i = 0;
        foreach ($tree[$pid] as $row) {
            $html[$i]['id'] = $row['id'];
            $html[$i]['text'] = $row['text'];
//            $html[$i]['alias'] = $row['alias'];
//            $html[$i]['image'] = $row['image'];
//            $html[$i]['level'] = $row['level'];

            if (isset($tree[$row['id']])) {

                $html[$i]['children'] = Categories::treePrint($tree, $row['id'], $id);
            }
            $i++;
        }

        return $html;
    }

    public static function byParent($parentId, $limit = null)
    {
        return (new Query())
            ->select(['categories.*'])
            ->from('categories')
            ->where(['pid' => $parentId])
            ->limit($limit)
            ->all();
    }

    public static function byAlias($alias, $limit = null)
    {
        return (new Query())
            ->select([
                'categories.id',
                'categories.alias',
                'categories.title',
                'categories.image',
            ])
            ->from('categories')
            ->innerJoin('categories AS parent', 'categories.pid = parent.id')
            ->where(['categories.alias' => $alias])
            ->limit($limit)
            ->all();
    }

    public static function byId($id, $limit = null)
    {
        return (new Query())
            ->select([
                'categories.id',
                'categories.alias',
                'categories.title',
                'categories.level',
                'categories.image',
            ])
            ->from('categories')
            ->where(['categories.pid' => $id])
            ->limit($limit)
            ->all();
    }

    public static function oneById($id)
    {
        return (new Query())
            ->select([
                'categories.id',
                'categories.pid',
                'categories.alias',
                'categories.title',
                'categories.image',
                'categories.level',
            ])
            ->from('categories')
            ->where(['categories.id' => $id])
            ->one();
    }

    public static function one($alias)
    {
        return (new Query())
            ->select([
                'categories.*',
            ])
            ->from('categories')
            ->where(['alias' => $alias])
            ->one();
    }

    public static function items($category_id, $level, $limit = 15)
    {
        if ($level == 2) {
            return (new Query())
                ->select([
                    'categories.id',
                    'categories.pid',
                    'items.*',
                ])
                ->from('categories')
                ->where(['categories.pid' => $category_id])
                ->innerJoin('items', 'items.cid = categories.id')
                ->limit($limit)
                ->all();
        } else {
            return (new Query())
                ->select([
                    'items.*',
                ])
                ->from('items')
                ->where(['cid' => $category_id])
                ->limit($limit)
                ->all();
        }
    }

    public static function getLink($alias, $level)
    {
        return (new Query())
            ->select([
                'alias',
                'title',
            ])
            ->from('categories')
            ->where(['alias' => $alias, 'level' => $level])
            ->one();
    }

    public static function getParents($current, $parents)
    {
        return (new Query())
            ->select([
                'id',
                'title',
                'alias',
                'level',
                'pid',
            ])
            ->from('categories')
            ->where([
                'or',
                [
                    'and',
                    ['>', 'level', 0],
                    ['<', 'level', $current['level']],
                ],
                [
                    'and',
                    ['pid' => $parents],
//                    ['level' => $current['level']],
                ],
            ])
            ->orderBy('ord')
            ->all();
    }

    public static function getLinks(Array $array)
    {
        $select = [];

        $concat = 'alias, "||", title, "||", level';

        foreach ($array as $key => $alias) {
            if ($key == 0) continue;

            $select[$key . '_id'] = (new Query())
                ->select([
                    'id'
                ])
                ->from('categories')
                ->where([
                    'and',
                    ['alias' => $array[$key]],
                    'pid = ' . ($key - 1) . '_id',
                ]);

            $select[$key . '_params'] = (new Query())
                ->select([
                    'CONCAT(' . $concat . ')',
                ])
                ->from('categories')
                ->where([
                    'and',
                    ['alias' => $array[$key]],
                    'pid = ' . ($key - 1) . '_id',
                ]);
        }

        $query = (new Query())
            ->select([
                    'id AS 0_id',
                    'CONCAT(' . $concat . ') AS 0_params',
                ] + $select)
            ->from('categories')
            ->where([
                'and',
                ['alias' => $array[0]],
                ['pid' => null],
            ])
            ->one();

        $links = [];
        foreach ($query as $key => $val) {
            preg_match('/(\d+)_/', $key, $index);
            preg_match('/_(.*)/', $key, $type);

            if ($type[1] == 'id') {
                $links[$index[1]] = ['id' => $val];
            }
            if ($type[1] == 'params') {
                $params = explode('||', $val);
                $links[$index[1]] += ['alias' => $params[0], 'title' => $params[1]];
            }

        }
        return $links;
    }
}