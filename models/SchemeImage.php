<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 04.03.16
 * Time: 11:12
 */

namespace app\models;

use app\behaviors\DateTimeBehavior;
use app\modules\upload\behaviors\UploadBehavior;

class SchemeImage extends \app\tables\SchemeImage
{
    
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
            ],
            DateTimeBehavior::className(),
        ];
    }

}