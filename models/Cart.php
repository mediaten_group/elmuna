<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 13:13
 */

namespace app\models;


use app\behaviors\DateTimeBehavior;
use Yii;

class Cart extends \app\tables\Cart
{
    public $count;
    public $price;

    public function beforeValidate()
    {
        if(!$this->key) $this->key = Yii::$app->security->generateRandomString(8);
        if(!$this->user_id && !Yii::$app->user->isGuest) $this->user_id = Yii::$app->user->getId();
        return true;
    }

    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }
}