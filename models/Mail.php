<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 14.03.16
 * Time: 17:22
 */

namespace app\models;


use app\components\Info;
use yii\base\Model;
use yii;

class Mail extends Model
{

    private $admin_email;

    public static function resetPassword($user, $email)
    {
        return Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], ['user' => $user])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Сброс пароля на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function activateEmail($user, $email)
    {
        return Yii::$app->mailer->compose(['html' => 'activateEmail-html'], ['user' => $user])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Активация почты на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function changeEmail($user, $email)
    {
        return Yii::$app->mailer->compose(['html' => 'changeEmail-html'], ['user' => $user])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Изменение почты на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function registerOrder($email, $order, $items)
    {
        $elmuna_email = Info::get('email');
//        $elmuna_email = 'dev@mediaten.ru';
        Yii::$app->mailer->compose(['html' => 'orderCopy-html'], ['order' => $order,'email'=>$email,'items'=>$items])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($elmuna_email)
            ->setSubject('Регистрация заказа на ' . Yii::$app->params['siteName'])
            ->send();
        return Yii::$app->mailer->compose(['html' => 'registerOrder-html'], ['order' => $order,'items'=>$items])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Регистрация заказа на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function completeOrder($email, $order)
    {
        return Yii::$app->mailer->compose(['html' => 'completeOrder-html'], ['order' => $order])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Готовность заказа на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function sendMail($subject, $body, $email = false)
    {
        $adminEmail = Yii::$app->params['adminEmail'];
        $adminName = Yii::$app->params['adminName'];
        if ($email == false) {
            $email = Yii::$app->params['adminEmail'];
        }
        $return = Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$adminEmail => $adminName])
            ->setSubject($subject)
            ->setHtmlBody($body)
            ->send();
        return $return;
    }

    public static function distribution($subject, $body) {
        $emailList = User::find()
            ->asArray()
            ->where([
                'activate' => 1
            ])
            ->select([
                'username'
            ])
            ->indexBy('email')
            ->column();
//        dd($emailList);
        return Yii::$app->mailer->compose(['html'=>'distribution-html'],['body'=>$body])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setBcc($emailList)
            ->setSubject($subject)
            ->send();
    }

    public static function sendCallbackRequest($email, $request)
    {
        $user = User::findOne($request['user_id']);
        return Yii::$app->mailer->compose(['html' => 'callbackRequest-html'], ['request' => $request,'user'=>$user])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Заказан обратный звонок на ' . Yii::$app->params['siteName'])
            ->send();
    }

    public static function statusChange($email, $order)
    {
        return Yii::$app->mailer->compose(['html' => 'statusChange-html'], ['order' => $order])
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['adminName']])
            ->setTo($email)
            ->setSubject('Изменение статуса товара на ' . Yii::$app->params['siteName'])
            ->send();
    }


}