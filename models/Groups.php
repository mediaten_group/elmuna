<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 04.02.2016
 * Time: 12:34
 */

namespace app\models;


use app\lib\My;
use app\tables\CategoryLink;
use app\tables\GroupsLinks;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Groups extends \app\tables\Groups
{
    public static function getRecent($cid, $limit = 4)
    {
        return $cid ? self::find()
            ->asArray()
            ->select([
                'groups.*',
                '(SELECT upload.name FROM upload WHERE upload.id = groups.cover_image_id) AS cover',
            ])
            ->where([
                'categories.id' => $cid,
                'groups.status' => 1,
            ])
            ->leftJoin('category_link', 'category_link.group_id = groups.id')
            ->leftJoin('categories', 'categories.id = category_link.category_id')
            ->limit($limit)
            ->orderBy('random()')
            ->all() : [];
    }

    public static function search($q, $limit = null, $offset = null)
    {
        $offset = ($offset) ? "OFFSET $offset " : '';
        $limit = ($limit) ? "LIMIT $limit" : '';
        $sql = <<<SQL
SELECT
	"public".groups."id",
	"public".groups.title,
  (SELECT upload."name" FROM upload WHERE upload.id = groups.cover_image_id) AS src,
(SELECT json_agg(i)
  FROM (
    SELECT
			items.id,
			items.alias,
			items.title,
			items.price,
			items.article,
			items.color,
			(SELECT upload.name FROM upload WHERE upload.id = items.cover_image_id) AS src
		FROM items WHERE items."group_id" = groups."id"
  ) i
) AS "item",
	"public".groups.status
FROM
	"public".groups
LEFT JOIN "public".items ON "public".groups."id" = "public".items."group_id"
WHERE
	LOWER("items"."article") LIKE LOWER('%$q%') OR LOWER("items"."title") LIKE LOWER('%$q%')
AND "public".groups.status = 1
GROUP BY
	groups."id"
ORDER BY
	"public".groups.created_at DESC
$offset
$limit
SQL;
        return Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
    }

    public static function parser($title, $cat_name, $cat_href, $version, $source)
    {
        $group = Groups::findOne(['title' => $title]);
        if ($group) {
        } else {
            $group = new Groups();
            $group->status = 1;
            $group->title = $title;
            $group->alias = My::str2url($group->title);
            $group->created_at = My::dateTime();
            $group->save();
            $links = new GroupsLinks();
            $links->group_id = $group->id;
            $links->version = $version;
            $links->orig_cat_href = $cat_href;
            $links->orig_cat_name = $cat_name;
            $links->source = $source;
            $links->save();
        }
        return $group;
    }

    public static function count_items($group_id)
    {
        return count(Items::findAll(['group_id' => $group_id]));
    }

    public static function is_flash($id)
    {
        $cat_link = CategoryLink::findAll([
            'group_id' => $id
        ]);
        $t = 0;
        $base_cat = Categories::findOne([
            'alias' => 'fleshki'
        ])->id;
        foreach ($cat_link as $link) {
            $this_cat = Categories::findOne($link->category_id)->path;
            $this_cat = substr($this_cat, 1, strlen($this_cat) - 2);
            $arr = explode(',', $this_cat);
            $t += in_array($base_cat, $arr);
        }
        return $t;
    }

    public static function getSizes($items)
    {
        $sizes = [];
        foreach ($items as $item) {
            $data = explode(', ', $item['title']);
            $size = end($data);
            $sizes[$size] = $item;
        }
        return $sizes;
    }

    public static function getVolume($items)
    {
//        pree($items);
        $sizes = [];
        foreach ($items as $item) {
            $params = ArrayHelper::getValue($item, 'params');

            if ($params) {
                if (!is_array($params)) {
                    $params = Json::decode($params);
                }

                $size = $params['attr']['Объем'];
                $sizes[$size] = $item;
            }
        }
        return $sizes;
    }

    public static function getVolumeOne($item)
    {
        $sizes = [];
        $params = $item['params'];
        if (!is_array($params)) {
            $params = Json::decode($params);
        }

        $size = $params['attr']['Объем'];
        return $size;
    }

}