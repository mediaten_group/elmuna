<?php

namespace app\models;

use yii\db\Query;

class Technology extends \app\tables\Technology
{
    public static function all()
    {
        return (new Query())
            ->select([
                'technology.id',
                'technology.alias',
                'technology.title',
                '(SELECT upload."name" FROM upload WHERE upload.id = technology.cover_image_id) AS src',
            ])
            ->from('technology')
            ->orderBy('sort')
            ->all();
    }

    public static function mainPage()
    {
        return (new Query())
            ->select([
                'technology.alias',
                'technology.intro',
                'technology.title',
                'technology.image',
                '(SELECT upload."name" FROM upload WHERE upload.id = technology.cover_image_id) AS src',
            ])
            ->from('technology')
            ->where(['and','main_page','status'])
            ->orderBy('sort')
            ->all();
    }

    public static function one($alias)
    {
        return (new Query())
            ->select([
                'technology.alias',
                'technology.title',
                'technology.image',
                'technology.text',
            ])
            ->from('technology')
            ->where(['alias' => $alias])
            ->one();
    }

    public static function oneByAlias($alias)
    {
        return self::find()
            ->select([
                '*',
            ])
            ->where(['alias' => $alias])
            ->one();
    }
}