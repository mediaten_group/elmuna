<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 28.01.2016
 * Time: 12:48
 */

namespace app\models;


use app\behaviors\DateTimeBehavior;

class UserProfile extends \app\tables\UserProfile
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['name', 'string', 'min' => 3, 'max' => 25, 'message' => 'Длина имени должна быть от 3 до 25 символов.'];
        $rules[] = ['address', 'string', 'max' => 255, 'message' => 'Адрес не должен превышать 255 символов.'];
//        $rules[] = ['email', 'email', 'message' => 'Введите корректный Email.'];
        $rules[] = ['phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'Введите корректный номер телефона.'];
        return $rules;
    }

    public function behaviors()
    {
        return [
            DateTimeBehavior::className(),
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Имя';
        $labels['address'] = 'Адрес';
//        $labels['email'] = 'Email';
        $labels['phone'] = 'Телефон';
        return $labels;
    }

    public static function getInfo($user_id)
    {
        return self::find()
            ->asArray()
            ->select([
                'name',
                'address',
                'phone'
            ])
            ->where(['user_id' => $user_id])
            ->one();
    }
}