<?php

namespace app\models;

use yii\db\Query;

class News extends \app\tables\News
{
    public $cover;

    public static function get()
    {
        return self::find()
            ->select([
                'news.*',
                '(SELECT upload."name" FROM upload WHERE upload.id = news.cover_image_id) AS cover',
            ])
            ->where([
                'status' => 1,
            ]);
    }



    public static function getOne($condition)
    {
        return self::find()
            ->select([
                'news.*',
                '(SELECT upload."name" FROM upload WHERE upload.id = news.cover_image_id) AS cover',
            ])
            ->where($condition)
            ->one();
    }

    public static function news()
    {
        return (new Query())
            ->select([
                'news.id',
                'news.title',
                'news.alias',
                'news.date',
                'news.intro_text'
            ])
            ->from('news')
            ->where([
                'news.state' => 1,
            ])
            ->all();
    }

    public static function article($alias)
    {
        return (new Query())
            ->select([
                'news.id',
                'news.title',
                'news.alias',
                'news.text',
            ])
            ->from('news')
            ->where([
                'news.alias' => $alias,
                'news.state' => 1
            ])
            ->one();
    }

    public static function getImage()
    {
        return '/images/news.png';
    }
}