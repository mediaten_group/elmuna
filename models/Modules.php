<?php

namespace app\models;

use yii\db\Query;

class Modules extends \app\tables\Modules
{
    public static function getText($position)
    {
        return (new Query())
            ->select([
                'modules.text',
            ])
            ->from('modules')
            ->where(['modules.alias' => $position])
            ->one();
    }

    public static function getType($position)
    {
        return (new Query())
            ->select([
                'modules.alias',
                'modules.text',
            ])
            ->from('modules')
            ->where(['modules.type' => $position])
            ->all();
    }

    public static function getModules()
    {
        return (new Query())
            ->select([
                'modules.alias AS position',
                'modules.text',
            ])
            ->from('modules')
            ->where(['modules.type' => ''])
            ->all();
    }
}