<?php
/**
 * Created by PhpStorm.
 * User: dan.judex
 * Date: 29.09.2015
 * Time: 12:02
 */

namespace app\models;

use Yii;

/**
 * Class Tree
 * @package app\models
 */
class Tree extends \kartik\tree\models\Tree
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'safe'],
            [['on_front_page'], 'boolean'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Псевдоним',
            'on_front_page' => 'На главной странице',
        ]);
    }
}
