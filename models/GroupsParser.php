<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 23.03.16
 * Time: 17:59
 */

namespace app\models;

use app\lib\My;

class GroupsParser extends \app\tables\GroupsParser
{

    public static function doGroup($category, $title, $parser_version, $source,$id = null)
    {
        if ($id) {
            $group = GroupsParser::findOne($id);
        } else {
            $group = GroupsParser::findOne(['title' => $title, 'categories_link_id' => $category]);
        }
        if ($group) {
            if ($group->parser_version < $parser_version) {
            } else {
                $group->title = $title;
                $group->parser_version = $parser_version;
                $group->source = $source;
                $group->date = My::dateTime();
                $group->save();
            }
        } else {
            $group = new GroupsParser();
            $group->categories_link_id = $category;
            $group->title = $title;
            $group->parser_version = $parser_version;
            $group->source = $source;
            $group->date = My::dateTime();
            $group->save();
        }
        return $group->id;
    }

}