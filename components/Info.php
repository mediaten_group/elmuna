<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 14:20
 */

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;

class Info
{
    private static $_info = null;

    public static function init()
    {
        $info = \app\tables\Info::find()
            ->asArray()
            ->select('
                    "key",
                    CASE
                        WHEN ("type" = \'image\') THEN
                            (
                                SELECT
                                    upload."name"
                                FROM
                                    upload
                                WHERE
                                    upload. ID = info."value" :: INT
                            )
                        ELSE
                            "value"
                        END AS "value"
            ')
            ->all();

        return self::$_info = ArrayHelper::map($info, 'key', 'value');
    }

    public static function get($key)
    {
        if (!self::$_info) self::init();
        if (self::$_info && array_key_exists($key, self::$_info)) return self::$_info[$key];

        return '';
    }

    public static function getNumber($key)
    {
        if (!self::$_info) self::init();
        if (self::$_info && array_key_exists($key, self::$_info)) {
            $s1 = self::$_info[$key];
            $s2 = '';
            for ($i = 0; $i < strlen($s1); $i++) {
                $d = $s1[$i];
                if (is_numeric($d) || $d == '+') {
                    $s2 .= $d;
                }
            }
            return $s2;
        }

        return '';

    }
}