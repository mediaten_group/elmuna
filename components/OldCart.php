<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 14:20
 */

namespace app\components;


use app\models\Items;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\helpers\Json;

class OldCart
{
    private static $_cart;
    private static $_count;
    private static $_price;

    public static function set()
    {
        if (!self::getCookie() && !Yii::$app->user->isGuest) { // Если авторизован, но нет куков
            $cart = self::getItems(); // Ищет в БД
            if ($cart) {
                $count = $price = 0;
                foreach ($cart as $c) {
                    $count++;
                    $price += ($c['price'] * $c['count']);
                }

                self::setCookie($cart, $count, $price);

                self::$_cart = $cart;
                self::$_count = $count;
                self::$_price = $price;
            }
        }
        if (!self::$_cart) self::$_cart = self::getCookie('cart');
        if (!self::$_count) self::$_count = self::getCookie('cart-count');
        if (!self::$_price) self::$_price = self::getCookie('cart-price');
    }

    public static function getItems()
    {
        return \app\models\Cart::find()
            ->asArray()
            ->select([
                'item_id',
                'count',
                '(SELECT items.price FROM items WHERE items.id = cart.item_id) AS price',
            ])
            ->where([
                'user_id' => Yii::$app->user->getId(),
                'status' => 0,
            ])
            ->indexBy('item_id')
            ->all();
    }

    public static function getCart()
    {
        if (Yii::$app->user->isGuest) {
            $items = [];
            $cart = [];
            $cookie = self::getCookie('cart');
            foreach ($cookie as $c) {
                $items[] = $c['item_id'];
            }
            $items = Items::find()
                ->asArray()
                ->select([
                    'id',
                    'price',
                    'title',
                    'article',
                    '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS cover',
                ])
                ->where(['id' => $items, 'status' => 1])
                ->indexBy('id')
                ->all();
            foreach ($items as $i) {
                $cart[] = [
                    'item_id' => $i['id'],
                    'count' => $cookie[$i['id']]['count'],
                    'price' => $i['price'],
                    'title' => $i['title'],
                    'article' => $i['article'],
                    'cover' => $i['cover'],
                ];
            }
        } else {
            $cart = \app\models\Cart::find()
                ->asArray()
                ->select([
                    'cart.item_id',
                    'cart.count',
                    'items.price',
                    'items.title',
                    'items.article',
                    '(SELECT upload."name" FROM upload WHERE upload.id = items.cover_image_id) AS cover',
                ])
                ->where([
                    'cart.user_id' => Yii::$app->user->getId(),
                    'cart.status' => 0,
                    'items.status' => 1,
                ])
                ->innerJoin('items', 'items.id = cart.item_id')
                ->orderBy('cart.updated_at,cart.created_at')
                ->all();
        }

        $count = $price = 0;
        $cookieCart = [];
        foreach ($cart as $key => $c) {
            $cookieCart[$key] = [
                'item_id' => $c['item_id'],
                'price' => $c['price'],
                'count' => $c['count'],
            ];
            $count++;
            $price += ($c['price'] * $c['count']);
        }
        self::setCookie($cookieCart, $count, $price);

        return $cart;
    }

    public static function getPrice()
    {
        return self::$_price ?: 0;
    }

    public static function getCount()
    {
        return self::$_count ?: 0;
    }

    public static function merge()
    {
        $cart = self::getCookie('cart');
        if (!$cart) return;

        $items = self::getItems(); // Товары из БД

        if (!$items) {
            foreach ($cart as $i) { // Все товары из корзины добавляем в БД
                self::addItem($i['item_id'], $i['count']);
            }
        } else {
            $cartCount = $cartPrice = 0;
            foreach ($cart as $i) { // Перебираем товары из корзины
                $cartCount++;
                $cartPrice += $i['price'] * $i['count'];
                self::addItem($i['item_id'], $i['count']); // Добавляем товар или количество в БД
            }
            foreach ($items as $i) { // Перебираем товары из БД
                if (!array_key_exists($i['item_id'], $cart)) { // Если товара нет в корзине, добавляем
                    $cartCount++;
                    $cartPrice += $i['price'] * $i['count'];
                    $cart[$i['item_id']] = $i;
                } else { // Прибавляем количество, если такой товар есть в корзине
                    $cartPrice += $i['price'] * $i['count'];
                    $cart[$i['item_id']]['count'] += $i['count'];
                }
            }
            self::setCookie($cart, $cartCount, $cartPrice);
        }
    }

    public static function setCookie($cart, $cartCount = null, $cartPrice = null)
    {
//        if (is_array($cart)) $cart = Json::encode($cart);

//        Yii::$app->session->set('cart-count', $cartCount);
//        Yii::$app->session->set('cart-price', $cartPrice);
//        Yii::$app->session->set('cart', $cart);

        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'cart',
            'expire' => time() + (3600 * 24 * 60),
            'path' => '/',
            'value' => [
                'cart' => $cart,
                'cart-count' => $cartCount,
                'cart-price' => $cartPrice,
            ]
        ]));
//        }
    }

    public static function getCookie($param = false)
    {
        $cart = Yii::$app->request->cookies->getValue('cart');

        if (!$cart) return [];
        $cookie['cart'] = $cart['cart'];
        $cookie['cart-count'] = $cart['cart-count'];
        $cookie['cart-price'] = $cart['cart-price'];

        if ($param) {
            $return = array_key_exists($param, $cookie) ? $cookie[$param] : null;
        } else {
            $return = $cookie;
        }

        return $return;
    }

    public static function issetCookie()
    {

    }

    public static function addItem($item_id, $count)
    {
        $user_id = Yii::$app->user->getId();

        /** @var \app\models\Cart $cart */
        $cart = \app\models\Cart::findOne(['item_id' => $item_id, 'user_id' => $user_id, 'status' => 0]);

        if (!$cart) {
            $cart = new \app\models\Cart();

            $data = [
                'item_id' => $item_id,
                'user_id' => $user_id,
                'count' => (int)$count,
                'status' => 0,
            ];
        } else {
            $data = [
                'count' => $cart->count + (int)$count
            ];
        }

        if ($cart->load($data, '')) {
            if (!$cart->save()) throw new Exception(Json::encode($cart->errors));
        } else {
            throw new Exception(Json::encode($cart->errors));
        }
        return true;
    }
}