<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 14:20
 */

namespace app\components;


use app\models\CartItems;
use app\models\Items;
use app\models\Mail;
use app\models\Order;
use app\models\Scheme;
use app\tables\OrderItems;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Cart
{
    private static $_cart = '';
    private static $_count = 0;
    private static $_price = 0;

    public static function set()
    {
//        var_dump($_COOKIE);
//        dd(Yii::$app->request->cookies);

        $cookie = self::getCookie();

        if ($cookie) {
            $cart = self::getCartByCookie($cookie);

            if ($cart) {
                $items = self::getItems($cart);
                $sum = 0;
                $count = 0;
                if ($items) {
                    foreach ($items as $item) {
                        $count++;
                        $sum += ($item['tech_price'] + $item['price']) * $item['count'];
                    }
                }
                self::$_cart = self::getHash($cart);
                self::$_count = $cart['count'];
                self::$_price = $sum;
            }
        } elseif (!Yii::$app->user->isGuest) {
            self::setCookie(self::getUserCart());
        }
    }

    public static function setByCartId($cart_id)
    {
        $cart = \app\models\Cart::find()
            ->select([
                'cart.id',
                'cart.key',
                'COUNT(cart_items.id) as count',
                'SUM(items.price * count) as price',
            ])
            ->where([
                'cart.id' => $cart_id,
                'cart_items.status' => 0,
                'items.status' => 1,
            ])
            ->leftJoin('cart_items', 'cart_items.cart_id = cart.id')
            ->innerJoin('items', 'cart_items.item_id = items.id')
            ->groupBy('cart.id')
            ->one();
        $items = self::getItems($cart);
        $sum = 0;
        $count = 0;
        if ($items) {
            foreach ($items as $item) {
                $count++;
                $sum += ($item['tech_price'] + $item['price']) * $item['count'];
            }
        }
        self::$_cart = self::getHash($cart);
        self::$_count = $cart['count'];
        self::$_price = $sum;

    }

    public static function getCart()
    {
        return Yii::$app->user->isGuest ? self::getCartByCookie() : self::getUserCart();
    }

    public static function getUserCart()
    {
        $cart = \app\models\Cart::find()
            ->select([
                'cart.id',
                'cart.key',
                'COUNT(cart_items.id) as count',
                'SUM(items.price * count) as price',
            ])
            ->where([
                'cart.user_id' => Yii::$app->user->getId(),
                'cart_items.status' => 0,
                'items.status' => 1,
            ])
            ->leftJoin('cart_items', 'cart_items.cart_id = cart.id')
            ->innerJoin('items', 'cart_items.item_id = items.id')
            ->groupBy('cart.id')
            ->one();
        if ($cart) {
            $items = self::getItems($cart);
            $sum = 0;
            $count = 0;
            if ($items) {
                foreach ($items as $item) {
                    $count++;
                    $sum += ($item['tech_price'] + $item['price']) * $item['count'];
                }
            }
            self::$_cart = self::getHash($cart);
            self::$_count = $cart['count'];
            self::$_price = $sum;

            return $cart;
        }

        return null;
    }

    public static function getCartByCookie($cookie = null, $new = false)
    {
        $cookie = $cookie ?: self::getCookie();

        if ($cookie) {
            $cart = \app\models\Cart::find()
                ->select([
                    'cart.id',
                    'cart.key',
                    'COUNT(cart_items.id) as count',
                    'SUM(items.price * count) as price',
                ])
                ->where([
                    'cart.id' => $cookie['id'],
                    'cart.key' => $cookie['key'],
                    'cart_items.status' => 0,
                    'items.status' => 1,
                ])
                ->leftJoin('cart_items', 'cart_items.cart_id = cart.id')
                ->leftJoin('items', 'cart_items.item_id = items.id')
                ->groupBy('cart.id')
                ->one();
            if (!$cart) $cart = \app\models\Cart::find()
                ->select([
                    'cart.id',
                    'cart.key',
                    '(0) as count',
                    '(0) as price',
                ])
                ->where([
                    'cart.id' => $cookie['id'],
                    'cart.key' => $cookie['key'],
                ])
                ->one();
        } else {
            $cart = $new ? new \app\models\Cart() : null;
        }

        return $cart;
    }

    public static function getPrice()
    {
        return self::$_price;
    }

    public static function getCount()
    {
        return self::$_count;
    }

    public static function merge()
    {
        $cookieCart = self::getCartByCookie();
        if (!$cookieCart) return; // Если в куках нет корзины, то и тут нечего делать

        /** @var Cart $userCart */
        $userCart = self::getUserCart();

        if (!$userCart) { // Если в БД нет корзины, а в куках есть, то просто записываем её на пользователя
            $cookieCart->user_id = Yii::$app->user->getId();
            if ($cookieCart->save()) return;
        }

        $cookieItems = self::getItems($cookieCart); // Товары из Сессии

        foreach ($cookieItems as $i) { // Перебираем товары из корзины
            self::addItem($userCart['id'], $i['id'], $i['count'], $i['tech']); // Добавляем товар или количество в БД
        }

        self::removeCart($cookieCart, true); // Удаляем корзину, которая была привязана к кукам
    }

    public static function getCookie()
    {
        $cookie = Yii::$app->request->cookies->getValue('cart') ?: ArrayHelper::getValue($_COOKIE, 'cart');

        if (!$cookie) return null;

        $cookie = self::getValue($cookie);

        if (!isset($cookie[1])) return null;

        return ['id' => $cookie[0], 'key' => $cookie[1]];
    }

    public static function setCookie($cart)
    {
        $hash = self::getHash($cart);

        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'cart',
            'expire' => time() + (3600 * 24 * 60),
            'path' => '/',
            'httpOnly' => false,
            'value' => $hash
        ]));

        return $hash;
    }

    public static function getItem($item_id)
    {

    }

    public static function addItem($cart_id, $item_id, $count, $tech)
    {
        if ($tech)
            $tech_json = Json::encode($tech);
        else
            $tech_json = null;
        /** @var \app\models\CartItems $cart_item */
        $cart_item = \app\models\CartItems::findOne(['item_id' => $item_id, 'cart_id' => $cart_id, 'status' => 0, 'tech' => $tech_json]);

        if (!$cart_item) {
            $cart_item = new \app\models\CartItems();

            $data = [
                'item_id' => $item_id,
                'cart_id' => $cart_id,
                'count' => (int)$count,
                'status' => 0,
                'tech' => $tech_json,
            ];
        } else {
            $data = [
                'count' => $cart_item->count + (int)$count
            ];
        }

        if ($cart_item->load($data, '')) {
            if ($cart_item->save()) {
                self::set();
            } else {
                throw new Exception(Json::encode($cart_item->errors));
            }
        } else {
            throw new Exception(Json::encode($cart_item->errors));
        }
        return true;
    }

    public static function getItems($cart)
    {
        $id = ArrayHelper::getValue($cart, 'id');
        if (!$id) return null;

        $items = CartItems::find()
            ->asArray()
            ->select([
                'items.id as item_id',
                'items.title',
                'items.price',
                'cart_items.count',
                'cart_items.cart_id',
                'cart_items.id as cartItemId',
                'upload.name as cover',
                'cart_items.tech'
            ])
            ->where([
                'cart_items.cart_id' => $id,
                'cart_items.status' => 0,
                'items.status' => 1,
            ])
            ->orderBy('cart_items.updated_at,cart_items.created_at')
            ->innerJoin('items', 'items.id = cart_items.item_id')
            ->leftJoin('upload', 'upload.id = items.cover_image_id')
            ->all();

        foreach ($items as &$item) {
            if ($item['tech']) {
                $item['tech_price'] = Scheme::getPrice($item['tech']);                
                if ($item['tech_price'] != 'Ошибка') {
                    $item['tech_text'] = Scheme::getPriceFields($item['tech']);
                }
                $item['tech_name'] = Scheme::getSchemeName($item['tech']);
            } else {
                $item['tech_price'] = 0;
                $item['tech_name'] = 'Нет';
            }
        }

        return $items;
    }

    public static function getHash($cart)
    {
        $id = ArrayHelper::getValue($cart, 'id');
        $key = ArrayHelper::getValue($cart, 'key');

        if (!$id || !$key) return false;

        return base64_encode(implode(',', [$id, $key]));
    }

    public static function getValue($hash)
    {
        $hash = explode(',', base64_decode($hash));

        if (isset($hash[1])) return $hash;

        return false;
    }

    public static function removeCart($cart, $remove_cookie = false)
    {
        $id = ArrayHelper::getValue($cart, 'id');

        /** @var \app\models\Cart $cart */
        $cart = \app\models\Cart::findOne($id);
        if (!$cart) return false;

        if ($cart->delete() && $remove_cookie) {
            Yii::$app->response->cookies->remove('cart');
        }

        return true;
    }

    public static function addOrder($order_id)
    {
        $cart = self::getCart();
        if (!$cart) throw new Exception('Корзина не найдена');

        $cart_items = self::getItems($cart);
        if (!$cart_items) throw new Exception('Товары не найдены');
        $order_array = array();

        $price = 0;
        foreach ($cart_items as $cart_item) {
            $order_item = new OrderItems();
            $order_item->load($cart_item, '');
            $order_item->order_id = $order_id;
            $order_item->tech = Json::encode(Scheme::getPriceFields($order_item->tech));
            if ($order_item->save()) {
                $item_price = $order_item->count * ((float)$order_item->price + (float)$order_item['tech_price']);
                $price += $item_price;
                /** @var CartItems $cart_item */
                $cart_item = CartItems::findOne($cart_item['cartItemId']);
                if (!$cart_item->delete()) throw new Exception('Товар из корзины не удалён');
            }
        }
        $order_array['price'] = $price;
        $order_array['date'] = date('Y-m-d H:i:s');

        /** @var Order $order */
        $order = Order::findOne($order_id);
        $items = OrderItems::find()->asArray()->where(['order_id' => $order_id])->all();
        $order->price = $price;
        if (!Yii::$app->user->isGuest) $order->user_id = Yii::$app->getUser()->getId();
        $order->save();
        Mail::registerOrder($order->email, $order_array, $items);

        Yii::$app->response->cookies->remove('cart');

        Yii::$app->session->setFlash('order', true);

        return true;
    }

    public static function itemQuantity($cart_item_id, $quantity)
    {
        $cart = self::getCart();
        $cart_item = CartItems::findOne($cart_item_id);
        if (!$cart_item) {
            return ['status' => 'error', 'error' => 'item not found'];
        }
        if ($cart_item->cart_id != $cart->id) {
            return ['status' => 'error', 'error' => 'auth'];
        };
        $cart_item->count = $quantity;
        if (!$cart_item->save()) {
            return ['status' => 'error', 'error' => $cart->errors];
        }
        $price = $cart_item->getItem()->one()->price;
        if ($cart_item['tech']) {
            $tech_price = Scheme::getPrice($cart_item['tech']);
        } else {
            $tech_price = 0;
        }
        $total_price = $quantity * ($price + $tech_price);
        $cart = self::getCart();
        $cart_price = self::getPrice();
        $cart_count = self::getCount();
        return ['status' => 'ok', 'item_price' => $total_price, 'cart_price' => $cart_price, 'cart_count' => $cart_count];
    }
}