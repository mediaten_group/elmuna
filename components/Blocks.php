<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 15.01.2016
 * Time: 17:38
 */

namespace app\components;


use Yii;

class Blocks
{
    private static $_blocks;
    private static $_once = [];

    public static function set($names, $once = false)
    {
        if (is_array($names)) {
            foreach ($names as $block => $name) {
                // Если стоит $once, то в эту позицию больше записываться ничего не будет
                if (isset(self::$_once[$block])) continue;
                if ($once === true || $name === false) {
                    self::$_once[$block] = true;
                    if($name === false) continue;
                }

                // Если передано [ключ => массив], то в таком виде оно и будет записано
                if (is_array($name)) {
                    foreach ($name as $val) {
                        self::$_blocks[$block][$val] = Yii::$app->view->render('@app/views/layouts/blocks/' . $val);
                    }
                } else {
                    self::$_blocks[$name] = Yii::$app->view->render('@app/views/layouts/blocks/' . $name);
                }
            }
        } else {
            if (isset(self::$_once[$names])) return;
            if ($once === true) {
                self::$_once[$names] = true;
            }
            self::$_blocks[$names] = Yii::$app->view->render('@app/views/layouts/blocks/' . $names);
        }
    }

    public static function get($name)
    {
        return isset(self::$_blocks[$name]) ? self::$_blocks[$name] : [];
    }
}
