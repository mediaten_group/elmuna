<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.01.2016
 * Time: 14:20
 */

namespace app\components;

use Yii;

class Index
{
    public static function markup()
    {
        $sql = <<<SQL
WITH RECURSIVE r AS (
  SELECT
    id,
    set_markup
  FROM categories 
  WHERE pid IS NULL
   
  UNION

  SELECT
    categories.id,
    CASE WHEN categories.set_markup IS NULL THEN
      r.set_markup
    ELSE
      categories.set_markup
    END
  FROM categories
  INNER JOIN r ON r.id = categories.pid
) 
UPDATE categories 
SET markup = r.set_markup 
FROM (
  SELECT * FROM r
) AS r
WHERE categories.id = r.id
SQL;

        $cmd = Yii::$app->db->createCommand($sql);

        $cmd->execute();

        $sql = <<<SQL
UPDATE items
SET price = (
	r.set_price + r.set_price * r.markup / 100
)
FROM
	(
		SELECT
			CASE
		WHEN categories.markup IS NULL THEN
			0
		ELSE
			categories.markup
		END AS markup,
		items.set_price,
		items. ID
	FROM
		categories
	INNER JOIN category_link ON category_link.category_id = categories. ID
	INNER JOIN items ON items.group_id = category_link.group_id
	) AS r
WHERE
	items. ID = r. ID
SQL;

        $cmd = Yii::$app->db->createCommand($sql);

        $cmd->execute();
    }
}
