<?php

namespace app\components;

use Yii;
use yii\base\Component;


class Init extends Component
{
    public function init()
    {
        /** Запуск приложения */

        Yii::$app->view->params['main_meta'] = [
            'description'=>Info::get('meta_description'),
            'keywords'=>Info::get('meta_keywords')
        ];
    }
}
