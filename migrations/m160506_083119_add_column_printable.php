<?php

use yii\db\Migration;

class m160506_083119_add_column_printable extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'is_printable', 'boolean');
    }

    public function down()
    {
        $this->dropColumn('pages', 'is_printable');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}