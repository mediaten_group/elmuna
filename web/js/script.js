!function ($) {
    var footerFix = function () {
        $('body').css('margin-bottom', Math.ceil($('#footer').height()) + 'px');
    };

    //$(document).ready(footerFix);
    //
    //$(window).resize(footerFix);

    var collapsy = function (elem, options) {
        var $that = elem;
        var $html;
        var $status = true;
        var $button = options.button;

        this.$elem = $that;
        this.$overlay = $('<div class="collapsy-overlay" style="display: block;"></div>');
        this.$text = $('<div class="collapsy-text">' + $that.html() + '</div>');

        $that.css('height', 'auto');
        $that.html('');
        $that.append(this.$text);
        $that.append(this.$overlay);

        if ($button) {
            this.$action = $('<div class="collapsy-action"><a class="readmore">Читать далее</a></div>');
            $that.append(this.$action);

            this.$action.on('click', $.proxy(function (e) {
                $status = !$status;
                if ($status) {
                    this.$text.css('height', '170px');
                    this.$overlay.show();
                    this.$action.children('a').text('Читать далее');
                } else {
                    this.$text.css('height', 'auto');
                    this.$overlay.hide();
                    this.$action.children('a').text('Свернуть');
                }
            }, this))
        } else {
            this.$overlay.on('click', $.proxy(function (e) {
                $status = !$status;
                if ($status) {
                    this.$text.css('height', '170px');
                    this.$overlay.show();
                } else {
                    this.$text.css('height', 'auto');
                    this.$overlay.hide();
                }
            }, this))
        }

        this.$text.css('height', '170px');

    };


    var fixedHeader = function () {
        if (document.body.clientWidth > 767) {
            //console.log(document.body.clientWidth);
            var headerWrapperHeight = $('.header-wrapper').outerHeight(),
                header = $('.header'),
                addHeader = $('.add-header'),
                navbar = $('.navbar-default'),
                cap = $('.cap'),
                notifications = $('.notifications');
                line = headerWrapperHeight - 55;

            cap.height(navbar.outerHeight(true));
            $(window).scroll(function () {
                var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                if (scrollTop >= line) {
                    addHeader.addClass('active');
                    navbar.addClass('fixed');
                    header.addClass('fixed');
                    notifications.addClass('fixed');
                    cap.show();
                } else {
                    if (addHeader.hasClass('active')) {
                        addHeader.removeClass('active');
                        navbar.removeClass('fixed');
                        header.removeClass('fixed');
                        notifications.removeClass('fixed');
                        cap.hide();
                    }
                }
            });
            $(window).resize(function (e) {
                headerWrapperHeight = $('.header-wrapper').outerHeight();
                line = headerWrapperHeight - 55;
                var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                if (scrollTop >= line) {
                    addHeader.addClass('active');
                    navbar.addClass('fixed');
                    header.addClass('fixed');
                    cap.show();
                } else {
                    if (addHeader.hasClass('active')) {
                        addHeader.removeClass('active');
                        navbar.removeClass('fixed');
                        header.removeClass('fixed');
                        cap.hide();
                    }
                }
            });
            $('body').on('show.bs.modal', '.modal', function () {
                if (addHeader.hasClass('active')) {
                    addHeader.css('padding-right', '17px');
                    navbar.css('padding-right', '17px');
                }
            }).on('hidden.bs.modal', '.modal', function () {
                if (addHeader.hasClass('active')) {
                    addHeader.css('padding-right', '0');
                    navbar.css('padding-right', '0');
                }
            });
        }
    }();
    var showToTop = function () {
        if ($(this).scrollTop() > $(this).height()) {
            $('a[href="#top"]').addClass('display');
        } else {
            $('a[href="#top"]').removeClass('display');
        }
    };
    $(window).resize(fixedHeader).scroll(showToTop);

    $('.form-icon input').on('focusin', function () {
        $(this).parents('.form-icon').addClass('active');
    });

    $('a[href="#top"]').click(function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 500);
    });

    $('.shopping-cart').on('mouseenter', function () {
        $('.cart-content').html('<img class="loadingu" src="/images/loading.gif">');
        $.ajax('/cart/cart-hover')
            .done(function (msg) {
                $('.cart-content').html(msg);
            });
    });

    $('.search-field').on('input', function (e) {
        var search = $(this).val(),
            fields = $('.search-field');
        if (fields.eq(0).val() != search)
            fields.eq(0).val(search);
        if (fields.eq(1).val() != search)
            fields.eq(1).val(search);
    });

    $('.search form').on('submit', function (e) {
        var search = $('.search-field').val();
        if (!search)
            e.preventDefault();
    })

    $.fn.collapsy = function (options) {
        new collapsy(this, options);
    };

}(jQuery);