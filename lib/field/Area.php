<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

class Area extends Model
{

    public $width_max;
    public $width_min;
    public $width_max_desc;
    public $width_min_desc;
    public $height_max;
    public $height_min;
    public $height_max_desc;
    public $height_min_desc;
    public $area_max;
    public $area_max_desc;
    public $area_min;
    public $area_min_desc;
    public $price;
    public $mult;

    public function rules()
    {
        return [
            [['width_max', 'width_min', 'height_max', 'height_min', 'price', 'area_max', 'area_min'], 'number'],
            ['mult', 'boolean'],
            [
                ['width_max_desc', 'width_min_desc', 'height_max_desc', 'height_min_desc', 'area_max_desc', 'area_min_desc'],
                'filter',
                'filter' => 'strip_tags'
            ]
        ];
    }

    public static function getTechPrice($field, $f)
    {
        $value = Json::decode($field['value']);
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];

        if (!$f) {
            $errors[] = "Значение поля неверно";
        } else {
            if ($value['height_max'] && $f[0] > $value['height_max']) {
                $errors[] = "Превышение высоты у [$field[name]]: $f[0] > $value[height_max]";
            }
            if (!$value['height_min'])
                $value['height_min'] = 0;
            if ($f[0] < $value['height_min']) {
                $errors[] = "Высота слишком малая у [$field[name]]: $f[0] < $value[height_min]";
            }
            if ($value['width_max'] && $f[1] > $value['width_max']) {
                $errors[] = "Превышение ширины у [$field[name]]: $f[1] > $value[width_max]";
            }
            if ($f[1] < $value['width_min']) {
                $errors[] = "Ширина слишком малая у [$field[name]]: $f[1] < $value[width_min]";
            }
            if (!$value['width_min'])
                $value['width_min'] = 0;
            if (!$value['area_max'])
                $value['area_max'] = $value['height_max']*$value['width_max'];
            if ($f[2] > $value['area_max']) {
                $errors[] = "Превышение площади у [$field[name]]: $f[2] > $value[area_max]";
            }
            if (!$value['area_min'])
                $value['area_min'] = $value['height_min']*$value['width_min'];
            if ($f[2] < $value['area_min']) {
                $errors[] = "Площадь слишком малая у [$field[name]]: $f[2] < $value[area_min]";
            }
            if ($f[0] * $f[1] != $f[2]) {
                $errors[] = "Значение поля неверно";
            }
        }

        if (!$errors) {
            if (isset($value['mult']) && $value['mult']) {
                $priceMult = $f[2];
                $priceSumm = $value['price'];
            } else {
                $priceSumm = $f[2] * $value['price'];
            }
            if ($field['separate']) {
                $priceMult = 1;
                $priceSep = $priceSumm;
                $priceSumm = 0;
            }
        }

        return [$priceSumm, $priceMult, $priceSep, $errors];
    }

    public static function getTechName($field, $left)
    {
        $text = '';
        $text .= Html::tag('p', "$field[name]: Ширина $left[1]; Высота $left[0]; Площадь $left[2]");

        return [$text, []];
    }

    public static function checkTech($field, $f, &$errors)
    {
        $value = Json::decode($field['value']);

        if (!$f) {
            $errors[] = "Значение поля неверно";
        } else {
            if ($value['height_max'] && $f[0] > $value['height_max']) {
                $errors[] = "Превышение высоты у [$field[name]]: $f[0] > $value[height_max]";
            }
            if (!$value['height_min'])
                $value['height_min'] = 0;
            if ($f[0] < $value['height_min']) {
                $errors[] = "Высота слишком малая у [$field[name]]: $f[0] < $value[height_min]";
            }
            if ($value['width_max'] && $f[1] > $value['width_max']) {
                $errors[] = "Превышение ширины у [$field[name]]: $f[1] > $value[width_max]";
            }
            if ($f[1] < $value['width_min']) {
                $errors[] = "Ширина слишком малая у [$field[name]]: $f[1] < $value[width_min]";
            }
            if (!$value['width_min'])
                $value['width_min'] = 0;
            if (!$value['area_max'])
                $value['area_max'] = $value['height_max']*$value['width_max'];
            if ($f[2] > $value['area_max']) {
                $errors[] = "Превышение площади у [$field[name]]: $f[2] > $value[area_max]";
            }
            if (!$value['area_min'])
                $value['area_min'] = $value['height_min']*$value['width_min'];
            if ($f[2] < $value['area_min']) {
                $errors[] = "Площадь слишком малая у [$field[name]]: $f[2] < $value[area_min]";
            }
            if ($f[0] * $f[1] != $f[2]) {
                $errors[] = "Значение поля неверно";
            }
        }
    }

}