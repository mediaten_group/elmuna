<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

class Number extends Model
{

    public $max;
    public $min;
    public $max_desc;
    public $min_desc;
    public $price;
    public $mult;

    public static function checkTech($field, $f, &$errors)
    {
        $value = Json::decode($field['value']);

        if ($value['max'] && $f > $value['max']) {
            $errors[] = "Значение [$field[name]] слишком большое: $f > $value[max]";
        }
        if (!$value['min'])
            $value['min'] = 0;
        if ($f < $value['min']) {
            $errors[] = "Значение [$field[name]] слишком низкое: $f < $value[min]";
        }
    }

    public function rules()
    {
        return [
            [['max', 'min', 'price', 'mult'], 'number'],
            [['max_desc', 'min_desc'], 'filter', 'filter' => 'strip_tags']
        ];
    }

    public static function getTechPrice($field, $f)
    {
        $value = Json::decode($field['value']);
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];

        if ($value['max'] && $f > $value['max']) {
            $errors[] = "Значение [$field[name]] слишком большое: $f > $value[max]";
        }
        if (!$value['min'])
            $value['min'] = 0;
        if ($f < $value['min']) {
            $errors[] = "Значение [$field[name]] слишком низкое: $f < $value[min]";
        }

        if (!$errors) {
            if (isset($value['mult']) && $value['mult']) {
                $priceMult = $f;
                $priceSumm = $value['price'];
            } else {
                $priceSumm = $f * $value['price'];
            }
            if ($field['separate']) {
                $priceMult = 1;
                $priceSep = $priceSumm;
                $priceSumm = 0;
            }
        }

        return [$priceSumm, $priceMult, $priceSep, $errors];
    }

    public static function getTechName($field, $left)
    {

        $text = '';
        $text .= Html::tag('p',"$field[name]: $left");

        return [$text,[]];
    }


}