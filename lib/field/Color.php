<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use app\tables\FieldColors;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

class Color extends Model
{

    public $select_data = array();
    public $price;
    public $price_mult;

    public static function checkTech($field, $f, &$errors)
    {
        $value = Json::decode($field['value']);

        if (!in_array($f, $value['select_data'])) {
            $errors[] = "Выбранный цвет поля [$field[name]] не найден";
        } else {
            $fieldOption = FieldColors::findOne($f);
            if (!$fieldOption) {
                $errors[] = "Выбранный цвет поля [$field[name]] не найден";
            }
        }
    }

    public function rules()
    {
        return [
            ['select_data', 'string'],
            [['price', 'price_mult'], 'number']
        ];
    }

    public static function getTechPrice($field, $f)
    {
        $value = Json::decode($field['value']);
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];

        if (!in_array($f, $value['select_data'])) {
            $errors[] = "Выбранный цвет поля [$field[name]] не найден";
        } else {
            $fieldOption = FieldColors::findOne($f);
            if (!$fieldOption) {
                $errors[] = "Выбранный цвет поля [$field[name]] не найден";
            }
        }

        if (!$errors) {
            if ($field['separate']) {
                $priceSep = $value['price'][$f];
            } else {
                $priceSumm = $value['price'][$f];
            }
        }

        return [$priceSumm, $priceMult, $priceSep, $errors];
    }

    public static function getTechName($field, $left)
    {
        $color = FieldColors::findOne($left)->getUpload()->one()['name'];

        $text = '';
        $text .= Html::beginTag('p');
        $text .= Html::tag('span', "$field[name]: ");
        $text .= Html::img("/uploads/images/thumbs25/$color");
        $text .= Html::endTag('p');

        return [$text, []];
    }

}