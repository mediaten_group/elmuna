<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 11.03.16
 * Time: 14:01
 */

namespace app\lib\field\cart;

use yii\base\Model;

class FloatField extends Model
{

    public $value;

    public function rules()
    {
        return [
            [['value'], 'number'],
        ];
    }

}