<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use yii\base\Model;

class Image extends Model
{

    public $multiple;

    public function rules()
    {
        return [
            ['multiple', 'integer'],
        ];
    }

}