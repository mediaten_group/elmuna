<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use app\tables\FieldOptions;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

class Select extends Model
{

    public $data;
    public $separate;

    public static function checkTech($field, $f, &$errors)
    {
        $value = Json::decode($field['value']);
        if (!in_array($f, $value['sort'])) {
            $errors[] = "Выбранное значение поля [$field[name]] не найдено";
        } else {
            $fieldOption = FieldOptions::findOne($f);
            if (!$fieldOption) {
                $errors[] = "Выбранное значение поля [$field[name]] не найдено";
            }
        }
    }

    public function rules()
    {
        return [
            ['data', 'string']
        ];
    }

    public static function getTechPrice($field, $f)
    {
        $value = Json::decode($field['value']);
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];

        if (!in_array($f, $value['sort'])) {
            $errors[] = "Выбранное значение поля [$field[name]] не найдено";
        } else {
            $fieldOption = FieldOptions::findOne($f);
            if (!$fieldOption) {
                $errors[] = "Выбранное значение поля [$field[name]] не найдено";
            }
        }

        if (!$errors) {
            if ($field['separate']) {
                $priceSep = $value['price'][$f];
            } else {
                $priceSumm = $value['price'][$f];
            }
        }

        return [$priceSumm, $priceMult, $priceSep, $errors];
    }

    public static function getTechName($field, $left)
    {
        $value = $field['value']['value'][$left];

        $text = '';
        $text .= Html::tag('p',"$field[name]: $value");

        return [$text,[]];
    }

}