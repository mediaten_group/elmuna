<?php

/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 14:51
 */

namespace app\lib\field;

use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

class Checkbox extends Model
{

    public $price;
    public $price_mult;

    public static function checkTech($field, $f, &$errors)
    {
        if (!$f === true) {
            $errors[] = "Значение поля [$field[name]] неверно";
        }
    }

    public function rules()
    {
        return [
            [['price', 'price_mult'], 'number'],
        ];
    }

    public static function getTechPrice($field, $f)
    {
        $value = Json::decode($field['value']);
        $priceSumm = 0;
        $priceMult = 1;
        $priceSep = 0;
        $errors = [];

        if (!$f === true) {
            $errors[] = "Значение поля [$field[name]] неверно";
        }
        if (!$errors) {
            if ($field['separate']) {
                $priceSep = $value['price'];
                if (isset($value['price_mult'])) {
                    $priceSep = $value['price'] * $value['price_mult'];
                }
            } else {
                if (isset($value['price_mult'])) {
                    $priceMult = $value['price_mult'];
                }
                $priceSumm = $value['price'];
            }
        }

        return [$priceSumm, $priceMult, $priceSep, $errors];
    }

    public static function getTechName($field, $left)
    {
        $text = '';
        $text .= Html::tag('p',"+$field[name]");

        return [$text,[]];
    }

}