<?php

namespace app\lib;

use yii;

class My
{
    public static function slickSettings()
    {
        return [
            'slick' => [
                'infinite' => true,
                'autoplay' => false,
                'slidesToShow' => 4,
                'slidesToScroll' => 4,
                'speed' => 250,
                'responsive' => [
                    [
                        'breakpoint' => 768,
                        'settings' => [
                            'arrows' => false,
                            'slidesToShow' => 3,
                            'slidesToScroll' => 3,
                        ]
                    ],
                    [
                        'breakpoint' => 512,
                        'settings' => [
                            'arrows' => false,
                            'slidesToShow' => 2,
                            'slidesToScroll' => 2,
                        ]
                    ],
                ],
            ],
        ];
    }

    public static function slickSettingsSmall()
    {
        return [
            'slick' => [
                'infinite' => true,
                'autoplay' => false,
                'slidesToShow' => 3,
                'slidesToScroll' => 3,
                'speed' => 250,
                'responsive' => [
                    [
                        'breakpoint' => 768,
                        'settings' => [
                            'arrows' => false,
                            'slidesToShow' => 2,
                            'slidesToScroll' => 2,
                        ]
                    ]
                ],
            ],
        ];
    }

    public static function itemImage($item_id, $name)
    {
        return '/uploads/items/' . $item_id . '/thumbs/400/' . $name;
    }

    public static function cover($item_id = 0, $image = '')
    {
        if (!$image) {
            return '/images/no_photo.png';
        }
        return '/uploads/items/' . $item_id . '/thumbs/400/' . $image;
    }

    public static function categoryCover($image = '')
    {
        if (!$image) {
            return '/images/no_photo.png';
        }
        return '/uploads/categories/' . $image;
    }

    public static function thumb($src = null)
    {
        $path = Yii::getAlias('@webroot') . $src;
        return is_file($path) ? $src : '/images/no-thumb.png';
    }

    public static function pageClass()
    {
        $url = Yii::$app->params['route']['url'];
        $url = trim($url, '/');
        $url = str_replace('/', '-', $url);
        return $url;
    }

    public static function sord($sord)
    {
        if ($sord == 'desc') return SORT_DESC;
        return SORT_ASC;
    }

    public static function order($array)
    {
        if (isset($array['sidx']) && $array['sidx']) {
            $order = isset($array['sord']) && $array['sord'] ? $array['sord'] : 'asc';
            return [$array['sidx'] => self::sord($order)];
        }

        return [];
    }

    public static function uniqueName($dir, $ext, $length = 8)
    {
        for ($i = 0; $i < 100; $i++) {
            $name = Yii::$app->security->generateRandomString($length) . '.' . $ext;
            if (!is_file($dir . '/' . $name)) return $name;
        }

        return null;
    }

    public static function arrToOpt($array, $empty = 'Не выбранно')
    {
        $html = '<option value="">' . $empty . '</option>';
        foreach ($array as $key => $value) {
            $html .= '<option value="' . $key . '">' . $value . '</option>';
        }
        return $html;
    }

    public static function dateTime($timestamp = null)
    {
        if ($timestamp) return date('Y-m-d H:i:s', $timestamp);
        return date('Y-m-d H:i:s');
    }

    public static function breadcrumbs($json, $url)
    {
        $breadcrumbs = json_decode($json);
        $output = [];

        if ($breadcrumbs) {
            /* Обрезаем концевые слеши */
            $url = trim($url, '/');
            $parts = explode('/', $url);
            foreach ($breadcrumbs as $key => $link) {
                $url = join('/', array_slice($parts, 0, $key + 1));
                $output[] = ['label' => $link, 'url' => '/' . $url];
            }
        }

        return $output;
    }


    public static function getUri()
    {
        $url = $_SERVER['REQUEST_URI'];
        $uri = explode('/', trim($url, '\/'));
        return $uri;
    }

    /**
     * @return null|\app\models\User
     */
    public static function identify()
    {
        return Yii::$app->user->identity;
    }

    public static function getPrevText($text, $maxchar = 50)
    {
        //$text=strip_tags($text);
        $words = explode(' ', $text);
        $text = '';
        foreach ($words as $word) {
            if (mb_strlen($text . ' ' . $word) < $maxchar) {
                $text .= ' ' . $word;
            } else {
                $text .= '...';
                break;
            }
        }
        return $text;
    }

    public static function mysql_russian_date($datestr = '')
    {
        if ($datestr == '')
            return '';

        // получаем значение даты и времени
        list($day) = explode(' ', $datestr);

        switch ($day) {
            // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $result = 'Сегодня';
                break;

            //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $result = 'Вчера';
                break;
            // Если дата совпадает с заврашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"))):
                $result = 'Завтра';
                break;

            default: {
                // Разделяем отображение даты на составляющие
                list($y, $m, $d) = explode('-', $day);

                $month_str = array(
                    'января', 'февраля', 'марта',
                    'апреля', 'мая', 'июня',
                    'июля', 'августа', 'сентября',
                    'октября', 'ноября', 'декабря'
                );
                $month_int = array(
                    '01', '02', '03',
                    '04', '05', '06',
                    '07', '08', '09',
                    '10', '11', '12'
                );

                // Замена числового обозначения месяца на словесное (склоненное в падеже)
                $m = str_replace($month_int, $month_str, $m);
                if ($d[0] == 0) $d = $d[1];
                // Формирование окончательного результата
                $result = $d . ' ' . $m . ' ' . $y;
            }
        }
        return $result;


    }

    public static function mysql_russian_datetime($datestr = '')
    {
        if ($datestr == '')
            return '';

        // Разбиение строки в 3 части - date, time and AM/PM
        $dt_elements = explode(' ', $datestr);

        list($day) = explode(' ', $datestr);
        switch ($day) {
            // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $ymd = 'Сегодня';
                break;

            //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $ymd = 'Вчера';
                break;

        }
        // Разбиение даты
        $date_elements = explode('-', $dt_elements[0]);

        // Разбиение времени
        $time_elements = explode(':', $dt_elements[1]);

        // вывод результата
        $result1 = mktime($time_elements[0], $time_elements[1], $time_elements[2], $date_elements[1], $date_elements[2], $date_elements[0]);

        $monthes =
            array(' ', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $days =
            array(' ', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');
        $day = date("j", $result1);
        $month = $monthes[date("n", $result1)];
        $year = date("Y", $result1);
        $hour = date("G", $result1);
        $minute = date("i", $result1);
        $dayofweek = $days[date("N", $result1)];
        if (!isset($ymd)) {
            $result = $day . ' ' . $month . ' ' . $year . ' в ' . $hour . ':' . $minute;
        } else {
            $result = $ymd . ' в ' . $hour . ':' . $minute;
        }


        return $result;


    }

    public static function rus2translit($string)
    {
        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public static function str2url($str)
    {
        // переводим в транслит
        $str = My::rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    public static function can($role)
    {
        return Yii::$app->user->can($role);
    }


}
