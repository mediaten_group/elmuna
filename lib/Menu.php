<?php

namespace app\lib;

use yii;
use yii\db\Query;

class Menu
{
    public static function sidebar()
    {
        $parent = Route::parent();
        $route = $parent ? $parent : Route::url();

        $query = (new Query())
            ->select([
                'routes.label',
                'routes.url',
            ])
            ->from('routes')
            ->where(['like', 'url', $route . '%', false])
            ->orderBy([
                'lvl' => SORT_ASC,
                'routes.ordering' => SORT_ASC
            ]);

        return $query->all();
    }
}
