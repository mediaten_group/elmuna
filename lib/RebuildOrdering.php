<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.09.2015
 * Time: 10:34
 */

namespace app\lib;


class RebuildOrdering
{
    public $res = [];
    public $array = [];

    public function rebuild($array)
    {
        foreach($array as $pid => $id) {
            if (is_array($id)) {
                $this->rebuild($id);
            } elseif(!in_array($id, $this->res)) {
                if(isset($this->array[$id])){
                    $this->res[] = $id;
                    $this->rebuild($this->array[$id]);
                } else {
                    $this->res[] = $id;
                }
            }
        }
    }
}