<?php

namespace app\lib;

use app\tables\Routes;
use app\tables\Types;
use yii;
use yii\db\Query;
use yii\web\UrlRuleInterface;
use yii\base\Object;

class UrlRules extends Object implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        /* Склейка и обрезка концевых слешей добавление начального слеша */
        $pathInfo = preg_replace('/\/+/', '/', $pathInfo);
        $pathInfo = trim($pathInfo, '/');
        $pathInfo = '/' . $pathInfo;
        $parts = explode('/', $pathInfo);

        /** @var $route Routes */
        $route = (new Query())
            ->select([
                'routes.url',
                'routes.breadcrumbs',
                'routes.element_id',
                'routes.controller AS controller',
                'types.controller AS type_controller',
            ])
            ->from('routes')
            ->leftJoin('types', 'types.id = routes.type_id')
            ->where(['routes.url' => $pathInfo])
            ->one();

        if ($route == null) return false;

        $route['breadcrumbs'] = My::breadcrumbs($route['breadcrumbs'], $route['url']);
        Yii::$app->params['route'] = $route;
        Yii::$app->params['route']['parent'] = join('/', array_slice($parts, 0, -1));

        if ($route['controller']) {
            return ['/' . $route['controller'] . '/index', []];
        } else {
            return ['/' . $route['type_controller'] . '/index', ['elem_id' => $route['element_id']]];
        }
    }
}