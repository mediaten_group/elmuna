<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 23.09.2015
 * Time: 18:29
 */

namespace app\lib;


class Links
{
    public static function getCatalogLink($current = '', $childs = [], $parents = [])
    {
        $previous = '';
        $next = '';
        if ($parents) {
            foreach ($parents as $p) {
                $previous .= '/' . $p;
            }
        }
        if ($current) $current = '/' . $current;
        if ($childs) {
            foreach ($childs as $c) {
                $next .= '/' . $c;
            }
        }
        return '/catalog' . $previous . $current . $next;
    }

    public static function getImageSource($source, $params = [])
    {
        $path = '';
        $source = '/' . $source;
        if ($params) {
            foreach ($params as $p) {
                $path .= '/' . $p;
            }
        }
        return '/uploads' . $source . $path;
    }
}