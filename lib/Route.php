<?php

namespace app\lib;

use yii;
use yii\db\Query;

class Route
{
    public static function elementTitle($table, $element_id)
    {
        return (new Query())
            ->select('title')
            ->from($table)
            ->where(['id' => $element_id])
            ->one()['title'];
    }

    public static function url()
    {
        return isset(Yii::$app->params['route']) ? Yii::$app->params['route']['url'] : null;
    }

    public static function parent()
    {
        return isset(Yii::$app->params['route']) ? Yii::$app->params['route']['parent'] : null;
    }
}
