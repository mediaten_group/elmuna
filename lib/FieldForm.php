<?php
/**
 * Created by PhpStorm.
 * User: mt4
 * Date: 29.02.16
 * Time: 10:37
 */

namespace app\lib;


use app\lib\field\Area;
use app\lib\field\Checkbox;
use app\lib\field\Color;
use app\lib\field\Image;
use app\lib\field\Number;
use app\lib\field\Select;
use app\tables\FieldColors;
use app\tables\FieldOptions;
use app\tables\FieldType;
use app\tables\Upload;
use yii\bootstrap\Html;
use yii\helpers\Json;
use app\tables\Field;
use kartik\form\ActiveForm;

class FieldForm
{

    public function getBlankForm($type_id)
    {
        $fieldType = FieldType::findOne($type_id)->type;
        switch ($fieldType) {
            case 'area':
                return $this->area();
            case 'color':
                return $this->color();
            case 'checkbox':
                return $this->checkbox();
            case 'select':
                return $this->select();
            case 'number':
                return $this->number();
        }
        return FALSE;
    }

    public function getForm($id)
    {
        $field = Field::findOne($id);
        $fieldType = FieldType::findOne($field->field_type_id)->type;
        switch ($fieldType) {
            case 'area':
                return $this->area($field);
            case 'color':
                return $this->color($field);
            case 'checkbox':
                return $this->checkbox($field);
            case 'select':
                return $this->select($field);
            case 'number':
                return $this->number($field);
        }
        return FALSE;
    }

    public static function saveForm($id, $data)
    {
        $field = Field::findOne($id);
        $fieldType = FieldType::findOne($field->field_type_id)->type;
        if (!$data) {
            $field->value = null;
            $field->save();
            return TRUE;
        }

        switch ($fieldType) {
            case 'area':
                $model = new Area();
                break;
            case 'color':
                $model = new Color();
                break;
            case 'checkbox':
                $model = new Checkbox();
                break;
            case 'select':
                $model = new Select();
                break;
            case 'number':
                $model = new Number();
                break;
        };

        if (!isset($model))
            return FALSE;

        if (isset($data) && ($model->load($data, ''))) {
            $field->value = Json::encode($data);
            if ($field->save()) {
                if ($fieldType == 'select') {
                    $sort = 0;
                    $sort_arr = [];
                    foreach ($data['value'] as $did => $value) {
                        $option = FieldOptions::findOne($did);
                        $sort_arr[$sort] = $did;
                        $option->sort = $sort++;
                        $option->active = true;
                        $option->field_id = $id;
                        $option->save();
                    }
                    FieldOptions::deleteAll([
                        'and',
                        ['field_id' => $id],
                        'active = 0::bit'
                    ]);
                    $data['sort'] = $sort_arr;
                    $field->value = Json::encode($data);
                    $field->save();
                }
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function saveFields($scheme_id, $data)
    {
        $fields = Field::find()
            ->where(['scheme_id' => $scheme_id])
            ->all();

        foreach ($fields as $field) {
            $type = FieldType::findOne($field->field_type_id)->type;
            switch ($type) {
                case 'area':
                    $model = new Area();
                    break;
                case 'color':
                    $model = new Color();
                    break;
                case 'checkbox':
                    $model = new Checkbox();
                    break;
                case 'select':
                    $model = new Select();
                    break;
            };

//            if (isset($data[$field->id]) && ($model->load($data[$field->id], ''))) {
            $field->value = $data[$field->id];
            $field->save();
//            }
        }

        return TRUE;
    }

    public static function number($field = null)
    {
        $data = [
            'max' => null,
            'min' => null,
            'max_desc' => null,
            'min_desc' => null,
            'mult'=>false,
            'price'=>null
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL
        ]);
        $model = new Number();
        $model->load($data, '');
        $html .= $form->field($model, 'max', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[max_desc]' value='$model[max_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[max]",
                'style' => 'width:40%',
                'placeholder' => 'Максимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Максимум');
        $html .= $form->field($model, 'min', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[min_desc]' value='$model[min_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[min]",
                'style' => 'width:40%',
                'placeholder' => 'Минимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Минимум');
        $html .= $form->field($model, 'price', [
            'inputOptions' => [
                'placeholder' => 'Влияние на стоимость',
                'name' => 'value[price]'
            ]
        ])->label('Влияние на стоимость');
        $html .= $form->field($model, 'mult')->checkbox(['label'=>'Общий множитель','name' => 'value[mult]'])->label(false);
        ActiveForm::end();
        $html .= '<hr>';
        return $html;
    }

    public static function area($field = null)
    {
        $data = [
            'height_max' => null,
            'height_min' => null,
            'height_max_desc' => null,
            'height_min_desc' => null,
            'width_max' => null,
            'width_min' => null,
            'width_max_desc' => null,
            'width_min_desc' => null,
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL
        ]);
        $model = new Area();
        $model->load($data, '');
        $id = $field['id'];

        $html .= $form->field($model, 'height_max', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[height_max_desc]' value='$model[height_max_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[height_max]",
                'style' => 'width:40%',
                'placeholder' => 'Максимум',
                'type' => 'text',
                'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Максимальная высота');
        $html .= $form->field($model, 'height_min', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[height_min_desc]' value='$model[height_min_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[height_min]",
                'style' => 'width:40%',
                'placeholder' => 'Минимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Минимальная высота');

        $html .= $form->field($model, 'width_max', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[width_max_desc]' value='$model[width_max_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[width_max]",
                'style' => 'width:40%',
                'placeholder' => 'Максимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Максимальная ширина');
        $html .= $form->field($model, 'width_min', [
            'addon' => [
                'contentAfter' => "<input type='text' name='value[width_min_desc]' value='$model[width_min_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[width_min]",
                'style' => 'width:40%',
                'placeholder' => 'Минимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Минимальная ширина');
        $html.=$form->field($model,'area_max',[
            'addon' => [
                'contentAfter' => "<input type='text' name='value[area_max_desc]' value='$model[area_max_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[area_max]",
                'style' => 'width:40%',
                'placeholder' => 'максимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Максимальная площадь');
        $html.=$form->field($model,'area_min',[
            'addon' => [
                'contentAfter' => "<input type='text' name='value[area_min_desc]' value='$model[area_min_desc]'"
                    . " class='form-control' style='width:60%' placeholder='Подпись при превышении'>",
                'groupOptions' => [
                    'style' => 'width:100%',
                ],
            ],
            'inputOptions' => [
                'name' => "value[area_min]",
                'style' => 'width:40%',
                'placeholder' => 'минимум',
                'type' => 'text', 'pattern' => '\d+(\.\d)?'
            ],
        ])->label('Минимальная площадь');
        $html .= $form->field($model, 'price', [
            'inputOptions' => [
                'placeholder' => 'Влияние на стоимость (за 1 см площади)',
                'name' => 'value[price]'
            ]
        ])->label('Влияние на стоимость (за 1 см площади)');
        $html .= $form->field($model, 'mult')->checkbox(['label'=>'Общий множитель','name' => 'value[mult]'])->label(false);

        ActiveForm::end();
        return $html;
    }

    public static function image($field = null)
    {
        $data = [
            'multiple' => 'false'
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL
        ]);

        $model = new Image();
        $model->load($data, '');

        $html .= $form->field($model, 'multiple', [])->checkbox([
            'name' => 'value[multiple]',
            'checked' => $data['multiple'],
        ], false)->label('Разрешать загрузку нескольких изображений?');

        ActiveForm::end();
        return $html;
    }

    public static function color($field = null)
    {
        $data = [
            'select_data' => [],
            'price' => 0,
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';

        $fieldColors = FieldColors::find()
            ->asArray()
            ->all();

        $colors = array();

        foreach ($fieldColors as $fieldColor) {
            $image = Upload::findOne($fieldColor['upload_id']);
            $colors[$fieldColor['id']] =
                Html::img("/uploads/images/thumbs25/" . $image['name']) . ' ' . $fieldColor['title'];
        }

        $model = new Color();
        $model->load($data, '');
        $id = $field['id'];
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL
        ]);

        $html .= '<div class="box-body"><ul class="todo-list ui-selector">';
        foreach ($colors as $key => $value) {
            $checked = (in_array($key, $data['select_data'])) ? 'checked' : null;
            $disabled = !($checked) ? 'disabled="true"' : '';
            $p = isset($data['price'][$key]) ? $data['price'][$key] : null;
            $p_m = isset($data['price_mult'][$key]) ? $data['price_mult'][$key] : null;
            $html .= <<<HTML
        <li>
            <div class="form-inline">
                <span class="handle ui-sortable-handle">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                </span>
                <label>
                    <input class="checkbox image-checkbox" type="checkbox" name="value[select_data][]" $checked placeholder="Значение" value="$key">
                    $value
                </label>
                <input class="form-control" name="value[price][$key]" value="$p" $disabled placeholder="Увеличение стоимости">
                <input class="form-control" name="value[price_mult][$key]" value="$p_m" $disabled placeholder="Умножение стоимости">
                <button type="button" class="btn btn-danger pull-right delete-option">
                  <div class="glyphicon glyphicon-trash">
                  </div>
                </button>
            </div>
        </li>
HTML;
        }
        $html .= '</div>';
        ActiveForm::end();
        return $html;
    }

    public static function checkbox($field = null)
    {
        $data = [
            'price' => 0
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';
        $model = new Checkbox();
        $model->load($data, '');
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_VERTICAL
        ]);
        $html .= $form->field($model, 'price', [
            'inputOptions' => [
                'placeholder' => 'Увеличение стоимости',
                'name' => 'value[price]'
            ]
        ])->label('Увеличение стоимости');
        $html .= $form->field($model, 'price_mult', [
            'inputOptions' => [
                'placeholder' => 'Умножение стоимости',
                'name' => 'value[price_mult]'
            ]
        ])->label('Умножение стоимости');
        ActiveForm::end();
        return $html;
    }

    public function select($field = null)
    {

        $data = [
        ];
        if ($field['value'])
            $data = Json::decode($field['value']);
        $html = '';
        $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_INLINE
        ]);
        $html .= '<div class="box-body"><ul class="todo-list ui-selector">';
        $id = $field['id'];
        $options = FieldOptions::find()
            ->asArray()
            ->select('id')
            ->where([
                'and',
                ['field_id' => $id],
                'active = 1::bit',
            ])
            ->orderBy('sort')
            ->column();
        if (isset($data['value']))
            foreach ($options as $key) {
                $price = isset($data['price'][$key])?$data['price'][$key]:null;
                $price_mult = isset($data['price_mult'][$key])?$data['price_mult'][$key]:null;
                $value = $data['value'][$key];
                $html .= <<<HTML
        <li data-id="$key">
            <div class="form-inline">
                <span class="handle ui-sortable-handle">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                </span>
                <input class="form-control" name="value[value][$key]" value="$value" placeholder="Значение">
                <input class="form-control" name="value[price][$key]" value="$price" placeholder="Увеличение стоимости">
                <input class="form-control" name="value[price_mult][$key]" value="$price_mult" placeholder="Умножение стоимости">
                <button type="button" class="btn btn-danger pull-right delete-option">
                  <div class="glyphicon glyphicon-trash">                  
                  </div>
                </button>
            </div>
        </li>
HTML;
            }

        $html .= '</ul></div>';
        $html .= '<div class="box-footer clearfix no-border">';
        $html .= "<button type='button' id='add-option'" .
            "class='btn btn-default pull-right' data-id='$id'>Добавить опцию</button>";
        $html .= '</div>';
        ActiveForm::end();
        return $html;
    }

}
