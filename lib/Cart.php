<?php

namespace app\lib;

use yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Cart
{
    private static $_count;
    private static $_price;

    public static function item($id)
    {
        $query = (new Query())
            ->select([
                'id',
                'price',
                'GROUP_CONCAT(ct_item_sizes.size) AS sizes',
            ])
            ->from('ct_items')
            ->leftJoin('ct_item_sizes', 'ct_item_sizes.item_id = ct_items.id')
            ->groupBy('ct_items.id')
            ->where([
                'ct_items.id' => $id,
            ]);

        $item = $query->one();

        if ($item['sizes']) {
            $sizes = explode(',', $item['sizes']);
            $item['sizes'] = array_flip($sizes);
        }

        $item['sizes'] = $item['sizes'] ?: null;

        return $item;
    }

    public static function items($itemList)
    {
        $items = (new Query())
            ->select([
                'id',
                'title',
                'alias',
                'price',
                'IF(cover_image, cover_image, latest_image) AS cover',
                'GROUP_CONCAT(ct_item_sizes.size) AS sizes',
            ])
            ->from('ct_items')
            ->leftJoin('ct_item_sizes', 'ct_item_sizes.item_id = ct_items.id')
            ->groupBy('ct_items.id')
            ->where(['ct_items.id' => $itemList])
            ->all();

        foreach ($items as &$item) {
            if ($item['sizes']) {
                $sizes = explode(',', $item['sizes']);
                $item['sizes'] = array_flip($sizes);
            }

            $item['sizes'] = $item['sizes'] ?: null;
        }

        return $items;
    }

    /**
     * @param $item
     * @param null $size
     * @return mixed
     */
    public static function addItem($item, $size = null)
    {
        $cart = Yii::$app->session['cart'];

        $found = false;

        if (isset($cart['items'])) {
            foreach ($cart['items'] as $key => $row) {
                if ($size) {
                    if ($row['size'] == $size && $row['id'] == $item['id']) {
                        $cart['items'][$key]['count']++;
                        $found = true;
                    }
                } else {
                    if ($row['id'] == $item['id']) {
                        $cart['items'][$key]['count']++;
                        $found = true;
                    }
                }
            }
        }

        if (!$found || !isset($cart['items'])) {
            $cart['items'][] = [
                'id' => $item['id'],
                'size' => $size,
                'count' => 1,
            ];
        }

        Yii::$app->session['cart'] = $cart;

        return $cart;
    }

    public static function calculate()
    {
        $cart = Yii::$app->session['cart'];

        if (isset($cart['items'])) {
            $count = 0;
            foreach ($cart['items'] as $row) {
                $count += $row['count'];
            }
            $cart['totalCount'] = $count;
            Yii::$app->session['cart'] = $cart;
        }

        return $cart;
    }

    public static function render()
    {
        $cart = Yii::$app->session['cart'];

        $itemList = [];

        if (isset($cart['items'])) {
            foreach ($cart['items'] as $row) {
                $itemList[] = $row['id'];
            }

            $itemList = array_unique($itemList);
            $items = self::items($itemList);
            $items = ArrayHelper::index($items, 'id');

            $totalPrice = 0;
            foreach ($cart['items'] as &$row) {
                $row['alias'] = $items[$row['id']]['alias'];
                $row['title'] = $items[$row['id']]['title'];
                $row['cover'] = $items[$row['id']]['cover'];
                $row['price'] = $items[$row['id']]['price'];
                $totalPrice += $row['price'] * $row['count'];
            }

            $cart['totalPrice'] = $totalPrice;
        }

        return $cart;
    }

    public static function cartItems($items)
    {
        $itemList = [];
        foreach ($items as $key => $value) {
            $itemList[] = $key;
        }

        $items = (new Query())
            ->select([
                'ct_items.id',
                'ct_items.alias',
                'ct_items.title',
                'ct_items.price',
                'IF(ct_items.cover_image, ct_items.cover_image, ct_items.latest_image) AS cover',
                'GROUP_CONCAT(ct_item_sizes.size) AS sizes',
            ])
            ->from('ct_items')
            ->leftJoin('ct_item_sizes', 'ct_item_sizes.item_id = ct_items.id')
            ->where(['ct_items.id' => $itemList])
            ->groupBy('ct_items.id')
            ->all();

        return ArrayHelper::index($items, 'id');
    }

    public static function getPrice()
    {
        if(!self::$_price) self::$_price = Yii::$app->session->get('cart-price');
        return self::$_price ?: 0;
    }

    public static function getCount()
    {
        if(!self::$_count) self::$_count = Yii::$app->session->get('cart-count');
        return self::$_count ?: 0;
    }
}
