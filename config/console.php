<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');
Yii::setAlias('@webroot', dirname(__DIR__) . '/web');

$params = require(__DIR__ . '/params.php');

$db = array_merge(
    require(__DIR__ . '/db.php'),
    is_file(__DIR__ . '/db-local.php') ? require(__DIR__ . '/db-local.php') : []
);

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii', 'upload'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://elmuna:elmuna@192.168.1.98:27017/elmuna_parser',
        ],
        'upload' => [
            'class' => '\app\modules\upload\models\Upload',
            'thumbs' => [
                'screen' => ['x' => 1920, 'y' => 1080, 'q' => 95, 'mode' => 'inset', 'dir' => ''],
                'small' => ['x' => 400, 'y' => 1000, 'q' => 95, 'mode' => 'inset', 'dir' => 'thumbs/'],
                '100' => ['x' => 100, 'y' => 100, 'q' => 95, 'mode' => 'outbound', 'dir' => 'thumbs100/'],
            ]
        ],
    ],
    'params' => $params,
];
