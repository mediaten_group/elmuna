<?php

//function dd($data)
//{
//    var_dump($data);
//    exit;
//}

function pre($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function pree($array)
{
    pre($array);
    exit();
}

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['app\components\Init', 'log', 'upload'],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'disabledCommands' => ['netmount'],
            'access' => ['@'],
            'root' => [
                'path' => 'fm',
                'name' => 'Файлы',
//                'options' => [
//                    'uploadDeny'  => ['all'],
//                    'uploadAllow'  => [
//                        'image/png',
//                        'image/jpeg',
//                        'image/gif',
//                        'application/x-rar-compressed',
//                        'application/zip',
//                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
//                        'application/msword',
//                        'application/vnd.ms-word',
//                        'application/vnd.ms-excel',
//                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//                        'application/pdf',
//                    ],
//                ],
            ],
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'QFJyLNwYLwkUtCWBkYfcxIOjwVgMMhcD',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',
                '<_a:(login|logout|signup|activate-email|request-password-reset|reset-password|change-email|download)>' => 'site/<_a>',
                '<_a:(print)>/<alias>' => 'pages/print',
                '<_m:(gii|admin|treemanager|upload)>' => '<_m>/default/index',
                '<_m:(gii|admin|treemanager|upload)>/<_c>' => '<_m>/<_c>/index',
                '<_m:(gii|admin|treemanager|upload)>/<_c>/<_a>' => '<_m>/<_c>/<_a>',
                '<_c:(catalog)>/<category:[\w_\/-]+>' => '<_c>/index',
                '<_c:(item)>/<id:\d+>' => '<_c>/view',
                '<_c:(news)>/<id:\d+>-<alias>' => '<_c>/view',
                '<_c:(technology)>/<alias>' => '<_c>/view',
                '<_c:(catalog|news|brands|cart|technology|profile|reviews|search|clients|test)>' => '<_c>/index',
                '<_c:(item|cart|profile|search)>/<_a>' => '<_c>/<_a>',
                '<_c:(elfinder)>/<_a>' => '<_c>/<_a>',
                '<_c:(elfinder)>' => '<_c>',
                '<_c>/<id>' => '<_c>/view',
                '<alias>' => 'pages',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => $params['adminEmail'],
                'password' => $params['adminEmailPassword'],
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => array_merge(
            require(__DIR__ . '/db.php'),
            is_file(__DIR__ . '/db-local.php') ? require(__DIR__ . '/db-local.php') : []
        ),
        'upload' => [
            'class' => '\app\modules\upload\models\Upload',
            'thumbs' => [
                'screen' => ['x' => 1920, 'y' => 1080, 'q' => 95, 'mode' => 'inset', 'dir' => ''],
                'small' => ['x' => 400, 'y' => 1000, 'q' => 95, 'mode' => 'inset', 'dir' => 'thumbs/'],
                'news' => ['x' => 1000, 'y' => 250, 'q' => 95, 'mode' => 'outbound', 'dir' => 'news/'],
                '100' => ['x' => 100, 'y' => 100, 'q' => 95, 'mode' => 'outbound', 'dir' => 'thumbs100/'],
                '25' => ['x' => 25, 'y' => 25, 'q' => 95, 'mode' => 'outbound', 'dir' => 'thumbs25/'],
            ]
        ]
    ],
    'params' => $params,
    'modules' => [
        'admin' => [
            'class' => '\app\modules\admin\Module',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'upload' => [
            'class' => '\app\modules\upload\Module',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => \yii\debug\Module::className(),
        'allowedIPs' => ['192.168.*.*', '10.*', '188.234.217.146'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => \yii\gii\Module::className(),
        'allowedIPs' => ['192.168.*.*'],
    ];
}

return $config;
