<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var array $bread
 * @var array $list
 * @var array $list2
 * @var array $main
 */

use yii\widgets\LinkPager;
use yii\bootstrap\Html;
use app\widgets\LeftNav;
use app\widgets\Requirements;
use yii\helpers\Json;
use app\assets\MagnificPopupAsset;

MagnificPopupAsset::register($this);

?>

<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?= LeftNav::widget([
                'list' => $list,
                'type' => 'pages',
                'category' => $main,
//            'previous' => $previous . '/',
                'current' => 'clients',
            ]) ?>
        </div>
    </div>
    <?php if ($list2): ?>
        <div class="mini-block">
            <div class="footer no-header">
                <?= LeftNav::widget([
                    'list' => $list2,
                    'type' => 'pages',
//            'category' => $parents[0],
//            'previous' => $previous . '/',
//            'current' => $category['id'],
                    'current' => 'clients',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <?php
        if ($bread) {
            echo \yii\widgets\Breadcrumbs::widget([
                'links' => $bread,
            ]);
        }
        ?>
    </div>
    <div class="col-lg-12">
        <h1>Наши клиенты</h1>
        <hr class="green">
    </div>
    <div class="col-lg-12">
        <?php
        /** @var $item \app\tables\Clients */
        foreach ($dataProvider->getModels() as $item): ?>
            <table class="box clients">
                <tr>
                    <td style="width:25%">
                        <div class="logo popup-gallery">
                            <a href='/uploads/images/<?= $item['logo'] ?>'>
                                <?= Html::img('/uploads/images/' . $item['logo']) ?>
                            </a>
                        </div>
                    </td>
                    <td style="width:75%">
                        <div class="title">
                            <?= $item['title'] ?>
                        </div>
                        <div class="description">
                            <?= $item['description'] ?>
                        </div>
                        <div class="works-description">
                            Примеры работ для этой компании
                        </div>
                        <div class="works popup-gallery">
                            <?php
                            $item['works'] = Json::decode($item['works']);
                            foreach ($item['works'] as $work) :
                                ?>
                                <div class="item">
                                    <a href='/uploads/images/<?= $work ?>'>
                                        <?= Html::img('/uploads/images/thumbs100/' . $work) ?>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </td>
                </tr>
            </table>
        <?php endforeach ?>
        <?= LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ]) ?>
    </div>
</div>
