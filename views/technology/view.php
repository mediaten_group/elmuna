<?php
/**
 * @var $this yii\web\View
 * @var $technologies
 * @var array $images
 * @var float $sum
 * @var \app\models\Technology $current
 * @var \app\models\Technology $parent
 * @var string $main_image
 */
use app\assets\MagnificPopupAsset;
use app\widgets\Requirements;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\LeftNav;

MagnificPopupAsset::register($this);

$this->registerMetaTag(['name' => 'keywords', 'content' => $current->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $current->meta_description ?: $this->params['main_meta']['description']]);

?>

<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <ul class="list-unstyled">
                <?php echo LeftNav::widget([
                    'list' => $technologies,
                    'category' => $parent,
                    'previous' => $parent['alias'] . '/',
                    'current' => $current['alias'],
                    'type' => 'pages'
                ]) ?>
            </ul>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li><a href="<?= Url::to(['/technology/index']) ?>">Технологии нанесения</a></li>
                <li class="active"><?= $current['title']; ?></li>
            </ol>
        </div>
        <div class="col-lg-12">
            <a class="btn-calc" type="button" data-toggle="modal" data-target="#calcModal">
                <i class="ion-calculator"></i>Расчет стоимости
            </a>
            <h1><?= $current['title']; ?></h1>
            <hr>
        </div>
        <div class="col-lg-12 technology">
            <?= $current['text']; ?>
            <?php if ($images): ?>
                <div class="works-description">Примеры работ</div>
                <div class="works popup-gallery">
                    <?php foreach ($images as $image) : ?>
                        <div class="item">
                            <a href="/uploads/images/<?= $image ?>">
                                <img src="/uploads/images/thumbs100/<?= $image ?>" alt="">
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="calcModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="modal-content">
                    <div class="modal-body calc-description">
                        <i class="ion-alert"></i>
                        <p>
                            Сумма минимального заказа - <span><?= $sum ?> руб</span>
                        </p>
                        <p>
                            Средний срок выполнения заказа - 3 дня
                        </p>
                        <p>
                            (при 100% оплате и согласованном макете)
                        </p>
                        <p>
                            Наличие товара уточняйте у менеджера.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="modal-content">
                    <div class="modal-body calc-image">
                        <div class="calc-text-name">
                            <?= $current['title'] ?>
                        </div>
                        <hr>
                        <?= Html::img(["/uploads/images/thumbs/$main_image"]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="modal-content">
                    <div class="modal-body calc-form">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <?= is_file(__DIR__ . '/calc/' . $current['alias'] . '.php') ?
                            $this->render("calc/$current[alias]") :
                            'Раздел в наполнении' ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="modal-content">
                    <div class="modal-body calc-price">
                        <p class="text-align_right">
                            Предварительный расчет
                        </p>
                        <p class="">
                            Цена нанесения на 1 шт
                        </p>
                        <p class="text-align_right">
                            <span class="price">

                            </span>
                        </p>
                        <p class="">
                            Цена может меняться в зависимости от тиража
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
