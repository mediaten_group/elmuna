<?php
/**
 * @var $this yii\web\View
 * @var $model
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Requirements;
use app\widgets\LeftNav;

$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['main_meta']['description']]);

?>
<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <ul class="list-unstyled">
                <?php
                echo LeftNav::widget([
                    'list' => $technologies,
                    'category' => $current,
                    'previous' => $current['alias'] . '/',
                    'current' => $current['alias'],
                    'type' => 'pages'
                ]) ?>
            </ul>
        </div>
    </div>
    <?= Requirements::widget() ?>

</div>
<div class="col-md-9">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active">Технологии нанесения</li>
        </ol>
    </div>
    <div class="col-lg-12">
        <h1>Технологии нанесения</h1>
        <hr>
    </div>
    <div class="col-lg-12 technology">
        <div class="row">
            <?php foreach ($technologies as $m): ?>
                <div class="col-md-4">
                    <a href="<?= Url::to(['/technology/view', 'alias' => $m['alias']]) ?>" class="link">
                        <div class="box">
                            <div
                                class="image"><?= Html::img(My::thumb('/uploads/images/thumbs/' . $m['src'])) ?></div>
                        </div>
                        <div class="title"><?= $m['title'] ?></div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
