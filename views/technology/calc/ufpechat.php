<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
$('.product-type-control').on('change',function(e) {
  var val = $(this).val();
  if (val==1) {
    $('#souvenir').addClass('active');
    $('#table').removeClass('active');
  } else {
    $('#table').addClass('active');
    $('#souvenir').removeClass('active');
  }
});

function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var type = $('.product-type-control').val();
    var height = $('.tab-pane.active .height').val();
    var width = $('.tab-pane.active .width').val();
    var area = parseFloat(height) * parseFloat(width);    
    $('.tab-pane.active .area').val(area);
    var price = '';
    var low,high;
    if (type==1) {
    
    }
    if (type==2) {
         
    }
    $('.calc-price .price').text(price);
}
$('select').on('change',calc);
$('input').on('input',calc);
JS;
$this->registerJs($js);
?>

<div class="form-group">
    <label for="calc-type">Тип изделия</label>
    <select id="calc-type" class="form-control product-type-control">
        <option value="1">Сувениры</option>
        <option value="2">Таблички</option>
    </select>
</div>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="souvenir">
        <div>
            <label>Площадь нанесения в см<sup>2</sup></label>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-3 font-style_italic">
                        <label for="calc-width">Ширина</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-height">Высота</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-area">Площадь</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <input id="calc-width" type="text" class="form-control width">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>X</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-height" type="text" class="form-control height">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>=</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-area" type="text" class="form-control area" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <label>
                <input type="checkbox" class="lak">
                Покрытие лаком
            </label>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="table">
        <div class="form-group">
            <label for="calc-material">Материал</label>
            <select id="calc-material" class="form-control material-control">
                <option value="pl05">Пластик 0,5мм.</option>
                <option value="pl15">Пластик 1,5мм.</option>
                <option value="pl30">Пластик 3мм.</option>
                <option value="org">Оргстекло 3мм</option>
                <option value="foil">Фольга стандарт 0,1 мм.</option>
                <option value="pvx">ПВХ 3мм</option>
                <option value="wood">Дерево</option>
                <option value="dizcarton">Дизайнерский картон</option>
            </select>
        </div>
        <div>
            <label>Площадь нанесения в см<sup>2</sup></label>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-3 font-style_italic">
                        <label for="calc-width">Ширина</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-height">Высота</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-area">Площадь</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <input id="calc-width" type="text" class="form-control width">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>X</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-height" type="text" class="form-control height">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>=</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-area" type="text" class="form-control area" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <label>
                <input type="checkbox" class="lak">
                Покрытие лаком
            </label>
        </div>
    </div>

</div>

<div>
    Максимальная площадь печати 42 х 30 см. (1260 см<sup>2</sup>), допустимая высота 15 см., допустимый вес 5 кг.
</div>
