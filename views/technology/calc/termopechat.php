<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var price = '';   
    
    $('.calc-price .price').text(price);
}

$('select').on('change',calc);
$('input').on('input',calc);

JS;
$this->registerJs($js);
?>

<div class="form-group">
    <label for="calc-area">Площадь нанесения в см<sup>2</sup></label>
    <select id="calc-area" class="form-control area-control">
        <option value="3x10">3см X 10см</option>
        <option value="6x10">6см X 10см</option>
        <option value="A7">10см X 10см</option>
        <option value="A6">A6 (10см X 15см)</option>
        <option value="A5">A5 (15см X 21см)</option>
        <option value="A4">A4 (21см X 30см)</option>
        <option value="A3">A3 (30см X 42см)</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-material">Материал</label>
    <select id="calc-material" class="form-control material-control">
        <option value="termotransfer">Термотрансфер</option>
        <option value="fleks">Флекс</option>
        <option value="flok">Флок</option>
        <option value="reflect">Светоотражающая пленка</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-color">Кол-во цветов</label>
    <select id="calc-color" class="form-control color-control">
        <option>1</option>
        <option class="second-color">2</option>
    </select>
</div>

<div>
    <p>Максимальная площадь термонанесения - 100 x 100 см (10000 см<sup>2</sup>)</p>
    <p>У термотрансфера и светоотражающей пленки может быть только один цвет.</p>
    <p>У термотрансфера также не может быть площадь нанесения быть больше A5</p>
</div>