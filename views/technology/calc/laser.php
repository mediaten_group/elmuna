<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
$('.product-type-control').on('change',function(e) {
  var val = $(this).val();
  if (val==1) {
    $('#souvenir').addClass('active');
    $('#table').removeClass('active');
  } else {
    $('#table').addClass('active');
    $('#souvenir').removeClass('active');
  }
});

function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var type = $('.product-type-control').val();
    var height = $('.tab-pane.active .height').val();
    var width = $('.tab-pane.active .width').val();
    var area = parseFloat(height) * parseFloat(width);    
    $('.tab-pane.active .area').val(area);
    var price = '';
    var low,high;
    if (type==1) {
        if (area>0 && area<8)
            price = '10-30';
        else if (area>=8 && area<10)
            price = '12-31';
        else if (area>=10 && area<12)
            price = '13-32'
        else if (area>=12 && area<13)
            price = '14-33';
        else {
            low = 0.7*area;
            high = 2.2*area;
            if (!low || !high) {
                price = '';
            } else {
                price = round10(low)+'-'+round10(high)
            }
        }
    }
    if (type==2) {
        var mat_mult = parseFloat($('.material-control').val());
        low = mat_mult*area;
        high = (mat_mult+0.9)*area;
        if (!low || !high) {
            price = '';
        } else {
            price = round10(low)+'-'+round10(high)
        }        
    }
    $('.calc-price .price').text(price);
}
$('select').on('change',calc);
$('input').on('input',calc);
JS;
$this->registerJs($js);


?>

<div class="form-group">
    <label for="calc-type">Тип изделия</label>
    <select class="form-control product-type-control" id="calc-type">
        <option value="1">Сувениры</option>
        <option value="2">Таблички</option>
    </select>
</div>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="souvenir">
        <div class="form-group">
            <label>Площадь нанесения в см<sup>2</sup></label>
            <div class="form-inline">
                <div class="row">
                    <div class="col-xs-3 font-style_italic">
                        <label for="calc-width">Ширина</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-height">Высота</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-area">Площадь</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <input id="calc-width" type="text" class="form-control width">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>X</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-height" type="text" class="form-control height">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>=</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-area" type="text" class="form-control area" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="table">
        <div class="form-group">
            <label for="calc-material">Материал</label>
            <select class="form-control material-control" id="calc-material">
                <option value="3.81">Металл (под камень)</option>
                <option value="2.21">Металл (золото-серебро)</option>
                <option value="2.41">Пластик 0,5мм.</option>
                <option value="2.51">Пластик 1,5мм.</option>
                <option value="3.01">Пластик 3мм.</option>
                <option value="1.61">Оргстекло 3мм</option>
                <option value="2.01">Фольга стандарт 0,1мм</option>
                <option value="1.56">Дерево</option>
                <option value="1.11">Дизайнерский картон</option>
            </select>
        </div>
        <div class="form-group">
            <label>Площадь нанесения в см<sup>2</sup></label>
            <div class="form-inline">
                <div class="row">
                    <div class="col-xs-3 font-style_italic">
                        <label for="calc-width">Ширина</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-height">Высота</label>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                        <label for="calc-area">Площадь</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <input id="calc-width" type="text" class="form-control width">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>X</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-height" type="text" class="form-control height">
                    </div>
                    <div class="col-xs-1 text-align_center">
                        <b>=</b>
                    </div>
                    <div class="col-xs-3">
                        <input id="calc-area" type="text" class="form-control area" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    Максимальная площадь гравировки на металле - 10 х 10 (100 см<sup>2</sup>), на других материалах 60 х 40 см (2400
    см <sup>2</sup>)
</div>


