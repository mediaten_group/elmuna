<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var price = '';   
    
    $('.calc-price .price').text(price);
}

$('select').on('change',calc);
$('input').on('input',calc);

JS;
$this->registerJs($js);
?>

<div class="form-group">
    <label for="calc-type">Тип бумаги</label>
    <select id="calc-type" class="form-control paper-control">
        <option value="Color Copy 90 г/м.">Color Copy 90 г/м.</option>
        <option value="Color Copy 100 г/м.">Color Copy 100 г/м.</option>
        <option value="Color Copy 120 г/м.">Color Copy 120 г/м.</option>
        <option value="Color Copy 160 г/м.">Color Copy 160 г/м.</option>
        <option value="Color Copy 200 г/м.">Color Copy 200 г/м.</option>
        <option value="Color Copy 220 г/м.">Color Copy 220 г/м.</option>
        <option value="Color Copy 250 г/м.">Color Copy 250 г/м.</option>
        <option value="Color Copy 280 г/м.">Color Copy 280 г/м.</option>
        <option value="Color Copy 300 г/м.">Color Copy 300 г/м.</option>
        <option value="Бумага самоклейка А3">Бумага самоклейка А3</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-format">Формат бумаги</label>
    <select id="calc-format" class="form-control format-control">
        <option value="90x50">90x50</option>
        <option value="150x105-euro">150x105-euro</option>
        <option value="210x100-euro">210x100-euro</option>
        <option value="A6">A6</option>
        <option value="A5">A5</option>
        <option value="A4">A4</option>
        <option value="A3">A3</option>
        <option value="100x70">100x70</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-color">Цветность</label>
    <select id="calc-color" class="form-control color-control">
        <option value="4+0">4+0</option>
        <option value="4+4">4+4</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-falc">Фальцовка</label>
    <select id="calc-falc" class="form-control falca-control">
        <option value="0">Без фальцовки</option>
        <option value="1">1 фальца</option>
        <option value="2">2 фальцы</option>
        <option value="3">3 фальцы</option>
    </select>
</div>
<div>
    <label>
        <input class="lam-control" type="checkbox">
        Ламинирование
    </label>
</div>

<div>
    Максимальная площадь нанесения - 60 x 40 см (или 40 x 60 см)
</div>