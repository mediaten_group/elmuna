<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var price = '';   
    
    $('.calc-price .price').text(price);
}

$('select').on('change',calc);
$('input').on('input',calc);

JS;
$this->registerJs($js);
?>

<div class="form-group">
    <label for="calc-color">Цвет материала</label>
    <select id="calc-color" class="form-control color-control">
        <option value="white">Белый</option>
        <option value="colors">Цветной</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-count">Количество цветов</label>
    <select id="calc-count" class="form-control color-count-control">
        <option value="1">1 цвет</option>
        <option value="2">2 цвет</option>
        <option value="3">3 цвет</option>
        <option value="4">4 цвет</option>
    </select>
</div>
<div class="form-group">
    <label for="calc-area">Площадь нанесения</label>
    <select id="calc-area" class="form-control area-control">
        <option value="A7">10x10</option>
        <option value="A6">A6</option>
        <option value="A5">A5</option>
        <option value="A4">A4</option>
        <option value="A3">A3</option>
    </select>
</div>

<div>
    Максимальная площадь нанесения - 60 x 40 см (или 40 x 60 см)
</div>

