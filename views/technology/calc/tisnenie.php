<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var mat = $('.material-control').val();
    var height = $('.height').val();
    var width = $('.width').val();
    var area = parseFloat(height) * parseFloat(width);    
    $('.area').val(area);
    var price = '';
    var low,high;
    var folga = $('.folga').prop('checked');
    
    if (folga) {
        price = '7-33'
    } else {
        price = '4-30'
    }
    
    $('.calc-price .price').text(price);
}

$('select').on('change',calc);
$('input').on('input',calc);

JS;
$this->registerJs($js);
?>


<div class="form-group">
    <label for="calc-color">Количество цветов</label>
    <select id="calc-color" class="form-control">
        <option>1</option>
        <option>2</option>
    </select>
</div>
<div>
    <label>Площадь нанесения в см<sup>2</sup></label>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-3 font-style_italic">
                <label for="calc-width">Ширина</label>
            </div>
            <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                <label for="calc-height">Высота</label>
            </div>
            <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                <label for="calc-area">Площадь</label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <input id="calc-width" type="text" class="form-control width">
            </div>
            <div class="col-xs-1 text-align_center">
                <b>X</b>
            </div>
            <div class="col-xs-3">
                <input id="calc-height" type="text" class="form-control height">
            </div>
            <div class="col-xs-1 text-align_center">
                <b>=</b>
            </div>
            <div class="col-xs-3">
                <input id="calc-area" type="text" class="form-control area" readonly>
            </div>
        </div>
    </div>
</div>
<div>
    <label>
        <input type="checkbox" class="folga">
        <span>Фольгирование</span>
    </label>
</div>


<div>
    Максимальная площадь тиснения - 100 x 100 см (10000 см<sup>2</sup>)
</div>
