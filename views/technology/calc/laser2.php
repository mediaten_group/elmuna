<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
function round10(number){
    number = number*10;
    number.round;
    number = number/10;
    return number;
}

function calc() {
    var mat = $('.material-control').val();
    var height = $('.height').val();
    var width = $('.width').val();
    var area = parseFloat(height) * parseFloat(width);    
    $('.area').val(area);
    var price = '';
    var low,high;
    
    
    $('.calc-price .price').text(price);
}

$('select').on('change',calc);
$('input').on('input',calc);

JS;
$this->registerJs($js);
?>

<div class="form-group">
    <label for="calc-material">Материал</label>
    <select id="calc-material" class="form-control material-control">
        <option value="carton">Картон,бумага</option>
        <option value="fanera">Фанера(4мм)</option>
        <option value="orgglass1">Оркстекло(1мм)</option>
        <option value="orgglass2">Оркстекло(2мм)</option>
        <option value="orgglass3">Оркстекло(3мм)</option>
        <option value="orgglass4">Оркстекло(4мм)</option>
        <option value="orgglass5">Оркстекло(5мм)</option>
        <option value="orgglass6">Оркстекло(6мм)</option>
        <option value="orgglass8">Оркстекло(8мм)</option>
        <option value="orgglass10">Оркстекло(10мм)</option>
        <option value="plastic5">Двухслойный пластик(0,5мм)</option>
        <option value="plastic15">Двухслойный пластик(1,5мм)</option>
        <option value="plastic30">Двухслойный пластик(3мм)</option>
    </select>
</div>
<div>
    <label>Площадь нанесения в см<sup>2</sup></label>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-3 font-style_italic">
                <label for="calc-width">Ширина</label>
            </div>
            <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                <label for="calc-height">Высота</label>
            </div>
            <div class="col-xs-3 col-xs-offset-1 font-style_italic">
                <label for="calc-area">Площадь</label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <input id="calc-width" type="text" class="form-control width">
            </div>
            <div class="col-xs-1 text-align_center">
                <b>X</b>
            </div>
            <div class="col-xs-3">
                <input id="calc-height" type="text" class="form-control height">
            </div>
            <div class="col-xs-1 text-align_center">
                <b>=</b>
            </div>
            <div class="col-xs-3">
                <input id="calc-area" type="text" class="form-control area" readonly>
            </div>
        </div>
    </div>
</div>

<div>
    Максимальная площадь резки - 60 х 40 см (или 40 х 60 см)
</div>