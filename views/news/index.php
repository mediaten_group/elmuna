<?php
/**
 * @var $dataProvider
 * @var $list array
 * @var $main array
 * @var $list2 array
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\LeftNav;
use app\widgets\Requirements;

$this->params['h1'] = $this->title;
?>
<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?= LeftNav::widget([
                'list' => $list,
                'type' => 'pages',
                'category' => $main,
//            'previous' => $previous . '/',
                'current' => 'news',
            ]) ?>
        </div>
    </div>
    <?php if ($list2): ?>
        <div class="mini-block">
            <div class="footer no-header">
                <?= LeftNav::widget([
                    'list' => $list2,
                    'type' => 'pages',
//            'category' => $parents[0],
//            'previous' => $previous . '/',
//            'current' => $category['id'],
                    'current' => 'news',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => ['label' => 'Новости'],
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <?php foreach ($dataProvider->getModels() as $item): ?>
            <a class="box news<?php if ($item['cover']) echo ' filled' ?>"
               href="<?= Url::to(['/news/view/', 'id' => $item['id'], 'alias' => $item['alias']]) ?>">
                <?php if ($item['cover']): ?>
                    <div class="image">
                        <?= Html::img(My::thumb('/uploads/images/news/' . $item['cover'])) ?>
                    </div>
                <?php endif ?>
                <div class="clearfix">
                    <h3><?= $item['title'] ?></h3>

                    <p class="date"><?= My::mysql_russian_date($item['created_at']) ?></p>
                </div>
                <?php if (!$item['cover']) echo '<hr>' ?>
                <p class="intro">
                    <?= $item['intro'] ?>
                </p>
            </a>
        <?php endforeach ?>
        <?= LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ]) ?>
    </div>
</div>
