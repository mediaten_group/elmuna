<?php
/**
 * @var \app\models\News $model
 * @var $links
 * @var $list array
 * @var $main array
 * @var $list2 array
 */
use app\assets\MagnificPopupAsset;
use app\lib\My;
use yii\helpers\Html;
use app\widgets\LeftNav;
use app\widgets\Requirements;

MagnificPopupAsset::register($this);

?>
<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?= LeftNav::widget([
                'list' => $list,
                'type' => 'pages',
                'category' => $main,
//            'previous' => $previous . '/',
                'current' => 'news',
            ]) ?>
        </div>
    </div>
    <?php if ($list2): ?>
        <div class="mini-block">
            <div class="footer no-header">
                <?= LeftNav::widget([
                    'list' => $list2,
                    'type' => 'pages',
//            'category' => $parents[0],
//            'previous' => $previous . '/',
//            'current' => $category['id'],
                    'current' => 'news',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="image">
            <a href="<?= My::thumb('/uploads/images/news/' . $model['cover']) ?>" class="popup">
                <?= Html::img(My::thumb('/uploads/images/news/' . $model['cover'])) ?>
            </a>
        </div>
        <?= $model['text'] ?>
    </div>
</div>
