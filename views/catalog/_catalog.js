var page = 0, work = 0;

$('.catalog-items').on('mouseover','.color, .size',function () {
    var $this = $(this),
        $box = $this.parents('.box'),
        $img = $box.find('img'),
        $src = $img.attr('src'),
        $price = $box.find('.price'),
        $article = $box.find('.article'),
        $title = $box.find('.title'),
        price = $this.data('price'),
        src = $this.data('src'),
        title = $this.data('title'),
        article = $this.data('article');
    if (!title) return;
    $src = $src.split('/');
    $src.pop();
    $src[$src.length] = src;
    $src = $src.join('/');
    $img.attr('src', $src);
    $title.html(title);
    $price.html(price);
    $article.html(article);
});

$(window).scroll(function (e) {
    var pos = $(this).scrollTop(),
        item = $('.catalog-items'),
        offset = item.prop('offsetTop'),
        height = item.prop('clientHeight'),
        client = $(window).height(),
        id = item.attr('data-id'),
        priceFrom = $('[name=priceFrom]').val(),
        priceTo = $('[name=priceTo]').val(),
        type = $('[name=type]').val(),
        sort = $('[name=sort]').val(),
        i;
    for (i = 0; i < 6; i++) {
        offset += item.prop('offsetTop');
        item = item.parent();
    }
    if ((pos + client) > (offset + height - 542)) {
        if (work == 0) {
            work = 1;
            $.ajax({
                method: "POST",
                url: "/catalog/next-page",
                data: {
                    id: id,
                    page: page,
                    filter: {'priceFrom': priceFrom, 'priceTo': priceTo, 'type': type, 'sort': sort}
                }
            }).done(function (msg) {
                $('.catalog-items').append(msg);
                work = 0;
                page++;
            }).fail(function () {
                work = 0;
            })
        }
    }
});