<?php
/**
 * @var $this yii\web\View
 * @var $current
 * @var $parents
 * @var $categories
 * @var $groups
 * @var $previous
 * @var $links
 * @var $colors
 * @var $filters
 * @var $material_list
 * @var $color_list
 * @var $page
 * @var bool $poly
 */

use yii\helpers\Url;
use yii\web\JqueryAsset;
use app\widgets\LeftNav;
use app\widgets\Requirements;
use kartik\select2\Select2;
use yii\web\JsExpression;

$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');
$this->registerJs($this->render('_catalog.js'));

$title = ($current->meta_title) ? $current->meta_title : $current->title;
$this->title = $title;

$this->registerMetaTag(['name' => 'keywords', 'content' => $current->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $current->meta_description ?: $this->params['main_meta']['description']]);

$escape = new JsExpression("function(m) { return m; }");
$js = <<<JS
$('#catalog-description').collapsy({
    button:false
});
JS;
$this->registerJs($js);

?>
<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?php if ($links) echo LeftNav::widget([
                'list' => $categories,
                'category' => $parents[0],
                'previous' => $previous . '/',
                'current' => $current['id'],
            ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">
    <div class="col-lg-12">
        <?php if ($links) echo \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $current['title']; ?></h1>
        <hr>
    </div>
    <?php if (!$page) : ?>
        <div class="col-lg-12">
            <?php if ($filters): ?>
                <form class="form-inline" action="<?= Url::to() ?>">
                    <div class="row" style="margin-bottom:5px;">
                        <div class="form-group filter-price">
                            <input type="text" class="form-control" name="priceFrom" id="pricefrom"
                                   placeholder="Цена от"
                                   value="<?= $filters->priceFrom ?>">
                        </div>
                        <div class="form-group filter-price">
                            <input type="text" class="form-control" name="priceTo" id="priceto" placeholder="Цена до"
                                   value="<?= $filters->priceTo ?>">
                        </div>
                        <div class="form-group filter-material">
                            <?= Select2::widget([
                                'name' => 'material',
                                'data' => $material_list,
                                'value' => $filters->material,
                                'theme' => 'bootstrap',
                                'addon' => [
                                    'groupOptions' => [
                                        'style' => 'width:200px'
                                    ]
                                ],
                            ]) ?>
                        </div>
                        <div class="form-group filter-color">
                            <?= Select2::widget([
                                'name' => 'color',
                                'data' => $color_list,
                                'value' => $filters->color,
                                'theme' => 'bootstrap',
                                'addon' => [
                                    'groupOptions' => [
                                        'style' => 'width:150px'
                                    ]
                                ],
                                'pluginOptions' => [
                                    'escapeMarkup' => $escape,
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group sort-type">
                            <select class="form-control" name="type">
                                <option value="min_price" <?= $filters->type == 'price' ? 'selected' : '' ?>>
                                    По цене
                                </option>
                                <option value="title" <?= $filters->type == 'title' ? 'selected' : '' ?>>
                                    По имени
                                </option>
                                <option value="created_at" <?= $filters->type == 'created_at' ? 'selected' : '' ?>>
                                    По дате
                                </option>
                            </select>
                        </div>
                        <div class="form-group sort-direction">
                            <select class="form-control" name="sort">
                                <option value="0" <?= $filters->sort ? '' : 'selected' ?>>Возрастающая</option>
                                <option value="1" <?= $filters->sort ? 'selected' : '' ?>>Убывающая</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary filter-btn">Фильтровать</button>
                    </div>
                </form>
            <?php endif; ?>
        </div>
        <div class="col-lg-12">
            <div class="row catalog-items <?= $filters ? '' : 'spec' ?> <?= $poly ? 'poly' : '' ?>"
                 data-id="<?= $current['id'] ?>">
                <?php foreach ($groups as $group) {
                    echo $this->render('_item', ['group' => $group, 'filters' => $filters ? 1 : 0, 'category' => $current]);
                } ?>
            </div>
        </div>
        <?php if ($current->text): ?>
            <div class="col-lg-12">
                <div id="catalog-description" class="box">
                    <?= $current->text ?>
                </div>
            </div>
        <?php endif; ?>
    <?php else : ?>
        <div class="col-xs-12 spoons">
            <?= $page->text ?>
        </div>
    <?php endif; ?>
</div>

