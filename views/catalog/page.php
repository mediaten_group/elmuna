<?php
/**
 * @var $this yii\web\View
 * @var $current
 * @var $parents
 * @var $categories
 * @var $previous
 * @var $links
 * @var \app\models\Pages $page
 */

use yii\helpers\Url;
use yii\web\JqueryAsset;
use app\widgets\LeftNav;
use app\widgets\Requirements;
use kartik\select2\Select2;
use yii\web\JsExpression;

$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');
$this->registerJs($this->render('_catalog.js'));

$title = ($current->meta_title) ? $current->meta_title : $current->title;
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $current->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $current->meta_description ?: $this->params['main_meta']['description']]);
$escape = new JsExpression("function(m) { return m; }");
$js = <<<JS
$('#catalog-description').collapsy({
    button:false
});
JS;
$this->registerJs($js);

?>
<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?php if ($links) echo LeftNav::widget([
                'list' => $categories,
                'category' => $parents[0],
                'previous' => $previous . '/',
                'current' => $current['id'],
            ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">
    <div class="col-lg-12">
        <?php if ($links) echo \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $current['title']; ?></h1>
        <hr>
    </div>
    <div class="col-xs-12 page-<?= $page->alias ?>">
        <?= $page->text ?>
    </div>
</div>

