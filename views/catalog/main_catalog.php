<?php

/**
 * @var $this yii\web\View
 * @var $current
 * @var $items
 */

use yii\helpers\Html;
use app\widgets\Requirements;

//$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
//$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');

?>
<div class="row">
    <div class="col-lg-12">
        <? if($links) echo \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $category['title']; ?></h1>
        <hr>
    </div>
    <div class="col-md-3">
        <ul class="list-unstyled">
            <?php
            //                            if ($childs) {
            foreach ($categories as $c) {
                echo Html::beginTag('li');
                echo Html::a($c['title'], [
                    '/catalog/catalog', 'level1' => $level1, 'level2' => $c['alias']
                ]);
                if ($c['alias'] == $level2) {
                    echo Html::beginTag('ul');
                    foreach ($childs as $child) {
                        echo Html::beginTag('li');
                        if($child['alias'] == $category['alias']) {
                            echo Html::a('<b>'.$child['title'].'</b>', [
                                '/catalog/catalog', 'level1' => $level1, 'level2' => $level2, 'level3' => $child['alias']
                            ]);
                        } else {
                            echo Html::a($child['title'], [
                                '/catalog/catalog', 'level1' => $level1, 'level2' => $level2, 'level3' => $child['alias']
                            ]);
                        }
                        echo Html::endTag('li');
                    }
                    echo Html::endTag('ul');
                }
                echo Html::endTag('li');
            }
            //                            } else {
            //                                foreach ($categories as $c) {
            //                                    echo Html::tag('li', Html::a($c['title'], [
            //                                        '/catalog/catalog', 'level1' => $level1, 'level2' => $c['alias']
            //                                    ]));
            //                                }
            //                            }
            ?>
        </ul>
        <hr class="orange">
        <?= Requirements::widget() ?>
    </div>
    <div class="col-md-9">
        <form class="form-inline">
            <div class="form-group">
                <select class="dropdown">
                    <option selected>Цвет</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <select class="dropdown">
                    <option selected>Материал</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="pricefrom" placeholder="Цена от">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="priceto" placeholder="Цена до">
            </div>
            <button type="submit" class="btn btn-primary">Фильтровать</button>
        </form>
        <div class="row">
            <?php foreach ($items as $item): ?>
                <div class="col-lg-4">
                    <a href="/item/<?= $item['id']; ?>" class="box">
                        <div class="colors">
                            <span class="color" style="background-color: #FF655D;"></span>
                            <span class="color" style="background-color: #119279;"></span>
                            <span class="color" style="background-color: #D8BB56;"></span>
                            <span class="color" style="background-color: #8B56D8;"></span>
                            <span class="color" style="background-color: #56B1D8;"></span>
                        </div>
                        <div class="image">
                            <img src="/uploads/images/<?= $item['image']; ?>">
                        </div>
                        <h4><?= $item['title']; ?></h4>
                        <span class="price"><?= $item['price']; ?></span>

                        <p>Артикул: <?= $item['article']; ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>