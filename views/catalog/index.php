<?php
/**
 * @var yii\web\View $this
 * @var \app\tables\Categories $current
 * @var \app\tables\Categories $categories
 */

use app\lib\My;
use app\widgets\LeftNav;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Requirements;

$title = ($current->meta_title) ? $current->meta_title : $current->title;
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $current->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $current->meta_description ?: $this->params['main_meta']['description']]);
$js = <<<JS

$('#catalog-description').collapsy({
    button:false
});

JS;
$this->registerJs($js);
?>
<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
                <?php echo LeftNav::widget([
                    'list' => $categories,
                    'category' => $current,
                    'previous' => $current['alias'] . '/',
                    'current' => $current['id'],
                ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active"><?= $current['title']; ?></li>
        </ol>
    </div>
    <div class="col-lg-12">
        <h1><?= $current['title']; ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <?php foreach ($categories as $category): ?>
                <div class="col-lg-4">
                    <!--                    <a class="category" href="-->
                    <? //= Links::getCatalogLink($current['alias'], [$item['alias']]) ?><!--">-->
                    <a class="category"
                       href="<?= Url::to(['/catalog/' . $current['alias'] . '/' . $category['alias']]) ?>">
                        <div class="image box">
                            <?= Html::img(My::thumb('/uploads/images/thumbs/' . $category['src'])) ?>
                        </div>
                        <p><?= $category['title']; ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php if ($current->text): ?>
        <div class="col-lg-12">
            <div id="catalog-description" class="box">
                <?= $current->text ?>
            </div>
        </div>
    <?php endif; ?>
</div>
