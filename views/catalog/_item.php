<?php
/**
 * @var array $group
 * @var bool $filters
 * @var \app\models\Categories $category
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Groups;
use yii\helpers\Json;

$is_flash = Groups::is_flash($group['id']);
$items = Json::decode($group['item']);
if ($is_flash) {
    $sizes = Groups::getSizes($items);
}
if (isset($category) && $category->id == 6465) {
    $sizes = Groups::getVolume($items);
}
$cover = $group['src'] ?: $items[0]['src'];
$price = $items[0]['price'];
$url = count($items) > 1 ? Url::to(['/group/view', 'id' => $group['id']]) :
    Url::to(['/item/view', 'id' => $items[0]['id']]);
?>
<div class="col-lg-4">
    <a href="<?= $url ?>" id="group<?= $group['id'] ?>" class="box" title="<?= $group['title'] ?>">
        <div class="colors">
            <?php if (isset($sizes) && count($sizes) > 1) :
                foreach ($sizes as $size => $item) {
                    $data = 'data-src="' . $item['src'] . '" data-price="' . number_format($item['price'],2) . '" data-article="' . $item['article'] . '" data-title="' . $item['title'] . '"';
                    echo "<span class='size' href='$url' style='background-color: #444' $data>$size</span>";
                }
            elseif (count($items) > 1) :
                $items_count = 0;
                foreach ($items as $item) {
                    if ($items_count++ < 4) {
                        $color = $item['color'] ?: '#ccc;';
                        $data = 'data-src="' . $item['src'] . '" data-price="' . number_format($item['price'],2) . '" data-article="' . $item['article'] . '" data-title="' . $item['title'] . '"';
                        echo '<span class="color" href="' . $url . '" style=\'background-image: url("/uploads/images/thumbs100/' . $item['src'] . '")\' ' . $data . '></span>';
                    }
                    if ($items_count == 4) {
                        echo '<span class="color" >+</span>';
                    }
                }
            else :
                if ($items[0]['color']) {
                    $color = $items[0]['color'] ?: '#ccc;';
                    echo '<span class="color" style="background-color: ' . $color . '"></span>';
                }
            endif; ?>

        </div>
        <div class="image">
            <?= Html::img(My::thumb('/uploads/images/thumbs/' . $cover)) ?>
        </div>
        <?php if ($items[0]['price']) : ?>
            <h4 class="title has-price"><?= $group['title'] ?></h4>
            <?=(isset($category->id) && ($category->id == 6465))?'от ':''?><span class="price"><?= $group['min_price'] ?></span>
        <?php else: ?>
            <h4 class="title"><?= $group['title'] ?></h4>
        <?php endif; ?>
    </a>
</div>
