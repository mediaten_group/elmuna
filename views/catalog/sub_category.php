<?php
/**
 * @var yii\web\View $this
 * @var \app\tables\Categories $current
 * @var \app\tables\Categories $categories
 * @var array $menu
 * @var array $parents
 * @var array $previous
 */

use app\lib\My;
use app\widgets\LeftNav;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Requirements;

$title = ($current->meta_title) ? $current->meta_title : $current->title;
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $current->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $current->meta_description ?: $this->params['main_meta']['description']]);
$js = <<<JS

$('#catalog-description').collapsy({
    button:false
});

JS;
$this->registerJs($js);
?>
<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?php echo LeftNav::widget([
                'list' => $menu,
                'category' => $parents[0],
                'previous' => $previous . '/',
                'current' => $current['id'],
            ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <?php if ($links) echo \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $current['title']; ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <?php foreach ($categories as $category): ?>
                <div class="col-lg-4">
                    <a class="category"
                       href="<?= $current['alias'].'/'.$category['alias'] ?>">
                        <div class="image box">
                            <?= Html::img(My::thumb('/uploads/images/thumbs/' . $category['src'])) ?>
                        </div>
                        <p><?= $category['title']; ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php if ($current->text): ?>
        <div class="col-lg-12">
            <div id="catalog-description" class="box">
                <?= $current->text ?>
            </div>
        </div>
    <?php endif; ?>
</div>
