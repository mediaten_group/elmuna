<?php
/**
 * @var Yii\web\view $this
 * @var array $cart
 * @var array $order
 * @var array $items
 */

use app\lib\My;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->registerJs($this->render('cart.js'));
//pree($items);
$total = 0;
$i = 1;
?>
    <div class="row">
        <div class="col-lg-12">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => ['label' => 'Корзина'],
            ]) ?>
        </div>
        <div class="col-lg-12">
            <h1><?= $this->title; ?></h1>
            <hr>
        </div>
        <div class="col-xs-12 printable">
            <div class="cart">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Номер</th>
                        <th>Выбранные товары</th>
                        <th>Цена за шт.</th>
                        <th>Тираж*</th>
                        <th>Нанесение</th>
                        <th>Итого</th>
                        <th>Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item): ?>
                        <tr <?= ($item['tech_price'] === 'Ошибка') ? 'class="cart-error"' : '' ?>
                            data-id="<?= $item['cartItemId'] ?>">
                            <td><?= $i++ ?></td>
                            <td>
                                <div class="row">
                                    <a href="<?= Url::to(['/item/view', 'id' => $item['item_id']]) ?>">
                                        <div class="col-lg-2">
                                            <?= Html::img(My::thumb('/uploads/images/thumbs100/' . $item['cover'])) ?>
                                        </div>
                                        <div class="col-lg-10">
                                            <?= $item['title'] ?>
                                        </div>
                                    </a>
                                </div>
                            </td>
                            <td><?= ($item['price'] > 0) ? $item['price'] : 'Из нанесения' ?></td>
                            <td>
                                <div class="form-group">
                                    <input type="number" value="<?= $item['count'] ?>"
                                           class="form-control item-quantity-change" min="1"
                                           max="10000">
                                </div>
                            </td>
                            <td>
                                <?php if (isset($item['tech_text']) && ($item['tech_price'] != 'Ошибка')) : ?>
                                    <?= $item['tech_name'] ?>
                                <?php else : ?>
                                    Нет
                                <?php endif; ?>
                            </td>
                            <td><?= ($item['count'] * ((float)$item['price'] + (float)$item['tech_price'])) ?></td>
                            <td><?= Html::a('<i class="ion-close"></i>', '#', [
                                    'class' => 'remove-item',
                                    'data-id' => $item['cartItemId']
                                ]) ?></td>

                        </tr>
                        <tr>
                            <td colspan="7" class="cart-tech-text">
                                <?php
                                if (isset($item['tech_text']) && ($item['tech_price'] != 'Ошибка')) {
                                    $tech = $item['tech_text'];
                                    if (isset($tech['left'])) {
                                        if (isset($tech['right']))
                                            echo Html::tag('h3', 'Покрытие с одной стороны');
                                        echo $tech['left']['text'];
                                    }
                                    if (isset($tech['right'])) {
                                        if (isset($tech['left']))
                                            echo Html::tag('h3', 'Покрытие с другой стороны');
                                        echo $tech['right']['text'];
                                    }
                                    echo "<p>Общая стоимость нанесения: <b class='price'>$item[tech_price]</b></p>";
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        $total += ($item['count'] * ((float)$item['price'] + (float)$item['tech_price']));
                    endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <p>
                        * В графе тираж вы можете поменять ранее выбранное количество товара.<br>
                        Конечная сумма будет пересчитана автоматически.
                    </p>
                    <p>
                        ** Внимание! Конечная стоимость может отличаться от представленной на сайте. Точную цену товаров
                        узнавайте у менеджеров
                    </p>
                    <a class="print-link" href="#" onclick="window.print();return false;">
                        <i class="ion-printer"></i> Распечатать<br> бланк заказа
                    </a>
                </div>
                <div class="col-lg-4 clearfix">
                    <div class="pull-right align-right">
                        <p>Сумма заказа: <strong class="total"><?= $total ?></strong> руб.</p>
                        <p>Расчет предварительный**</p>
                        <a href="#" class="btn btn-primary pull-right <?= ($total < 5000) ? 'disabled' : '' ?>"
                           data-toggle="modal" data-target="#order">
                            Оформить заявку
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo $this->render('_order', ['order' => $order]);