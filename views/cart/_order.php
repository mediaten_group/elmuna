<?php
/**
 * @var OrderForm $order
 */

use app\models\forms\OrderForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

Modal::begin([
    'header' => '<h2>Оформить заявку</h2>',
    'id' => 'order',
//    'toggleButton' => ['label' => 'click me'],
]);
$form = ActiveForm::begin([
    'id' => 'profileForm',
]); ?>
    <div class="row">
        <div class="col-lg-2 text-right">
            <p>ФИО</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($order, 'name')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Адрес</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($order, 'address')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Email</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($order, 'email')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Телефон</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($order, 'phone')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Комментарий</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($order, 'comment')->textarea()->label(false); ?>
        </div>
        <div class="col-lg-12 clearfix">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary pull-right']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>
<?php Modal::end() ?>