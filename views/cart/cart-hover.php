<?php
/**
 * @var array $items
 */

use yii\bootstrap\Html;
?>
<div class="cart">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Товар</th>
            <th>Тираж</th>
            <th>Цена</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item): ?>
            <tr <?= ($item['tech_price'] === 'ОШИБКА')?'class="cart-error"':'' ?>>
                <td>
                    <div class="row">
                            <?= isset($item['cover']) ? Html::img('/uploads/images/thumbs100/' . $item['cover'],['style'=>'margin-left:15px']) : '' ?>
                            <?= $item['title'] ?>
                    </div>
                </td>
                <td><?= $item['count'] ?></td>
                <td><?= $item['count'] * ((float)$item['price'] + (float)$item['tech_price']) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>