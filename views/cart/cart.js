function valid_cart() {
    if (parseFloat($('.total').text()) < 5000) {
        $('a[data-toggle=modal]').addClass('disabled');
    } else {
        var not_valid = $('tr.cart-error').size();
        if (not_valid) {
            $('a[data-toggle=modal]').addClass('disabled')
        } else {
            $('a[data-toggle=modal]').removeClass('disabled')
        }
    }
    return not_valid;

}

valid_cart();

$('.remove-item').click(function (e) {
    e.preventDefault();
    var $this = $(this),
        tr = $this.parents('tr'),
        data = {
            id: $this.attr('data-id')
        };
    $.ajax({
        type: "POST",
        url: "/cart/remove-item",
        data: data,
        dataType: 'json',
        success: function (msg) {
            if (msg.status == 'ok') {
                tr.remove();
                $('.cart-count').html(msg.count);
                $('#cart-price').html(msg.price);
            } else {
                console.dir(msg.message);
            }
        }
    });
});

$('a[data-toggle=modal]').click(function (e) {
    $.ajax({
        type: "POST",
        url: "/cart/check-valid",
        success: function (msg) {
            var trs = $('tr'), i;
            for (i = 0; i < trs.size(); i++) {
                var data_id = trs.eq(i).attr('data-id');
                if (msg.indexOf(data_id) >= 0) {
                    trs.eq(i).addClass('cart-error');
                } else {
                    trs.eq(i).removeClass('cart-error');
                }
            }
            if (valid_cart()) {
                $('#order').modal('hide');
            }
        }
    });
});

$('.item-quantity-change').on('input', function (e) {
    var tr = $(this).parents('tr');
    var max = parseInt($(this).attr('max'));
    var min = parseInt($(this).attr('min'));
    var val = parseInt($(this).val());
    var item_id = $(this).parents('tr').attr('data-id');
    if (val > max || val < min || !val) {
        $('a[data-toggle=modal]').addClass('disabled');
        $(this).parent().addClass('has-error');
    } else {
        $(this).parent().removeClass('has-error');

        $.ajax({
            url: '/cart/item-quantity',
            method: 'POST',
            data: {
                item_id: item_id,
                val: val
            },
            dataType: 'json',
            success: function (msg) {
                console.log(msg);
                if (msg.status == 'ok') {
                    tr.children().eq(5).text(msg.item_price);
                    $('strong.total').text(msg.cart_price);
                    $('#cart-price').text(msg.cart_price);
                    $('.cart-count').text(msg.cart_count);
                }
                valid_cart()
            }
        });
    }
});