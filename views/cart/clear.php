<?php
/**
 * @var \Yii\Web\View $this
 */
use yii\bootstrap\Modal;

if (Yii::$app->session->getFlash('order')) {
    Modal::begin([
        'header' => '<h2>Заявка оформлена</h2>',
        'id' => 'order',
//        'options' => [
//            'class' => 'modal fade in',
//        ]
//    'toggleButton' => ['label' => 'click me'],
    ]);
    echo '<p class="text-center">Наш менеджер свяжется с Вами в самое ближайшее время</p>';
    Modal::end();
    $this->registerJs("$('#order').modal('show');");
}
?>

<div class="row">
    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => ['label' => 'Корзина'],
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title; ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="box">
            <h3>Корзина пуста. Перейти в <a href="/">магазин</a></h3>
        </div>
    </div>
</div>