<div class="wrapper printable" style="visibility: hidden; width: 0px; height: 0px;">
    <div class="line1">
        <span>Общество с ограниченной ответственностью "ЭльМуна"</span>
    </div>
    <div class = "line2">
        <div class="logo_print">
            <img src="/images/logo_print.jpg"/>
        </div>
        <div class="logo_print-text">
            <p>ООО"ЭльМуна"; 620014, Свердловская обл, Екатеринбург г,
                <br>Шейнкмана ул, дом № 20, офис 1
                <br><strong>ИНН 6674380800  /КПП 667401001,</strong>
                <br>Расчетный счет  40702810300200010202
                <br>к/с 30101810400000000774
                <br>БАНК "НЕЙВА" ООО Г. ЕКАТЕРИНБУРГ  БИК 046577774
                <br><strong>ОГРН 1116674012674</strong>
                <br><strong class = "t2">Тел/факс </strong>(343) 371-80-08
                <br><strong class = "t2">E-mail: </strong><span class = "t1 t2">elmuna@inbox.ru</span>
            </p>
        </div>
    </div>
</div>