<?php
use app\components\Info;
use app\lib\My;
?>
<div class="gifts block">
    <div class="container">
        <h3 class="header">Сувенирная продукция</h3>
        <div class="row">
            <div class="col-md-9" style="padding-top: 20px">
                <?=Info::get('gifts_text')?>
            </div>
            <div class="col-md-3">
                <div class="image">
                    <img src="<?= My::thumb('/uploads/images/thumbs/'.Info::get('gifts_image'))?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>