<?php

use app\tables\News;
use yii\helpers\Url;

$news = News::find()->asArray()->where(['status' => 1])->orderBy('created_at DESC')->limit(3)->all();
$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;
?>

<div class="<?php echo $mainBlock ? 'mini-block ' : 'block '?>news">
    <div class="header"><a href="<?=Url::to(['/news/index'])?>">Новости</a></div>
    <div class="footer white">
        <?php foreach ($news as $n): ?>
            <div class="item">
                <div class="date"><?= date("d.m.Y", strtotime($n['created_at'])) ?></div>
                <a href="<?= Url::to(['/news/view/', 'id' => $n['id'], 'alias' => $n['alias']]) ?>" class="title">
                    <?= $n['title'] ?>
                </a>
            </div>
        <?php endforeach ?>
    </div>
</div>