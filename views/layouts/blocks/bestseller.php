<?php
use app\lib\My;
use app\models\Items;
use drmabuse\slick\SlickWidget;
use yii\helpers\Url;

$items = Items::getBestsellers();
//if (count($items) < 1) return false;

$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;

SlickWidget::widget([
    'container' => '.bestseller .images.items',
    'settings' => [
        'slick' => [
//            'infinite' => true,
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'arrows' => false,
            'speed' => 250,
            'asNavFor' => '.bestseller .titles.items',
        ],
    ],
]);
SlickWidget::widget([
    'container' => '.bestseller .titles.items',
    'settings' => [
        'slick' => [
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'autoplay' => true,
            'arrows' => false,
            'fade' => true,
            'speed' => 250,
            'asNavFor' => '.bestseller .images.items',
        ],
    ],
]);
?>
<div class="<?php echo $mainBlock ? 'mini-block ' : 'block ' ?>bestseller">
    <?php if (count($items) > 0) : ?>
        <div class="header">Хит продаж</div>
        <div class="images items">
            <?php foreach ($items as $item): ?>
                <div class="image">
                    <img src="<?= My::thumb("/uploads/images/thumbs/{$item['cover']}") ?>" alt="">
                </div>
            <?php endforeach ?>
        </div>
        <div class="titles items">
            <?php foreach ($items as $item): ?>
                <div class="text">
                    <div class="title"><?= $item['title'] ?></div>
                    <div class="price"><?= $item['price'] ?></div>
                    <a href="<?= Url::to(['/item/view', 'id' => $item['id']]) ?>" class="btn btn-primary">Подробнее</a>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif; ?>
    <div class="footer <?=(!count($items))?'no-header':''?>">
        <ul>
            <li class="buy">
                <a href="<?= Url::to(['/buy']) ?>"><i class="ion-ios-compose"></i><span>Как купить</span></a>
            </li>
            <li class="new">
                <a href="<?= Url::to(['/catalog/new']) ?>"><i class="ion-star"></i><span>Новинки</span></a>
            </li>
            <li class="sale">
                <a href="<?= Url::to(['/catalog/sale']) ?>"><i class="ion-pricetags"></i><span>Распродажа</span></a>
            </li>
            <li class="exclusive">
                <a href="<?= Url::to(['/catalog/exclusive']) ?>"><i class="ion-ios-book"></i><span>Эксклюзивные товары</span></a>
            </li>
        </ul>
    </div>
</div>
