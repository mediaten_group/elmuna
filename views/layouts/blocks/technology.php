<?php
/**
 * @var $this View
 */
use app\lib\My;
use yii\web\View;

$a = \app\models\Technology::mainPage();

$css = <<<CSS
.link {
   position: relative;
   -webkit-perspective: 1000;
   -moz-perspective: 1000;
   -ms-perspective: 1000;
   -o-perspective: 1000;
   margin: 0 0 20px 0;
}
.link .box {
   width: 200px;
   height: 50px;
   position: relative;
   -webkit-transform-style: preserve-3d;
   -webkit-transition: 0.7s 0s;
   -moz-transform-style: preserve-3d;
   -moz-transition: 0.7s 0s;
   -ms-transform-style: preserve-3d;
   -ms-transition: 0.7s 0s;
   -o-transform-style: preserve-3d;
   -o-transition: 0.7s;
   transition: 0.7s 0s;
}
.link:hover .box {
   -webkit-transform: rotateY(180deg);
   -moz-transform: rotateY(180deg);
   -ms-transform: rotateY(180deg);
   -o-transform: rotateY(180deg);
   transform: rotateY(180deg);
   -webkit-transition: 0.5s 0.7s;
   -moz-transition: 0.5s 0.7s;
   -ms-transition: 0.5s 0.7s;
   -o-transition: 0.5s 0.7s;
   transition: 0.5s 0.7s;
}
.front {
   -webkit-backface-visibility: hidden;
   -moz-backface-visibility: hidden;
   -ms-backface-visibility: hidden;
   -o-backface-visibility: hidden;
   backface-visibility: hidden;
   z-index: 10;
}
.back {
   position: absolute;
   -webkit-backface-visibility: hidden;
   -moz-backface-visibility: hidden;
   -ms-backface-visibility: hidden;
   -o-backface-visibility: hidden;
   backface-visibility: hidden;
   -webkit-transform: rotateY(180deg);
   -moz-transform: rotateY(180deg);
   -ms-transform: rotateY(180deg);
   -o-transform: rotateY(180deg);
   transform: rotateY(180deg);
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   padding: 15px;
   overflow: hidden;
   line-height: 50px;
   text-align: center;
    border-radius: 8px;
    background: #fff;
}
.back p {
    line-height: normal;
}
.container:hover .front {
   z-index: 0;
}


CSS;
$this->registerCss($css);

?>


<div id="technology" class="block">
    <h3 class="header">Технологии нанесения</h3>
    <div class="container">
        <div class="row">
            <?php foreach ($a as $row): ?>
                <div class="col-md-20 col-xs-6">
                    <a href="/technology/<?= $row['alias'] ?>" class="link">
                        <div class="box">
                            <div class="front">
                                <div class="image">
                                    <img src="<?= My::thumb('/uploads/images/thumbs/' . $row['src']) ?>" alt="">
                                </div>
                            </div>
                            <div class="back">
                                <p><?=$row['intro']?></p>
                            </div>
                        </div>
                        <div class="title"><?= $row['title'] ?></div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>