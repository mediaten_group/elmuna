<?php
/**
 * @var $this \yii\web\View
 */

use app\lib\My;
use app\models\Items;
use drmabuse\slick\SlickWidget;
use yii\helpers\Url;

$exclusives = Items::getExclusive();
$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;

SlickWidget::widget([
    'container' => '.exclusive .images.items',
    'settings' => [
        'slick' => [
//            'infinite' => true,
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'arrows' => false,
            'speed' => 250,
            'asNavFor' => '.exclusive .titles.items',
        ],
    ],
]);
SlickWidget::widget([
    'container' => '.exclusive .titles.items',
    'settings' => [
        'slick' => [
            'slidesToShow' => 1,
            'slidesToScroll' => 1,
            'autoplay' => true,
            'arrows' => false,
            'fade' => true,
            'speed' => 250,
            'asNavFor' => '.exclusive .images.items',
        ],
    ],
]);
?>


<?php if (count($exclusives) > 0) : ?>
    <div class="<?php echo $mainBlock ? 'mini-block ' : 'block ' ?>exclusive">
        <div class="header">Эксклюзивные товары</div>
        <div class="images items">
            <?php foreach ($exclusives as $exclusive): ?>
                <div class="image">
                    <img src="<?= My::thumb("/uploads/images/thumbs/{$exclusive['name']}") ?>" alt="">
                </div>
            <?php endforeach ?>
        </div>
        <div class="titles items">
            <?php foreach ($exclusives as $exclusive): ?>
                <div class="text">
                    <div class="title"><?= $exclusive['title'] ?></div>
                    <a href="<?= Url::to(['/catalog/exclusive/'.$exclusive['alias']]) ?>" class="btn btn-primary">Подробнее</a>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php endif; ?>
