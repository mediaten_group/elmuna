<?php

use app\models\Special;
use yii\bootstrap\Html;

$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;
$special = Special::getSpecial();

if ($special) :
    ?>

    <div class="<?php echo $mainBlock ? 'mini-block ' : 'block ' ?>specials">
        <div class="header">Спецпредложения</div>
        <div class="images items">
            <div class="specials-image">
                <div>
                    <?= Html::img('/uploads/images/thumbs/' . $special['image']) ?>
                </div>
            </div>
        </div>
        <div class="titles items">
            <div class="text">
                <div class="title">
                    <?= $special['title'] ?>
                </div>
                <div class="specials-text">
                    <?= $special['text'] ?>
                </div>
                <?= Html::a('Подробнее', $special['url'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

<?php endif; ?>