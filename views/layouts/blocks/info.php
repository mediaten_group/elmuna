<?php
use app\tables\Pages;
use yii\helpers\Url;

$pages = Pages::find()->asArray()->indexBy('alias')->all();

$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;
?>
<div class="<?php echo $mainBlock ? 'mini-block ' : 'block ' ?>info">
    <div class="header">Информация</div>
    <div class="footer white">
        <ul>
<!--            --><?php //if ($pages['buy']['status']): ?>
<!--                <li><a href="--><?//= Url::to(['/pages/buy']) ?><!--">Как купить</a></li>--><?php //endif ?>
            <?php if ($pages['about']['status']): ?>
                <li><a href="<?= Url::to(['/pages/about']) ?>">О компании</a></li><?php endif ?>
            <li><a href="<?= Url::to(['/pages/reviews']) ?>">Отзывы</a></li>
            <?php if ($pages['delivery']['status']): ?>
                <li><a href="<?= Url::to(['/pages/delivery']) ?>">Информация о доставке</a></li><?php endif ?>
            <?php if ($pages['contacts']['status']): ?>
                <li><a href="<?= Url::to(['/pages/contacts']) ?>">Контактная информация</a></li><?php endif ?>
            <?php if ($pages['sitemap']['status']): ?>
                <li><a href="<?= Url::to(['/pages/sitemap']) ?>">Карта сайта</a></li><?php endif ?>
        </ul>
    </div>
</div>