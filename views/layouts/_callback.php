<?php
/**
 * @var CallbackForm $callback ;
 */

use app\models\forms\CallbackForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (Yii::$app->session->get('callbackForm')) {
    Modal::begin([
        'header' => '<h2>Заявка отправлена</h2>',
        'id' => 'callback',
    ]);
    echo '<p class="text-center">Наш менеджер свяжется с Вами в самое ближайшее время</p>';
    Modal::end();
    if(Yii::$app->session->getFlash('callbackFormFlash')) {
        $this->registerJs("$('#callback').modal('show');");
    }
} else {
    Modal::begin([
        'header' => '<h2>Заказать звонок</h2>',
        'id' => 'callback',
//    'toggleButton' => ['label' => 'click me'],
    ]);
    $form = ActiveForm::begin([
        'id' => 'callbackForm',
    ]); ?>
    <div class="row">
        <div class="col-lg-2 text-right">
            <p>Имя</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($callback, 'name')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Телефон</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($callback, 'phone')->textInput()->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Сообщение</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($callback, 'text')->textarea()->label(false); ?>
        </div>
        <div class="col-lg-12 clearfix">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary pull-right']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
    <?php Modal::end();
}