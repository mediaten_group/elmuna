<?php
/**
 * @var yii\web\View $this
 * @var $content string
 */

use app\assets\AppAsset;
use app\components\Blocks;
use app\components\Cart;
use app\components\Info;
use app\models\Categories;
use app\models\forms\CallbackForm;
use app\tables\MainMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');

//$this->registerMetaTag(['name' => 'keywords', 'content' => '']);
//$this->registerMetaTag(['name' => 'description', 'content' => $this->params['main_meta']['description']]);

Cart::set();
//dd(Yii::$app->session->get('cart'));
//if (!array_key_exists('aside_blocks', $this->params)) {
//    $this->params['aside_blocks'] = [
//        'bestseller',
//        'specials',
//        'info',
//        'news',
//    ];
//}

$mm = MainMenu::find()->orderBy('sort')->all();
$catalog = ArrayHelper::getValue($this->params, 'catalog');

$query = isset($this->params['q']) ? $this->params['q'] : '';

Blocks::set([
    'aside' => [
        'bestseller',
//        'exclusive',
        'specials',
        'news',
    ]
]);
$aside = Blocks::get('aside');
$bottom = Blocks::get('bottom');

$mainBlock = !array_key_exists('mainBlock', $this->params) ? true : false;

$callback = new CallbackForm();
if ($callback->load(Yii::$app->request->post())) {
    if ($callback->save()) {
        $callback = new CallbackForm();
        Yii::$app->session->setFlash('callbackFormFlash', true);
        Yii::$app->session->set('callbackForm', true);
    }
}
$address = Info::get('address');
$counters = Info::get('counter');

$js = <<<JS
ymaps.ready(init);
function init(){
    var address = '$address';

     ymaps.geocode(address, {
        results: 1
     }).then(function (res) {
            var coords = res.geoObjects.get(0).geometry.getCoordinates();
            var myMap = new ymaps.Map('map', {
                center: coords,
                zoom: 13,
                controls: []
            });
            var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
                preset: 'islands#redDotIcon'
            });
            myMap.geoObjects.add(myPlacemark);
        }, function(err){
            var coords = [56.835574, 60.58506];
            var myMap = new ymaps.Map('map', {
                center: coords,
                zoom: 13,
                controls: []
            });
            var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
                preset: 'islands#redDotIcon'
            });
            myMap.geoObjects.add(myPlacemark);
        });
}
JS;
$this->registerJs($js);

if (!$this->title) $this->title = Info::get('mainTitle');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=1280, initial-scale=0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style></style>
</head>
<body
    id="top" <?php if (ArrayHelper::getValue($this->params, 'body_class')) echo 'class="' . $this->params['body_class'] . '"' ?>>

<?php $this->beginBody() ?>
<div class="bg"></div>
<a href="#top"><i class="ion-arrow-up-a"></i>Наверх</a>

<div class="header">
    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xs-6 header-height">
                    <a class="navbar-brand" href="/"><img src="/images/logo.png"></a>
                </div>
                <div class="col-lg-2 col-xs-6 header-height">
                    <ul class="navbar-info">
                        <li><a class="navbar-info" href="/about">О компании</a></li>
                        <li><a class="navbar-info" href="/work">Условия работы</a></li>
                        <li><a class="navbar-info" href="/contacts">Контакты</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-xs-6 header-height">
                    <a href="#" class="call" data-toggle="modal" data-target="#callback"><i
                            class="icon ion-android-call"></i><span>Заказать звонок</span></a>
                    <a href="tel:<?= Info::getNumber('phone') ?>" class="head-phone" target="_blank">
                        <?= Info::get('phone') ?>
                    </a>
                </div>
                <!--                <div class="col-xs-12 visible-xs">-->
                <!--                    <a href="#" class="head-phone">+7 (343) 371-80-08</a>-->
                <!--                </div>-->
                <div class="col-lg-2 col-xs-6 header-height">
                    <div class="login">
                        <?php if (Yii::$app->user->isGuest) {
                            echo Html::a('Регистрация', ['/site/signup']);
                        } else {
                            echo Html::a('Личный кабинет', ['/profile/index']);
                        } ?>
                        <?php if (Yii::$app->user->isGuest) {
                            echo Html::a('Вход', ['/site/login']);
                        } else {
                            echo Html::a('Выход', ['/site/logout'], ['data' => ['method' => 'post']]);
                        } ?>
                    </div>
                    <div class="search">
                        <form class="form-inline" action="<?= Url::to(['/search/index']) ?>">
                            <div class="form-icon">
                                <input type="text" name="q" class="search-field" placeholder="Поиск" maxlength="50"
                                       value="<?= $query ?>">
                                <button type="submit" class="search-submit"><i class="ion-ios-arrow-thin-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 cart-block">
                    <div class="shopping-cart col-xs-12">
                        <div class="col-lg-4 visible-lg">
                            <a href="<?= Url::to(['/cart/index']) ?>">
                                <img src="/images/cart-icon.png" class="cart-icon">
                            </a>
                        </div>
                        <div class="col-lg-8 col-xs-12">
                            <div class="col-lg-12 col-xs-4">
                                <a href="<?= Url::to(['/cart/index']) ?>"><h5>Корзина</h5></a>
                            </div>
                            <div class="col-lg-12 col-xs-4" style="height:20px">
                                <p>Товаров: <b class="cart-count"><?= Cart::getCount() ?></b></p>
                            </div>
                            <div class="col-lg-12 col-xs-4" style="height:20px; white-space: nowrap">
                                <p>Сумма: <b class="price" id="cart-price"><?= Cart::getPrice() ?></b></p>
                            </div>
                        </div>
                        <div class="cart-content">
                            <img class="loadingu" src="/images/loading.gif">
                        </div>
                    </div>
                    <div class="price-notice col-xs-12">
                        Минимальная сумма заказа от 5000 рублей
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="add-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <a href="#" class="call" style="margin: 11px 0;text-align: left;" data-toggle="modal"
                       data-target="#callback"><i
                            class="icon ion-android-call"></i><span>Заказать звонок</span></a>
                </div>
                <div class="col-md-3 col-xs-6">
                    <a href="http://tel:<?= Info::getNumber('phone') ?>" class="head-phone" style="margin: 13px;"
                       target="_blank">
                        <?= Info::get('phone') ?>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="search" style="margin: 10px 0 6px;">
                        <form class="form-inline" action="<?= Url::to(['/search/index']) ?>">
                            <div class="form-icon">
                                <input type="text" class="search-field" name="q" placeholder="Поиск" maxlength="50"
                                       value="<?= $query ?>">
                                <button type="submit" class="search-submit"><i class="ion-ios-arrow-thin-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 hidden-xs hidden-sm">
                    <div class="shopping-cart" style="font-size: 16px;margin: 16px 0;text-align: right;">
                        <p><?= Html::a('Товаров в корзине:', ['/cart/index']) ?> <b
                                class="cart-count"><?= Cart::getCount() ?></b></p>
                        <div class="cart-content">
                            <img class="loadingu" src="/images/loading.gif">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Переключить навигацию</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php
                    $i = 1;
                    foreach ($mm as $m) {
                        $active = ($catalog == $m['alias']) ? ' active ' : '';
                        echo Html::tag('li', Html::a(
                            '<i class="' . $m['icon_class'] . '"></i>' . '<span>' . $m['title'] . '</span>', ['catalog/' . $m['alias']]),
                            ['class' => 'item item-' . $i . $active]);
                        $i++;
                    }
                    echo Html::tag('li',
                        Html::a('<span>Технологии нанесения</span>', ['technology/index']),
                        ['class' => 'technology']
                    );
                    ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="cap"></div>
</div>
<?php /*
<div class="main container">
    <div class="row">
        <?php if ($aside) : ?>
            <div class="col-md-9">
                <?= $content ?>
            </div>
            <div class="col-md-3 aside">
                <?php foreach ($aside as $block) {
                    echo $block;
                } ?>
            </div>
        <?php else: ?>
            <div class="col-lg-12">
                <?= $content ?>
            </div>
        <?php endif ?>
    </div>
</div>
 */ ?>
<?php echo $this->render('print'); ?>
<?php if ($mainBlock): ?>

    <div class="main container">
        <div class="block">
            <?php if ($aside) : ?>
                <div class="focal">
                    <?= $content ?>
                </div>
                <div class="aside">
                    <?php foreach ($aside as $block) {
                        echo $block;
                    } ?>
                </div>
            <?php else: ?>
                <div>
                    <?= $content ?>
                </div>
            <?php endif ?>
        </div>
    </div>

<?php else: ?>

    <div class="main container">
        <div class="row">
            <?php if ($aside) : ?>
                <div class="col-md-9">
                    <?= $content ?>
                </div>
                <div class="col-md-3 aside">
                    <div class="block">
                        <?php foreach ($aside as $block) {
                            echo $block;
                        } ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="col-lg-12">
                    <?= $content ?>
                </div>
            <?php endif ?>
        </div>
    </div>

<?php endif ?>
<?php foreach ($bottom as $block) {
    echo $block;
} ?>
<footer id="footer">
    <div class="red-ribbon">
        <div class="container">
            <div class="phone">
                <span class="glyphicon glyphicon-earphone"></span> <?= Info::get('phone') ?>
            </div>
        </div>
    </div>
    <div class="blue-ribbon">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <p class="copyright">© 2013 "ElMuna"</p>

                    <p>
                        Копирование информации сайта разрешено только с письменного согласия администрации
                    </p>
                    <p>
                        Все цены приведены как справочная информация и не являются публичной офертой
                    </p>
                </div>

                <div class="col-md-3">
                    <p><?= Info::get('address') ?></p>

                    <p>тел.: <?= Info::get('phone') ?></p>

                    <p>e-mail: <?= Info::get('email') ?></p>
                </div>

                <div class="col-md-2">
                    <ul class="socials">
                        <li><a href="#"><img src="/images/fb.png" alt="">Facebook</a></li>
                        <li><a href="//vk.com/studiolaser"><img src="/images/vk.png" alt="">Vkontakte</a></li>
                        <li><a href="#"><img src="/images/tw.png" alt="">Twitter</a></li>
                    </ul>
                </div>

                <div class="col-md-4 col-xs-12" style="text-align: center">
                    <div id="map"></div>
                </div>
                <?php if ($counters) : ?>
                    <div class="col-xs-12">
                        <div class="counters pull-right">
                            <?= $counters ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
<?= $this->render('_callback', ['callback' => $callback]) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
