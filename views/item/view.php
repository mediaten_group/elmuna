<?php
/**
 * @var $this yii\web\View
 * @var $item
 * @var $group
 * @var $images
 * @var $recent Groups
 * @var $groupItems
 * @var $links
 * @var $list
 * @var $category
 * @var $previous
 * @var $categories
 * @var $scheme
 * @var $colors array
 * @var $tech_places array
 * @var $parents array
 * @var $is_flash bool
 * @var bool $always_tech
 */
use app\assets\JsCookieAsset;
use app\assets\MagnificPopupAsset;
use app\models\Groups;
use app\widgets\LeftNav;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use drmabuse\slick\SlickWidget;
use app\lib\My;
use app\widgets\Requirements;
use app\models\Items;
use yii\helpers\Json;


$title = ($item['meta_title']) ? $item['meta_title'] : $item['title'];
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $item['meta_keywords'] ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $item['meta_description'] ?: $this->params['main_meta']['description']]);

JsCookieAsset::register($this);
MagnificPopupAsset::register($this);
$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
$this->registerJs($this->render('item.js'));
$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');
if ($is_flash) {
    $sizes = [];
    $sizes = Groups::getSizes($groupItems);
    $sizes[Items::getSize($item)] = $item;
    $sizes = array_reverse($sizes);
}
if ($category['id'] == 6465) {
    $sizes = [];
    $sizes = Groups::getVolume($groupItems);
    $sizes[Items::getVolume($item)] = $item;
    $sizes = array_reverse($sizes);
}
SlickWidget::widget([
    'container' => '#slider' . $item['id'] . ' .items',
    'settings' => My::slickSettingsSmall(),
]);
Modal::begin([
    'header' => '<h2>Добавлено в ' . Html::a('корзину', ['/cart']) . '</h2>',
    'id' => 'cart-success',
//    'toggleButton' => ['label' => 'click me'],
]);
$item_attr = $item['params'] ? Json::decode($item['params']) : [];
$about_stone = ArrayHelper::getValue(ArrayHelper::getValue($item_attr, 'attr', []), 'О камне');

$js = <<<JS
$('#about-stone').collapsy({
    button:false
});
JS;
$this->registerJs($js);

?>
<table class="table">
    <tr>
        <td>
            <div class="image">
                <?= Html::img(My::thumb('/uploads/images/thumbs100/' . $item['src']), ['class' => 'inline-thumb']) ?>
            </div>
        </td>
        <td><?= $item['title'] ?></td>
        <td>Тираж: <strong id="add-count"></strong></td>
    </tr>
</table>

<?php Modal::end();

Modal::begin([
    'header' => '<h2>Добавление в корзину не удалось</h2>',
    'id' => 'cart-fail',
//    'toggleButton' => ['label' => 'click me'],
]);
?>
<div id="cart-fail-content">

</div>

<?php Modal::end() ?>

<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?php if ($links) echo LeftNav::widget([
                'list' => $list,
                'category' => $parents[0],
                'previous' => $previous . '/',
                'current' => $category['id'],
            ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">

    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <?php if (My::identify() && My::identify()->is_admin) : ?>
            <div class="pull-right" style="font-size:30px">
                <a href="<?= Url::to(['/admin/items/update', 'id' => $item['id']]) ?>" target="_blank">
                    <i class="glyphicon glyphicon-wrench"></i>
                </a>
            </div>
        <?php endif; ?>
        <h1><?= $item['title']; ?></h1>
        <hr>
    </div>

    <div class="col-md-5 popup-gallery">
        <div class="image">
            <a href="<?= My::thumb('/uploads/images/' . $item['src']) ?>">
                <img src="<?= My::thumb('/uploads/images/thumbs/' . $item['src']) ?>">
            </a>
        </div>
        <div class="row">
            <?php foreach ($images as $image): ?>
                <a href="/uploads/images/<?= $image['src'] ?>" class="col-lg-4 image">
                    <img src="<?= My::thumb('/uploads/images/thumbs100/' . $image['src']) ?>">
                </a>
            <?php endforeach ?>
        </div>
    </div>
    <div class="col-md-7 article">
        <div class="colors big-colors">
            <?php if (isset($sizes) && (count($sizes) > 1)) : ?>
                <?php foreach ($sizes as $size => $i): ?>
                    <a href="<?= Url::to(['/item/view', 'id' => $i['id']]) ?>"
                       class="size <?= ($i['id'] == $item['id']) ? 'active' : '' ?>"
                       style="background-color:#444;"><?= $size ?></a>
                <?php endforeach; ?>
            <?php else: ?>
                <?php foreach ($groupItems as $i): ?>
                    <a href="<?= Url::to(['/item/view', 'id' => $i['id'], 'alias' => $i['alias']]) ?>" class="color"
                       style="background-image: url('/uploads/images/thumbs100/<?= $i['image'] ?>')"></a>
                <?php endforeach ?>
            <?php endif; ?>
        </div>
        <?= $this->render('_controls', ['item' => $item, 'scheme' => $scheme, 'tech_places' => $tech_places, 'always_tech' => $always_tech]) ?>
    </div>
    <?php if ($recent) : ?>
        <div class="col-lg-12">
            <div id="slider<?= $item['id']; ?>" class="slider">
                <div class="header">
                    <h4>Похожие товары</h4>
                </div>
                <div class="items similar-items">
                    <?php foreach ($recent as $r): ?>
                        <div class="item">
                            <a href="<?= Url::to(['/item/view', 'id' => $r['id']]) ?>">
                                <div class="parent">
                                    <div class="image">
                                        <img src="<?= My::thumb('/uploads/images/thumbs/' . $r['cover']) ?>" alt="">
                                    </div>
                                </div>
                                <div class="title">
                                    <?= $r['title'] ?>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($about_stone) : ?>
        <div class="col-lg-12">
            <div class="box" id="about-stone">
                <p>
                    <strong>О камне</strong>
                </p>
                <p>
                    <?= $about_stone ?>
                </p>
            </div>
        </div>
    <?php endif; ?>
</div>