var tech;

$('#add-to-cart').click(function (e) {
    e.preventDefault();

    tech_fill();
    var itemId = $('#item-id').val();
    var count = $('#count').val();
    if (count == '' || !/^\d+$/.test(count) || count<1) {
        $('.notifications').html('<div class="alert alert-warning alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button> УКАЖИТЕ ТИРАЖ' +
            '</div>');
        setTimeout(function () {
            $('.alert').css('opacity', 0);
            setTimeout(function () {
                $('.alert').remove();
            }, 200)
        }, 5000);
        return;
    }

    var data = {
        'id': itemId,
        'count': count,
        'tech': tech
    };
    $.ajax({
        type: "POST",
        url: "/item/add-to-cart",
        data: data,
        dataType: 'json',
        success: function (msg) {
            var totalCount = msg.count,
                status = msg.status,
                errors = msg.errors,
                totalPrice = msg.price,
                hash = msg.hash;
            Cookies.set('cart', hash, {expires: 60, path: '/'});
            if (status == 'ok') {
                $('#add-count').html(count);
                // $('#cart-success').modal('show');
                if (totalCount) $('.cart-count').html(totalCount);
                if (totalPrice) $('#cart-price').html(totalPrice);

                $('.notifications').html('<div class="alert alert-success alert-dismissible fade in" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button> Товар добавлен' +
                    '</div>');
                setTimeout(function () {
                    $('.alert').css('opacity', 0);
                    setTimeout(function () {
                        $('.alert').remove();
                    }, 200)
                }, 2000);
            }
            if (status == 'fail') {
                $('#cart-fail').modal('show');
                $('#cart-fail-content').html(errors);
            }
        }
    });
});

function price() {
    var count_input = $('#count'),
        price = count_input.attr('data-price'),
        count = count_input.val(),
        current_option = $('#option1').prop('checked'),
        tech_price = 0,
        tech_mult = 1,
        sep = 0,
        field;
    if (!price) price = 0;
    //Делаем пересчет стоимости нанесения только если выбрано с нанесением
    if (current_option) {
        //Ходим по всем элементам левой схемы
        tech['left'].forEach(function (item, i, arr) {
            field = $('[name=' + i + ']');

            if (field.hasClass('select-form')) {
                if (field.attr('data-separated') == 1) {
                    sep += parseFloat(field.children('[data-option-id=' + item + ']').attr('data-price'));
                } else {
                    tech_price += parseFloat(field.children('[data-option-id=' + item + ']').attr('data-price'));
                }
                tech_mult *= parseFloat(field.children('[data-option-id=' + item + ']').attr('data-mult'));
            }
            else if (field.hasClass('checkbox-form')) {
                tech_price += parseFloat(field.attr('data-price'));
                tech_mult *= parseFloat(field.attr('data-mult'));
            }
            else if (field.hasClass('color-form')) {
                tech_price += parseFloat($('input.color-form[name=' + i + '][id=' + item + ']').attr('data-price'));
            }
            else if (field.hasClass('area-form')) {
                if (field.attr('data-mult') == 1) {
                    tech_price += parseFloat(field.attr('data-price'));
                    tech_mult *= item[2];
                } else {
                    tech_price += parseFloat(item[2] * field.attr('data-price'));
                }
            }
            else if (field.hasClass('number-form')) {
                if (field.attr('data-mult') == 1) {
                    tech_price += parseFloat(field.attr('data-price'));
                    tech_mult *= item;
                } else {
                    tech_price = parseFloat(tech_price) + parseFloat(item * field.attr('data-price'));
                }
            }
        });
        //Ходим по всем элементам правой схемы
        tech['right'].forEach(function (item, i, arr) {
            field = $('[name=' + i + ']');
            if (field.hasClass('select-form')) {
                tech_price += parseFloat(field.children('[data-option-id=' + item + ']').attr('data-price'));
                tech_mult *= parseFloat(field.children('[data-option-id=' + item + ']').attr('data-mult'));
            }
            else if (field.hasClass('checkbox-form')) {
                tech_price += parseFloat(field.attr('data-price'));
                tech_mult *= parseFloat(field.attr('data-mult'));
            }
            else if (field.hasClass('color-form')) {
                tech_price += parseFloat($('input.color-form[name=' + i + '][id=' + item + ']').attr('data-price'));
            }
            else if (field.hasClass('area-form')) {
                tech_price += item[2] * field.attr('data-price');
            }
            else if (field.hasClass('number-form')) {
                if (field.attr('data-mult') == 1) {
                    tech_price += parseFloat(field.attr('data-price'));
                    tech_mult *= item;
                } else {
                    tech_price += item * field.attr('data-price');
                }
            }
        });
    }
    var total_tech_price = parseFloat(tech_price * tech_mult) + parseFloat(sep);
    total_tech_price = total_tech_price.toFixed(2);
    var base_price = count * price;
    base_price = base_price.toFixed(2);
    var semi_full_price = parseFloat(price) + parseFloat(total_tech_price);
    semi_full_price = semi_full_price.toFixed(2);
    var full_price = count * semi_full_price;
    full_price = full_price.toFixed(2);
    //Эм?
    $('.semi-total-price').html(semi_full_price)
    $('.tech-price').html(total_tech_price);
    //Стоимость нанесения в общей таблице
    $('.price-details p .tech-price').html(total_tech_price);
    //Количество в итоговой таблица
    $('#total-count').html(count);
    //Если выбрано с нанесением
    if (current_option) {
        //То берем и базовую стоимость и стоимость нанесения
        $('.total.price').html(full_price);
    }
    else {
        //Иначе только базовую стоимость
        $('.total.price').html(base_price);
    }
}

function tech_fill(field, itself) {
    if (field) {
        var side = field.parents('.tab-pane').attr('role'),
            value, type;
        //Определение стороны нанесения
        if (side == 'tabpanel')
            side = 'left';
        else
            side = 'right';
        //Определения типа поля
        if (field.hasClass('select-form'))
            type = 'select';
        else if (field.hasClass('checkbox-form'))
            type = 'checkbox';
        else if (field.hasClass('color-form'))
            type = 'color';
        else if (field.hasClass('area-form'))
            type = 'area';
        else if (field.hasClass('number-form'))
            type = 'number';
        //Получение правильного значения поля согласно типу
        switch (type) {
            case 'select':
                value = parseInt(field.children().eq(field.prop('selectedIndex')).attr('data-option-id'));
                break;
            case 'checkbox':
                value = field.prop('checked');
                break;
            case 'color':
                if (field.prop('checked')) {
                    value = parseInt(field.attr('id'));
                }
                break;
            case 'area':
                var area_form = field.parents('.form-inline'),
                    width_field = area_form.find('.width-form'),
                    width = width_field.val(),
                    height_field = area_form.find('.height-form'),
                    height = height_field.val();
                field.val(width * height);
                value = [parseFloat(height), parseFloat(width), parseFloat(field.val())];
                break;
            case 'number':
                value = parseInt(field.val());
                if (!value) {
                    value = 0;
                }
                break;
        }
        if (value)
            tech[side][field.attr('name')] = value;
        if ((type == 'checkbox') && !(value)) {
            delete tech[side][field.attr('name')];
        }
    } else {
        if (!$('#option1').prop('checked')) {
            tech = {};
            price();
            return true;
        }
        tech = {
            'left': [],
            'right': []
        };
        tech['scheme_left'] = parseFloat($('.active[role=tabpanel]').attr('data-id'));
        tech['scheme_right'] = parseFloat($('.active[role=tabpanel-right]').attr('data-id'));
        tech['image_left'] = $('input[data-widget=w1]').val();
        tech['image_right'] = $('input[data-widget=w2]').val();
        tech['tech_place'] = parseInt($('.tech-place-selected').attr('data-id'));
        var panels = $('.active[role=tabpanel], .active[role=tabpanel-right]'),
            areas = panels.find('.area-form'),
            selects = panels.find('.select-form'),
            colors = panels.find('.color-form'),
            checks = panels.find('.checkbox-form'),
            numbers = panels.find('.number-form'),
            i;
        for (i = 0; i < areas.size(); i++) {
            tech_fill(areas.eq(i), true);
        }
        for (i = 0; i < selects.size(); i++) {
            tech_fill(selects.eq(i), true);
        }
        for (i = 0; i < colors.size(); i++) {
            tech_fill(colors.eq(i), true);
        }
        for (i = 0; i < checks.size(); i++) {
            tech_fill(checks.eq(i), true);
        }
        for (i = 0; i < numbers.size(); i++) {
            tech_fill(numbers.eq(i), true);
        }

    }
    if (!itself) {
        price();
    }
}

//Стартовый вызов для пересчета массива и цены
tech_fill();

//Изменение тиража
$('#count').on('input', function (e) {
    var max = parseInt($(this).attr('max')),
        min = parseInt($(this).attr('min')),
        val = parseInt($(this).val()),
        error = false,
        length = $(this).val().length;
    if (val > max) {
        $('.help-block.max').show();
        $(this).val(30000);
        error = true;
    } else {
        $('.help-block.max').hide();
    }
    if (val < min) {
        $('.help-block.min').show();
        $(this).val(0);
        error = true;
    } else {
        $('.help-block.min').hide();
    }
    if (this.value.length > 5) {
        this.value = this.value.slice(0,5);
    }
    // this.value = parseInt(this.value);
    if (error) {
        $(this).parent().addClass('has-error');
        tech_fill();
    } else {
        $(this).parent().removeClass('has-error');
        tech_fill();
    }
});
//Изменение поля select
$('.select-form').change(function () {
    tech_fill($(this));
});
//Изменение поля color
$('.color-form').change(function () {
    tech_fill($(this));
});
//Переключение того, нужно нанесение или нет
$('input[name=options]').change(function () {
    tech_fill();
});
//Изменение чекбокса
$('.checkbox-form').change(function () {
    tech_fill($(this));
});
//Переключение схемы первой стороны
$('.tab-control').on('change', function () {
    var index = $(this).children().eq($(this).context.selectedIndex).attr('data-id');
    $('div.active[role=tabpanel]').removeClass('active');
    $('#tab' + index).addClass('active');
    tech_fill();
});
//Переключение схемы второй стороны
$('.tab-control-right').on('change', function () {
    var index = $(this).children().eq($(this).context.selectedIndex).attr('data-id');
    $('div.active[role=tabpanel-right]').removeClass('active');
    $('#tab' + index + '[role=tabpanel-right]').addClass('active');
    tech_fill();
});
//Изменение поля площади
$('.width-form, .height-form').on('input', function () {
    tech_fill($(this).parents('.form-inline').find('.area-form'));
});
//Изменение поля числа
$('.number-form').on('input', function () {
    tech_fill($(this));
});