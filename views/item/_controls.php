<?php
/**
 * @var $item
 * @var $scheme
 * @var $this \yii\web\View
 * @var $colors array
 * @var $tech_places array
 * @var bool $always_tech
 */

use yii\bootstrap\Html;
use app\modules\upload\widgets\Upload;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use app\assets\OwlCarouselAsset;
use drmabuse\slick\assets\SlickAssets;

$style = <<<CSS

.help-block {
    display: none;
}

.visible {
    display: block;
}

CSS;
$this->registerCss($style);

$js = <<<JS

$('.tech-place-carousel').slick({
    'slidesToShow': 3,
    'swipeToSlide': true,
  });

$('.tech-place-select').on('click',function(e) {
  e.preventDefault();
  $('.tech-place-selected')
    .attr('data-id',$(this).attr('data-id'))
    .attr('src','/uploads/images/original/'+$(this).attr('data-image'));
})

JS;
$this->registerJs($js);

SlickAssets::register($this);

$scheme_count = 0;
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
$m = new \app\models\SchemeImage();

if (!$item['price'])
    $item['price'] = 0;

if (!is_array($item['params'])) {
    $item['params'] = Json::decode($item['params']);
}

?>
<?php if (!(isset($scheme['left']) || isset($scheme['right']))) : ?>
    <div class="base-price price">Цена: <?= $item['price']; ?></div>
<?php endif; ?>

<div class="notifications"></div>

<!--Блок места нанесения-->
<?php if ($tech_places) : ?>
    <div class="tech-place">
        <?= Html::img('/uploads/images/original/' . $tech_places[0]['name'], [
            'data-id' => $tech_places[0]['id'], 'class' => 'tech-place-selected'
        ]) ?>
    </div>
    <div class="tech-place-carousel">
        <?php foreach ($tech_places as $place) : ?>
            <div>
                <a href="#" class="tech-place-select" data-image="<?= $place['name'] ?>" data-id= <?= $place['id'] ?>>
                    <?= Html::img('/uploads/images/thumbs100/' . $place['name'], ['data-id' => $place['id']]) ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<!--Блок переключения необходимости нанесения-->
<?php if (!$always_tech && (isset($scheme['left']) || isset($scheme['right']))): ?>
    <div class="btn-group radio-group" data-toggle="buttons">
        <label class="btn btn-primary active">
            <input type="radio" name="options" id="option1" autocomplete="off" checked> С нанесением
        </label>
        <label class="btn btn-primary">
            <input type="radio" name="options" id="option2" autocomplete="off"> Без нанесения
        </label>
    </div>
<?php endif; ?>

<?php if ($always_tech) : ?>
    <input type="hidden" name="options" id="option1" autocomplete="off" checked>
<?php endif; ?>

<!--Блок нанесения-->
<div class="technology">

    <!--    Блок нанесения с первой стороны-->
    <?php if (isset($scheme['left'])): ?>
        <?php if (isset($scheme['right'])) : ?>
            <h3>Нанесение с одной стороны</h3>
        <?php endif; ?>
        <div class="form-group">
            <sub>Вид нанесения</sub>
            <select class="dropdown tab-control">
                <?php foreach ($scheme['left'] as $scheme_left) { ?>
                    <option data-id="<?= $scheme_left['scheme_id'] ?>"><?= $scheme_left['tech_name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="row">
            <div class="tab-content">
                <?php
                foreach ($scheme['left'] as $scheme_left) {
                    echo Html::beginTag('div', [
                        'role' => 'tabpanel',
                        'class' => 'tab-pane ' . ((!$scheme_count++) ? 'active' : ''),
                        'id' => 'tab' . $scheme_left['scheme_id'],
                        'data-id' => $scheme_left['scheme_id'],
                    ]);
                    $fields = Json::decode($scheme_left['fields']);
                    foreach ($fields as $sort => $field) {
                        echo $this->render("field/$field[5]", [
                            'field' => $field
                        ]);
                    }
                    echo Html::endTag('div');
                }
                echo '<div class="col-lg-12">';
                echo '<sub>Загрузить картинку</sub>';
                echo $form->field($m, '_upload[]')->widget(Upload::className(), [
                    'thumbs' => ['screen', 'small', '100'],
                    'cover' => 'upload_id',
                    'async' => true,
                    'required' => true,
                ])->label(false);
                echo '</div>';
                ?>
            </div>
        </div>
    <?php endif ?>

    <!--    Блок нанесения со второй стороны-->
    <?php if (isset($scheme['right'])): ?>
        <?php if (isset($scheme['left'])) : ?>
            <h3>Нанесение с другой стороны</h3>
        <?php endif; ?>
        <sub>Вид нанесения</sub>
        <select class="dropdown tab-control-right">
            <option active>Не использовать</option>
            <?php foreach ($scheme['right'] as $scheme_right) { ?>
                <option data-id="<?= $scheme_right['scheme_id'] ?>"><?= $scheme_right['tech_name'] ?></option>
            <?php } ?>
        </select>
        <div class="row">
            <div class="tab-content">
                <?php
                foreach ($scheme['right'] as $scheme_right) {
                    echo Html::beginTag('div', [
                        'role' => 'tabpanel-right',
                        'class' => 'tab-pane ',
                        'id' => 'tab' . $scheme_right['scheme_id'],
                        'data-id' => $scheme_right['scheme_id'],
                    ]);
                    $fields = Json::decode($scheme_right['fields']);
                    foreach ($fields as $sort => $field) {
                        echo $this->render("field/$field[5]", [
                            'field' => $field
                        ]);
                    }
                    echo '</div>';
                }
                echo '<div class="col-lg-12">';
                echo '<sub>Загрузить картинку</sub>';
                echo $form->field($m, '_upload[]')->widget(Upload::className(), [
                    'thumbs' => ['screen', 'small', '100'],
                    'cover' => 'upload_id',
                    'async' => true,
                ])->label(false);
                echo '</div>'; ?>
            </div>
        </div>
    <?php endif ?>

</div>

<?php if ($scheme): ?>
    <sub>Тираж</sub>
<?php else: ?>
    <sub>Количество</sub>
<?php endif ?>
<form>
    <div class="form-inline count-group">
        <input type="number" class="form-control" min="0" max="30000" id="count"
               data-price="<?= $item['price'] ?>">
        <input type="hidden" id="item-id" value="<?= $item['id'] ?>">
        <a href="#" id="add-to-cart" class="btn btn-primary in-cart">
            <i class="ion-android-cart"></i>В корзину</a>
        <div class="row">
            <span class="help-block min">Нельзя заказать 0 или меньше продуктов</span>
            <span class="help-block max">Нельзя заказать за раз больше 30000 продуктов</span>
        </div>
    </div>
</form>

<div class="price-details">
    <?php if ($scheme): ?>
        <?php if (!$always_tech) : ?>
            <p>Стоимость тиража<span class="right price base-price"><?= $item['price'] ?></span></p>
            <p>Стоимость нанесения<span class="right price tech-price"></span></p>
        <?php else: ?>
            <p>Стоимость за единицу<span class="right price semi-total-price">0</span></p>
        <?php endif; ?>
    <?php endif ?>
    <p>Итого за <span id="total-count">0</span> шт<span class="right total price"></span></p>
</div>
<?php ActiveForm::end() ?>
<?php if ($scheme): ?>
    <div class="price-info">
        <i class="fa fa-info-circle"></i>

        <p>Стоимость нанесения может увеличиваться при печати на цветных поверхностях, специальными красками или на
            двух и более местах. Точный расчет можно получить в корзине</p>
    </div>
<?php endif ?>

<div class="details">
    <?php if ($item['sizes']) : ?>
        <p><strong>Размеры: </strong><?= $item['sizes'] ?></p>
    <?php endif; ?>

    <?php if ($item['material']) : ?>
        <p><strong>Материал: </strong><?= $item['material'] ?></p>
    <?php endif; ?>

    <?php if ($item['article']) : ?>
        <p><strong>Артикул: </strong><?= $item['article']; ?></p>
    <?php endif; ?>

    <?php if ($scheme): ?>
        <p><strong>Способ нанесения:</strong>
            <?php
            if (isset($scheme['left'])) :
                foreach ($scheme['left'] as $sch):
                    if ($sch['tech_link'] == 'application') : ?>
                        <?= Html::a($sch['tech_name']) ?>
                    <?php else: ?>
                        <?= Html::a($sch['tech_name'], '/technology/' . $sch['tech_link']) ?>
                    <?php endif; ?>
                <?php endforeach;
            endif;
            if (isset($scheme['right'])) :
                foreach ($scheme['right'] as $sch): ?>
                    <?= Html::a($sch['tech_name'], '/technology/' . $sch['tech_link']) ?>
                <?php endforeach;
            endif; ?>
        </p>
    <?php endif ?>

    <?php if (isset($item['params']['attr'])) : ?>
        <?php foreach ($item['params']['attr'] as $title => $param) : ?>
            <?php if ($param && $title != 'О камне') : ?>
                <p><strong><?= $title ?>:</strong> <?= $param ?></p>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if ($item['source_url'] && $item['source_show']): ?>
        <p><strong>Каталог:</strong> <span class="catalog"><?= $item['source_name'] ?></span></p>
    <?php endif; ?>

    <?php if ($item['calc']) : ?>
        <p class="constructor">
            <strong>Конструктор: </strong>
            <a href="<?= $item['calc'] ?>">[скачать <span class="glyphicon glyphicon-download-alt"></span>]</a>
        </p>
    <?php endif; ?>
</div>
<p><?= $item['text'] ?></p>