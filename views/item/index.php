<?php
/**
 * @var $this yii\web\View
 * @var $links
 * @var $category
 * @var $item
 * @var $level1
 * @var $level2
 * @var $categories
 * @var $childs
 */
use yii\helpers\Html;
use yii\web\JqueryAsset;
use drmabuse\slick\SlickWidget;
use app\lib\My;
use app\widgets\Requirements;

$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');

SlickWidget::widget([
    'container' => '#slider' . $item['id'] . ' .items',
    'settings' => My::slickSettings(),
]);

?>
<div class="col-md-3">
    <ul class="list-unstyled">
        <?php
        foreach ($categories as $c) {
            echo Html::beginTag('li');
            echo Html::a($c['title'], [
                '/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $c['alias']
            ]);
            if ($c['alias'] == $level2['alias']) {
                echo Html::beginTag('ul');
                foreach ($childs as $child) {
                    echo Html::beginTag('li');
                    if ($child['alias'] == $category['alias']) {
                        echo Html::a('<b>' . $child['title'] . '</b>', [
                            '/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $level2['alias'], 'level3' => $child['alias']
                        ]);
                    } else {
                        echo Html::a($child['title'], [
                            '/catalog/catalog', 'level1' => $level1['alias'], 'level2' => $level2['alias'], 'level3' => $child['alias']
                        ]);
                    }
                    echo Html::endTag('li');
                }
                echo Html::endTag('ul');
            }
            echo Html::endTag('li');
        } ?>
    </ul>
    <hr class="orange">
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">

    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $item['title']; ?></h1>
        <hr>
    </div>

    <div class="col-md-5">
        <div class="image">
            <img src="/uploads/images/<?= $item['image']; ?>">
        </div>
    </div>
    <div class="col-md-7 article">
        <div class="colors">
            <span class="color" style="background-color: #FF655D;"></span>
            <span class="color" style="background-color: #119279;"></span>
            <span class="color" style="background-color: #D8BB56;"></span>
            <span class="color" style="background-color: #8B56D8;"></span>
            <span class="color" style="background-color: #56B1D8;"></span>
        </div>
        <div class="btn-group radio-group" data-toggle="buttons">
            <label class="btn btn-primary active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> С нанесением
            </label>
            <label class="btn btn-primary">
                <input type="radio" name="options" id="option2" autocomplete="off"> Без нанесения
            </label>
        </div>
        <sub>С нанесением</sub>

        <div class="price"><?= $item['price']; ?></div>
        <sub>Вид нанесения</sub>
        <select class="dropdown">
            <option selected>Н: Деколь</option>
            <option disabled>Ещё один</option>
            <option>Другой</option>
        </select>

        <div class="row">
            <div class="col-lg-6">
                <sub>Место</sub>
                <select class="dropdown">
                    <option selected>Стандартно</option>
                    <option disabled>Ещё один</option>
                    <option>Другой</option>
                </select>
            </div>
            <div class="col-lg-6">
                <sub>Площадь</sub>
                <select class="dropdown">
                    <option selected>До 10 см</option>
                    <option disabled>Ещё один</option>
                    <option>Другой</option>
                </select>
            </div>
        </div>
        <sub>Тираж</sub>

        <div class="form-inline">
            <input type="text" class="form-control" size="10" id="circulation">
            <a href="#" class="btn btn-primary in-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
        </div>

        <div class="price-details">
            <p>Стоимость тиража<span class="right price">12432</span></p>

            <p>Стоимость нанесения<span class="right price">643232</span></p>

            <p>Итого за <span id="total-count">53</span> шт<span class="right total price">643232</span></p>
        </div>
        <div class="price-info">
            <i class="fa fa-info-circle"></i>

            <p>Стоимость нанесения может увеличиваться при печати на цветных поверхностях, специальными красками или на
                двух и более местах. Точный расчет можно получить в корзине</p>
        </div>
        <p><strong>Констуктор: </strong><a href="#">construct.cdr <i class="fa fa-arrow-circle-o-down"></i></a></p>

        <div class="details">
            <p>Размеры: <strong>H=9,5 см, D=8,1 см</strong></p>

            <p>Материал: <strong>фарфор</strong></p>

            <p>Артикул: <strong><?= $item['article']; ?></strong></p>

            <p>Способ нанесения: <a href="#">Н-Деколь</a></p>

            <p>Каталог: <a href="#">Радуга</a></p>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere posuere ipsum vel dapibus.
            Curabitur maximus arcu turpis, non rutrum erat eleifend sed. Donec vel laoreet diam, non vehicula velit.
            Suspendisse ac posuere leo. In tincidunt, lorem a condimentum efficitur, orci tellus scelerisque mi, in
            feugiat lorem ligula ut ante.</p>
    </div>
    <div id="slider<?= $item['id']; ?>" class="slider">
        <div class="header"><h4>Похожие товары</h4></div>
        <div class="items similar-items">
            <?php foreach ($recent as $r): ?>
                <div class="item">
                    <a href="/item/<?= $r['id']; ?>">
                        <div class="parent">
                            <div class="image"><img src="/uploads/images/<?= $r['image'] ?>" alt=""></div>
                        </div>
                        <div class="title"><?= $r['title'] ?></div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>