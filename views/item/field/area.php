<?php
/**
 * @var \yii\web\View $this
 * @var array $field
 */
use yii\bootstrap\Html;
use yii\helpers\Json;

?>

<div class="col-lg-12 form-group">
    <sub><?= $field[4] ?></sub>
    <?php $values = Json::decode($field[2]); ?>
    <div class="form-inline select-group">
        <div class="col-xs-3">
            <?= Html::input('text', 'width' . $field[0], null, [
                'class' => 'width-form form-control',
                'max' => $values['width_max'],
                'min' => $values['width_min'],
                'placeholder' => 'Ширина',
                'required' => $field[6],
            ]) ?>
        </div>
        <div class="col-xs-1">
            <b>X</b>
        </div>
        <div class="col-xs-3">
            <?= Html::input('text', 'height' . $field[0], null, [
                'class' => 'height-form form-control',
                'max' => $values['height_max'],
                'min' => $values['height_min'],
                'placeholder' => 'Высота',
                'required' => $field[6],
            ]) ?>
        </div>
        <div class="col-xs-1">
            <b>=</b>
        </div>
        <?php
        $min_area = $values['area_min'] ?: $values['height_min'] * $values['width_min'];
        $max_area = $values['area_max'] ?: $values['height_max'] * $values['width_max'];
        $mult = isset($values['mult'])?$values['mult']:0;
        ?>
        <div class="col-xs-3">
            <?= Html::input('text', $field[0], null, [
                'class' => 'area-form form-control',
                'placeholder' => 'Площадь',
                'readonly' => 1,
                'min' => $min_area,
                'max' => $max_area,
                'data-price' => (Json::decode($field[3])) ? Json::decode($field[3]) : 0,
                'required' => $field[6],
                'data-mult'=>$mult
            ]); ?>
        </div>
    </div>
    <span class='help-block visible'>Возможная площадь нанесения - от <?= $min_area ?> до <?= $max_area ?>
        см<sup>2</sup></span>
</div>