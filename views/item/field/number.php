<?php
/**
 * @var \yii\web\View $this
 * @var array $field
 */

//pree($field);
use yii\bootstrap\Html;
use yii\helpers\Json;

$data = Json::decode($field[2]);

?>

<div class="col-lg-12 form-group form-inline">
    <sub><?= $field[4] ?></sub><br>
    <?= Html::input('number',$field[0],null,[
        'min'=>$data['min']?:0,
        'max'=>$data['max']?:1000,
        'data-price'=>$data['price'],
        'data-mult'=>$data['mult'],
        'class'=>'form-control number-form'
    ]) ?>
</div>
