<?php
/**
 * @var \yii\web\View $this
 * @var array $field
 */
use yii\helpers\Json;

$separated = $field[7] ? 'data-separated="1"' : '';

?>

<div class="col-lg-12 form-group">
    <sub><?= $field[4] ?></sub>
    <select class='dropdown select-form' name='<?= $field[0] ?>' <?=$separated?>>
        <?php
        $values = Json::decode($field[2]);
        foreach ($values['sort'] as $id) :
            $prices = Json::decode($field[3]);
            $price = ($prices[$id]) ? $prices[$id] : 0;
            $price_mult = (isset($values['price_mult']) && $values['price_mult'][$id]) ? $values['price_mult'][$id] : 1;
            $value = $values['value'][$id]; ?>
            <option data-price='<?= $price ?>' data-option-id='<?= $id ?>' data-mult="<?= $price_mult ?>">
                <?= $value ?>
            </option>
        <?php endforeach; ?>
    </select>
</div>
