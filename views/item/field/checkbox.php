<?php
/**
 * @var \yii\web\View $this
 * @var array $field
 */
use yii\bootstrap\Html;
use yii\helpers\Json;

$separated = $field[7] ? 'data-separated="1"' : '';
$field_2 = Json::decode($field[2])

?>

<div class="col-lg-12 form-group">
    <div class="checkbox" <?= $separated ?>>
        <label>
            <?= Html::input('checkbox', $field[0], null, [
                'data-price' => (Json::decode($field[3])) ? Json::decode($field[3]) : 0,
                'data-mult' => (isset($field_2['price_mult']) && $field_2['price_mult']) ? $field_2['price_mult'] : 1,
                'class' => 'checkbox-form'
            ]) ?>
            <?= $field[4] ?>
        </label>
    </div>
</div>
