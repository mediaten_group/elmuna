<?php
/**
 * @var \yii\web\View $this
 * @var array $field
 */
use yii\bootstrap\Html;
use yii\helpers\Json;

$separated = $field[7] ? 'data-separated="1"' : '';

?>


<div class="col-lg-12 form-group">
    <sub><?= $field[4] ?></sub>
    <div>
        <div class="btn-group" data-toggle="buttons" <?= $separated ?>>
            <?php
            $colors_count = 0;
            if (isset($field[1])) :
                $colors = Json::decode($field[1]);
                $prices = Json::decode($field[3]);
                foreach ($colors as $color_id => $value) :
                    $active = (!$colors_count) ? 'active' : '';
                    $checked = (!$colors_count++) ? 'checked' : '';
                    $price = ($prices[$color_id]) ? $prices[$color_id] : 0;
                    ?>
                    <label class='btn btn-default color-select <?= $active ?>'>
                        <input class='color-form' type='radio' name='<?= $field[0] ?>' data-price='<?= $price ?>'
                            <?= $checked ?> id='<?= $color_id ?>' autocomplete='off'>
                        <?= Html::img("/uploads/images/thumbs25/" . $value) ?>
                    </label>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
