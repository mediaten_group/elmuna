<?php
/**
 * @var $review ReviewForm
 */

use app\models\forms\ReviewForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


if (Yii::$app->session->get('reviewForm')) {
    Modal::begin([
        'header' => '<h2>Отзыв отправлен</h2>',
        'id' => 'review',
    ]);
    echo '<p class="text-center">Спасибо за отзыв!</p>' .
        '<p class="text-center">Ваш отзыв будет отображен после проверки модератором</p>';
    Modal::end();
    if (Yii::$app->session->getFlash('reviewForm')) {
        $this->registerJs("$('#review').modal('show');");
    }
} else {
    Modal::begin([
        'header' => '<h2>Оставить отзыв</h2>',
        'id' => 'review',
//    'toggleButton' => ['label' => 'click me'],
    ]);
    $form = ActiveForm::begin([
        'id' => 'reviewForm',
    ]); ?>
    <div class="row">
        <div class="col-lg-2 text-right">
            <p>ФИО</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($review, 'fio')->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Email</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($review, 'email')->label(false); ?>
        </div>
        <div class="col-lg-2 text-right">
            <p>Текст</p>
        </div>
        <div class="col-lg-10">
            <?= $form->field($review, 'text')->textarea(['rows' => 8])->label(false); ?>
        </div>
        <div class="col-lg-12 clearfix">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary pull-right']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
    <?php Modal::end();
}