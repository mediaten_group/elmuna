<?php
/**
 * @var $review ReviewForm
 * @var $reviews \yii\data\ActiveDataProvider
 * @var $bread array
 * @var $list array
 * @var $list2 array
 * @var $main array
 */

use app\lib\My;
use app\models\forms\ReviewForm;
use yii\helpers\Url;
use app\widgets\LeftNav;
use app\widgets\Requirements;

$href = '#';
echo $this->render('_modal', ['review' => $review]);
$this->registerJs("$('#add-review').click(function(e){e.preventDefault();$('#review').modal('show');});");

?>
<div class="col-lg-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?= LeftNav::widget([
                'list' => $list,
                'type' => 'pages',
                'category' => $main,
//            'previous' => $previous . '/',
                'current' => 'reviews',
            ]) ?>
        </div>
    </div>
    <?php if ($list2): ?>
        <div class="mini-block">
            <div class="footer">
                <?= LeftNav::widget([
                    'list' => $list2,
                    'type' => 'pages',
//            'category' => $parents[0],
//            'previous' => $previous . '/',
//            'current' => $category['id'],
                    'current' => 'reviews',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= Requirements::widget() ?>
</div>
<div class="col-lg-9">
    <div class="col-lg-12">
        <?php
        if ($bread) {
            echo \yii\widgets\Breadcrumbs::widget([
                'links' => $bread,
            ]);
        }
        ?>
    </div>
    <div class="col-lg-12">
        <div class="clearfix">
            <h1><?= $this->title ?>
                <a class="btn btn-primary pull-right" href="<?= $href ?>" id="add-review">
                    Добавить
                </a>
            </h1>
        </div>
        <hr>
    </div>
    <div class="col-lg-12">
        <?php if ($reviews->count > 0): ?>
            <?php foreach ($reviews->getModels() as $r): ?>
                <div class="box">
                    <div class="clearfix">
                        <h3><?= $r['fio'] ?></h3>

                        <p class="date"><?= My::mysql_russian_datetime($r['created_at']) ?></p>
                    </div>
                    <hr>
                    <p class="intro">
                        <?= $r['text'] ?>
                    </p>
                    <?php if ($r['answer']): ?>
                        <hr class="grey">
                        <p><strong><?= $r['answer'] ?></strong></p>
                    <?php endif ?>
                </div>
            <?php endforeach ?>
        <?php else: ?>
            <div class="box">
                <h4>Отзывов пока нет</h4>
            </div>
        <?php endif ?>
    </div>
</div>
