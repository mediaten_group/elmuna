<?php
/**
 * @var array $group
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;

$items = \yii\helpers\Json::decode($group['item']);
$cover = $group['src'] ?: $items[0]['src'];
$price = $items[0]['price'];
$url = count($items) > 1 ? Url::to(['/group/view', 'id' => $group['id']]) :
    Url::to(['/item/view', 'id' => $items[0]['id'], 'alias' => $items[0]['alias']]);
?>
<div class="col-lg-4">
    <a href="<?= $url ?>" id="group<?= $group['id'] ?>" class="box">
        <div class="colors">
            <?php if (count($items) > 1) {
                $items_count = 0;
                foreach ($items as $item) {
                    if ($items_count++ < 6) {
//                    $url = My::thumb($item['name']);
//                    echo Html::img($url);
                        $color = $item['color'] ?: '#ccc;';
//                    $url = Url::to(['/item', 'id' => $item['id']]);
                        $data = 'data-src="' . $item['src'] . '" data-price="' . $item['price'] . '" data-article="' . $item['article'] . '" data-title="' . $item['title'] . '"';
                        echo '<span class="color" href="' . $url . '" style="background-color: ' . $color . '" ' . $data . '></span>';
                    }
                    if ($items_count == 6) {
                        echo '<span class="color" >+</span>';
                    }
                }
            } else {
                if ($items[0]['color']) {
                    $color = $items[0]['color'] ?: '#ccc;';
                    echo '<span class="color" style="background-color: ' . $color . '"></span>';
                }
            } ?>
        </div>
        <div class="image">
            <?= Html::img(My::thumb('/uploads/images/thumbs/' . $cover)) ?>
        </div>
        <h4 class="title"><?= $group['title'] ?></h4>
        <span class="price"><?= $items[0]['price'] ?></span>
    </a>
</div>
