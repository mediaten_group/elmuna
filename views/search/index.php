<?php
/**
 * @var $groups array
 * @var $categories array
 * @var $q string
 * @var $this \yii\web\View;
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

$this->registerJs($this->render('_search.js'))

?>
<?php if ($categories): ?>
    <div class="row">
        <?php foreach ($categories as $category): ?>
            <div class="col-lg-4">
                <?php
                $path = '';
                foreach (Json::decode($category['path']) as $p) {
                    $path .= '/' . $p['alias'];
                }
                ?>
                <a class="category"
                   href="<?= Url::to(['/catalog' . $path]) ?>">
                    <div class="image">
                        <?= Html::img(My::thumb('/uploads/images/thumbs/' . $category['src'])) ?>
                    </div>
                    <p><?= $category['title']; ?></p>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif ?>
<?php if ($groups): ?>
    <div class="row">
        <div class="col-xs-12">
            <h3>Товары</h3>
        </div>
        <div class="col-xs-12">
            <hr>
        </div>
        <div class="col-xs-12 items-search">
            <?php foreach ($groups as $group) {
                echo $this->render('_item', ['group' => $group]);
            } ?>
        </div>
    </div>
<?php endif ?>
<?php if (!($groups || $categories)): ?>
    <div class="col-lg-12">
        <h4>По вашему запросу "<?= $q ?>" ничего не найдено.</h4>
        <?= Html::a('Вернуться на главную', '/', ['class' => 'btn btn-default']) ?>
    </div>
<?php endif ?>
