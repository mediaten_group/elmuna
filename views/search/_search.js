/**
 * Created by mt4 on 18.03.16.
 */
var page = 0, work = 0, q = $('.search-field').val();

$('.items-search').on('mouseover','.color',function () {
    var $this = $(this),
        $box = $this.parents('.box'),
        $img = $box.find('img'),
        $src = $img.attr('src'),
        $price = $box.find('.price'),
        $article = $box.find('.article'),
        $title = $box.find('.title'),
        price = $this.data('price'),
        src = $this.data('src'),
        title = $this.data('title'),
        article = $this.data('article');
    if (!title) return;
    $src = $src.split('/');
    $src.pop();
    $src[$src.length] = src;
    $src = $src.join('/');
    $img.attr('src', $src);
    $title.html(title);
    $price.html(price);
    $article.html(article);
});

$(window).scroll(function (e) {
    var pos = $(this).scrollTop(),
        item = $('.focal'),
        offset = item.prop('offsetTop'),
        height = item.prop('clientHeight'),
        client = $(window).height(),
        id = item.attr('data-id'),
        i;
    for (i = 0; i < 5; i++) {
        offset += item.prop('offsetTop');
        item = item.parent();
    }
    if ((pos + client) > (offset + height - 1084)) {
        if (work == 0) {
            work = 1;
            $.ajax({
                method: "POST",
                url: "/search/next-page",
                data: {
                    q: q,
                    page: page
                }
            }).done(function (msg) {
                $('.focal .row').last().append(msg);
                work = 0;
                page++;
            }).fail(function () {
                work = 0;
            })
        }
    }
});