<?php
/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

use yii\helpers\Html;

$this->title = $name;

//Yii::$app->layout = 'clean';

?>
<div class="<?= Yii::$app->controller->id . '-' . Yii::$app->controller->action->id ?>">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <pre> <?=$message ?></pre>
    <?= Html::a('Вернуться на главную', '/', ['class' => 'btn btn-default']) ?>
</div>
