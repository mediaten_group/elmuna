<?php
/**
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model app\models\LoginForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminLteAsset;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
$this->params['noSidebar'] = true;
$this->params['noFooter'] = true;
$pageClass = Yii::$app->controller->id . '-' . Yii::$app->controller->action->id;

AdminLteAsset::register($this);

$css = <<<CSS
body {
        background: linear-gradient(60deg, rgba(255, 107, 107, 0.5), rgba(85, 98, 112, 0.5) 50%), url(/uploads/images/bg-login.jpg);
    background-size: cover;
    background-position: bottom, center;
}
.login-box, .register-box {
    width: 360px;
    height: 320px;
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
CSS;
$this->registerCss($css);

?>

<div class="login-box">
    <div class="login-logo" style="color: #fff;">
        <?= Html::encode($this->title) ?>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Введите логин и пароль</p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => [
                'template' => '{input}',
                'options' => ['class'=>'form-group has-feedback'],
//                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>
        <?= $form->field($model, 'username', ['template'=>'{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>'])->textInput(['placeholder' => 'Логин']) ?>

        <?= $form->field($model, 'password', ['template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>'])->passwordInput(['placeholder'=>'Пароль']) ?>


        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">

                    <?= $form->field($model, 'rememberMe')->checkbox([
//                                'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'label' => 'Запомнить',
                        'options' => ['class'=>'icheckbox_square-blue']
                    ]) ?>

                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <a href="/" class="text-center">Вернуться на сайт</a>
        <a href="/request-password-reset" class="text-center pull-right">Забыли пароль?</a>
    </div>
    <!-- /.login-box-body -->
</div>
