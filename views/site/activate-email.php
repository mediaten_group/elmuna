<?php
/**
 * @var String $message
 * @var int $code
 */
use yii\bootstrap\Html;

?>

<div class="block">
    <div class="">
        <?= $message ?>
    </div>
    <div class="">
        <?= Html::a('Вернуться на главную', '/', ['class' => 'btn btn-default']) ?>
    </div>
</div>
