<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use drmabuse\slick\SlickWidget;

SlickWidget::widget([
    'container' => '.slider.gifts .items',
    'settings' => My::slickSettings(),
]);

$models = [
    ['title' => 'Посуда', 'image' => 'cap.png'],
    ['title' => 'Офис', 'image' => 'pencils.png'],
    ['title' => 'Отдых', 'image' => 'statuette.png'],
    ['title' => 'Аксессуары', 'image' => 'wallet.png'],
];
$models = array_merge($models, $models);
?>
<div class="slider gifts">
    <div class="header"><a href="#">Наградная продукция</a></div>
    <div class="items">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <div class="parent">
                    <div class="image"><img src="/uploads/images/<?= $model['image'] ?>" alt=""></div>
                </div>
                <div class="title"><?= $model['title'] ?></div>
            </div>
        <?php endforeach ?>
    </div>
</div>
