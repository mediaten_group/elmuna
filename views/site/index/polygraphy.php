<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use drmabuse\slick\SlickWidget;

SlickWidget::widget([
    'container' => '.slider.polygraphy .items',
    'settings' => My::slickSettings(),
]);

$models = [
    ['title' => 'Календари', 'image' => 'calendars.png'],
    ['title' => 'Блокноты', 'image' => 'notepads.png'],
    ['title' => 'Наклейки', 'image' => 'sticker.png'],
    ['title' => 'Бейджи', 'image' => 'badges.png'],
];
$models = array_merge($models, $models);
?>
<div class="slider polygraphy">
    <div class="header"><a href="#">Полиграфия</a></div>
    <div class="items">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <div class="parent">
                    <div class="image"><img src="/uploads/images/<?= $model['image'] ?>" alt=""></div>
                </div>
                <div class="title"><?= $model['title'] ?></div>
            </div>
        <?php endforeach ?>
    </div>
</div>
