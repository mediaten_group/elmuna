<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use drmabuse\slick\SlickWidget;

SlickWidget::widget([
    'container' => '.slider.awards .items',
    'settings' => My::slickSettings(),
]);

$models = [
    ['title' => 'Медали', 'image' => 'ribbon.png'],
    ['title' => 'Кубки', 'image' => 'cup.png'],
    ['title' => 'Фигурки', 'image' => 'statuette.png'],
    ['title' => 'Награды', 'image' => 'honors.png'],
];
$models = array_merge($models, $models);
?>
<div class="slider awards">
    <div class="header"><a href="#">Наградная продукция</a></div>
    <div class="items">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <div class="parent">
                    <div class="image"><img src="/uploads/images/<?= $model['image'] ?>" alt=""></div>
                </div>
                <div class="title"><?= $model['title'] ?></div>
            </div>
        <?php endforeach ?>
    </div>
</div>
