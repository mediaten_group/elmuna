<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use drmabuse\slick\SlickWidget;

SlickWidget::widget([
    'container' => '.slider.advertising .items',
    'settings' => My::slickSettings(),

]);

$models = [
    ['title' => 'Таблички', 'image' => 'plates.png'],
    ['title' => 'Буклеты', 'image' => 'booklets.png'],
    ['title' => 'Афиши', 'image' => 'baseball.png'],
    ['title' => 'Доски', 'image' => 'boards.png'],
];
$models = array_merge($models, $models);
?>
<div class="slider advertising">
    <div class="header"><a href="#">Наружная реклама</a></div>
    <div class="items">
        <?php foreach ($models as $model): ?>
            <div class="card effect__hover item">
                <div class="card__front">
                    <div class="parent">
                        <div class="image"><img src="/uploads/images/<?= $model['image'] ?>" alt=""></div>
                    </div>
                    <div class="title"><?= $model['title'] ?></div>
                </div>
                <div class="card__back">
                    <div class="title"><?= $model['title'] ?></div>
                    <a href="/">Посмотреть все...</a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
