<?php
/**
 * @var $this yii\web\View
 */
use app\lib\My;
use drmabuse\slick\SlickWidget;

SlickWidget::widget([
    'container' => '.slider.textiles .items',
    'settings' => My::slickSettings(),
]);

$models = [
    ['title' => 'Футболки', 'image' => 'shirts.png'],
    ['title' => 'Рубашки поло', 'image' => 'polo_shirts.png'],
    ['title' => 'Бейсболки', 'image' => 'baseball.png'],
    ['title' => 'Толстовки', 'image' => 'sweatshirts.png'],
];
$models = array_merge($models, $models);
?>
<div class="slider textiles">
    <div class="header"><a href="#">Изделия из текстиля</a></div>
    <div class="items">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <div class="parent">
                    <div class="image"><img src="/uploads/images/<?= $model['image'] ?>" alt=""></div>
                </div>
                <div class="title"><?= $model['title'] ?></div>
            </div>
        <?php endforeach ?>
    </div>
</div>
