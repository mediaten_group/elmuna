<?php
/**
 * @var $this yii\web\View
 * @var $categories
 */

use app\widgets\mainSlider\MainSlider;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;

$this->params['bottom_blocks'] = [
    'technology',
    'gifts'
];

$js = <<<JS
    $('#just-registered-modal').modal('show');
JS;

$this->registerJs($js);

if (Yii::$app->session->getFlash('just-registered')) {
    $modal = Modal::begin([
        'header' => '<h4>Спасибо за регистрацию</h4>',
        'id' => 'just-registered-modal'
    ]);
    echo 'Вы успешно зарегистрированы, заполните пожалуйста свой '.Html::a('профиль',['/profile']);
    Modal::end();
    $modal->trigger('show');
}

$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['main_meta']['description']]);

//echo JsTree::widget([
//    'name' => 'js_tree',
//    'core' => [
//        'data' => $categories,
//    ],
//    'plugins' => ['types', 'dnd', 'contextmenu', 'wholerow', 'state'],
//]);
//$data = \yii\helpers\Json::encode($categories);
////pre($data);
//$js = <<<JS
//$('#using_json_2').jstree({
//'core' : {
//    'data' : $data,
//    "check_callback" : true
//} ,
//    'plugins': ['types', 'dnd', 'contextmenu', 'wholerow', 'state']
//    }).bind("move_node.jstree", function (e, data) {
////        console.dir(data.parent);
////        console.dir(data.position);
//        console.dir(data.node);
//        var ajaxData = {
//            id: data.node.id,
//            position: data.position,
//            parent: data.parent
//        };
//            $.ajax({
//                type: "POST",
//                url: "/site/ajax",
//                data: ajaxData,
//                success: function(msg){
//                    console.log( msg );
//                }
//            });
//        });
//JS;
//$this->registerJs($js);
//echo '<div id="using_json_2"></div>';

//$items = [];
//foreach ($categories as $category) {
//    $items[] = $category;
////    pre($items);
//}

if ($categories) {
    foreach ($categories as $category) {
        echo MainSlider::widget([
            'title' => $category['title'],
            'alias' => $category['alias'],
            'items' => isset($category['children'])?$category['children']:[],
        ]);
    }
}