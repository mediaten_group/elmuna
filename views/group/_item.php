<?php
/**
 * @var array $item
 * @var array $category
 */
use app\lib\My;
use yii\helpers\Html;
use yii\helpers\Url;

$url = Url::to(['/item/view', 'id' => $item['id']]);
$volume = '';
if ($category['id'] = 6465) {
    $volume = \app\models\Groups::getVolumeOne($item);
}
?>
<div class="col-lg-4">
    <a href="<?= $url ?>" id="item<?= $item['id'] ?>" class="box">
        <?php if ($volume): ?>
            <div class="colors">
                <span class="size" style="background-color: #444"><?= $volume ?></span>
            </div>
        <?php endif; ?>
        <div class="image">
            <?= Html::img(My::thumb('/uploads/images/thumbs/' . $item['src'])) ?>
        </div>
        <h4 class="title"><?= $item['title'] ?></h4>
        <span class="price"><?= $item['price'] ?></span>
    </a>
</div>
