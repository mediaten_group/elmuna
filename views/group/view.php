<?php
/**
 * @var $this yii\web\View
 * @var $group
 * @var $links
 * @var $list
 * @var $category
 * @var $previous
 * @var $categories
 * @var $parents array
 */
use app\widgets\LeftNav;
use app\widgets\Requirements;
use yii\web\JqueryAsset;
use drmabuse\slick\SlickWidget;
use app\lib\My;

$this->registerJsFile('@web' . '/plugins/easydropdown/easydropdown.js', ['depends' => JqueryAsset::className()]);
$this->registerCssFile('@web' . '/plugins/easydropdown/easydropdown.css');

SlickWidget::widget([
    'container' => '#slider' . $group['id'] . ' .items',
    'settings' => My::slickSettings(),
]);

?>

<div class="col-md-3 aside">
    <div class="mini-block">
        <div class="footer">
            <?php if ($links) echo LeftNav::widget([
                'list' => $list,
                'category' => $parents[0],
                'previous' => $previous . '/',
                'current' => $category['id'],
            ]) ?>
        </div>
    </div>
    <?= Requirements::widget() ?>
</div>
<div class="col-md-9">
    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => $links,
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $group['title']; ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <?php foreach ($items as $item) {
                echo $this->render('_item', ['item' => $item,'category'=>$category]);
            } ?>
        </div>
    </div>
</div>
