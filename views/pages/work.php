<?php
/**
 * @var \app\tables\Pages $model
 * @var \yii\web\View $this
 */
use yii\helpers\Url;
$title = ($model->meta_title) ? $model->meta_title : $model->title;
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);
?>
<div class="row">
    <div class="col-lg-12">
        <h1><?= $model->title ?></h1>
        <hr class="green">
    </div>
    <div class="col-lg-3">
        <ul class="list-unstyled">
            <li>
                <a href="/contract">Заключение договора</a>
            </li>
            <li>
                <a href="/payment">Оплата</a>
            </li>
            <li>
                <a href="/requisites">Реквизиты ООО и ИП</a>
            </li>
            <li>
                <a href="/delivery">Доставка</a>
            </li>
        </ul>
        <hr class="orange">
        <a href="<?= Url::to(['/pages/requirements']) ?>" class="requirements">
            <i class="ion-alert-circled"></i>Требования к макетам
        </a>
    </div>
    <div class="col-lg-9">
        <?= $model->text ?>
    </div>
</div>
