<?php
/**
 * @var \app\tables\Pages $model
 * @var \yii\web\View $this
 * @var array $list
 * @var array $list2
 * @var array $main
 * @var array $bread
 */
use app\widgets\LeftNav;
use app\widgets\Requirements;
use app\components\Alert;

$title = ($model->meta_title) ? $model->meta_title : $model->title;
$this->title = $title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords ?: $this->params['main_meta']['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description ?: $this->params['main_meta']['description']]);
?>
<?php if (($list) || ($model['alias'] == 'contacts')): ?>
    <div class="col-lg-3 aside">
        <div class="mini-block">
            <div class="footer">
                <?= LeftNav::widget([
                    'list' => $list,
                    'type' => 'pages',
                    'category' => $main,
//            'previous' => $previous . '/',
                    'current' => $model['alias'],
                ]) ?>
            </div>
        </div>
        <?php if ($list2): ?>
            <div class="mini-block">
                <div class="footer no-header">
                    <?= LeftNav::widget([
                        'list' => $list2,
                        'type' => 'pages',
//            'category' => $parents[0],
//            'previous' => $previous . '/',
//            'current' => $category['id'],
                        'current' => $model['alias'],
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>
        <?= Requirements::widget() ?>
    </div>
    <div class="col-lg-9 wider">
        <div class="col-lg-12 bread">
            <?php
            if ($bread) {
                echo \yii\widgets\Breadcrumbs::widget([
                    'links' => $bread,
                ]);
            }
            ?>
        </div>
        <div class="col-lg-12 title">
            <?php if ($model->is_printable) : ?>
                <a class="print-link pull-right" href="#" onclick="window.print();return false;">
                    Распечатать страницу <i class="ion-printer"></i>
                </a>
            <?php endif; ?>
            <h1><?= $model->title ?></h1>
            <hr class="green">
        </div>
        <div class="col-xs-12 printable">
            <?php echo Alert::widget(); ?>
            <?= $model->text ?>
        </div>
    </div>
<?php else: ?>
    <div class="col-lg-12">
        <div class="col-lg-12 bread">
            <?php
            if ($bread) {
                echo \yii\widgets\Breadcrumbs::widget([
                    'links' => $bread,
                ]);
            }
            ?>
        </div>
        <div class="col-lg-12 title">
            <?php if ($model->is_printable) : ?>
                <a class="print-link pull-right" href="#" onclick="window.print();return false;">
                    Распечатать страницу <i class="ion-printer"></i>
                </a>
            <?php endif; ?>
            <h1><?= $model->title ?></h1>
            <hr class="green">
        </div>
        <div class="col-xs-12">
            <?= $model->text ?>
        </div>
    </div>
<?php endif; ?>
