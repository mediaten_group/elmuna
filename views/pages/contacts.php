<?php
/**
 * @var \app\tables\Pages $model
 * @var \yii\web\View $this
 */
use yii\helpers\Url;
$title = ($model->meta_title) ? $model->meta_title : $model->title;
$this->title = $title;

$css = <<<CSS
body {
    visibility:hidden;
}
.print {
    visibility: visible;
}
CSS;

$this->registerCss($css,['media'=>'print']);

?>
<div class="row">
    <div class="col-lg-12">
        <h1><?= $model->title ?></h1>
        <hr class="green">
    </div>
    <div class="col-lg-3">
        <a href="#" onclick="window.print();return false;">
            <i class="ion-printer"></i> Распечатать
        </a>
        <hr class="orange">
        <a href="<?= Url::to(['/pages/requirements']) ?>" class="requirements">
            <i class="ion-alert-circled"></i>Требования к макетам
        </a>
    </div>
    <div class="col-lg-9 print">
        <?= $model->text ?>
    </div>
</div>