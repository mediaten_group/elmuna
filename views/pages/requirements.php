<?php
/**
 * @var \app\tables\Pages $model
 */
$title = ($model->meta_title) ? $model->meta_title : $model->title;
$this->title = $title;
?>
<div class="row">
    <div class="col-lg-12">
        <h1><?= $model->title ?></h1>
        <hr class="green">
    </div>
    <div class="col-lg-12">
        <?= $model->text ?>
    </div>
</div>
