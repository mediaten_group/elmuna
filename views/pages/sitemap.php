<?php
/**
 * @var \app\tables\Pages $model
 */
use yii\helpers\Url;
$title = ($model->meta_title) ? $model->meta_title : $model->title;
$this->title = $title;
?>
<div class="row">
    <div class="col-lg-12">
        <h1><?= $model->title ?></h1>
        <hr class="green">
    </div>
    <div class="col-lg-12">
        <?= $model->text ?>
    </div>
    <div class="col-lg-4">
        <h4>Страницы</h4>
        <ul class="list-unstyled">
            <li>
                <a href="/">Главная</a>
            </li>
            <li>
                <a href="<?= Url::to(['/pages/buy']) ?>">Как купить</a>
            </li>
            <li>
                <a href="<?= Url::to(['/pages/about']) ?>">О компании</a>
            </li>
            <li>
                <a href="<?= Url::to(['/pages/reviews']) ?>">Отзывы</a>
            </li>
            <li>
                <a href="<?= Url::to(['/pages/delivery']) ?>">Информация о доставке</a>
            </li>
            <li>
                <a href="<?= Url::to(['/pages/contacts']) ?>">Контактная информация</a>
            </li>
        </ul>
    </div>
</div>