<?php
/**
 * @var \Yii\Web\View $this
 * @var UserProfile $profile
 * @var UserDataForm $user_data_form
 * @var Array $orders
 */
use app\models\forms\UserDataForm;
use app\tables\UserProfile;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>

<div class="row profile-page">
    <div class="col-lg-12">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => ['label' => $this->title],
        ]) ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title ?></h1>
        <hr>
    </div>
    <div class="col-lg-12">
        <div class="box">
            <div class="clearfix">
                <h4 class="pull-left">Информация</h4>
                <?php if (Yii::$app->session->getFlash('user_profile_form')): ?>
                    <h4 class="pull-right">Сохранено <i class="ion-checkmark"></i></h4>
                <?php endif ?>
            </div>
            <hr class="green">
            <?php $form = ActiveForm::begin([
                'id' => 'profileForm',
            ]); ?>
            <div class="row">
                <div class="col-lg-2 text-right">
                    <p>Имя</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($profile, 'name')->textInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Адрес</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($profile, 'address')->textInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Телефон</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($profile, 'phone')->textInput()->label(false); ?>
                </div>
                <div class="col-lg-12 clearfix">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary pull-right']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="box">
            <div class="clearfix">
                <h4 class="pull-left">Данные аккаунта</h4>
                <?php if (Yii::$app->session->getFlash('user_data_form')): ?>
                    <h4 class="pull-right">Сохранено <i class="ion-checkmark"></i></h4>
                <?php endif ?>
            </div>
            <hr class="green">
            <?php $form = ActiveForm::begin([
                'id' => 'userDataForm',
            ]); ?>
            <div class="row">
                <div class="col-lg-2 text-right">
                    <p>Логин</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($user_data_form, 'username')->textInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Email</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($user_data_form, 'email')->textInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Новый пароль</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($user_data_form, 'password')->passwordInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Подтверждение пароля</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($user_data_form, 'password_repeat')->passwordInput()->label(false); ?>
                </div>
                <div class="col-lg-2 text-right">
                    <p>Старый пароль</p>
                </div>
                <div class="col-lg-10">
                    <?= $form->field($user_data_form, 'old_password')->passwordInput()->label(false); ?>
                </div>
                <div class="col-lg-12 clearfix">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary pull-right']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
    <?php if ($orders): ?>
        <div class="col-lg-12">
            <h3>Заказы</h3>
            <hr>
        </div>
    <?php endif ?>
    <?php foreach ($orders as $order) {
        echo $this->render('_order', ['order' => $order]);
    } ?>
</div>