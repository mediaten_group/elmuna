<?php
/**
 * @var Array $order
 */
use app\lib\My;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

$i = 1;

$js = <<<JS

$('a[role=info]').on('click',function(e) {
  e.preventDefault();
  $(this).parents('tr').next().toggle();
})

JS;

$this->registerJs($js);

?>
<div class="col-lg-12">
    <div class="box">
        <div class="clearfix">
            <h4 class="pull-left">Заказ № <?= $order['id'] ?>
                от <?= My::mysql_russian_datetime($order['created_at']) ?></h4>
            <h4 class="pull-right"><?= $order['price'] ?> <span class="light">рублей</span></h4>
        </div>
        <hr class="green">
        <div class="row">
            <div class="col-lg-8">
                <div class="cart">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Номер</th>
                            <th>Выбранные товары</th>
                            <th>Цена за шт.</th>
                            <th>Тираж</th>
                            <th>Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($order['orderItems'] as $item): ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <?= Html::a(Html::img(My::thumb('/uploads/images/thumbs100/' . $item['cover'])),
                                                ['/item/view', 'id' => $item['item_id']]) ?>
                                        </div>
                                        <div class="col-lg-10">
                                            <?= Html::a($item['title'],
                                                ['/item/view', 'id' => $item['item_id']]) ?>
                                        </div>
                                    </div>
                                </td>
                                <td><?= ($item['price'] + $item['tech_price']) ?>р.</td>
                                <td><?= $item['count'] ?> шт.</td>
                                <td><?= ($item['count'] * ($item['price'] + $item['tech_price'])) ?>р.</td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <?php
                                    if ($item['tech_price'] && $item['tech']) {
                                        $tech = Json::decode($item['tech']);

                                        if ($tech) {
                                            if (isset($tech['tech_place'])) {
                                                echo Html::tag('h3', 'Место для нанесения');
                                                echo Html::img("/uploads/images/thumbs100/$tech[tech_place]");
                                            }
                                            if (isset($tech['left'])) {
                                                echo Html::tag('h3', 'Покрытие с одной стороны');
                                                echo $tech['left']['text'];
                                            }
                                            if (isset($tech['right'])) {
                                                echo Html::tag('h3', 'Покрытие с другой стороны');
                                                echo $tech['right']['text'];
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <h5>Информация по заказу</h5>
                <table class="table">
                    <tr>
                        <td class="text-right">Адрес:</td>
                        <td><strong><?= $order['address'] ?></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Email:</td>
                        <td><strong><?= $order['email'] ?></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Телефон:</td>
                        <td><strong><?= $order['phone'] ?></strong></td>
                    </tr>
                    <tr>
                        <td class="text-right">Статус:</td>
                        <td>
                            <?php
                            $status = '';
                            switch ($order['status']) {
                                case \app\models\Order::NOT_READY:
                                    $status = 'Не подтвержден';
                                    break;
                                case \app\models\Order::IN_WORK:
                                    $status = 'В работе';
                                    break;
                                case \app\models\Order::IN_DELIVERY:
                                    $status = 'В доставке';
                                    break;
                                case \app\models\Order::COMPLETED:
                                    $status = 'Выполнен';
                                    break;
                                case \app\models\Order::CANCELED:
                                    $status = 'Отменен';
                                    break;
                                default:
                                    $status = 'Не известно';
                            }
                            ?>
                            <strong<?php if ($order['status'] == \app\models\Order::COMPLETED) echo ' class="bg-success"' ?>>
                                <?= $status ?>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Комментарий:</td>
                        <td><?= $order['comment'] ?></td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>